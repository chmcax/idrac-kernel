/*
 * Renesas R0P7757LC0012RL Support.
 *
 * Copyright (C) 2009 - 2010  Renesas Solutions Corp.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/irq.h>
#include <linux/regulator/fixed.h>
#include <linux/regulator/machine.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/io.h>
#include <linux/mmc/host.h>
#include <linux/mmc/sh_mmcif.h>
#include <linux/mmc/sh_mobile_sdhi.h>
#include <linux/sh_eth.h>
#include <linux/usb/ohci_pdriver.h>
//<<<<<<< HEAD
#include <linux/usb/r8a66597.h>
#include <linux/i2c.h>
#include <linux/platform_data/at24.h>
#include <linux/serial_8250.h>
#include <linux/delay.h>
#include <linux/tracebuf.h>
//=======
#include <linux/sh_intc.h>
/*#include <linux/usb/renesas_usbhs.h>
 */
//>>>>>>> dell-v3.10
#include <cpu/sh7757.h>
#include <asm/heartbeat.h>
#include <asm/reboot.h>
#include <asm/tlbflush.h>
#include <asm/traps.h>
#include <asm/hw_regs.h>
/* Add by Avocent --- */
#include <linux/mtd/partitions.h>
/* --- */
#define CPLD_BASE_ADDR      0x14000000
void __iomem *_cpldaddr;

/* global gpio lock */
DEFINE_SPINLOCK(aess_gpio_lock);
EXPORT_SYMBOL(aess_gpio_lock);


#define VFLASH_POWER_CTRL       // Enable Board-specific sd card power
#define USE_DELL_HEARTBEAT      // Use the same heartbeat as in 12g, in Phils fp driver.
#ifndef USE_DELL_HEARTBEAT

static struct resource heartbeat_resource = {
	.start	= 0xffec005c,	/* PUDR */
	.end	= 0xffec005c,
	.flags	= IORESOURCE_MEM | IORESOURCE_MEM_8BIT,
};

static unsigned char heartbeat_bit_pos[] = { 0, 1, 2, 3 };

static struct heartbeat_data heartbeat_data = {
	.bit_pos	= heartbeat_bit_pos,
	.nr_bits	= ARRAY_SIZE(heartbeat_bit_pos),
	.flags		= HEARTBEAT_INVERTED,
};
static void heartbeat_release(struct device * dev)
{
    return ;
}

static struct platform_device heartbeat_device = {
	.name		= "heartbeat",
	.id		= -1,
	.dev	= {
		.platform_data	= &heartbeat_data,
        .release = heartbeat_release
	},
	.num_resources	= 1,
	.resource	= &heartbeat_resource,
};
#endif
unsigned int idrac_type = 0;  

int Dell_init_idrac_type(void)
{
//Till we get cpld with dcs type.
#define	IDRAC_TYPE_OFFSET	0x0A
#define IDRAC_TYPE_BIT_POSITION 3
#define MASK_OF_IDRAC_TYPE     0x08
#define LEN_OF_IDRAC_TYPE      1

    struct resource		*tmp_mem;

    /** - Request the memory region for AMEA present */
    tmp_mem = request_mem_region(CPLD_BASE_ADDR+IDRAC_TYPE_OFFSET, LEN_OF_IDRAC_TYPE, "sh7757_eth");
    if (!tmp_mem) 
    {
        printk(KERN_ERR "%s: failed to request io memory region 0x%x.\n", 
                __FUNCTION__, CPLD_BASE_ADDR+IDRAC_TYPE_OFFSET);
        return -1;
    }
    /** - Remap the whole CPLD memory region */
	_cpldaddr = 0;
    _cpldaddr = ioremap_nocache(CPLD_BASE_ADDR, 0x90);

    if (0 == _cpldaddr) 
    {
	    release_mem_region(CPLD_BASE_ADDR+IDRAC_TYPE_OFFSET, LEN_OF_IDRAC_TYPE);
        printk(KERN_ERR "%s: failed to ioremap_nocache() io memory region to CPLD.\n", 
                          __FUNCTION__);
        return -1;
    }
	/** - Read the status */
           
    idrac_type = (ioread8(_cpldaddr+IDRAC_TYPE_OFFSET) & MASK_OF_IDRAC_TYPE) >> IDRAC_TYPE_BIT_POSITION;
    printk("Dell_get_idrac_type: %x\n",idrac_type);

	
    /** - Release the memory region */
    release_mem_region(CPLD_BASE_ADDR+IDRAC_TYPE_OFFSET, LEN_OF_IDRAC_TYPE);
    /** - Return the status */
    return 0;
}

/* Fast Ethernet */
#define GBECONT		0xffc10100
#define GBECONT_RMII1	BIT(17)
#define GBECONT_RMII0	BIT(16)
static void sh7757_eth_set_mdio_gate(void *addr)
{
	if (((unsigned long)addr & 0x00000fff) < 0x0800) {
		if (boot_cpu_data.type == CPU_SH7758)
			gpio_set_value(GPIO_PTT4, 1);
		writel(readl(GBECONT) | GBECONT_RMII0, GBECONT);
	} else {
		if (boot_cpu_data.type == CPU_SH7758)
			gpio_set_value(GPIO_PTT4, 0);
		writel(readl(GBECONT) | GBECONT_RMII1, GBECONT);
	}
}

static struct resource sh_eth0_resources[] = {
	{
		.start  = 0xfef00000,
		.end    = 0xfef001ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = evt2irq(0xc80),
		.end    = evt2irq(0xc80),
		.flags  = IORESOURCE_IRQ,
	},
};

static struct sh_eth_plat_data sh7757_eth0_pdata = {
	.phy = 4,
	.edmac_endian = EDMAC_LITTLE_ENDIAN,
        .register_type = SH_ETH_REG_FAST_SH4,
	.set_mdio_gate = sh7757_eth_set_mdio_gate,
};
static void sh7757_eth0_release(struct device * dev)
{
    return ;
}

static struct platform_device sh7757_eth0_device = {
	.name		= "sh-eth",
	.resource	= sh_eth0_resources,
	.id		= 0,
	.num_resources	= ARRAY_SIZE(sh_eth0_resources),
	.dev		= {
		.platform_data = &sh7757_eth0_pdata,
        .release = sh7757_eth0_release
	},
};

static struct platform_device sh7757_eth2_dcs_device = {
	.name		= "sh-eth",
	.resource	= sh_eth0_resources,
	.id		= 2,
	.num_resources	= ARRAY_SIZE(sh_eth0_resources),
	.dev		= {
		.platform_data = &sh7757_eth0_pdata,
        .release = sh7757_eth0_release
	},
};


static struct resource sh_eth1_resources[] = {
	{
		.start  = 0xfef00800,
		.end    = 0xfef009ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = evt2irq(0xc80),
		.end    = evt2irq(0xc80),
		.flags  = IORESOURCE_IRQ,
	},
};

static struct sh_eth_plat_data sh7757_eth1_pdata = {
	.phy = 1,
	.edmac_endian = EDMAC_LITTLE_ENDIAN,
        .register_type = SH_ETH_REG_FAST_SH4,
	.set_mdio_gate = sh7757_eth_set_mdio_gate,
};

static void sh7757_eth1_release(struct device * dev)
{
    return ;
}

static struct platform_device sh7757_eth1_device = {
	.name		= "sh-eth",
	.resource	= sh_eth1_resources,
	.id		= 1,
	.num_resources	= ARRAY_SIZE(sh_eth1_resources),
	.dev		= {
		.platform_data = &sh7757_eth1_pdata,
        .release = sh7757_eth0_release
	},
};

static void sh7757_eth_giga_set_mdio_gate(void *addr)
{
	if (((unsigned long)addr & 0x00000fff) < 0x0800) {
		gpio_set_value(GPIO_PTT4, 1);
		writel(readl(GBECONT) & ~GBECONT_RMII0, GBECONT);
	} else {
		gpio_set_value(GPIO_PTT4, 0);
		writel(readl(GBECONT) & ~GBECONT_RMII1, GBECONT);
	}
}

static struct resource sh_eth_giga0_resources[] = {
	{
		.start  = 0xfee00000,
		.end    = 0xfee007ff,
		.flags  = IORESOURCE_MEM,
	},
    {
		/* TSU */
		.start  = 0xfee01800,
		.end    = 0xfee01fff,
		.flags  = IORESOURCE_MEM,
//<<<<<<< HEAD
/*	},
    {
		.start  = 315,
		.end    = 315,
*/
//=======
	}, {
		.start  = evt2irq(0x2960),
		.end    = evt2irq(0x2960),
//>>>>>>> dell-v3.10
		.flags  = IORESOURCE_IRQ,
	},
};

static struct sh_eth_plat_data sh7757_eth_giga0_pdata = {
	.phy = 1, //18,
	.edmac_endian = EDMAC_LITTLE_ENDIAN,
	.register_type = SH_ETH_REG_GIGABIT,
        .set_mdio_gate = sh7757_eth_giga_set_mdio_gate,
	.phy_interface = PHY_INTERFACE_MODE_RGMII_ID,
};

static struct platform_device sh7757_eth_giga0_device = {
	.name		= "sh-eth",
	.resource	= sh_eth_giga0_resources,
	.id		= 0,
	.num_resources	= ARRAY_SIZE(sh_eth_giga0_resources),
	.dev		= {
		.platform_data = &sh7757_eth_giga0_pdata,
	},
};

static struct resource sh_eth_giga1_resources[] = {
	{
		.start  = 0xfee00800,
		.end    = 0xfee00fff,
		.flags  = IORESOURCE_MEM,
	}, 
    {
		/* TSU */
		.start  = 0xfee01800,
		.end    = 0xfee01fff,
		.flags  = IORESOURCE_MEM,
//<<<<<<< HEAD
/*	},
    {
		.start  = 316,
		.end    = 316,
*/
//=======
	}, {
		.start  = evt2irq(0x2980),
		.end    = evt2irq(0x2980),
//>>>>>>> dell-v3.10
		.flags  = IORESOURCE_IRQ,
	},
};

static struct sh_eth_plat_data sh7757_eth_giga1_pdata = {
	.phy = 19,
	.edmac_endian = EDMAC_LITTLE_ENDIAN,
        .register_type = SH_ETH_REG_GIGABIT,
	.set_mdio_gate = sh7757_eth_giga_set_mdio_gate,
	.phy_interface = PHY_INTERFACE_MODE_RGMII_ID,
};

static struct platform_device sh7757_eth_giga1_device = {
	.name		= "sh-eth",
	.resource	= sh_eth_giga1_resources,
	.id		= 3,
	.num_resources	= ARRAY_SIZE(sh_eth_giga1_resources),
	.dev		= {
		.platform_data = &sh7757_eth_giga1_pdata,
	},
};

/* Fixed 3.3V regulator to be used by SDHI0, MMCIF */
static struct regulator_consumer_supply fixed3v3_power_consumers[] =
{
	REGULATOR_SUPPLY("vmmc", "sh_mobile_sdhi.0"),
	REGULATOR_SUPPLY("vqmmc", "sh_mobile_sdhi.0"),
	REGULATOR_SUPPLY("vmmc", "sh_mmcif.0"),
	REGULATOR_SUPPLY("vqmmc", "sh_mmcif.0"),
};

/* SH_MMCIF */
static struct resource sh_mmcif_resources[] = {
	[0] = {
		.start	= 0xffcb0000,
		.end	= 0xffcb00ff,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= evt2irq(0x1c60),
		.flags	= IORESOURCE_IRQ,
	},
	[2] = {
		.start	= evt2irq(0x1c80),
		.flags	= IORESOURCE_IRQ,
	},
};

static struct sh_mmcif_plat_data sh_mmcif_plat = {
	.sup_pclk	= 0x0f,
	.caps		= MMC_CAP_4_BIT_DATA | MMC_CAP_8_BIT_DATA |
			  MMC_CAP_NONREMOVABLE | MMC_CAP_CMD23,
	.ocr		= MMC_VDD_32_33 | MMC_VDD_33_34,
	.slave_id_tx	= SHDMA_SLAVE_MMCIF_TX,
	.slave_id_rx	= SHDMA_SLAVE_MMCIF_RX,
};

static struct platform_device sh_mmcif_device = {
	.name		= "sh_mmcif",
	.id		= 0,
	.dev		= {
		.platform_data		= &sh_mmcif_plat,
	},
	.num_resources	= ARRAY_SIZE(sh_mmcif_resources),
	.resource	= sh_mmcif_resources,
};

/* SDHI0 */
#ifdef VFLASH_POWER_CTRL

#define VFLASH_POWER_CTRL_GPIO_DATA_REG     0xffec005c
#define VFLASH_POWER_CTRL_GPIO_OUTPUT       0x0001
#define VFLASH_POWER_CTRL_GPIO_BIT          0x01

#define VFLASH_POWER_CTRL_WAIT_TIME         100

static int vflash_init(struct platform_device *pdev, const struct sh_mobile_sdhi_ops *ops)
{
#define VFLASH_POWER_CTRL_GPIO_CTRL_REG     0xffec0028
#define VFLASH_POWER_CTRL_GPIO_MASK         0x0003
	u16 data;

	/* change to output pin */
	data = readw(VFLASH_POWER_CTRL_GPIO_CTRL_REG);
	data &= ~(VFLASH_POWER_CTRL_GPIO_MASK);
	data |= VFLASH_POWER_CTRL_GPIO_OUTPUT;

	/* output low */
	writeb(readb(VFLASH_POWER_CTRL_GPIO_DATA_REG) 
		& ~(VFLASH_POWER_CTRL_GPIO_BIT), 
		VFLASH_POWER_CTRL_GPIO_DATA_REG);

	printk(KERN_INFO "%s: power control pin initiated\n", __FUNCTION__);
    return(0);
}

static void vflash_set_pwr(struct platform_device *pdev, int state)
{
    u8 data;
    unsigned long flags;

    /* just in case, mutex protect this whole dealio */
    spin_lock_irqsave(&aess_gpio_lock, flags);

    data = readb(VFLASH_POWER_CTRL_GPIO_DATA_REG);
    
    if(state) {
        if(0==(data&VFLASH_POWER_CTRL_GPIO_BIT) )
	        goto unlock;						/* already enabled */

        data &= ~(VFLASH_POWER_CTRL_GPIO_BIT);  /* output low */

    	//printk(KERN_DEBUG "%s: power on - %x\n", __FUNCTION__, data & VFLASH_POWER_CTRL_GPIO_BIT);

    } else {
        if(data & VFLASH_POWER_CTRL_GPIO_BIT)	
	        goto unlock;						/* already disabled */

    	data |= VFLASH_POWER_CTRL_GPIO_BIT;    	/* output high */

    	//printk(KERN_DEBUG "%s: power off - %x\n", __FUNCTION__, data & VFLASH_POWER_CTRL_GPIO_BIT);
    }
	writeb(data, VFLASH_POWER_CTRL_GPIO_DATA_REG);

    spin_unlock_irqrestore(&aess_gpio_lock, flags);
	msleep(VFLASH_POWER_CTRL_WAIT_TIME);        /* wait for power stable */
    goto done;
unlock:
    spin_unlock_irqrestore(&aess_gpio_lock, flags);
done:
    return ;
}
#endif

static struct sh_mobile_sdhi_info sdhi_info = {
#ifdef VFLASH_POWER_CTRL 
    .init       = vflash_init,
    .set_pwr    = vflash_set_pwr,
#endif
	.dma_slave_tx	= SHDMA_SLAVE_SDHI_TX,
	.dma_slave_rx	= SHDMA_SLAVE_SDHI_RX,
	.tmio_caps	= MMC_CAP_SD_HIGHSPEED,
        .tmio_ocr_mask  = MMC_VDD_32_33 | MMC_VDD_33_34,
};

static struct resource sdhi_resources[] = {
	[0] = {
		.start  = 0xffe50000,
		.end    = 0xffe500ff,
		.flags  = IORESOURCE_MEM,
	},
	[1] = {
		.start  = evt2irq(0x480),
		.flags  = IORESOURCE_IRQ,
	},
};

static struct platform_device sdhi_device = {
	.name           = "sh_mobile_sdhi",
	.num_resources  = ARRAY_SIZE(sdhi_resources),
	.resource       = sdhi_resources,
	.id             = 0,
	.dev	= {
		.platform_data	= &sdhi_info,
	},
};

static struct resource usb0_resources[] = {
	{
		.start	= 0xfe450000,
		.end	= 0xfe4501ff,
		.flags	= IORESOURCE_MEM,
//<<<<<<< HEAD
	}, {
		.name	= "sudmac",
		.start	= 0xfe451000,
		.end	= 0xfe4510ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "hub",
		.start	= 0xfe452000,
		.end	= 0xfe4521ff,
		.flags	= IORESOURCE_MEM,
	}, {
/*		.start	= 50,
		.end	= 50,
*/
//=======
	},
	{
		.start	= evt2irq(0x840),
		.end	= evt2irq(0x840),
//>>>>>>> dell-v3.10
		.flags	= IORESOURCE_IRQ,
	},
};

struct r8a66597_platdata usb0_hub_data = {
	.on_chip	= 1,
	.buswait	= 2,
	.sudmac		= 1,
	.suspmode	= 1,
	.ghub		= 1,
};

struct r8a66597_platdata usb0_func_dma0_data = {
	.on_chip	= 1,
	.buswait	= 2,
	.sudmac		= 1,
	.suspmode	= 1,
	.gfunc		= 1,
};

struct r8a66597_platdata usb0_func_dma1_data = {
	.on_chip	= 1,
	.buswait	= 2,
	.sudmac		= 1,
	.sudmac_ch	= 1,
	.suspmode	= 1,
	.gfunc		= 1,
};

static struct platform_device usb0_device = {
	.name		= "r8a66597_udc",
	.id		= 0,
	.dev = {
		.platform_data		= &usb0_hub_data,
	},
	.num_resources	= ARRAY_SIZE(usb0_resources),
	.resource	= usb0_resources,
};

static struct resource usb0_f1_resources[] = {
	{
		.start	= 0xfe450200,
		.end	= 0xfe4503ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "sudmac",
		.start	= 0xfe451200,
		.end	= 0xfe4512ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.start	= 50,
		.end	= 50,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device usb0_f1_device = {
	.name		= "r8a66597_udc",
	.id		= 1,
	.dev = {
		.platform_data		= &usb0_func_dma0_data,
	},
	.num_resources	= ARRAY_SIZE(usb0_f1_resources),
	.resource	= usb0_f1_resources,
};

static struct resource usb0_f2_resources[] = {
	{
		.start	= 0xfe450400,
		.end	= 0xfe4505ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "sudmac",
		.start	= 0xfe451200,
		.end	= 0xfe4512ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.start	= 50,
		.end	= 50,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device usb0_f2_device = {
	.name		= "r8a66597_udc",
	.id		= 2,
	.dev = {
		.platform_data		= &usb0_func_dma1_data,
	},
	.num_resources	= ARRAY_SIZE(usb0_f2_resources),
	.resource	= usb0_f2_resources,
};

static struct resource usb0_f3_resources[] = {
	{
		.start	= 0xfe450600,
		.end	= 0xfe4507ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "sudmac",
		.start	= 0xfe451400,
		.end	= 0xfe4514ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.start	= 50,
		.end	= 50,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device usb0_f3_device = {
	.name		= "r8a66597_udc",
	.id		= 3,
	.dev = {
		.platform_data		= &usb0_func_dma0_data,
	},
	.num_resources	= ARRAY_SIZE(usb0_f3_resources),
	.resource	= usb0_f3_resources,
};

static struct resource usb0_f4_resources[] = {
	{
		.start	= 0xfe450800,
		.end	= 0xfe4509ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "sudmac",
		.start	= 0xfe451400,
		.end	= 0xfe4514ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.start	= 50,
		.end	= 50,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device usb0_f4_device = {
	.name		= "r8a66597_udc",
	.id		= 4,
	.dev = {
		.platform_data		= &usb0_func_dma1_data,
	},
	.num_resources	= ARRAY_SIZE(usb0_f4_resources),
	.resource	= usb0_f4_resources,
};

static struct resource usb0_f5_resources[] = {
	{
		.start	= 0xfe450a00,
		.end	= 0xfe450bff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "sudmac",
		.start	= 0xfe451600,
		.end	= 0xfe4516ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.start	= 50,
		.end	= 50,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device usb0_f5_device = {
	.name		= "r8a66597_udc",
	.id		= 5,
	.dev = {
		.platform_data		= &usb0_func_dma0_data,
	},
	.num_resources	= ARRAY_SIZE(usb0_f5_resources),
	.resource	= usb0_f5_resources,
};

static struct resource usb0_f6_resources[] = {
	{
		.start	= 0xfe450c00,
		.end	= 0xfe450dff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "sudmac",
		.start	= 0xfe451600,
		.end	= 0xfe4516ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.start	= 50,
		.end	= 50,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device usb0_f6_device = {
	.name		= "r8a66597_udc",
	.id		= 6,
	.dev = {
		.platform_data		= &usb0_func_dma1_data,
	},
	.num_resources	= ARRAY_SIZE(usb0_f6_resources),
	.resource	= usb0_f6_resources,
};

#if defined(CONFIG_USB_EHCI_HCD) || defined(CONFIG_USB_EHCI_HCD_MODULE)
#define ENABLE_EHCI
static struct resource usb_ehci_resources[] = {
	[0] = {
		.start	= 0xfe4f1000,
		.end	= 0xfe4f10ff,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= 57,
		.end	= 57,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct platform_device usb_ehci_device = {
	.name		= "sh_ehci",
	.id		= -1,
	.dev = {
		.dma_mask = &usb_ehci_device.dev.coherent_dma_mask,
		.coherent_dma_mask = DMA_BIT_MASK(32),
	},
	.num_resources	= ARRAY_SIZE(usb_ehci_resources),
	.resource	= usb_ehci_resources,
};
#endif

#if defined(CONFIG_USB_OHCI_HCD) || defined(CONFIG_USB_OHCI_HCD_MODULE)
#define ENABLE_OHCI
static struct resource usb_ohci_resources[] = {
	[0] = {
		.start	= 0xfe4f1800,
		.end	= 0xfe4f18ff,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= 57,
		.end	= 57,
		.flags	= IORESOURCE_IRQ,
	},
};

static struct usb_ohci_pdata usb_ohci_pdata;

static struct platform_device usb_ohci_device = {
	.name		= "ohci-platform",
	.id		= -1,
	.dev = {
		.dma_mask = &usb_ohci_device.dev.coherent_dma_mask,
		.coherent_dma_mask = DMA_BIT_MASK(32),
    .platform_data  = &usb_ohci_pdata,
	},
	.num_resources	= ARRAY_SIZE(usb_ohci_resources),
	.resource	= usb_ohci_resources,
};
#endif

#if 1	//!defined(ENABLE_OHCI) && !defined(ENABLE_EHCI)
#define ENABLE_USB1_UDC
static struct resource usb1_resources[] = {
	{
		.start	= 0xfe4f0000,
		.end	= 0xfe4f01ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.name	= "sudmac",
		.start	= 0xfe4f0300,
		.end	= 0xfe4f03ff,
		.flags	= IORESOURCE_MEM,
	}, {
		.start	= 57,
		.end	= 57,
		.flags	= IORESOURCE_IRQ,
	},
};

struct r8a66597_platdata usb1_data = {
	.on_chip	= 1,
	.buswait	= 2,
	.sudmac		= 1,
	.suspmode	= 1,
};

static struct platform_device usb1_device = {
	.name		= "r8a66597_udc",
	.id		= 7,
	.dev = {
		.platform_data		= &usb1_data,
	},
	.num_resources	= ARRAY_SIZE(usb1_resources),
	.resource	= usb1_resources,
};
#endif


/* create riic0_device to riic9_device */
#define RIIC_DEVICE(_id)				\
static struct platform_device riic##_id##_device = {	\
	.name		= "i2c-riic",			\
	.id		= _id				\
};
RIIC_DEVICE(0);
RIIC_DEVICE(1);
RIIC_DEVICE(2);
RIIC_DEVICE(3);
RIIC_DEVICE(4);
RIIC_DEVICE(5);
RIIC_DEVICE(6);
RIIC_DEVICE(7);
RIIC_DEVICE(8);
RIIC_DEVICE(9);

static struct plat_serial8250_port scif01_platform_data[] = {
	{
		.membase	= (void __iomem *)0xfe410000,
		.mapbase	= 0xfe410000,
		.flags		= UPF_BOOT_AUTOCONF | UPF_SKIP_TEST,
		.iotype		= UPIO_MEM,
		.uartclk	= 48000000 / 26,
		.irq		= evt2irq(0xb40),
	}, {
		.membase	= (void __iomem *)0xfe420000,
		.mapbase	= 0xfe420000,
		.flags		= UPF_BOOT_AUTOCONF | UPF_SKIP_TEST,
		.iotype		= UPIO_MEM,
		.uartclk	= 48000000 / 26,
		.irq		= evt2irq(0xb60),
	}, {
	},
};

static struct platform_device scif01_device = {
	.name		= "serial8250",
	.id		= PLAT8250_DEV_PLATFORM,
	.dev = {
		.platform_data = scif01_platform_data,
	},
};

static struct resource shwdt_resources[] = {
	{
		.start  = 0xffcc0000,
		.end    = 0xffcc00ff,
		.flags  = IORESOURCE_MEM,
	},
};

static struct platform_device shwdt_device = {
	.name		= "sh-wdt",
	.id		= -1,
	.num_resources	= ARRAY_SIZE(shwdt_resources),
	.resource	= shwdt_resources,
};

static struct resource pwmu_resources[] = {
	{
		.start  = 0xffd70000,
		.end    = 0xffd700ff,
		.flags  = IORESOURCE_MEM,
	}
};

static struct platform_device pwmu_device = {
	.name		= "sh-pwmu",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(pwmu_resources),
	.resource	= pwmu_resources,
};

static struct resource pwmx0_resources[] = {
	{
		.start  = 0xfff00000,
		.end    = 0xfff000ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 0xfff20000,	/* common */
		.end    = 0xfff200ff,
		.flags  = IORESOURCE_MEM,
	}
};

static struct platform_device pwmx0_device = {
	.name		= "sh-pwmx",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(pwmx0_resources),
	.resource	= pwmx0_resources,
};

static struct resource evc0_resources[] = {
	{
		.start  = 0xffe80000,
		.end    = 0xffe800ff,
		.flags  = IORESOURCE_MEM,
	}
};

static struct platform_device evc0_device = {
	.name		= "sh-evc",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(evc0_resources),
	.resource	= evc0_resources,
};

static struct resource adc0_resources[] = {
	{
		.start  = 0xffe30000,
		.end    = 0xffe300ff,
		.flags  = IORESOURCE_MEM,
	},
};

static struct platform_device adc0_device = {
	.name		= "sh-adc",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(adc0_resources),
	.resource	= adc0_resources,
};

static struct resource sgpio0_resources[] = {
	{
		.start  = 0xffed0000,
		.end    = 0xffed07ff,
		.flags  = IORESOURCE_MEM,
	},
	{
		.start  = evt2irq(0x1f80),
		.flags  = IORESOURCE_IRQ,
	},
};

static struct platform_device sgpio0_device = {
	.name		= "sh-sgpio",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(sgpio0_resources),
	.resource	= sgpio0_resources,
};

/* Add by Avocent --- */
static struct mtd_partition spi_flash_partitions[] = {
    {
	.name	    = "u-boot1",
	.offset	    = 0x00000000,
	.size	    = 512 * 1024,
    }, {
	.name	    = "u-boot2",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 512 * 1024,
    }, {
	.name	    = "env1",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 64 * 1024,
    }, {
	.name	    = "env2",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 64 * 1024,
    }, {
	.name	    = "fru",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 64 * 1024,
    }, {
	.name	    = "res1",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 64 * 1024,
    }, {
	.name	    = "tracebuf",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 512 * 1024,
    }, {
	.name	    = "lcl",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 24 * 64 * 1024,
    }, {
	.name	    = "res2",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 12 * 64 * 1024,
    },
};
/* --- */

static struct flash_platform_data spi_flash_data = {
	.name = "m25p80",
	.parts	    = spi_flash_partitions,
	.nr_parts   = ARRAY_SIZE(spi_flash_partitions),
};

#ifdef DELL_USE_SPI1
/* Add by Avocent --- */
static struct mtd_partition spi1_flash_partitions[] = {
    {
	.name	    = "SH7757 SPI1 FLASH (Test Partition 1)",
	.offset	    = 0x00000000,
	.size	    = 1 * 1024 * 1024,
    }, {
	.name	    = "SH7757 SPI1 FLASH (Test Partition 2)",
	.offset	    = MTDPART_OFS_APPEND,
	.size	    = 2 * 1024 * 1024,
    },
};
/* --- */

static struct flash_platform_data spi1_flash_data = {
	.name = "m25p80",
	.type = "s25sl016a",
/* Add by Avocent --- */
	.parts	    = spi1_flash_partitions,
	.nr_parts   = ARRAY_SIZE(spi1_flash_partitions),
/* --- */
};
#endif

static struct mtd_partition spi2_flash_partitions[] = {
    {
	.name	    = "fpspi",
	.offset	    = 0x00000000,
	.size	    = 4 * 1024 * 1024,
    }
};

static struct flash_platform_data spi2_flash_data = {
	.name = "m25p80",
	.type = "mx25l3205d",
	.parts	    = spi2_flash_partitions,
	.nr_parts   = ARRAY_SIZE(spi2_flash_partitions),
};

static struct spi_board_info spi_board_info[] = {
	{
		.modalias = "m25p80",
		.max_speed_hz = 25000000,
		.bus_num = 0,
		.chip_select = 1,
		.platform_data = &spi_flash_data,
	},
#ifdef DELL_USE_SPI1
	{
		.modalias = "m25p80",
		.max_speed_hz = 25000000,
		.bus_num = 1,
		.chip_select = 0,
		.platform_data = &spi1_flash_data,
	},
#endif
	{
		.modalias = "m25p80",
		.max_speed_hz = 4000000,    //12Mhz for flash, 4Mhz for LCD
		.bus_num = 2,
		.chip_select = 1,
		.platform_data = &spi2_flash_data,
	}

};

#define PCIE_BASE	0xffca0000
#define LADMSK0		(PCIE_BASE + 0x1264)
#define BARMAP		(PCIE_BASE + 0x1420)

static int __init sh_pcie_init(void)
{
	printk(KERN_INFO "enable PCIe shared memory area\n");

	__raw_writel(0x00000ff2, LADMSK0);
	__raw_writel(0x00000001, BARMAP);

	return 0;
}
static struct at24_platform_data at24_platdata = {
	.byte_len	= 8192,
	.page_size	= 64,
	.flags		= AT24_FLAG_ADDR16,
};

static struct resource i2c0_resources[] = {
	{
		.start  = 0xfe500000,
		.end    = 0xfe5000ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 144,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 145,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 146,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 147,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c0_device = {
	.name		= "i2c-sh7757",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(i2c0_resources),
	.resource	= i2c0_resources,
};

static struct resource i2c1_resources[] = {
	{
		.start  = 0xfe510000,
		.end    = 0xfe5100ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 148,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 151,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 152,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 153,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c1_device = {
	.name		= "i2c-sh7757",
	.id		= 1,
	.num_resources	= ARRAY_SIZE(i2c1_resources),
	.resource	= i2c1_resources,
};

static struct resource i2c2_resources[] = {
	{
		.start  = 0xfe520000,
		.end    = 0xfe5200ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 154,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 155,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 156,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 160,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c2_device = {
	.name		= "i2c-sh7757",
	.id		= 2,
	.num_resources	= ARRAY_SIZE(i2c2_resources),
	.resource	= i2c2_resources,
};

static struct resource i2c3_resources[] = {
	{
		.start  = 0xfe530000,
		.end    = 0xfe5300ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 161,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 162,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 167,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 168,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c3_device = {
	.name		= "i2c-sh7757",
	.id		= 3,
	.num_resources	= ARRAY_SIZE(i2c3_resources),
	.resource	= i2c3_resources,
};

static struct resource i2c4_resources[] = {
	{
		.start  = 0xfe540000,
		.end    = 0xfe5400ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 174,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 176,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 177,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 178,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c4_device = {
	.name		= "i2c-sh7757",
	.id		= 4,
	.num_resources	= ARRAY_SIZE(i2c4_resources),
	.resource	= i2c4_resources,
};

static struct resource i2c5_resources[] = {
	{
		.start  = 0xfe550000,
		.end    = 0xfe5500ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 179,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 180,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 181,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 182,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c5_device = {
	.name		= "i2c-sh7757",
	.id		= 5,
	.num_resources	= ARRAY_SIZE(i2c5_resources),
	.resource	= i2c5_resources,
};

static struct resource i2c6_resources[] = {
	{
		.start  = 0xfe560000,
		.end    = 0xfe5600ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 183,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 184,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 185,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 188,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c6_device = {
	.name		= "i2c-sh7757",
	.id		= 6,
	.num_resources	= ARRAY_SIZE(i2c6_resources),
	.resource	= i2c6_resources,
};

static struct resource i2c7_resources[] = {
	{
		.start  = 0xfe570000,
		.end    = 0xfe5700ff,
		.flags  = IORESOURCE_MEM,
	}, {
		.start  = 189,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 192,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 193,
		.flags  = IORESOURCE_IRQ,
	}, {
		.start  = 194,
		.flags  = IORESOURCE_IRQ,
	}
};

static struct platform_device i2c7_device = {
	.name		= "i2c-sh7757",
	.id		= 7,
	.num_resources	= ARRAY_SIZE(i2c7_resources),
	.resource	= i2c7_resources,
};


/* Based on arch/sh/kernel/reboot.c: native_machine_restart() */
static void sh7757lcr_machine_restart(char *__unused)
{
	int i;
	unsigned long reg_save;
	printk("Mask interrupts, flush ITLB, disable eth,DMA,I2C,eMMC,USB \n");

	local_irq_disable();

	/* Destroy all of the TLBs in preparation for reset by MMU */
	__flush_tlb_global();

	/* Stop Ethernet */
	__raw_writel(0, 0xFEF00100);	/* ECMR0 */
	__raw_writel(0, 0xFEF00900);	/* ECMR1 */

	/* Stop Giga Ethernet */
	__raw_writel(0, 0xFEE00500);	/* ECMR0 */
	__raw_writel(0, 0xFEE00D00);	/* ECMR1 */

	/* Disable all DMA channels */
	__raw_writel(0, 0xFF70802C);	/* 12 */
	__raw_writel(0, 0xFF70803C);	/* 13 */
	__raw_writel(0, 0xFF70804C);	/* 14 */
	__raw_writel(0, 0xFF70805C);	/* 15 */
	__raw_writel(0, 0xFF70807C);	/* 16 */
	__raw_writel(0, 0xFF70808C);	/* 17 */
	__raw_writel(0, 0xFF71802C);	/* 18 */
	__raw_writel(0, 0xFF71803C);	/* 19 */
	__raw_writel(0, 0xFF71804C);	/* 20 */
	__raw_writel(0, 0xFF71805C);	/* 21 */
	__raw_writel(0, 0xFF71807C);	/* 22 */
	__raw_writel(0, 0xFF71808C);	/* 23 */

	/* RESET the I2C (uses DMA) (optional) */
	reg_save = __raw_readl(0xFFD50038);	/* SRSTR0 */
	__raw_writel(reg_save | 0x0000F000, 0xFFD50038);
	__raw_writel(reg_save, 0xFFD50038);

	/* RESET the eMMC (uses DMA) (optional) */
	reg_save = __raw_readl(0xFFC10048);	/* SRSTR2 */
	__raw_writel(reg_save | 0x00000020, 0xFFC10048);
	__raw_writel(reg_save, 0xFFC10048);

	/* Stop USB-DMA transaction */
	__raw_writel(0, 0xFE451030);	/* Port 0 - SUDMAC00 CH0DEN*/
	__raw_writel(0, 0xFE451230);	/* Port 0 - SUDMAC12 CH0DEN */
	__raw_writel(0, 0xFE451234);	/* Port 0 - SUDMAC12 CH1DEN */
	__raw_writel(0, 0xFE451430);	/* Port 0 - SUDMAC34 CH0DEN */
	__raw_writel(0, 0xFE451434);	/* Port 0 - SUDMAC34 CH1DEN */
	__raw_writel(0, 0xFE451630);	/* Port 0 - SUDMAC56 CH0DEN */
	__raw_writel(0, 0xFE451634);	/* Port 0 - SUDMAC56 CH1DEN */
	__raw_writel(0, 0xFE4F0330);	/* Port 1 - SUDMAC00 CH0DEN */


	for (i = 0; i < 500; i++)
		udelay(1000);	/* 1 ms */

    /*If we got here because of early watchdog, then let the HW reset us (~6s)*/
    if(ctrl_inb(WRSTCSR_R)&0x10)
    {
    	for(i=0;i<10000;i++) {
    		udelay(1000);	/* 1 ms */
        }
    }

	/* Address error with SR.BL=1 first. */
	trigger_address_error();

	/*
	 * Give up and sleep.
	 */
	while (1)
		cpu_sleep();
}

/*
 * If we turn S6-4 on, we assume it runs on reworked sh7758 EVB.
 * Since we cannot use gpio_get_value() in sh7757lcr_setup(), we read PWDR.
 */
#define is_reworked_sh7758evb()	(!(__raw_readb(0xffec0060) & 0x08))
static struct platform_device *sh7757lcr_eth_devices[3] __initdata;

static struct platform_device *sh7757lcr_devices[] __initdata = {
#ifndef USE_DELL_HEARTBEAT
	&heartbeat_device,
#endif
	&sh_mmcif_device,
	&sdhi_device,
	&usb0_device,
	&usb0_f1_device,
	&usb0_f2_device,
	&usb0_f3_device,
	&usb0_f4_device,
	&usb0_f5_device,
	&usb0_f6_device,
#if defined(ENABLE_EHCI)
	&usb_ehci_device,
#endif
#if defined(ENABLE_OHCI)
	&usb_ohci_device,
#endif
#if defined(ENABLE_USB1_UDC)
	&usb1_device,
#endif
	&i2c0_device,
	&i2c1_device,
	&i2c2_device,
	&i2c3_device,
	&i2c4_device,
	&i2c5_device,
	&i2c6_device,
	&i2c7_device,
	&adc0_device,
};

static int __init sh7757lcr_devices_setup(void)
{
//<<<<<<< HEAD
        int ret=0;
	/* Change restart function */
	machine_ops.restart = sh7757lcr_machine_restart;
//=======
	regulator_register_always_on(0, "fixed-3.3V", fixed3v3_power_consumers,
				     ARRAY_SIZE(fixed3v3_power_consumers), 3300000);
//>>>>>>> dell-v3.10
	/* RGMII (PTA) */
	gpio_request(GPIO_FN_ET0_MDC, NULL);
	gpio_request(GPIO_FN_ET0_MDIO, NULL);
	gpio_request(GPIO_FN_ET1_MDC, NULL);
	gpio_request(GPIO_FN_ET1_MDIO, NULL);

	/* ONFI (PTB, PTZ) */
	gpio_request(GPIO_FN_ON_NRE, NULL);
	gpio_request(GPIO_FN_ON_NWE, NULL);
	gpio_request(GPIO_FN_ON_NWP, NULL);
	gpio_request(GPIO_FN_ON_NCE0, NULL);
	gpio_request(GPIO_FN_ON_R_B0, NULL);
	gpio_request(GPIO_FN_ON_ALE, NULL);
	gpio_request(GPIO_FN_ON_CLE, NULL);

	gpio_request(GPIO_FN_ON_DQ7, NULL);
	gpio_request(GPIO_FN_ON_DQ6, NULL);
	gpio_request(GPIO_FN_ON_DQ5, NULL);
	gpio_request(GPIO_FN_ON_DQ4, NULL);
	gpio_request(GPIO_FN_ON_DQ3, NULL);
	gpio_request(GPIO_FN_ON_DQ2, NULL);
	gpio_request(GPIO_FN_ON_DQ1, NULL);
	gpio_request(GPIO_FN_ON_DQ0, NULL);

	/* IRQ8 to 0 (PTB, PTC) */
	gpio_request(GPIO_FN_IRQ8, NULL);
	gpio_request(GPIO_FN_IRQ7, NULL);
	gpio_request(GPIO_FN_IRQ6, NULL);
	gpio_request(GPIO_FN_IRQ5, NULL);
	gpio_request(GPIO_FN_IRQ4, NULL);
	gpio_request(GPIO_FN_IRQ3, NULL);
	gpio_request(GPIO_FN_IRQ2, NULL);
	gpio_request(GPIO_FN_IRQ1, NULL);
	gpio_request(GPIO_FN_IRQ0, NULL);

	/* SPI0 (PTD) */
	gpio_request(GPIO_FN_SP0_MOSI, NULL);
	gpio_request(GPIO_FN_SP0_MISO, NULL);
	gpio_request(GPIO_FN_SP0_SCK, NULL);
	gpio_request(GPIO_FN_SP0_SCK_FB, NULL);
	gpio_request(GPIO_FN_SP0_SS0, NULL);
	gpio_request(GPIO_FN_SP0_SS1, NULL);
	gpio_request(GPIO_FN_SP0_SS2, NULL);
	gpio_request(GPIO_FN_SP0_SS3, NULL);

	/* RMII 0/1 (PTE, PTF) */
	gpio_request(GPIO_FN_RMII0_CRS_DV, NULL);
	gpio_request(GPIO_FN_RMII0_TXD1, NULL);
	gpio_request(GPIO_FN_RMII0_TXD0, NULL);
	gpio_request(GPIO_FN_RMII0_TXEN, NULL);
	gpio_request(GPIO_FN_RMII0_REFCLK, NULL);
	gpio_request(GPIO_FN_RMII0_RXD1, NULL);
	gpio_request(GPIO_FN_RMII0_RXD0, NULL);
	gpio_request(GPIO_FN_RMII0_RX_ER, NULL);
	gpio_request(GPIO_FN_RMII1_CRS_DV, NULL);
	gpio_request(GPIO_FN_RMII1_TXD1, NULL);
	gpio_request(GPIO_FN_RMII1_TXD0, NULL);
	gpio_request(GPIO_FN_RMII1_TXEN, NULL);
	gpio_request(GPIO_FN_RMII1_REFCLK, NULL);
	gpio_request(GPIO_FN_RMII1_RXD1, NULL);
	gpio_request(GPIO_FN_RMII1_RXD0, NULL);
	gpio_request(GPIO_FN_RMII1_RX_ER, NULL);

	/* eMMC (PTG) */
	gpio_request(GPIO_FN_MMCCLK, NULL);
	gpio_request(GPIO_FN_MMCCMD, NULL);
	gpio_request(GPIO_FN_MMCDAT7, NULL);
	gpio_request(GPIO_FN_MMCDAT6, NULL);
	gpio_request(GPIO_FN_MMCDAT5, NULL);
	gpio_request(GPIO_FN_MMCDAT4, NULL);
	gpio_request(GPIO_FN_MMCDAT3, NULL);
	gpio_request(GPIO_FN_MMCDAT2, NULL);
	gpio_request(GPIO_FN_MMCDAT1, NULL);
	gpio_request(GPIO_FN_MMCDAT0, NULL);

	/* LPC (PTG, PTH, PTQ, PTU) */
	gpio_request(GPIO_FN_SERIRQ, NULL);
	gpio_request(GPIO_FN_LPCPD, NULL);
	gpio_request(GPIO_FN_LDRQ, NULL);
	gpio_request(GPIO_FN_WP, NULL);
	gpio_request(GPIO_FN_FMS0, NULL);
	gpio_request(GPIO_FN_LAD3, NULL);
	gpio_request(GPIO_FN_LAD2, NULL);
	gpio_request(GPIO_FN_LAD1, NULL);
	gpio_request(GPIO_FN_LAD0, NULL);
	gpio_request(GPIO_FN_LFRAME, NULL);
	gpio_request(GPIO_FN_LRESET, NULL);
	gpio_request(GPIO_FN_LCLK, NULL);
	gpio_request(GPIO_FN_LGPIO7, NULL);
	//gpio_request(GPIO_FN_LGPIO6, NULL);
	gpio_request(GPIO_FN_LGPIO5, NULL);
	gpio_request(GPIO_FN_LGPIO4, NULL);

	/* SPI1 (PTH) */
	gpio_request(GPIO_FN_SP1_MOSI, NULL);
	gpio_request(GPIO_FN_SP1_MISO, NULL);
	gpio_request(GPIO_FN_SP1_SCK, NULL);
	gpio_request(GPIO_FN_SP1_SCK_FB, NULL);
	gpio_request(GPIO_FN_SP1_SS0, NULL);
	gpio_request(GPIO_FN_SP1_SS1, NULL);

#if defined(CONFIG_SH_SH7757LCR_SPI1_BY_CPU)
	/* SPI1 */
	if (boot_cpu_data.type == CPU_SH7758)
		__raw_writeb(0x40, 0xffd8ee88);		/* CR12 */
	else
		__raw_writeb(0x40, 0xffd8ee80);		/* CR8 */
#endif

	/* SDHI (PTI) */
	gpio_request(GPIO_FN_SD_WP, NULL);
	gpio_request(GPIO_FN_SD_CD, NULL);
	gpio_request(GPIO_FN_SD_CLK, NULL);
	gpio_request(GPIO_FN_SD_CMD, NULL);
	gpio_request(GPIO_FN_SD_D3, NULL);
	gpio_request(GPIO_FN_SD_D2, NULL);
	gpio_request(GPIO_FN_SD_D1, NULL);
	gpio_request(GPIO_FN_SD_D0, NULL);

	/* SCIF3/4 (PTJ, PTW) */
	gpio_request(GPIO_FN_RTS3, NULL);
	gpio_request(GPIO_FN_CTS3, NULL);
	gpio_request(GPIO_FN_TXD3, NULL);
	gpio_request(GPIO_FN_RXD3, NULL);
	gpio_request(GPIO_FN_RTS4, NULL);
	gpio_request(GPIO_FN_RXD4, NULL);
	gpio_request(GPIO_FN_TXD4, NULL);
	gpio_request(GPIO_FN_CTS4, NULL);

	/* SERMUX (PTK, PTL, PTO, PTV) */
	//gpio_request(GPIO_FN_COM2_TXD, NULL);
	//gpio_request(GPIO_FN_COM2_RXD, NULL);
	gpio_request(GPIO_FN_COM2_RTS, NULL);
	gpio_request(GPIO_FN_COM2_CTS, NULL);
	gpio_request(GPIO_FN_COM2_DTR, NULL);
	gpio_request(GPIO_FN_COM2_DSR, NULL);
	gpio_request(GPIO_FN_COM2_DCD, NULL);
	gpio_request(GPIO_FN_COM2_RI, NULL);
	//gpio_request(GPIO_FN_RAC_RXD, NULL);
	//gpio_request(GPIO_FN_RAC_RTS, NULL);
	//gpio_request(GPIO_FN_RAC_CTS, NULL);
	//gpio_request(GPIO_FN_RAC_DTR, NULL);
	//gpio_request(GPIO_FN_RAC_DSR, NULL);
	//gpio_request(GPIO_FN_RAC_DCD, NULL);
	//gpio_request(GPIO_FN_RAC_TXD, NULL);
	gpio_request(GPIO_FN_COM1_TXD, NULL);
	gpio_request(GPIO_FN_COM1_RXD, NULL);
	gpio_request(GPIO_FN_COM1_RTS, NULL);
	gpio_request(GPIO_FN_COM1_CTS, NULL);

	writeb(0x10, 0xfe470000);	/* SMR0: SerMux mode 0 */

#if defined(CONFIG_SH_SH7757LCR_SCIF01_BY_CPU)
	/* for SCIF01 */
	//__raw_writeb(0x06, 0xffd8ee0f);	/* HICR5 */
	//__raw_writeb(0x03, 0xfe410008);	/* SCIFCR0 */
	//__raw_writeb(0x03, 0xfe420008);	/* SCIFCR1 */
	//__raw_writeb(0x00, 0xfe410008);	/* SCIFCR0 */
	//__raw_writeb(0x00, 0xfe420008);	/* SCIFCR1 */
#endif

	/* IIC (PTM, PTR, PTS) */
	gpio_request(GPIO_FN_SDA9, NULL);
	gpio_request(GPIO_FN_SCL9, NULL);
	gpio_request(GPIO_FN_SDA8, NULL);
	gpio_request(GPIO_FN_SCL8, NULL);
	gpio_request(GPIO_FN_SDA7, NULL);
	gpio_request(GPIO_FN_SCL7, NULL);
	gpio_request(GPIO_FN_SDA6, NULL);
	gpio_request(GPIO_FN_SCL6, NULL);
	gpio_request(GPIO_FN_SDA5, NULL);
	gpio_request(GPIO_FN_SCL5, NULL);
	gpio_request(GPIO_FN_SDA4, NULL);
	gpio_request(GPIO_FN_SCL4, NULL);
	gpio_request(GPIO_FN_SDA3, NULL);
	gpio_request(GPIO_FN_SCL3, NULL);
	gpio_request(GPIO_FN_SDA2, NULL);
	gpio_request(GPIO_FN_SCL2, NULL);
	gpio_request(GPIO_FN_SDA1, NULL);
	gpio_request(GPIO_FN_SCL1, NULL);
	gpio_request(GPIO_FN_SDA0, NULL);
	gpio_request(GPIO_FN_SCL0, NULL);

	/* USB (PTN) */
	gpio_request(GPIO_FN_VBUS_EN, NULL);
	gpio_request(GPIO_FN_VBUS_OC, NULL);

	/* SGPIO1/0 (PTN, PTO) */
	gpio_request(GPIO_FN_SGPIO1_CLK, NULL);
	gpio_request(GPIO_FN_SGPIO1_LOAD, NULL);
	gpio_request(GPIO_FN_SGPIO1_DI, NULL);
	gpio_request(GPIO_FN_SGPIO1_DO, NULL);
	gpio_request(GPIO_FN_SGPIO0_CLK, NULL);
	gpio_request(GPIO_FN_SGPIO0_LOAD, NULL);
	gpio_request(GPIO_FN_SGPIO0_DI, NULL);
	gpio_request(GPIO_FN_SGPIO0_DO, NULL);

	/* WDT (PTN) */
	gpio_request(GPIO_FN_SUB_CLKIN, NULL);

	/* System (PTT) */
	//gpio_request(GPIO_FN_STATUS1, NULL);
	//gpio_request(GPIO_FN_STATUS0, NULL);

	/* PWMX (PTT) */
	//gpio_request(GPIO_FN_PWMX1, NULL);
	//gpio_request(GPIO_FN_PWMX0, NULL);

	/* R-SPI (PTV) */
	gpio_request(GPIO_FN_R_SPI_MOSI, NULL);
	gpio_request(GPIO_FN_R_SPI_MISO, NULL);
	gpio_request(GPIO_FN_R_SPI_RSPCK, NULL);
	gpio_request(GPIO_FN_R_SPI_SSL0, NULL);
	gpio_request(GPIO_FN_R_SPI_SSL1, NULL);

	/* EVC (PTV, PTW) */
	gpio_request(GPIO_FN_EVENT7, NULL);
	gpio_request(GPIO_FN_EVENT6, NULL);
	gpio_request(GPIO_FN_EVENT5, NULL);
	gpio_request(GPIO_FN_EVENT4, NULL);
	gpio_request(GPIO_FN_EVENT3, NULL);
	gpio_request(GPIO_FN_EVENT2, NULL);
	gpio_request(GPIO_FN_EVENT1, NULL);
	gpio_request(GPIO_FN_EVENT0, NULL);


	if (boot_cpu_data.type == CPU_SH7758){
        /* Need to configure gpios for fpspi */
        gpio_request(GPIO_PTA1, NULL);    // some platforms share USB_PRES_N and RSPI_MISO
        gpio_direction_input(GPIO_PTA1);        

        gpio_request(GPIO_PTY5, NULL);         // point fp mux to RSPI flash, not shifter
      	gpio_direction_output(GPIO_PTY5, 1);    
    }

#ifndef USE_DELL_HEARTBEAT
	/* LED for heartbeat */
	gpio_request(GPIO_PTU3, NULL);
	gpio_direction_output(GPIO_PTU3, 1);
	gpio_request(GPIO_PTU2, NULL);
	gpio_direction_output(GPIO_PTU2, 1);
	gpio_request(GPIO_PTU1, NULL);
	gpio_direction_output(GPIO_PTU1, 1);
	gpio_request(GPIO_PTU0, NULL);
	gpio_direction_output(GPIO_PTU0, 1);

	gpio_request(GPIO_PTU5, NULL);
	gpio_direction_output(GPIO_PTU5, 0);
#endif
	/* control for MDIO of Gigabit Ethernet */
	//gpio_request(GPIO_PTT4, NULL);
	//gpio_direction_output(GPIO_PTT4, 1);

	/* control for eMMC */
	//gpio_request(GPIO_PTT7, NULL);		/* eMMC_RST# */
	//gpio_direction_output(GPIO_PTT7, 0);
	//gpio_request(GPIO_PTT6, NULL);		/* eMMC_INDEX# */
	//gpio_direction_output(GPIO_PTT6, 0);
	//gpio_request(GPIO_PTT5, NULL);		/* eMMC_PRST# */
	//gpio_direction_output(GPIO_PTT5, 1);

	/* CPLD */
	gpio_request(GPIO_FN_CS5, NULL);
	/* register SPI device information */
	spi_register_board_info(spi_board_info,
				ARRAY_SIZE(spi_board_info));

	/* Ethernet platform */
	if (readl(GBECONT) & GBECONT_RMII0) {
		sh7757lcr_eth_devices[0] = &sh7757_eth0_device;
	} else {
		sh7757lcr_eth_devices[0] = &sh7757_eth_giga0_device;
	}
	if (readl(GBECONT) & GBECONT_RMII1) {
		sh7757lcr_eth_devices[1] = &sh7757_eth1_device;
	} else {
		sh7757lcr_eth_devices[1] = &sh7757_eth_giga1_device;
	}
        ret=Dell_init_idrac_type();
        if (ret == 0 && (idrac_type == 1))
        {
	    sh7757lcr_eth_devices[2] = &sh7757_eth2_dcs_device;
	    platform_add_devices(sh7757lcr_eth_devices,
					ARRAY_SIZE(sh7757lcr_eth_devices));
        }
        else
        {
	    sh7757lcr_eth_devices[2] = NULL;
	    platform_add_devices(sh7757lcr_eth_devices,
					ARRAY_SIZE(sh7757lcr_eth_devices)-1);
        }
	sh_pcie_init();

    /* reset LPC and PCC modules
       HICR0, bit 7-5, LPCxE, LPC Enable 3 to 1, write 0.
       HICR2, bit 3-1, IBFIEx, IBFIx Interrupt Enable, write 0.
       HICR2, bit 0, ERRIE, Error Interrupt Enable, write 0.
       PCCCS, bit 14, FIE, FIFO IRQ Enable, write 0.
       PCCCR, bit 0, PCCWIE, PCC Write Interrupt Enable, write 0. */
    writeb((readb(0xFFD8EE30) & 0x1F), 0xFFD8EE30);
    writeb((readb(0xFFD8EE32) & 0xF0), 0xFFD8EE32);
    writew((readw(0xFFD8EEC6) & 0xBFFF), 0xFFD8EEC6);
    writeb((readb(0xFFD8EECF) & 0xFE), 0xFFD8EECF);

      /* change KCS interrupt priority to 15 */
      intc_set_priority(69, 15);
      intc_set_priority(70, 15);
      intc_set_priority(71, 15);
      intc_set_priority(72, 15);
      intc_set_priority(73, 15);
      

#if 0	// defined(ENABLE_USB1_UDC)
	__raw_writew(0x0100, 0xfe5f0000);	/* RESET: set reset */
	__raw_writel(0, 0xfe4f2000);		/* PORT1SEL: Function */
	__raw_writew(0x0111, 0xfe5f0000);	/* RESET: clear reset */
#endif

	/* General platform */
	return platform_add_devices(sh7757lcr_devices,
				    ARRAY_SIZE(sh7757lcr_devices));
}
arch_initcall(sh7757lcr_devices_setup);

/* Initialize IRQ setting */
void __init init_sh7757lcr_IRQ(void)
{
	plat_irq_setup_pins(IRQ_MODE_IRQ7654);
	plat_irq_setup_pins(IRQ_MODE_IRQ3210);
}

/* Initialize the board */
static void __init sh7757lcr_setup(char **cmdline_p)
{
	if (boot_cpu_data.type == CPU_SH7758)
		printk(KERN_INFO "Renesas%sSH7758 EVB support.\n",
			is_reworked_sh7758evb() ? " reworked ": " ");
	else
		printk(KERN_INFO "Renesas R0P7757LC0012RL support.\n");
}

static int sh7757lcr_mode_pins(void)
{
	int value = 0;

	/* These are the factory default settings of S3 (Low active).
	 * If you change these dip switches then you will need to
	 * adjust the values below as well.
	 */
	value |= MODE_PIN0;	/* Clock Mode: 1 */

	return value;
}

#ifdef DELL_NMI_HANDLER
#define SH7757LCR_INTERNAL_RAM_ADDR	((void *)0xe5000000)
#define SH7757LCR_CORE_RESET_ADDR	((void *)0xa0000000)
#define SH7757LCR_CORE_RESET_SIZE	256
/* This function is run on internal RAM. */
static void sh7757lcr_core_reset_on_internal_ram(void)
{
	void (*core_reset)(void) = SH7757LCR_CORE_RESET_ADDR;

	__raw_writel(0, 0xff00001c);	/* disable cache */
	__raw_writel(0, 0xff000010);	/* disable MMU */
	__raw_writel(0, 0xff000070);	/* change MMU mode to 29bit */

	core_reset();
}

#define CONFIG_RAM_BOOT_PHYS	0xE5200000
#define CONFIG_SPI_ADDR		0x00000000
#define CONFIG_SPI_LENGTH	(0x00000800 >> 5)

#define SPIWDMADR	0xFE001018
#define SPIWDMCNTR	0xFE001020
#define SPIDMCOR	0xFE001028
#define SPIDMINTSR	0xFE001188
#define SPIDMINTMR	0xFE001190

#define SPIDMINTSR_DMEND	0x00000004

#define TBR	0xFE002000
#define RBR	0xFE002000

#define CR1	0xFE002008
#define CR2	0xFE002010
#define CR3	0xFE002018

/* CR1 */
#define SPI_TBE		0x80
#define SPI_TBF		0x40
#define SPI_RBE		0x20
#define SPI_RBF		0x10
#define SPI_PFONRD	0x08
#define SPI_SSDB	0x04
#define SPI_SSD		0x02
#define SPI_SSA		0x01

/* CR2 */
#define SPI_RSTF	0x80
#define SPI_LOOPBK	0x40
#define SPI_CPOL	0x20
#define SPI_CPHA	0x10
#define SPI_L1M0	0x08

/* M25P80 */
#define M25_READ	0x03
void machine_restart(char * __unused);

static void spi_reset(void) 
{
	__raw_writel(0xfe, CR1);

	__raw_writel(0, SPIDMCOR);
	__raw_writel(0x00, CR1);

	__raw_writel(__raw_readl(CR2) | SPI_RSTF, CR2);	/* fifo reset */
	__raw_writel(__raw_readl(CR2) & ~SPI_RSTF, CR2);
}

static void spi_read_flash(void *buf, unsigned long addr, unsigned long len) 
{
	volatile unsigned long i;

	__raw_writel(M25_READ, TBR);
	__raw_writel((addr >> 16) & 0xFF, TBR);
	__raw_writel((addr >> 8) & 0xFF, TBR);
	__raw_writel(addr & 0xFF, TBR);

	__raw_writel(SPIDMINTSR_DMEND, SPIDMINTSR);
	__raw_writel(buf, SPIWDMADR);
	__raw_writel(len & 0xFFFFFFE0, SPIWDMCNTR);
	__raw_writel(1, SPIDMCOR);

	__raw_writel(0xff, CR3);
	__raw_writel(__raw_readl(CR1) | SPI_SSDB, CR1);
	__raw_writel(__raw_readl(CR1) | SPI_SSA, CR1);

	while (!(__raw_readl(SPIDMINTSR) & SPIDMINTSR_DMEND))
		;

	/* deassert CS */
	__raw_writel(__raw_readl(CR2) | SPI_RSTF, CR2);	/* fifo reset */
	__raw_writel(__raw_readl(CR2) & ~SPI_RSTF, CR2);
	__raw_writel(0x1, CR3);
}

void spiboot_main(void)
{
	spi_reset();
	spi_read_flash((void *)CONFIG_RAM_BOOT_PHYS, CONFIG_SPI_ADDR,
			CONFIG_SPI_LENGTH);
}

void sh7757lcr_nmi_reset(void)
{
#define CPLD_MODE1_OFFSET 0x06
	printk("%s(): attempting to graceful shutdown...\n",__FUNCTION__);


	void (*goto_internal_ram)(void) = SH7757LCR_INTERNAL_RAM_ADDR;

	local_irq_disable();

    if (nmi_buf_start) {
        /* if there's a home set aside in u-boot for it, save tracebuffer */
        memcpy(nmi_buf_start, trabbuf, TRAB_SIZE);

        //
        // set cpld bit to signal to u-boot that this is an nmi
        // so it will copy the buffer to spi flash 
        //
    	iowrite8((ioread8(_cpldaddr+CPLD_MODE1_OFFSET) | 0x04), _cpldaddr+CPLD_MODE1_OFFSET);
    }

	/* stop all watchdog */
	__raw_writew(0xa500, 0xffcc0002);	/* ch0 */
	__raw_writew(0xa500, 0xffcf0002);	/* ch1 */
	__raw_writew(0xa500, 0xfff90002);	/* ch2 */

	/* enable WDT2/alt boot, u-boot will disable upon boot*/
	__raw_writew(0xa500, 0xfff90002);	/*CSR disable timer*/
	__raw_writew(0x6940, 0xfff90002);	/*RSTCSR clr WOVF en rst*/
	__raw_writew(0xa55c, 0xfff90002);	/*CSR sel WDT, use Po2048 clk*/
	__raw_writew(0x0000, 0xfff90000);	/*CNT will be 20seconds*/
	__raw_writew(0xa57c, 0xfff90002);	/*CSR turn on counter*/

	spiboot_main();

	memcpy(SH7757LCR_INTERNAL_RAM_ADDR,
		sh7757lcr_core_reset_on_internal_ram,
		SH7757LCR_CORE_RESET_SIZE);

	printk( "Right Before A Machine Reset\n" );
	machine_restart(' ');
}

EXPORT_SYMBOL(sh7757lcr_nmi_reset);
#endif //DELL_NMI_HANDLER


/* The Machine Vector */
static struct sh_machine_vector mv_sh7757lcr __initmv = {
	.mv_name		= "SH7757LCR",
	.mv_setup		= sh7757lcr_setup,
	.mv_init_irq		= init_sh7757lcr_IRQ,
	.mv_mode_pins		= sh7757lcr_mode_pins,
};
