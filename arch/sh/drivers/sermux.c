/*
 * SERMUX driver for SH7757
 *
 * Copyright (C) 2010-2012  Renesas Solutions Corp.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/irq.h>

#define SERMUX_SMR0	0x00
#define SERMUX_SMR1	0x01
#define SERMUX_SMR2	0x02
#define SERMUX_SMR3	0x03
#define SERMUX_SMR4	0x04
#define SERMUX_SMR5	0x05

#define SMR0_MS3	0x20
#define SMR0_MS210_MASK	0x07

#define DRIVER_VERSION	"2012-10-11"

struct sermux {
	void * __iomem *reg;
};

static inline void sermux_write(struct sermux *ser, unsigned long data,
				unsigned long offset)
{
	iowrite8(data, ser->reg + offset);
}

static inline unsigned long sermux_read(struct sermux *ser,
					unsigned long offset)
{
	return ioread8(ser->reg + offset);
}

static int sermux_get_mode(struct sermux *ser)
{
	int mode;
	unsigned long tmp;

	tmp = sermux_read(ser, SERMUX_SMR0);
	mode = tmp & SMR0_MS210_MASK;
	if (tmp & SMR0_MS3)
		mode += 8;

	return mode;
}

static int sermux_set_mode(struct sermux *ser, int mode)
{
	unsigned long tmp;

	if (mode < 0 || mode == 7 || mode >= 14)
		return -EINVAL;

	tmp = mode & 0x07;
	if (mode >= 8)
		tmp |= SMR0_MS3;
	tmp |= sermux_read(ser, SERMUX_SMR0) & ~(SMR0_MS3 | SMR0_MS210_MASK);
	sermux_write(ser, tmp, SERMUX_SMR0);

	return 0;
}

static ssize_t sermux_show_mode(struct device *dev,
				struct device_attribute *attr, char *buf)
{
	struct sermux *ser = dev_get_drvdata(dev);
	int mode;

	mode = sermux_get_mode(ser);

	return sprintf(buf, "%d\n", mode);
}

static ssize_t sermux_store_mode(struct device *dev,
				struct device_attribute *attr, const char *buf,
				size_t count)
{
	struct sermux *ser = dev_get_drvdata(dev);
	int mode, ret;

	sscanf(buf, "%d", &mode);
	ret = sermux_set_mode(ser, mode);
	if (ret < 0)
		return ret;

	return count;
}

static struct device_attribute sermux_device_attributes[] = {
	__ATTR(mode, 0644, sermux_show_mode, sermux_store_mode),
};

static int sermux_probe(struct platform_device *pdev)
{
	struct resource *res = NULL;
	void __iomem *reg = NULL;
	int i;
	int ret = 0;
	struct sermux *ser = NULL;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		ret = -ENODEV;
		dev_err(&pdev->dev, "platform_get_resource failed.\n");
		goto clean_up;
	}

	reg = ioremap(res->start, resource_size(res));
	if (reg == NULL) {
		ret = -ENOMEM;
		dev_err(&pdev->dev, "ioremap failed.\n");
		goto clean_up;
	}

	ser = kzalloc(sizeof(struct sermux), GFP_KERNEL);
	if (ser == NULL) {
		ret = -ENOMEM;
		dev_err(&pdev->dev, "kzalloc failed.\n");
		goto clean_up;
	}
	ser->reg = reg;

	platform_set_drvdata(pdev, ser);

	for (i = 0; i < ARRAY_SIZE(sermux_device_attributes); i++) {
		ret = device_create_file(&pdev->dev,
					 &sermux_device_attributes[i]);
		if (ret < 0) {
			printk(KERN_ERR "device_create_file failed\n");
			goto clean_up;
		}
	}

	dev_info(&pdev->dev, "version %s\n", DRIVER_VERSION);
	return ret;

clean_up:
	if (reg)
		iounmap(reg);
	kfree(ser);

	return ret;
}

static int sermux_remove(struct platform_device *pdev)
{
	struct sermux *ser = platform_get_drvdata(pdev);
	int i;

	for (i = 0; i < ARRAY_SIZE(sermux_device_attributes); i++)
		device_remove_file(&pdev->dev,
					 &sermux_device_attributes[i]);
	iounmap(ser->reg);
	kfree(ser);

	return 0;
}

static struct platform_driver sermux_driver = {
	.probe =	sermux_probe,
	.remove =	sermux_remove,
	.driver		= {
		.name	= "sermux",
		.owner = THIS_MODULE,
	},
};
module_platform_driver(sermux_driver);

MODULE_DESCRIPTION("SERMUX driver");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Yoshihiro Shimoda");
MODULE_ALIAS("platform:sermux");
