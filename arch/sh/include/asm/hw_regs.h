
#ifndef __HW_REGS_H__
#define __HW_REGS_H__           1

#include <asm/addrspace.h>
//#define SH7757_VIRT_TO_PHYS(x)  (x & 0x1FFFFFFF)


/* INTC */
#define SH7757_INTC_VA          0xFFD00000


/* SPI0 */
#define SH7757_SPI0_VA         0xFE002000

/* RSPI */
#define SH7757_RSPI_VA         0xFE480000

/* WATCHDOG TIMER */
#define SH7757_WDT0_VA          0xFFCC0000
#define SH7757_WDT1_VA          0xFFCD0000
#define SH7757_WDT2_VA          0xFFF90000

#define WTCNT		0xffcc0000
#define WTCSR		0xffcc0002
#define WRSTCSR		0xffcc0002
#define WRSTCSR_R   0xffcc0003
#define WCNTEW      0xffcc0004


/* GIGABIT ETHERNET CONTROLLER */
#define SH7757_GETHERC0_VA       0xFEE00000
#define SH7757_GETHERC1_VA       0xFEE00800


/* ETHERNET CONTROLLER */
#define SH7757_ETHERC0_VA       0xFEF00100
#define SH7757_ETHERC1_VA       0xFEF00800


/* LPC */
#define SH7757_LPC_VA           0xFFD80000


/* SCIF */
#define SH7757_SCIF0_VA         0xFE410000
#define SH7757_SCIF1_VA         0xFE420000

#define SH7757_SCIF2_VA         0xFE4B0000
#define SH7757_SCIF3_VA         0xFE4C0000
#define SH7757_SCIF4_VA         0xFE4D0000


/* SERMUX */
#define SH7757_SMR0_VA          0xFE470000


/* PECI */
//#define SH7757_PECI_VA          0xFF300000


/* GPIO */
#define SH7757_GPIO_VA          0xFFEC0000


/* SGPIO */
#define SH7757_SGPIO0_VA         0xFFED0000
#define SH7757_SGPIO1_VA         0xFFED1000
#define SH7757_SGPIO2_VA         0xFFED2000


/* I2C */
#define SH7757_I2C0_VA          0xFE500000
#define SH7757_I2C1_VA          0xFE510020
#define SH7757_I2C2_VA          0xFE520000
#define SH7757_I2C3_VA          0xFE530020
#define SH7757_I2C4_VA          0xFE540000
#define SH7757_I2C5_VA          0xFE550020
#define SH7757_I2C6_VA          0xFE560000
#define SH7757_I2C7_VA          0xFE570020
#define SH7757_I2C8_VA          0xFE580000
#define SH7757_I2C9_VA          0xFE590020


/* USB */
#define SH7757_USB0_VA          0xFE450000
#define SH7757_USB1_VA          0xFE4F0000


/* GRAPHICS CONTROLLER */
#define SH7757_GRA_VA           0xFFC10000


/* LBSC */
#define SH7757_BCR_VA           0xFF800000


/* SIM CARD */
#define SH7757_SIM_VA           0xFE490000


/* CRC */
#define SH7757_CRC_VA           0xFFEB0000


/* PCI CONTROLLER */
#define SH7757_PCI_VA           0xFEC40000


/* VIDEO COMPRESSION */
//#define SH7757_DVC_VA           0xFEA00000


/* AVC */
//#define SH7757_AVC_VA           0xFEA00000


/* EVENT COUNTER */
#define SH7757_EVC_VA           0xFFE80000


/* ARC4 */
//#define SH7757_ARC4_VA          0xFFFB0000


/* ADC */
#define SH7757_ADC0_VA           0xFFE30000
#define SH7757_ADC1_VA           0xFFE40000


/* UBC */
#define SH7757_UBC_VA           0xFF200000


/* GCTRL */
#define SH7757_GCTRL_VA         0xFFC10000


/* PWMX */
#define SH7757_PWMX0_VA         0xFFF00000
#define SH7757_PWMX1_VA         0xFFF10000
#define SH7757_PWMX2_VA         0xFFF40000
#define SH7757_PWMX3_VA         0xFFF50000

/* FAN EVENT */
#define FAN_PSEL6 0xFFEC007C 
#define FAN_PSEL7 0xFFEC0082

/* FRT */
#define SH7757_FRT_VA           0xFFEA0000


/* TMR */
#define SH7757_TMR0_VA          0xFFE00000
#define SH7757_TMR1_VA          0xFFE10000


/* TMU */
#define SH7757_TMU_VA           0xFE430000


#endif

