/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     ADC linux driver. \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_adcsensordrv_platform.h
 *  @brief  This is the header file for ON Chip ADC sensor driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef AESSADCSENSORDRV_PLATFORM_H
#define AESSADCSENSORDRV_PLATFORM_H

#include "aess_adcsensordrv_common.h"

#define ADC_MAX_CHNL_NUM    0xFF /**< define ADC_MAX_CHNL_NUM is 0xff */

#endif   /* AESSADCSENSORDRV_PLATFORM_H */
/* End of code */
