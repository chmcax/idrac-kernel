/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     SGPIO driver                                                   \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_sgpiodrv_platform.h
 *  @brief  sgpio driver 
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef SGPIO_DRV_PLATFORM_H
#define SGPIO_DRV_PLATFORM_H

#include "aess_sgpiodrv_common.h"


#define MAX_PORT_NUM 3

/** SGPIO u8CommandType , use to access SGPIO register*/
#define SGPIO_REG_ACCESS 3

/** The u8CommandNum list of SGPIO_REG_ACCESS */
enum{
    REG_XFERCFG		    = 0 ,
    REG_XFERNUM		    , // 1
    REG_XFERBLMON		  , // 2 
    REG_SCLOCKFREQ	,   // 3
    REG_MANUALCNTL	,   // 4
    REG_STATEMON		,   // 5
    REG_TRSDATA0		,   // 6
    REG_TRSDATA1		,   // 7
    REG_TRSDATA2		,   // 8
    REG_TRSDATA3		,   // 9
    REG_RCVDATA0		,   // 10
    REG_RCVDATA1		,   // 11
    REG_RCVDATA2		,   // 12
    REG_RCVDATA3		,   // 13
    REG_INTEN		,       // 14 
    REG_INTSRC		,     // 15
    REG_GPICFG		,     // 16
    REG_GPILATCHEN0	,   // 17
    REG_GPILATCHEN1	,   // 18
    REG_GPILATCHEN2	,   // 19
    REG_GPILATCHEN3	,   // 20
    REG_GPIPOLARITY0	, // 21
    REG_GPIPOLARITY1	, // 22
    REG_GPIPOLARITY2	, // 23
    REG_GPIPOLARITY3	, // 24
    REG_BITMATCH0		,   // 25
    REG_BITMATCH1		,   // 26
    REG_BITMATCH2		,   // 27
    REG_BITMATCH3   ,   // 28
    REG_SRCSEL      ,   // 29
    REG_TOTAL_NUM		    
};


/** SGPIO u8CommandType , use to start /stop SGPIO port bit stream. */                         
#define SGPIO_CMD_BSTREAM_CTRL           0x10
/** The commandNum list of SGPIO_CMD_BSTREAM_CTRL */
/** SGPIO u8CommandNum, use to start SGPIO port 0 bit stream. */
#define SBSTREAM0_ON            0x01
/** SGPIO u8CommandNum, use to stop SGPIO port 0 bit stream. */
#define SBSTREAM0_OFF           0x00
/** SGPIO u8CommandNum, use to start SGPIO port 1 bit stream. */
#define SBSTREAM1_ON            0x11
/** SGPIO u8CommandNum, use to stop SGPIO port 1 bit stream. */
#define SBSTREAM1_OFF           0x10
/* The PinNum indicate the Max. ternsfer bit fot the bit stream.*/
#define SBSTREAM2_ON            0x21
/** SGPIO u8CommandNum, use to stop SGPIO port 1 bit stream. */
#define SBSTREAM2_OFF           0x20
/* The PinNum indicate the Max. ternsfer bit fot the bit stream.*/





/** SGPIO u8CommandType , use to set the SGPIO port 0 clock speed. */  
#define SGPIO_CMD_CLK0_CHG           0x11
/** SGPIO u8CommandType , use to set the SGPIO port 1 clock speed. */  
#define SGPIO_CMD_CLK1_CHG           0x12
/** SGPIO u8CommandType , use to set the SGPIO port 1 clock speed. */  
#define SGPIO_CMD_CLK2_CHG           0x13

/** Haredware default(Pclk) speed.*/
#define SCLOCKFREQ_1        0x0000000f            
/** (Pclk)/2 Hz */
#define SCLOCKFREQ_2        0x00	/* Pclk / 2 */
/** (Pclk)/4 Hz */
#define SCLOCKFREQ_4        0x01
/** (Pclk)/8 Hz */
#define SCLOCKFREQ_8        0x02     
/** (Pclk)/16 Hz */
#define SCLOCKFREQ_16       0x03     //default
/** (Pclk)/32 Hz */
#define SCLOCKFREQ_32       0x04
/** (Pclk)/64 Hz */
#define SCLOCKFREQ_64       0x05
/** (Pclk)/128 Hz */
#define SCLOCKFREQ_128      0x06
/** (Pclk)/256 Hz */
#define SCLOCKFREQ_256      0x07
/** (Pclk)/512 Hz */
#define SCLOCKFREQ_512      0x08



/** SGPIO u8CommandType , use to set the SGPIO port 0 Max. transfer bit number. */  
#define SGPIO_CMD_PORT0_XFERNUM      0x20
/** SGPIO u8CommandType , use to set the SGPIO port 1 Max. transfer bit number. */  
#define SGPIO_CMD_PORT1_XFERNUM      0x21
/** SGPIO u8CommandType , use to set the SGPIO port 1 Max. transfer bit number. */  
#define SGPIO_CMD_PORT2_XFERNUM      0x22


/* private define */
/** SGPIO IRQ number */
#define IRQ_SGPIO     236 

/** SGPIO IRQ pin number min and max*/
#define IRQ_SGPIO_MIN           255
#define IRQ_SGPIO_MAX           641


/******************************************************************************
*   STRUCT      :   sSGPIO_INTSTATUS
******************************************************************************/
/**
 *  @brief   Structure to SGPIO driver related SGPIO pin interrupt status.
 *
 *****************************************************************************/
typedef struct 
{
    /** SGPIO port 0 bit 127~0 bitmatch interrupt status.*/ 
    UINT32 SGPIO[MAX_PORT_NUM][4];

} sSGPIO_INTSTATUS ;


extern int aess_sgpio_isr_handler( sSGPIO_INTSTATUS *psgpio_intstatus );
extern int aess_sgpio_ctrl_api( sSGPIOData *pSGPIOStruct );



#endif

/* End of code */
