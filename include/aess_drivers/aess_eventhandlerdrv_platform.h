/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     linux driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_eventhandlerdrv_platform.h
 *  @brief  This is the kernel-layer event handler
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef AESSEVENTHANDLER_PLATFORM_H
#define AESSEVENTHANDLER_PLATFORM_H

#include "aess_eventhandlerdrv_common.h"

#endif   /* AESSEVENTHANDLER_COMMON_H */

/* End of code */
