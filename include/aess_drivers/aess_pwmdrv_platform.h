/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     PWM driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_pwmdrv_platform.h
 *  @brief  The header file of PWM driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/
 
#if !defined (AESSPWMDRV_PLATFORM_H)
#define AESSPWMDRV_PLATFORM_H

/** Maximum PWM Channel Number */
#define PWM_MAX_CHN_NUM 8

#include "aess_pwmdrv_common.h"

#endif   /* AESSPWMDRV_PLATFORM_H */

/* End of code */
