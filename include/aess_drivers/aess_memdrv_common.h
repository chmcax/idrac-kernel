/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     memory driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_memdrv_common.h
 *  @brief  The header file of memory device driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef AESSMEMDRV_COMMON_H
#define AESSMEMDRV_COMMON_H

/* ioctl definitions */
#define AESS_MEMDRV_IOC_MAGIC    0xB4  /**< IO magic number of memory driver. */
#define AESS_MEMDRV_READ         _IOWR(AESS_MEMDRV_IOC_MAGIC, 0, int) /**< I/O control command: read memory. */
#define AESS_MEMDRV_WRITE        _IOWR(AESS_MEMDRV_IOC_MAGIC, 1, int) /**< I/O control command: write memory. */
#define AESS_MEMDRV_REQUEST      _IOWR(AESS_MEMDRV_IOC_MAGIC, 2, int) /**< I/O control command: request a memory region. */
#define AESS_MEMDRV_RELEASE      _IOWR(AESS_MEMDRV_IOC_MAGIC, 3, int) /**< I/O control command: release memory region. */

#define MEM_DRV_MAX_REGION_NUM      10 /**< Define the maximum region numbers. */


#define MEM_DRIVER_NAME "aess_memdrv"  /**< Define memory driver name. */

/******************************************************************************
*   Enum      :   eMemDrvDataWidthType
******************************************************************************/
/**
 *  @brief   memory driver data width enumeration type
 *
 *****************************************************************************/
typedef enum
{
	/** Byte */
	MEM_DRV_BYTE = 0,
	
	/** Word */
	MEM_DRV_WORD,
	
	/** Double word */
	MEM_DRV_DWORD
	
} eMemDrvDataWidthType;



/******************************************************************************
*   STRUCT      :   sMemDrvInfoType
******************************************************************************/
/**
 *  @brief   Structure to memory driver related data parameter.
 *
 *****************************************************************************/
typedef struct
{
	/** physical base address of request region */
	UINT32 u32BaseAddr;
    
	/** read/write data buffer pointer */
	void *pDataPtr;
    
	/** size of request region in bytes */
	UINT16 u16RegionSize;
    
	/** offset of base address in bytes */
	UINT16 u16Offset;
    
	/** data size to read/write */
	UINT16 u16DataSize;
    
	/** read/write data width */
	UINT8 u8DataWidth;
	
	/** identifier */
	UINT8 u8ID;
	
} sMemDrvInfoType;

#endif   /* AESSMEMDRV_COMMON_H */

/* End of code */
