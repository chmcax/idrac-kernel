/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     SGPIO driver                                                   \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_sgpiodrv_common.h
 *  @brief  sgpio driver 
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef SGPIO_DRV_COMMON_H
#define SGPIO_DRV_COMMON_H

#define SGPIO_DRIVER_NAME "aess_sgpiodrv"  /**< Define SGPIO driver name. */

/** SGPIO ioctl magic number.  */
#define AESS_SGPIODRV_IOC_MAGIC    0xCC

/** SGPIO ioctl command, used to read data from the assigned SGPIO pin.  */
#define AESS_SGPIODRV_R        _IOWR(AESS_SGPIODRV_IOC_MAGIC, 0, int)

/** SGPIO ioctl command, used to write data to the assigned SGPIO pin.  */
#define AESS_SGPIODRV_W        _IOWR(AESS_SGPIODRV_IOC_MAGIC, 1, int)

/** SGPIO ioctl command, used to config the assigned SGPIO pin interrupt .  */
#define AESS_SGPIODRV_CFG         _IOWR(AESS_SGPIODRV_IOC_MAGIC, 2, int)

/** SGPIO ioctl command, to access SGPIO control unit register.  */
#define AESS_SGPIODRV_D           _IOWR(AESS_SGPIODRV_IOC_MAGIC, 3, int)



/******************************************************************************
*   STRUCT      :   sSGPIOData
******************************************************************************/
/**
 *  @brief   Structure to SGPIO driver related data parameter.
 *
 *****************************************************************************/
typedef struct
{
    /** The pointer to passed data. */
    void *pData;	     
    
    /** The assigned  pin number.  */
    UINT32 u32PinNum ;       

    /** The command type.  */
    UINT8 u8CommandType;

    /** The command number. */
    UINT8 u8CommandNum;	

    /** Reserved. */
    UINT8 u8Reserved;
  
} sSGPIOData;


/** u8CommandType list                                      */
/** SGPIO u8CommandType, used to config a SGPIO pin.  */
#define SGPIO_CONFIG    0 
/** SGPIO u8CommandType, used to write data to a SGPIO pin.  */
#define SGPIO_WRITE     1
/** SGPIO u8CommandType, used to read data from a SGPIO pin.  */
#define SGPIO_READ      2


/** The u8CommandNum list of SGPIO_CONFIG */
/** SGPIO u8CommandNum, for disable interrupt.  */
#define SGPIO_CMD_INT_OFF           0x0
/** SGPIO u8CommandNum, for enable interrupt with high level trigger.  */
#define SGPIO_CMD_INT_HIGH          0x1
/** SGPIO u8CommandNum, for enable interrupt with low level trigger.  */
#define SGPIO_CMD_INT_LOW           0x2
/** SGPIO u8CommandNum, for clear interrupt status.  */
#define SGPIO_CMD_INT_CLR           0x3



#endif   /* AESSSGPIODRV_H */


/* End of code */
