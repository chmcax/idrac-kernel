/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     PECI driver\n
 *----------------------------------------------------------------------------\n
 *  @file   aess_pecisensordrv_common.h
 *  @brief  Header file of On chip peci sensor driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/
#include <asm/ioctl.h>
#if !defined (AESSPECISENSORDRV_COMMON_H)
#define AESSPECISENSORDRV_COMMON_H

#define PECI_DATA_INOUT_REG_NUMBER  16         /**< PECI command buffer size. */

#define PECI_DRIVER_NAME "aess_pecisensordrv"  /**< Define name of PECI device. */

/******************************************************************************
*   STRUCT      :   sPECICommandData
******************************************************************************/
/**
 *  @brief   Structure to PECI driver command data parameter.
 *
 *****************************************************************************/
typedef struct
{
	/** Set PECI Client Address */
	UINT8 u8ClientAddress;     

	/** Set PECI Write Length */
	UINT8 u8WriteLength;     

	/** Set PECI Read Length */
	UINT8 u8ReadLength;     
	
	/** Set PECI Command Code */
	UINT8 u8CommandCode;     	

	/** Set PECI Command Data */
	UINT8 u8CommandData[PECI_DATA_INOUT_REG_NUMBER];   
			
} sPECICommandData;

/* ioctl definitions */
#define AESS_PECIDRV_IOC_MAGIC  0xC8            /**< IO magic number of PECI driver. */
#define AESS_PECIDRV_R          _IOR(AESS_PECIDRV_IOC_MAGIC, 0, int)  /**< I/O control command: read temperature. */
#define AESS_PECIDRV_Q          _IOR(AESS_PECIDRV_IOC_MAGIC, 1, int)  /**< I/O control command: query cpu. */
#define AESS_PECIDRV_COMMAND    _IOR(AESS_PECIDRV_IOC_MAGIC, 2, int)  /**< I/O control command: execute a PECI command. */

#define DOMAIN_NUM_MASK             0xf     /**< Mask for domain number. */
#define PROCESS_NUM_MASK            0xf0    /**< Mask for processor number. */
#define GET_PROCESS_NUM             4       /**< Get processor num. */

#define MAX_CPU_NUMBER              4       /**< Max cpu number. */

#define PECICLIENTADDR1             0x30    /**< PECI client address 1. */
#define PECICLIENTADDR2             0x31    /**< PECI client address 2. */
#define PECICLIENTADDR3             0x32    /**< PECI client address 3. */
#define PECICLIENTADDR4             0x33    /**< PECI client address 4. */

#define PECICOMMAND_PING            0       /**< Command code of ping. */
#define PECICOMMAND_GETTEMP         1       /**< Command code of get temperature. */

#define PECI_READING_VALUE_MASK     0xFFFF  /**< PECI reading value mask. */
#define SHIFT_FRACTIONAL            6       /**< Shift bit number for fractional. */

#define PECI_TEMP_THRESHOLD         0x80    /**< Sensor threshold. */


#endif   /* AESSPECISENSORDRV_COMMON_H */
/* End of code */
