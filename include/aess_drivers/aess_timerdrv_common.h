/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     Timer Driver                                                  \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_timerdrv_common.h
 *  @brief  Timer driver
 *  @internal
 *----------------------------------------------------------------------------*/
#if !defined (AESSTIMERDRV_COMMON_H)
#define AESSTIMERDRV_COMMON_H

#define TIMER_DRIVER_NAME "aess_timerdrv"  /**< Define timer driver name. */

/* IOCTL command */
#define AESS_HWTIMERDRV_IOC_MAGIC    0xC0
#define AESS_HWTIMERDRV_ADD          _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 0, int)
#define AESS_HWTIMERDRV_ADDBH        _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 1, int)
#define AESS_HWTIMERDRV_DEL          _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 2, int)
#define AESS_HWTIMERDRV_START        _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 3, int)
#define AESS_HWTIMERDRV_STOP         _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 4, int)
#define AESS_HWTIMERDRV_RESET        _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 5, int)
#define AESS_HWTIMERDRV_RESUME       _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 6, int)
#define AESS_HWTIMERDRV_WAITTIMEOUT  _IOWR(AESS_HWTIMERDRV_IOC_MAGIC, 7, int)


// ERROR CODE
#define EC_INIT_FAIL
#define EC_INVALID_CLCOK_TYPE
#define EC_NO_FREE_TIMER_UINT
#define EC_INVALID_TICK_VALUE


/* Callback function execute area */
#define USER_SPACE              0x1
#define KERNEL_SPACE            0x2
#define BOTTOM_HALF             0x3

/* MISC */

#define DISABLE                 0x0
#define ENABLE                  0x1
#define CLEAR_INTERRUPT         0x1
#define USE_ORIGIN_VALUE        0x0
#define ONE_SHOT_TIMER          0x0
#define ALWAYS_TIMER            0x1
#define EXECUTE_BH              0x1


/* For temporary use, because no type.h now */
#define STATUS_OK     0
#define STATUS_FAIL   1

/* init flag */
#define INIT_OK                 0x0
#define INIT_FAIL               0x1

/* Timer status */
#define NOT_USED_YET            0x0
#define USED_BY_TIMERDRV        0x1
#define USED_BY_TIMERDRV_STOP   0x2
#define NOT_TIMERDRV_RESOURCE   0x3


/******************************************************************************
*   STRUCT      :   sTimerInfo
******************************************************************************/
/**
 *  @brief   Structure to event handler driver related data parameter.
 *
 *****************************************************************************/
typedef struct
{
    /** Function pointer for call-back function */
    void *func;

    /** 32bits parameter for call back function */
    UINT32 u32Parameter;

    /** Count match value for timer, */
    UINT32 u32TimerCounter;

    /** Clock type , */
    UINT8 u8TimerClkType;

    /** Timer number */
    UINT8 u8Number;

    /** One shot timer (0), always (1) */
    UINT8 u8Type;

    /** Timer callback function is in user space(1), kernel space(2)
    	or kernel space - bottom half type(3) */
    UINT8 u8Space;

} sTimerInfo;

#endif   /* AESSTIMERDRV_COMMON_H */


/* End of code */
