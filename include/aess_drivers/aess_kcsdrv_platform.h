/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     KCS Driver                                                    \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_kcsdrv_platform.h
 *  @brief  KCS driver and interrupt service routine
 *  @internal
 *----------------------------------------------------------------------------*/

 
 
#ifndef AESSKCSDRV_PLATFORM_H
#define AESSKCSDRV_PLATFORM_H

#include "aess_kcsdrv_common.h"

/** Define the KCS_TOTAL_CHANNEL is 0x03. The maximal support channel number.*/
#define KCS_TOTAL_CHANNEL           0x03


/** KCS ioctl command, used to control SIRQCR. SH7757 support only. */
#define AESS_KCSDRV_SIRQCR      _IOWR(AESS_KCSDRV_IOC_MAGIC, 5, int)

enum //SERIRQ_CONTROL
{
    SIRQCR_IRQ1E1 = 0 ,
    SIRQCR_IRQ12E1,
    
    SIRQCR_IRQ6E2 = 8, 
    SIRQCR_IRQ9E2,
    SIRQCR_IRQ10E2,
    SIRQCR_IRQ11E2,
    SIRQCR_IRQ6E3,
    SIRQCR_IRQ9E3,
    SIRQCR_IRQ10E3,
    SIRQCR_IRQ11E3,

    SIRQCR_SELIRQ1  = 24,
    SIRQCR_SELIRQ12 = 25,
    SIRQCR_SELIRQ6  = 27,
    SIRQCR_SELIRQ9  = 28,
    SIRQCR_SELIRQ10 = 29,
    SIRQCR_SELIRQ11 = 30
    
}; 

#define OperationRead 0
#define OperationWrite 1

/******************************************************************************
*   STRUCT      :   sKCSSIRQCRInfo
******************************************************************************/
/**
 *  @brief   KCS SIRQ related parameter definitions from user space.
 *
 *****************************************************************************/ 

typedef struct
{

    /** indicate the sepcific bit. */
    UINT16 u16KCSSIRQCR_Index;

    UINT8 u8Operation;
    
    /** setting data . */
    UINT8 u8Value;
    
} sKCSSIRQCRInfo;


#endif   /* AESSKCSDRV_PLATFORM_H */


/* End of code */
