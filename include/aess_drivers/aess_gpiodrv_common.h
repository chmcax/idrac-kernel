/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     GPIO driver                                                   \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_gpiodrv_common.h
 *  @brief  gpio driver 
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef GPIO_DRV_COMMON_H
#define GPIO_DRV_COMMON_H

#define GPIO_DRIVER_NAME "aess_gpiodrv"

/* ioctl definitions */
/** GPIO ioctl magic number.  */
#define AESS_GPIODRV_IOC_MAGIC    0xB5

/** GPIO ioctl command, used to read data from a GPIO pin.  */
#define AESS_GPIODRV_R           _IOWR(AESS_GPIODRV_IOC_MAGIC, 0, int)
/** GPIO ioctl command, used to write data to a GPIO pin.  */
#define AESS_GPIODRV_W           _IOWR(AESS_GPIODRV_IOC_MAGIC, 1, int)
/** GPIO ioctl command, used to config a GPIO pin.  */
#define AESS_GPIODRV_CFG         _IOWR(AESS_GPIODRV_IOC_MAGIC, 2, int)

/* GPIO command type list                                      */
/** GPIO command function's command, used to config a GPIO pin.  */
#define GPIO_CONFIG    0 
/** GPIO command function's command, used to write data to a GPIO pin.  */
#define GPIO_WRITE     1
/** GPIO command function's command, used to read data from a GPIO pin.  */
#define GPIO_READ      2


/* GPIO_CONFIG command list*/
/** GPIO config command, used to disable interrupt.  */
#define GPIO_CMD_INT_OFF            0x0
/** GPIO config command, used to enable interrupt with rising edge trigger.  */
#define GPIO_CMD_INT_RISING         0x1
/** GPIO config command, used to enable interrupt with falling edge trigger.  */
#define GPIO_CMD_INT_FALLING        0x2
/** GPIO config command, used to enable interrupt with high level trigger.  */
#define GPIO_CMD_INT_HIGH           0x3
/** GPIO config command, used to enable interrupt with low level trigger.  */
#define GPIO_CMD_INT_LOW            0x4
/** GPIO config command, used to enable interrupt with both edge trigger.  */
#define GPIO_CMD_INT_BOTH_EDGE      0x5
/** GPIO config command, used to switch pin to GPIO function.  */
#define GPIO_CMD_PIN_INIT           0x6
/** GPIO config command, used to enable interrupt with both level trigger.  */
#define GPIO_CMD_INT_BOTH_LEVEL      0x7


/* GPIO_WRITE  and GPIO_READ command list */
/** GPIO write and read command, used to write a data to gpio pin or read a data from gpio pin.  */
#define GPIO_CMD_IO_DATA            0x0
/** GPIO read command, used to read pin interrupt event.  */
#define GPIO_CMD_INT_STATUS         0x1
/** GPIO write command, used to clear pin interrupt event.  */
#define GPIO_CMD_INT_CLR            0x1
/** GPIO write and read command, used to enable / disable interrupt or get interrupt status.  */
#define GPIO_CMD_INT_EN             0x2
/** GPIO write and read command, used to change pin direction or get pin direction.  */
#define GPIO_CMD_DIRECTION          0x3
/** GPIO write and read command, used to enable / disable open drain or get open drain status.  */
#define GPIO_CMD_OPEN_DRAIN          0x4
/** GPIO write and read command, used to enable / disable debounce or get debounce status.  */
#define GPIO_CMD_DEBOUNCE           0x5
/** GPIO read command, used to change direction to input and read pin data back.  */
#define GPIO_CMD_EXTREAD            0x6
/** GPIO write command, used to change direction to output and write to pin with init value.  */
#define GPIO_CMD_EXTWRITE           0x6


/** GPIO pin direction definitions, used for input   */
#define DIR_INPUT                   0x0
/** GPIO pin direction definitions, used for output   */
#define DIR_OUTPUT                  0x1

/******************************************************************************
*   STRUCT      :   sGPIOData
******************************************************************************/
/**
 *  @brief   Structure to GPIO driver related data parameter.
 *
 *****************************************************************************/
typedef struct
{
    /** Set read/write command type */
    UINT8 u8CommandType;
    
    /** Set command number */
    UINT8 u8CommandNum;	
    
    /** reserve for future  */
    UINT8 reserve ;       
    
    /** Set GPIO Pin number */
    UINT8 u8PinNum;        
    
    /** Data buffer */
    void *pData;			    

} sGPIOData;



#endif   /* AESSGPIODRV_H */


/* End of code */
