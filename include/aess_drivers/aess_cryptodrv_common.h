/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     Crypto driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_cryptodrv_common.h
 *  @brief  The header file of crypto driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/
 
#ifndef AESSCRYPTODRV_COMMON_H
#define AESSCRYPTODRV_COMMON_H

/** Crypto driver ioctl magic number. */
#define AESS_ENCRPDRV_IOC_MAGIC 0xC2
/** Crypto ioctl command, used for AES encryption. */
#define AESS_CRYPTODRV_ENCRYPT     _IOWR(AESS_ENCRPDRV_IOC_MAGIC, 0, sCryptoInfoType)

/** Crypto ioctl command, used for AES decryption. */
#define AESS_CRYPTODRV_DECRYPT     _IOWR(AESS_ENCRPDRV_IOC_MAGIC, 1, sCryptoInfoType)

/** Crypto driver name. */
#define CRYPTO_DRIVER_NAME                "aess_cryptodrv"

/** AES crypto algorithm. */
#define AES_CRYPTO_ALGORITHM           0x0



/******************************************************************************
*   Enum      :   eCryptoDrvKeySizeType
******************************************************************************/
/**
 *  @brief   Crypto driver key size enumeration type
 *
 *****************************************************************************/
typedef enum
{
	/** 128 bits */
	AES_DRV_128_BITS = 0,
	
	/** Cipher block chaining, CBC mode */
	AES_DRV_192_BITS,
	
	/** Cipher feedback, CFB mode */
	AES_DRV_256_BITS
	
} eCryptoDrvKeySizeType;


/******************************************************************************
*   Enum      :   eCryptoDrvModeType
******************************************************************************/
/**
 *  @brief   AES driver mode enumeration type
 *
 *****************************************************************************/
typedef enum
{
	/** Electronic code book, ECB mode */
	AES_DRV_MODE_ECB = 0,
	
	/** Cipher block chaining, CBC mode */
	AES_DRV_MODE_CBC,
	
	/** Cipher feedback, CFB mode */
	AES_DRV_MODE_CFB,
	
	/** Output feedback, OFB mode */
	AES_DRV_MODE_OFB,
	
	/** Counter, CTR mode */
	AES_DRV_MODE_CTR,
	
} eCryptoDrvModeType;


/******************************************************************************
*   STRUCT      :    sCryptoInfoType
******************************************************************************/
/**
 *  @brief   Structure to crypto driver related data parameter.
 *
 *****************************************************************************/
typedef struct 
{
    /** Initial key */
    UINT8 *pu8InitKey;
    
    /** Initial vector*/
    UINT8 *pu8InitVector;
    
    /** Pointer to the source buffer */
    UINT8 *pu8Input;
    
    /** Pointer to the target buffer */
    UINT8 *pu8Output;
    
    /** Data size option */
    UINT32 u32DataLen;
    
    /** Key size option */
    UINT16 u16KeySize;

    /** Mode option */
    UINT8 u8Mode;
    
    /** Crypto algorithm */
    UINT8 u8CryptoAlg;
    
} sCryptoInfoType;


#endif   /* AESSCRYPTODRV_COMMON_H */

/* End of code */
