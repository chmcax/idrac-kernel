/**
 *<center>
 * Avocent Corporation. Proprietary Information.
 * \n<em>
 *      This software is supplied under the terms of a license agreement or
 *      nondisclosure agreement with Avocent Corporation, or its subsidiary, and
 *      may not be copied, disseminated, or distributed except in accordance
 *      with the terms of that agreement.
 *
 *      2001 Gateway Place, Suite 520W, San Jose, California, 95110 U.S.A.
 *\n
 *                  US phone: 408.436.6333
 *
 *        Copyright &copy; 2008 Avocent Corporation.
 *</em> </center>
 *----------------------------------------------------------------------------\n
 *  MODULES     memory driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_memdrv_common.h
 *  @brief  The header file of memory device driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef SHAREDMEMDRV_COMMON_H
#define SHAREDMEMDRV_COMMON_H

/* ioctl definitions */
#define SHMEM_MEMDRV_IOC_MAGIC    0xBC  /**< IO magic number of memory driver. */
#define SHMEM_MEMDRV_READ         _IOWR(SHMEM_MEMDRV_IOC_MAGIC, 0, int) /**< I/O control command: read memory. */
#define SHMEM_MEMDRV_WRITE        _IOWR(SHMEM_MEMDRV_IOC_MAGIC, 1, int) /**< I/O control command: write memory. */
#define SHMEM_MEMDRV_REQUEST      _IOWR(SHMEM_MEMDRV_IOC_MAGIC, 2, int) /**< I/O control command: request a memory region. */
#define SHMEM_MEMDRV_RELEASE      _IOWR(SHMEM_MEMDRV_IOC_MAGIC, 3, int) /**< I/O control command: release memory region. */
#define SHMEM_MEMDRV_PID         _IOWR(SHMEM_MEMDRV_IOC_MAGIC, 4, int) /**< I/O control command: release memory region. */
#define SHMEM_CPLD_RW         _IOWR(SHMEM_MEMDRV_IOC_MAGIC, 5, int) /**< I/O control command: release memory region. */

#define MEM_DRV_MAX_REGION_NUM      10 /**< Define the maximum region numbers. */


char *driver_name="shared_memdrv"; 
#define DRIVER_NAME "shared_memdrv"  /**< Define memory driver name. */

/******************************************************************************
*   Enum      :   eMemDrvDataWidthType
******************************************************************************/
/**
 *  @brief   memory driver data width enumeration type
 *
 *****************************************************************************/
typedef enum
{
	/** Byte */
	MEM_DRV_BYTE = 0,
	
	/** Word */
	MEM_DRV_WORD,
	
	/** Double word */
	MEM_DRV_DWORD
	
} eMemDrvDataWidthType;



/******************************************************************************
*   STRUCT      :   sMemDrvInfoType
******************************************************************************/
/**
 *  @brief   Structure to memory driver related data parameter.
 *
 *****************************************************************************/
typedef struct
{
	/** physical base address of request region */
	UINT32 u32BaseAddr;
    
	/** read/write data buffer pointer */
	void *pDataPtr;
    
	/** size of request region in bytes */
	UINT16 u16RegionSize;
    
	/** offset of base address in bytes */
	UINT16 u16Offset;
    
	/** data size to read/write */
	UINT16 u16DataSize;
    
	/** read/write data width */
	UINT8 u8DataWidth;
	
	/** identifier */
	UINT8 u8ID;
	
} sMemDrvInfoType;

#endif   /* AESSMEMDRV_COMMON_H */

/* End of code */
