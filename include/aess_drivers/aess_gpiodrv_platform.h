/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     GPIO driver                                                   \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_gpiodrv_platform.h
 *  @brief  gpio driver 
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef GPIO_DRV_PLATFORM_H
#define GPIO_DRV_PLATFORM_H

#include "aess_gpiodrv_common.h"

/** Define the GPIO_PIN_MASK is 0xFF. */
#define GPIO_PIN_MASK           0xFF 
 
/** Define the GPIO_PIN_MAX is (208). */
#define GPIO_PIN_MAX            (208)

#endif   /* GPIO_DRV_PLATFORM_H */


/* End of code */
