/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     I2C driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_i2cdrv_platform.h
 *  @brief  The header file of I2C device driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#if !defined (AESSI2CDRV_PLATFORM_H)
#define AESSI2CDRV_PLATFORM_H

#define I2C_DRV_MAX_BUS             10                 /**< Define maximum I2C channel number. */
#define I2C_DRV_MAX_PI2C_BUS        I2C_DRV_MAX_BUS   /**< Define maximum private I2C channel number. */
#define I2C_DRV_MAX_IPMB_BUS        I2C_DRV_MAX_BUS   /**< Define maximum IPMB I2C channel number. */

#include "aess_i2cdrv_common.h"

#endif   /* AESSI2CDRV_PLATFORM_H */

/* End of code */
