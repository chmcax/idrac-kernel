/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     PECI driver\n
 *----------------------------------------------------------------------------\n
 *  @file   aess_pecisensordrv_platform.h
 *  @brief  Header file of On chip peci sensor driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/
#include <asm/ioctl.h>

#if !defined (AESSPECISENSORDRV_PLATFORM_H)
#define AESSPECISENSORDRV_PLATFORM_H

#include "aess_pecisensordrv_common.h"
 

#endif   /* AESSPECISENSORDRV_PLATFORM_H */
/* End of code */
