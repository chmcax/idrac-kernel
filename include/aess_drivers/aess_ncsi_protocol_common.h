/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     NC-SI passthrough library. 
 *----------------------------------------------------------------------------\n
 *  @file   aess_ncsi_protocol_common.h
 *  @brief  The header file of NC-SI driver and passthrough library.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#if !defined (AESSNCSIPROTOCOL_COMMON_H)
#define AESSNCSIPROTOCOL_COMMON_H


/******************************************************************************
*   STRUCT      :   sNCSI_NCSI_STATISTICS
******************************************************************************/
/**
 *  @brief   NCSI Get Statistics Information
 *
 *****************************************************************************/
struct sNCSI_NCSI_STATISTICS
{
    /** NC-SI Commands Received. */
    u32 commands_received;
    /** NC-SI Control Packets Dropped. */
    u32 control_packets_dropped;
    /** NC-SI Command Type Errors. */
    u32 command_type_errors;
    /** NC-SI Command Checksum Errors. */
    u32 command_checksum_errors;
    /** NC-SI Receive Packets. */
    u32 receive_packets;
    /** NC-SI Transmit Packets. */
    u32 transmit_packets;
    /** AENs Sent. */
    u32 aens_sent;
    /** Response code. */
    u16 response_code;
    /** Reason code. */
    u16 reason_code;
};

/******************************************************************************
*   STRUCT      :   sNCSILIB_NCSI_STATISTICS
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to get NC-SI statistics from driver.
 *
 *****************************************************************************/
struct sNCSILIB_NCSI_STATISTICS
{
    struct sNCSI_NCSI_STATISTICS ncsi_statistics;
    int retries;
    int timeout;
};

/******************************************************************************
*   STRUCT      :   sNCSI_CONTROL_PACKET_STATISTICS  
******************************************************************************/
/**
 *  @brief   Get controller packet statistics command
 *
 *****************************************************************************/
struct sNCSI_CONTROL_PACKET_STATISTICS  
{
    /** Counters cleared from last read (MS Bits).*/
    u32 counters_cleared_from_last_read_ms;
    /** Counters cleared from last read (LS Bits).*/
    u32 counters_cleared_from_last_read_ls;
    /** Total bytes received.*/
    u64 total_bytes_received;
    /** Total bytes transmitted.*/
    u64 total_bytes_transmitted;
    /** Total unicast packets received.*/
    u64 total_unicast_packets_received;
    /** Total multicast packets received.*/
    u64 total_multicast_packets_received;
    /** Total broadcast packets received.*/
    u64 total_broadcast_packets_received;
    /** Total unicast packets transmitted.*/
    u64 total_unicast_packets_transmitted;
    /** Total multicast packets transmitted.*/
    u64 total_multicast_packets_transmitted;
    /** Total broadcast packets transmitted.*/
    u64 total_broadcast_packets_transmitted;
    /** FCS receive errors.*/
    u32 fcs_receive_errors;
    /** Alignment errors.*/
    u32 alignment_errors;
    /** False carrier detections.*/
    u32 false_carrier_detections;
    /** Runt packets received.*/
    u32 runt_packets_received;
    /** Jabber packets received.*/
    u32 jabber_packets_received;
    /** Pause XON frames received.*/
    u32 pause_xon_frames_received;
    /** Pause XOFF frames received.*/
    u32 pause_xoff_frames_received;
    /** Pause XON frames transmitted.*/
    u32 pause_xon_frames_transmitted;
    /** Pause XOFF frames_transmitted.*/
    u32 pause_xoff_frames_transmitted;
    /** Single collision transmit frames.*/
    u32 single_collision_transmit_frames;
    /** Multiple collision transmit frames.*/
    u32 multiple_collision_transmit_frames;
    /** Late collision frames.*/
    u32 late_collision_frames;
    /** Excessive collision frames.*/
    u32 excessive_collision_frames;
    /** Control frames received.*/
    u32 control_frames_received;
    /** 64-Byte frames received.*/
    u32 byte_64_frames_received;
    /** 65-127 Byte frames received.*/
    u32 byte_65_127_frames_received;
    /** 128-255 Byte frames received.*/
    u32 byte_128_255_frames_received;
    /** 256-511 Byte frames received.*/
    u32 byte_256_511_frames_received;
    /** 512-1023 Byte frames received.*/
    u32 byte_512_1023_frames_received;
    /** 1024-1522 Byte frames received.*/
    u32 byte_1024_1522_frames_received;
    /** 1523-9022 Byte frames received.*/
    u32 byte_1523_9022_frames_received;
    /** 64-Byte frames transmitted.*/
    u32 byte_64_frames_transmitted;
    /** 65-127 Byte frames transmitted.*/
    u32 byte_65_127_frames_transmitted;
    /** 128-255 Byte frames transmitted.*/
    u32 byte_128_255_frames_transmitted;
    /** 256-511 Byte frames transmitted.*/
    u32 byte_256_511_frames_transmitted;
    /** 512-1023 Byte frames transmitted.*/
    u32 byte_512_1023_frames_transmitted;
    /** 1024-1522 Byte frames transmitted.*/
    u32 byte_1024_1522_frames_transmitted;
    /** 1523-9022 Byte frames transmitted.*/
    u64 byte_1523_9022_frames_transmitted;
    /** Valid bytes received.*/
    u64 valid_bytes_received;
    /** Error runt packets received.*/
    u32 error_runt_packets_received;
    /** Error jabber packets received.*/
    u32 error_jabber_packets_received;
    /** Response code. */
    u16 response_code;
    /** Reason code. */
    u16 reason_code;
};

/******************************************************************************
*   STRUCT      :   sNCSILIB_NCSI_STATISTICS
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to get controller packet statistics command from driver.
 *
 *****************************************************************************/
struct sNCSILIB_CONTROL_PACKET_STATISTICS  
{
    struct sNCSI_CONTROL_PACKET_STATISTICS ncsi_control_packet_statistics;
    int retries;
    int timeout;
};

/******************************************************************************
*   STRUCT      :   sNCSI_Link_Status
******************************************************************************/
/**
 *  @brief   NCSI device link status information
 *
 *****************************************************************************/
struct sNCSI_Link_Status
{ 
    /** Link Flag */
    u8 link_flag;
    /** Speed and Duplex */
    u8 speed_and_duplex;
    /** Auto Negotiation is enabled */
    u8 auto_negotiate_flag;
    /** Auto Negotiation has completed */
    u8 auto_negotiate_complete;
    /** Parallel Detection Flag  */
    u8 parallel_detection_flag;
    /** Link Partner Advertised Speed and Duplex 1000TFD */
    u8 link_partner_advertised_1000TFD;
    /** Link Partner Advertised Speed and Duplex 1000THD */
    u8 link_partner_advertised_1000THD;
    /** Link Partner Advertised Speed and Duplex 100T4 */
    u8 link_partner_advertised_100T4;
    /** Link Partner Advertised Speed and Duplex 100TXFD */
    u8 link_partner_advertised_100TXFD;
    /** Link Partner Advertised Speed and Duplex 100TXHD */
    u8 link_partner_advertised_100TXHD;
    /** Link Partner Advertised Speed and Duplex 10TFD */
    u8 link_partner_advertised_10TFD;
    /** Link Partner Advertised Speed and Duplex 10THD */
    u8 link_partner_advertised_10THD;
    /** Tx Flow Control Flag */
    u8 tx_flow_control_flag;
    /** Rx Flow Control Flag */
    u8 rx_flow_control_flag;
    /** Link Partner Advertised Flow Control */
    u8 link_partner_advertised_flow_control;
    /** SerDes Link Status */
    u8 serdes_link;
    /** OEM link speed valid */
    u8 oem_link_speed_valid;
    /** reserved */
    u8 reserved1;
    /** reserved */
    u8 reserved2;
    /** reserved */
    u8 reserved3;
    /** Response code. */
    u16 response_code;
    /** Reason code. */
    u16 reason_code;
};

/******************************************************************************
*   STRUCT      :   sNCSILIB_Link_Status
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to get link status from driver.
 *
 *****************************************************************************/
struct sNCSILIB_Link_Status
{
    struct sNCSI_Link_Status ncsi_link_status;
    int retries;
    int timeout;
};


/******************************************************************************
*   STRUCT      :   sNCSI_CHNL_CAPABILITIES
******************************************************************************/
/**
 *  @brief   NCSI Channel Capabilities Information
 *
 *****************************************************************************/
struct sNCSI_CHNL_CAPABILITIES 
{
    /** capabilities flag */
    u32 capabilities_flags;
    /** broadcast packet filter capabilities */
    u32 bcast_filter_flag;
    /** multicast packet filter capabilities */
    u32 mcast_filter_flag;
    /** buffering capabilities */
    u32 buf_cap;
    /** aen control support field */
    u32 aen_ctrl;
    /** VLAN filter count */
    u8  vlan_filter_cnt;
    /** Mixed filter count */
    u8  mixed_filter_cnt;
    /** Multicast filter count */
    u8  mcast_filter_cnt;
    /** Unicast filter count */
    u8  ucast_filter_cnt;
    /** reserved */
    u16 reserved2;
    /** vlan mode support field */
    u8  vlan_mode;
    /**  Channel count field */
    u8  chnl_cnt;
    /** Response code. */
    u16 response_code;
    /** Reason code. */
    u16 reason_code;
};


/******************************************************************************
*   STRUCT      :   sNCSILIB_CHNL_CAPABILITIES
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to get channel capabilities from driver.
 *
 *****************************************************************************/
struct sNCSILIB_CHNL_CAPABILITIES
{
    struct sNCSI_CHNL_CAPABILITIES ncsi_channel_capabilities;
    int retries;
    int timeout;
};


/******************************************************************************
*   STRUCT      :   sNCSI_VERSION
******************************************************************************/
/**
 *  @brief   NCSI Version definition
 *
 *****************************************************************************/
struct sNCSI_VERSION 
{
    /** version number of the NCSI spec. */
    u8 ncsi_version[4];
    /** reserve and alpha byte */
    u32 reserved_alpha2;
    /** Firmware name string */
    u8 fw_name[12];
    /** firmware version number */
    u32 fw_version;
    /** PCI ID */
    u16 pci_did;
    /** PCI ID */
    u16 pci_vid;
    /** PCI ID */
    u16 pci_ssid;
    /** PCI ID */
    u16 pci_svid;
    /** Manufacturer ID */
    u32 iana;
    /** Response code. */
    u16 response_code;
    /** Reason code. */
    u16 reason_code;
};

/******************************************************************************
*   STRUCT      :   sNCSILIB_VERSION
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to get controller-related version information from driver.
 *
 *****************************************************************************/
struct sNCSILIB_VERSION
{
    struct sNCSI_VERSION ncsi_version;
    int retries;
    int timeout;
};

/******************************************************************************
*   STRUCT      :   sNCSI_PASSTHROUGH_STATISTICS
******************************************************************************/
/**
 *  @brief   NCSI Get NC-SI Pass-through Statistics
 *
 *****************************************************************************/
struct sNCSI_PASSTHROUGH_STATISTICS
{
    /** Pass-through TX Packets Received on NC-SI RX Interface. (Management Controller to Network Controller). */
    u64 pt_tx_pkts_rcved_on_ncsi_rx_interface;
    /** Pass-through TX Packets Dropped. */
    u32 pt_tx_pkts_dropped;
    /** Pass-throught TX Packet Channel State Errors. */
    u32 pt_tx_pkt_chn_state_errors;
    /** Pass-throught TX Packet Undersized Errors. */
    u32 pt_tx_pkt_undersized_errors;
    /** Pass-through TX Packet Oversized Errors. */
    u32 pt_tx_pkt_oversized_errors;
    /** Pass-throught RX Packets Received on LAN Interface. */
    u32 pt_rx_pkts_rcved_on_lan_interface;
    /** Total Pass-through RX Packets Dropped. */
    u32 total_pt_rx_pkts_dropped;
    /** Pass-through RX Packet Channel State Errors. */
    u32 pt_rx_pkt_chn_state_errors;
    /** Pass-through RX Packet Undersized Errors. */
    u32 pt_rx_pkt_undersized_errors;
    /** Pass-through RX Packet Oversized Errors. */
    u32 pt_rx_pkt_oversized_errors;
    /** Response code. */
    u16 response_code;
    /** Reason code. */
    u16 reason_code;
};

/******************************************************************************
*   STRUCT      :   sNCSI_PASSTHROUGH_STATISTICS
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to get NC-SI passthrough packet statistics from driver.
 *
 *****************************************************************************/
struct sNCSILIB_PASSTHROUGH_STATISTICS
{
    struct sNCSI_PASSTHROUGH_STATISTICS ncsi_passthrough_statistics;
    int retries;
    int timeout;
};

/******************************************************************************
*   STRUCT      :   sNCSILIB_OEM_COMMAND
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to send OEM command.
 *
 *****************************************************************************/
struct sNCSILIB_OEM_COMMAND
{
    void *inputData;
    int   inputDataSize;
    void *outputData;
    int  *outputDataSize;
    int   retries;
    int   timeout;
};

/******************************************************************************
*   STRUCT      :   sNCSILIB_SEND_MESSAGE
******************************************************************************/
/**
 *  @brief   For NC-SI passthrough library to send any command.
 *
 *****************************************************************************/
struct sNCSILIB_SEND_MESSAGE
{
    void *inputData;
    int   inputDataSize;
    void *outputData;
    int  *outputDataSize;
    int   retries;
    int   timeout;
};

#define GET_VLAN_CONFIG_CMD                  (SIOCDEVPRIVATE)   /** I/O control command: Get VLAN configuration. */
#define SET_VLAN_CONFIG_CMD                  (SIOCDEVPRIVATE+1) /** I/O control command: Set VLAN configuration. */
#define SET_VLAN_ENABLED_CMD                 (SIOCDEVPRIVATE+2) /** I/O control command: Enabled VLAN. */
#define SET_VLAN_DISABLED_CMD                (SIOCDEVPRIVATE+3) /** I/O control command: Disabled VLAN. */
#define SENDOEM50_CMD                        (SIOCDEVPRIVATE+4) /** I/O control command: Send OEM command. */
#define GET_STATISTICS_CMD                   (SIOCDEVPRIVATE+5) /** I/O control command: Get NC-SI Statistics. */
#define GET_LINK_STATUS_CMD                  (SIOCDEVPRIVATE+6) /** I/O control command: Get link status. */
#define GET_CONTROLLER_PACKET_STATISTICS_CMD (SIOCDEVPRIVATE+7) /** I/O control command: Get controller packet statistics.*/
#define GET_PASSTHROUGH_STATISTICS_CMD       (SIOCDEVPRIVATE+8) /** I/O control command: Get NC-SI Pass-through statistics. */
#define GET_VERSION_ID_CMD                   (SIOCDEVPRIVATE+9) /** I/O control command: Get version ID. */
#define GET_CAPABILITIES_CMD                 (SIOCDEVPRIVATE+10)/** I/O control command: Get capabilities. */
#define SEND_COMMAND                         (SIOCDEVPRIVATE+11)/** I/O control command: Send any commands. */
#define GET_NCSI_DRIVER_VERSION_CMD          (SIOCDEVPRIVATE+12)/** I/O control command: Get NC-SI driver version. */

#define NCSI_MAX_VERSION_SIZE (100)

#define STATUS_OK               0 
#define STATUS_FAIL             1
#define STATUS_BUSY             2
#endif
