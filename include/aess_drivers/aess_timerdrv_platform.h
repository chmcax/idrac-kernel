/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     Timer Driver                                            \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_timerdrv_platform.h
 *  @brief  Timer driver
 *  @internal
 *----------------------------------------------------------------------------*/
#if !defined (AESSTIMERDRV_PLATFORM_H)
#define AESSTIMERDRV_PLATFORM_H

#include "aess_timerdrv_common.h"


#define MAX_TIMER_NUMBER      3

#define TMR_IRQ_0    64
#define TMR_IRQ_1    65
#define TMR_IRQ_2    66


/* Timer Clock Source Type */
// if Pck = 48MHz
enum
{
    TIMER_PCK_DIV_8192 = 0 ,
    TIMER_PCK_DIV_1024     ,
    TIMER_PCK_DIV_64       ,
    TIMER_PCK_DIV_32       ,
    TIMER_PCK_DIV_8        ,
    TIMER_PCK_DIV_2        ,
    TIMER_PCK_UNKNOWN  = -1
};

/* Timer resource definition */
#define TIMER_RS0               0x0
#define TIMER_RS1               0x1
#define TIMER_RS2               0x2

#endif   /* AESSTIMERDRV_PLATFORM_H */


/* End of code */
