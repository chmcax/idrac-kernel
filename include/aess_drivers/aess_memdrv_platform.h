/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     memory driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_memdrv_platform.h
 *  @brief  The header file of memory device driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef AESSMEMDRV_PLATFORM_H
#define AESSMEMDRV_PLATFORM_H

#include "aess_memdrv_common.h"

#endif   /* AESSMEMDRV_PLATFORM_H */

/* End of code */
