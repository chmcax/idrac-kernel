/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     KCS driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_kcsdrv_common.h
 *  @brief  Header file for KCS state machine part.
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef KCS_STATE_MACHINE_COMMON_H
#define KCS_STATE_MACHINE_COMMON_H

/** KCS ioctl magic number. */
#define AESS_KCSDRV_IOC_MAGIC    0xBA
/** KCS ioctl command, used to initialized. */
#define AESS_KCSDRV_INIT         _IOWR(AESS_KCSDRV_IOC_MAGIC, 0, int)
/** KCS ioctl command, used to read messages. */
#define AESS_KCSDRV_READ         _IOWR(AESS_KCSDRV_IOC_MAGIC, 1, int)
/** KCS ioctl command, used to write messages. */
#define AESS_KCSDRV_WRITE        _IOWR(AESS_KCSDRV_IOC_MAGIC, 2, int)
/** KCS ioctl command, used to software SMI, not supported currently. */
#define AESS_KCSDRV_SWSMI        _IOWR(AESS_KCSDRV_IOC_MAGIC, 3, int)
/** KCS ioctl command, used to set callback ID. */
#define AESS_KCSDRV_SETCBID      _IOWR(AESS_KCSDRV_IOC_MAGIC, 4, int)

/** KCS driver name definition. */
#define KCS_DRIVER_NAME "aess_kcsdrv"

/** KCS system attention bits definition. */
#define SMS_ATN_SET         1

/** KCS interface package and structure definition. */
//#define KCS_PACKAGE_MAX             0xFC
#define KCS_PACKAGE_MAX             0xCB  // JIT-23417: IPMI application defines max 0xC8 data bytes + 1 (NetFn) +1 (Cmd) +1 (KCS Seq Num) = 0xCB

/** KCS maximum hook function number. */
#define HIF_MAX_HOOK_FUNC_NUM 4

/** LPC reset hook function API pointer. */
typedef void (* HIFLRSTHookAPIPtr) (void);

#define KCS_NOT_INIT               1
#define KCS_INIT_OK                0

#define KCS_CH_INIT                1


/******************************************************************************
*   STRUCT      :   sKCSInfo
******************************************************************************/
/**
 *  @brief   KCS parameter definitions from user space.
 *
 *****************************************************************************/ 
typedef struct
{

    /** Receive complete event, deprecated. */
    UINT32 u32KCSRxOKEvent;
    
    /** Transmit done event, deprecated. */
    UINT32 u32KCSTxOKEvent;
    
    /** Transmit fail event, deprecated. */
    UINT32 u32KCSTxFailEvent;

    /** Callback Function event ID. */
    UINT32 u32CBFunEventID;

    /** Read data length. */
    UINT8 *pu8ReadLength;    
    
    /** Pointer to a data buffer, IPMI command length is only 40 bytes. */
    UINT8 *pu8Data;

    /** KCS base address. */
    UINT16 u16BaseAddress;

    /** Event group driver ID. */
    UINT16  u16DriverID;

    /** Callback Function event group driver ID. */
    UINT16 u16CBFunDriverID;

    /** Reserved. */
    UINT16 u16Reserve;

    /** KCS channel number. */
    UINT8 u8Channel;        
    
    /** Set for SMS_ATTEN Bit. */
    UINT8 u8Control;
  
    /** Write data length. */
    UINT8 u8WriteLength;
    
    /** Reserved. */
    UINT8 u8Reserve;
    
} sKCSInfo;


#endif   /* KCS_STATE_MACHINE_COMMON_H */


/* End of code */
