/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     Dynamic IRQ Driver                                            \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_dynairqdrv_platform.h
 *  @brief  Dynamic IRQ driver
 *  @internal
 *----------------------------------------------------------------------------*/
#if !defined (AESSDYNAIRQDRV_PLATFORM_H)
#define AESSDYNAIRQDRV_PLATFORM_H

#include "aess_dynairqdrv_common.h"


#endif   /* AESSDYNAIRQDRV_PLATFORM_H */


/* End of code */
