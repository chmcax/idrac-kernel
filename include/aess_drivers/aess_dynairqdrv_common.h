/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     Dynamic IRQ driver                                            \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_dynairqdrv_common.h
 *  @brief  Dynamic IRQ driver
 *  @internal
 *----------------------------------------------------------------------------*/
#if !defined (AESSDYNAIRQDRV_COMMON_H)
#define AESSDYNAIRQDRV_COMMON_H


#ifndef AESSDYNAIRQDRV_H

#define NAME_GENERIC_ISR "AESS_GENERIC_ISR"

enum INT_TRIGGER_INFO {
	INTT_RISING_EDGE = 0,   /* No use in vitesse */
	INTT_FALLING_EDGE,
	INTT_HIGH_LEVEL,
	INTT_LOW_LEVEL ,
	INTT_BOTH_EDGE ,
	INTT_BOTH_LEVEL,
	INTT_UNASSIGNED   // DELL: must be last!!
};
#endif //AESSDYNAIRQDRV_H

/* ioctl definitions */
/** Dynamic IRQ ioctl magic number.  */
#define AESS_IRQDRV_IOC_MAGIC		0xB9

/** Dynamic IRQ ioctl command, used to init driver parameters.  */
#define AESS_IOCTL_DRIVER_INIT      _IOWR(AESS_IRQDRV_IOC_MAGIC, 0, int)
/** Dynamic IRQ ioctl command, used to init hardware for I/O Table setting.  */
#define AESS_IOCTL_DYNAIRQ_INIT     _IOWR(AESS_IRQDRV_IOC_MAGIC, 1, int)
/** Dynamic IRQ ioctl command, used to config IRQ setting.  */
#define AESS_IOCTL_DYNAIRQ_CONFIG   _IOWR(AESS_IRQDRV_IOC_MAGIC, 2, int)
/** Dynamic IRQ ioctl command, used to clear interrupt event.  */
#define AESS_IOCTL_DYNAIRQ_CLEAR    _IOWR(AESS_IRQDRV_IOC_MAGIC, 3, int)
/** Dynamic IRQ ioctl command, used to pass EventHandler driver ID to driver.  */
#define AESS_IOCTL_GENEISR_INIT     _IOWR(AESS_IRQDRV_IOC_MAGIC, 4, int)
/** Dynamic IRQ ioctl command, used to pass user IRQ register information to driver.  */
#define AESS_IOCTL_USERMODE_ISRID   _IOWR(AESS_IRQDRV_IOC_MAGIC, 5, int)


/** Dynamic IRQ driver name.  */
#define DYNAMICIRQ_DRIVER_NAME "aess_dynairqdrv"

/******************************************************************************
*   STRUCT       :   sIRQInfoType
******************************************************************************/
/**
 *  @brief   Dynamic IRQ Paramters Structure
 *
 *****************************************************************************/
typedef struct
{
    
    union
    /** Parameter2 */
    {
        
        /** data */
        ushort data;

        /** IRQ number */
        ushort u16IRQNum;
    }param1;
    
    /** Parameter2 */
    union
    {
        /** data */
        ushort data;

        /** Reserved */
        ushort u16Reserved;
    }param2;

    /** Parameter3 */
    UINT32 param3;

    /** ISR name */
    char *pISRName;

} sIRQInfoType;

/******************************************************************************
*   STRUCT       :   sUserModeRecordID
******************************************************************************/
/**
 *  @brief   sUserModeRecordID
 *
 *****************************************************************************/
typedef struct
{
    /** event id */    
    UINT32 u32EventID;
    
    /** irq number */    
    UINT16 u16NumIRQ;
    
    /** reserve */    
    UINT16 reserve16;
    
} sUserModeRecordID;


#endif   /* AESSDYNAIRQDRV_COMMON_H */


/* End of code */
