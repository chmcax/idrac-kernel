/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     Crypto driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_cryptodrv_platform.h
 *  @brief  The header file of crypto driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/
 
#ifndef AESSCRYPTODRV_PLATFORM_H
#define AESSCRYPTODRV_PLATFORM_H

/** Crypto ioctl command, used for RC4 encryption/decryption. */
#define AESS_CRYPTODRV_CRYPT       _IOWR(AESS_ENCRPDRV_IOC_MAGIC, 2, sCryptoInfoType)

/** RC4 crypto algorithm */  
#define RC4_CRYPTO_ALGORITHM           0x1

#include "aess_cryptodrv_common.h"

#endif   /* AESSCRYPTODRV_PLATFORM_H */

/* End of code */
