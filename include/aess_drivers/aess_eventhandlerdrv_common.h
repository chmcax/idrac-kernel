/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     Event handler driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_eventhandlerdrv_common.h
 *  @brief  This is the kernel-layer event handler
 *
 *  @internal
 *----------------------------------------------------------------------------*/
#ifndef AESSEVENTHANDLER_COMMON_H
#define AESSEVENTHANDLER_COMMON_H

/** Event handler driver name. */
#define EVENTHANDLER_DRIVER_NAME "aess_eventhandlerdrv"
#define INIT_OK 1
#define INIT_FAIL 0

/******************************************************************************
*   STRUCT      :   sEventData
******************************************************************************/
/**
 *  @brief   Structure of event handler driver related data parameter.
 *
 *****************************************************************************/
typedef struct
{
	/** Driver ID. */
	UINT16 u16DriverID;		
	
	/** Event command ID. */
	UINT32 u32EventID;     
	
} sEventData;


#endif   /* AESSEVENTHANDLER_COMMON_H */

/* End of code */
