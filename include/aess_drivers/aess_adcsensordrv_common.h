/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     ADC linux driver. \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_adcsensordrv_common.h
 *  @brief  This is the header file for ON Chip ADC sensor driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef AESSADCSENSORDRV_COMMON_H
#define AESSADCSENSORDRV_COMMON_H

#define ADC_DRIVER_NAME "aess_adcsensordrv"  /**< Name of device node of adc sensor driver. */
/* ioctl definitions */
#define AESS_ADCDRV_IOC_MAGIC       0xBB    /**< I/O magic number of adc driver. */
#define AESS_ADCDRV_R               _IOR(AESS_ADCDRV_IOC_MAGIC, 0, int)  /**< I/O control command: Read value from ADC. */
#define AESS_TEMPDRV_R              _IOR(AESS_ADCDRV_IOC_MAGIC, 1, int)  /**< I/O control command: Read temperature reading from ADC sensor. */


/******************************************************************************
*   STRUCT      :   sADCSensorData
******************************************************************************/
/**
 *  @brief   Structure used by application to pass data parameter to adc sensor driver.
 *
 *****************************************************************************/
typedef struct
{
    /** ADC reading value will be stored in this field. */
    UINT32 u32ADCReading;     

    /** ADC channel number that application want to read. */
    UINT8  u8ADCChannelNum;     

    /** Reserved. */
    UINT8  u8RSV[3];
} sADCSensorData;


#endif   /* AESSADCSENSORDRV_COMMON_H */
/* End of code */
