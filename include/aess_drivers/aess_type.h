/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     AESS TYPE DEFINITION. \n
 *----------------------------------------------------------------------------\n
 *  @file   aess_type.h
 *  @brief  This is the header file for AESS specific type definition.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef AESSTYPE_H
#define AESSTYPE_H
#ifndef SYS_HOST_SH // this is to avoid IPMI build error and these definitions are not nesessary for IPMI.
typedef unsigned char UINT8;
typedef unsigned short UINT16;
typedef unsigned long int   UINT32; /**< type redefine UINT32 */
typedef unsigned long long UINT64;
typedef signed char SINT8;
typedef signed short SINT16;
typedef signed int SINT32;
typedef signed long long SINT64;

typedef char                INT8;   /**< type redefine INT8  */
typedef short int           INT16;  /**< type redefine INT16 */
typedef long int            INT32;  /**< type redefine INT32 */

#define    hw_char         *(volatile unsigned char *)
#define    hw_short        *(volatile unsigned short *)

#ifndef STATUS_OK
#define STATUS_OK           0
#endif
#ifndef STATUS_FAIL
#define STATUS_FAIL         1
#endif
#endif   /* SYS_HOST_SH */
#endif   /* AESSTYPE_H */
/* End of code */
