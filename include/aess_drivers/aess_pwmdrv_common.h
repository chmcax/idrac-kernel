/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES    PWM driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_pwmdrv_common.h
 *  @brief  The header file of PWM driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/
#if !defined (AESSPWMDRV_COMMON_H)
#define AESSPWMDRV_COMMON_H

/** PWM ioctl magic number.  */
#define AESS_PWMDRV_IOC_MAGIC           0xBE

/** PWM ioctl command, used to initialize channel.  */
#define AESS_PWM_CONFIG_INIT             _IOWR(AESS_PWMDRV_IOC_MAGIC, 0, sPWMDevConfig)
/** PWM ioctl command, used to set duty cycle.  */
#define AESS_PWM_CONFIG_SET             _IOWR(AESS_PWMDRV_IOC_MAGIC, 1, sPWMDevConfig)
/** PWM ioctl command, used to print register information via sysfs.  */
#define AESS_PWM_CONFIG_INFO             _IOWR(AESS_PWMDRV_IOC_MAGIC, 2, sPWMDevConfig)
/** PWM ioctl command, used to set inverter.  */
#define AESS_PWM_SET_INVERTER           _IOWR(AESS_PWMDRV_IOC_MAGIC, 3, sPWMDevConfig)
/** PWM ioctl command, used to clear inverter.  */
#define AESS_PWM_CLEAR_INVERTER         _IOWR(AESS_PWMDRV_IOC_MAGIC, 4, sPWMDevConfig)
/** support test program */
#define AESS_PWM_CONFIG_TEST             _IOWR(AESS_PWMDRV_IOC_MAGIC, 5, sPWMDevConfig)


/** PWM driver name.  */
#define PWM_DRIVER_NAME                     "aess_pwmdrv"

/******************************************************************************
*   STRUCT      :   sPWMDevConfig
******************************************************************************/
/**
 *  @brief   Structure to PWM driver config.
 *
 *****************************************************************************/
typedef struct
{    
	/** PWM Channel number */
	UINT8 u8PWMChannelNum;
	
	/** PWM Base Cycle Frequency */
	UINT8 u8PWMBaseCycleFrequency;
	
	/** PWM Frequency Divider */
	UINT8 u8PWMFrequencyDivider; 

	/** PWM Duty Cycle */
	UINT8 u8PWMDutyCycle; 
	
} sPWMDevConfig;

#endif   /* AESSPWMDRV_COMMON_H */

/* End of code */
