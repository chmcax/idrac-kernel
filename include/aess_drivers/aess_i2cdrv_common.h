/*
* 
* Copyright (C) 2009,2010 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU 
* General Public License Version 2. This program is distributed in the hope 
* that it will be useful, but WITHOUT ANY WARRANTY; without even 
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     I2C driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_i2cdrv_common.h
 *  @brief  The header file of I2C device driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#if !defined (AESSI2CDRV_COMMON_H)
#define AESSI2CDRV_COMMON_H

#define I2C_DRV_MAX_MSG_SIZE        0xFFFF  /**< Max size of internal I2C buffer. */
#define I2C_DRV_IPMB_MSG_MIN        7

/* definitions for I2C Bus Speed Fequency */
#define I2C_DRV_FREQ_100            0                  /**< Define of frequency 100Khz. */
#define I2C_DRV_FREQ_400            1                  /**< Define of frequency 400Khz. */
#define I2C_DRV_FREQ_50             2                  /**< Define of frequency 50Khz. */
#define I2C_DRV_FREQ_1000           3                  /**< Define of frequency 1Mhz. */

#define I2C_DRV_TRANS_NORMAL        0     /**< I2C normal transmission. */
#define I2C_DRV_TRANS_MUX           1     /**< I2C mux transmission. */
#define I2C_DRV_TRANS_SMBUS_BLK     2     /**< SMBUS block read. */

#define I2C_DRV_LOCK_MODE_LOCK      1       /**< Transmission lock. */
#define I2C_DRV_LOCK_MODE_UNLOCK    0       /**< Transmission unlock. */

#define I2C_DRIVER_NAME "aess_i2cdrv"  /**< Define I2C driver name. */

/* definitions for bus status */
#define I2C_SET_BUS_ERROR_BIT       0x08
#define I2C_SET_BUS_BUSY_BIT        0x04
#define I2C_SET_BUS_SCL_HIGH_BIT    0x02
#define I2C_SET_BUS_SDA_HIGH_BIT    0x01

/* ioctl definitions */
/* IOCTL command */
/** IO magic number of I2C driver. */
#define AESS_I2CDRV_IOC_MAGIC       0xB7  

/** I/O control command: initial I2C bus. */  
#define AESS_I2CDRV_INIT            _IOWR(AESS_I2CDRV_IOC_MAGIC, 0, \
										  sI2CDrvBusInfoType)  
/** I/O control command: config I2C bus. */   
#define AESS_I2CDRV_CONFIG          _IOWR(AESS_I2CDRV_IOC_MAGIC, 1, \
										  sI2CDrvBusInfoType)     
/** I/O control command: execute an I2C read/write. */
#define AESS_I2CDRV_WR              _IOWR(AESS_I2CDRV_IOC_MAGIC, 2, \
										  sI2CDrvBufferInfoType)  
/** I/O control command: get Received data. */
#define AESS_I2CDRV_GET_MSG         _IOWR(AESS_I2CDRV_IOC_MAGIC, 3, \
										  sI2CDrvBufferInfoType)  
/** I/O control command: reset I2C channel. */
#define AESS_I2CDRV_RESET           _IOWR(AESS_I2CDRV_IOC_MAGIC, 4, \
										  sI2CDrvBusInfoType)     
/** I/O control command: get I2C bus status. */
#define AESS_I2CDRV_GET_STATUS      _IOWR(AESS_I2CDRV_IOC_MAGIC, 5, \
										  sI2CDrvBusInfoType)     
/** I/O control command: get hardware status. */
#define AESS_I2CDRV_GET_HW_STATUS   _IOWR(AESS_I2CDRV_IOC_MAGIC, 6, \
										  sI2CDrvBusInfoType)     
/** I/O control command: bus recovery. */
#define AESS_I2CDRV_CTRL_HW         _IOWR(AESS_I2CDRV_IOC_MAGIC, 7, \
										  sI2CDrvBusInfoType)     
/** I/O control command: Transmission lock. */
#define AESS_I2CDRV_TRANS_LOCK      _IOWR(AESS_I2CDRV_IOC_MAGIC, 8, \
                                          sI2CDrvBusInfoType)     
/** I/O control command: get slave read. */
#define AESS_I2CDRV_GET_SLAVE_READ  _IOWR(AESS_I2CDRV_IOC_MAGIC, 9, \
                                                              sI2CDrvBufferInfoType)                                           
/** I/O control command: slave write. */
#define AESS_I2CDRV_SLAVE_WRITE     _IOWR(AESS_I2CDRV_IOC_MAGIC, 10, \
                                                              sI2CDrvBufferInfoType)
/** I/O control command: execute an I2C read/write with long data size */
#define AESS_I2CDRV_WR_LONG         _IOWR(AESS_I2CDRV_IOC_MAGIC, 11, \
                                                              sI2CDrvBufferInfoType)  

/** I/O control command: execute an I2C get TX time diff command between TX TEND and STOP*/
#define AESS_I2CDRV_GET_TXTENDSTOP_TIMEDIFF         _IOWR(AESS_I2CDRV_IOC_MAGIC, 12, \
                                                              sI2CDrvBufferInfoType)                                                             
                                                            
/** I/O control command: switch to an I2C Driver */
#define AESS_I2CDRV_SWITCHDRV_RIIC         _IOWR(AESS_I2CDRV_IOC_MAGIC, 13, \
                                                              int)    

/** I/O control command: switch to an I2C Driver */
#define AESS_I2CDRV_OTHER_RIIC         _IOWR(AESS_I2CDRV_IOC_MAGIC, 14, \
                                                              int)


/** I/O control command: execute an I2C read/write with long data size */
#define AESS_I2CDRV_SLV_NACK_ON_TIMEOUT   _IOWR(AESS_I2CDRV_IOC_MAGIC, 15, \
                                                              sI2CDrvBufferInfoType)

/******************************************************************************
*   Enum      :   eI2CDrvErrorType
******************************************************************************/
/**
 *  @brief   I2C driver error enumeration type
 *
 ******************************************************************************/
typedef enum
{
    /** Successful */
    I2C_DRV_OK = 0,
    
    /** General failure */
    I2C_DRV_FAIL,
    
    /** Memory allocation failed */
    I2C_DRV_MEM_FAIL,
    
    /** Memory pool creation failed */
    I2C_DRV_POOL_FAIL,
    
    /** Queue creation failed */
    I2C_DRV_Q_FAIL,
    
    /** Event flags group creation failed */
    I2C_DRV_FLAGS_FAIL,
    
    /** Semaphore creation failed */
    I2C_DRV_SEM_FAIL,
    
    /** Timer creation failed */
    I2C_DRV_TIMER_FAIL
} eI2CDrvErrorType;

/******************************************************************************
*   Enum      :   eI2CDrvErrorStatusType
******************************************************************************/
/**
 *  @brief   I2C driver error status enumeration type
 *
 *****************************************************************************/
typedef enum
{
    /** No errors */
    I2C_DRV_ERROR_NONE = 0,
    
    /** NACK error */
    I2C_DRV_ERROR_NACK,
    
    /** Arbitration loss error */
    I2C_DRV_ERROR_AL,
    
    /** Start error */
    I2C_DRV_ERROR_START,
    
    /** Stop error */
    I2C_DRV_ERROR_STOP,
    
    /** bus busy */
    I2C_DRV_ERROR_BUSY,
    
    /** Timeout error */
    I2C_DRV_ERROR_TIMEOUT,
    
    /** General bus error */
    I2C_DRV_ERROR_BUS,
    
    /** Buffer overflow error */
    I2C_DRV_ERROR_OVRFL,
    
    /** Bus reet error */
    I2C_DRV_ERROR_RESET,

    /** Bus protocol error */
    I2C_DRV_ERROR_PROTOCOL	
} eI2CDrvErrorStatusType;


/******************************************************************************
*   Enum      :   eI2CDrvModeType
******************************************************************************/
/**
 *  @brief   I2C driver mode enumeration type
 *
 *****************************************************************************/
typedef enum
{
    /** Private I2C mode */
    I2C_DRV_MODE_PI2C = 0,
    
    /** IPMB mode */
    I2C_DRV_MODE_IPMB
} eI2CDrvModeType;

/******************************************************************************
*   Enum      :   eI2CDrvCtrlHardwareType
******************************************************************************/
/**
 *  @brief   I2C driver control hardware enumeration type
 *
 *****************************************************************************/
typedef enum
{
    /** Pulse the SCL manually and generate a stop condition */
    I2C_DRV_CLK_PULSE,
    
    /** Generate a stop condition */
    I2C_DRV_FORCE_STOP,
    
    /** Issue SMBus 2.0 reset pulse (SCL low for > 30ms */
    I2C_DRV_RESET_SMBUS
    
} eI2CDrvCtrlHardwareType;

/******************************************************************************
*   STRUCT      :   sI2CDrvBufferInfoType
******************************************************************************/
/**
 *  @brief   structure of I2C driver message buffer information
 *
 *****************************************************************************/
typedef struct
{
    /** Message transmit buffer */
    UINT8 *pu8MsgSendBuffer;

    /** Message receive buffer */
    UINT8 *pu8MsgRecBuffer;

    /** Transaction start flag. */
    UINT8 u8TransLock;
	
    /** Reserved */
    UINT8 u8Reserve;
  
    /** Reserved */	
    UINT16 u16Reserved;

    /** I2C bus channel */
    UINT8 u8Channel;
	
    /** device slave address */
    UINT8 u8DeviceAddr;
	
    /** Bus error status */
    UINT8 u8ErrorStatus;
	
    /** Message transmit data length */
    UINT8 u8MsgSendDataSize;
	
    /** Message receive data length */
    UINT8 u8MsgRecDataSize;

    /** Trans. type setting */
    UINT8 u8TransType;	

} sI2CDrvBufferInfoType;

/******************************************************************************
*   STRUCT      :   sI2CDrvBufferInfoTypeLong
******************************************************************************/
/**
 *  @brief   structure of I2C driver message buffer information
 *
 *****************************************************************************/
typedef struct
{
    sI2CDrvBufferInfoType buffer;
	
    /** Message transmit data length */
    UINT32 u32MsgSendDataSize;
	
    /** Message receive data length */
    UINT32 u32MsgRecDataSize;

} sI2CDrvBufferInfoTypeLong;

/******************************************************************************
*   STRUCT      :   sI2CDrvBusInfoType
******************************************************************************/
/**
 *  @brief   structure of I2C driver bus information
 *
 *****************************************************************************/
typedef struct
{
    /** IPMB msg received flag */
    UINT32 u32RecFlag;

    /** ID for kernel event handler */
    UINT16 u16DriverID;
	
    /** Current start tracking count */
    UINT16 u16CurStartCount;
	
    /** Current stop tracking count */
    UINT16 u16CurStopCount;

    /** Transaction timeout. */
    UINT16 u16TransactionTimeout;

    /** I2C bus channel */
    UINT8 u8Channel;
	
    /** Initial mode (IPMB or PI2C) */
    UINT8 u8InitMode;
	
    /** Current mode (IPMB or PI2C) */
    UINT8 u8CurMode;
	
    /** IPMB slave address */
    UINT8 u8SlaveAddr;
	
    /** I2C bus frequency selection */
    UINT8 u8Frequency;
	
    /** Bus error status */
    UINT8 u8ErrorStatus;
	
    /** Bus status */
    UINT8 u8BusStatus;
	
    /** Control hardware */
    UINT8 u8CtrlHW;

    /** I2CTool Slave Read flag*/
    UINT8 u8SlaveReadFlag;
    
    /** Shared Library Flag. */
    UINT8 u8SharedLibFlag;

    /** Reserved. */
    UINT16 u16Reserved;

    /** Added for 13G SlvRd2 timeout*/
    UINT32 u32SlvRd2Timeout;

    UINT8 u8SlvRd2TimeoutFlag;

} sI2CDrvBusInfoType;

#endif   /* AESSI2CDRV_COMMON_H */

/* End of code */
