/**
 *<center>
 * Avocent Corporation. Proprietary Information.
 * \n<em>
 *      This software is supplied under the terms of a license agreement or
 *      nondisclosure agreement with Avocent Corporation, or its subsidiary, and
 *      may not be copied, disseminated, or distributed except in accordance
 *      with the terms of that agreement.
 *
 *      2001 Gateway Place, Suite 520W, San Jose, California, 95110 U.S.A.
 *\n
 *                  US phone: 408.436.6333
 *
 *        Copyright &copy; 2008 Avocent Corporation.
 *</em> </center>
 *----------------------------------------------------------------------------\n
 *  MODULES     memory driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_memdrv_platform.h
 *  @brief  The header file of memory device driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef SHAREDMEMDRV_PLATFORM_H
#define SHAREDMEMDRV_PLATFORM_H

#include "shared_memdrv_common.h"

#endif   /* SHAREDMEMDRV_PLATFORM_H */

/* End of code */
