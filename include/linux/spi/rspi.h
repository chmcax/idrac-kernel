/*
 * Renesas SPI driver
 *
 * Copyright (C) 2012  Renesas Solutions Corp.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef __LINUX_SPI_RENESAS_SPI_H__
#define __LINUX_SPI_RENESAS_SPI_H__

#define RSPI_DRV_MAX_MSG_SIZE           512
#define RSPI_DRV_MAX_CMD_REG            8

/******************************************************************************
 **   STRUCT       :   rSPIConfigType
 *******************************************************************************/
/**
 **  @brief   rSPIConfigType
 **
 ******************************************************************************/

typedef struct
{
    /** Commands Mask for environment parameter setting */
    u32   u32CommandsMask;

    /** Chip Select */
    u8   u8ChipSelect;

    /** SPMS setting (SPI operation or Clock synchronous operation) */
    u8   u8SPMS;

    /** SSLP setting (SSL signal 0-active or 1-active) */
    u8   u8SSLP;

    /** MOIFE setting (MOSI idle value fixing enable) */
    u8   u8MOIFE;

    /** MOIFV setting (MOSI idle fixed value) */
    u8   u8MOIFV;

    /** SPOM setting (RSPI output pin mode) */
    u8   u8SPOM;

    /** SPSLN setting (RSPI sequence length specification) */
    u8   u8SPSLN;

    /** SPBR setting (bit rate) */
    u8   u8SPBR;

    /** SPFC setting (Number of frames specification) */
    u8   u8SPFC;

    /** SCKDL setting (RSPCK delay setting) */
    u8   u8SCKDL;

    /** SLNDL setting (SSL negation delay setting) */
    u8   u8SLNDL;

    /** SPND setting (RSPI next-access delay setting) */
    u8   u8SPND;

    /** SPOE setting (Parity mode) */
    u8   u8SPOE;

    /** SPPE setting (Parity enable) */
    u8   u8SPPE;

    /** TXMD setting (Transmit Only) */
    u8   u8TXMD;

    /** SPLW (Long Word SPDR access) */
    u8   u8SPLW;

    /** No DMA in transfers */
    u8   u8NoDMA;

    /** SCKDEN setting Mask */
    u8   u8SCKDENMask;

    /** SLNDEN setting Mask */
    u8   u8SLNDENMask;

    /** SPNDEN setting Mask */
    u8   u8SPNDENMask;

    /** LSBF setting Mask */
    u8   u8LSBFMask;

    /** SPB setting Mask */
    u8   u8SPBMask;

    /** SSLKP setting Mask */
    u8   u8SSLKPMask;

    /** BRDV setting Mask */
    u8   u8BRDVMask;

    /** CPOL-CPHA setting Mask */
    u8   u8SPIModeMask;

    /** Reserve 1 */
    u8   u8Reserve1[2];

    /** SCKDEN setting (RSPCK delay setting enable) */
    u8   u8SCKDEN[RSPI_DRV_MAX_CMD_REG];

    /** SLNDEN setting (SSL negation delay setting enable) */
    u16   u8SLNDEN[RSPI_DRV_MAX_CMD_REG];

    /** SPNDEN setting (RSPI next-access delay enable) */
    u16   u8SPNDEN[RSPI_DRV_MAX_CMD_REG];

    /** LSBF setting (MSB / LSB select) */
    u16   u8MsbLsb[RSPI_DRV_MAX_CMD_REG];

    /** SPB setting (SRPI data length setting) */
    u16   u8SPB[RSPI_DRV_MAX_CMD_REG];

    /** SSLKP setting (SSL signal level keeping) */
    u16   u8SSLKP[RSPI_DRV_MAX_CMD_REG];

    /** BRDV setting (Bit rate Division setting) */
    u16   u8BRDV[RSPI_DRV_MAX_CMD_REG];

    /** CPOL-CPHA setting (SPI Mode: RSPCK poloarity setting / RSPCK phase setting) */
    u16   u8SPIMode[RSPI_DRV_MAX_CMD_REG];
} rSPIConfigType;


/******************************************************************************
 * *   STRUCT       :   rSPIDrvInfoType
 * ******************************************************************************/
/**
 *  *  @brief   rSPIDrvInfoType
 *   *
 *    *****************************************************************************/
typedef struct
{
    /** Msg transmit data length */
    u32 u32MsgSendDataSize;

    /** Msg receive data length */
    u32 u32MsgRecDataSize;

    /** Msg transmit buffer */
    u8 *pu8MsgSendBuffer;

    /** Msg receive buffer */
    u8 *pu8MsgRecBuffer;

    /** SPI chip select */
    u8 u8ChipSelect;

} rSPIDrvInfoType;

int aess_rspi_wr_int(
        /** kernel space rSPIDrvInfoType structure point */
        rSPIDrvInfoType *rspiDrvInfo
        );

int aess_rspi_config(

        /** rSPIConfigType structure point */
        rSPIConfigType *rspiDrvConfig
        );

struct rspi_plat_data {
    unsigned int dma_tx_id;
    unsigned int dma_rx_id;
    unsigned dma_width_16bit:1;
    u16 num_chipselect;
};

#endif
