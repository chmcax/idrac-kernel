#ifndef __DELL_TRAB_H
#define __DELL_TRAB_H

/* TODO: move this flag into the main makefile */
#define TRAB_ENABLE 1
/*
 * Copyright (C) 2010 Dell Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifdef TRAB_ENABLE
/*--------------------------------------------------------------------
 * Linux include files
 *--------------------------------------------------------------------*/
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/jiffies.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <asm/io.h>
#include <linux/util_non_os_mem.h>

/*--------------------------------------------------------------------
 * Dell include files
 *--------------------------------------------------------------------*/

//#define LOOKUP_NAME_LEN 6
#define LOOKUP_NAME_LEN 8

#pragma pack(push,1)
struct execution_msg {
    unsigned long timestamp;
    unsigned short frc;
    short pid;
};
/*
struct trab_lookup_t {
    short pid;
    char name[LOOKUP_NAME_LEN];
};
*/
struct trab_lookup_t { 
    char name [LOOKUP_NAME_LEN];
};

#pragma pack(pop)


#define TRAB_SIZE (512u*1024u)
/* trab execution log size 256kB */
#define TRAB_EXEC_SIZE (256u*1024u)
#define TRAB_EXEC_MAX_MSG_LEN (sizeof(struct execution_msg))
#define TRAB_EXEC_MAX_MSGS (TRAB_EXEC_SIZE/TRAB_EXEC_MAX_MSG_LEN)
#define TRAB_EXEC_MAX_OFFS ((unsigned long)(trabbuf)+TRAB_EXEC_SIZE-2*sizeof(struct execution_msg))

#define TRAB_LOOKUP_SIZE (256u*1024u)
#define TRAB_MAX_PIDS (TRAB_LOOKUP_SIZE / sizeof(struct trab_lookup_t))
#define TRAB_IOC_MAGIC 0xD3
#define TRAB_IOC_FILTER _IOW(TRAB_IOC_MAGIC,  2, int)
#define TRAB_PROC_MASK 0x01     // 0 = off, 1 = on
#define TRAB_INT_MASK  0x02      // 0 = off, 1 = on

extern struct execution_msg *trabbuf;         /* pointer to beginning of buffer */
extern struct execution_msg *trabbuf_next;    /* pointer to write next entry into */
extern struct trab_lookup_t *trab_lookup;
extern unsigned char *nmi_buf_start;
extern int trace_proc_enabled;
extern int trace_int_enabled;

static inline void trab_init(void)
{
    unsigned char tcrval;
    trabbuf = (struct execution_msg *) (((t_non_os_mem*)memory_end)->tracebuf);
    trabbuf_next = trabbuf;
    trab_lookup = (struct trab_lookup_t*)((unsigned long)trabbuf + TRAB_EXEC_SIZE);
    if (trabbuf) {
        memset(trabbuf, 0, TRAB_SIZE);
        printk(KERN_INFO "TRAB buffer at %lx allocated\n", (unsigned long)trabbuf);
    	printk(KERN_INFO "TRABLOOKUP buffer at %lx allocated\n", (unsigned long)trab_lookup);
    } else
        printk(KERN_WARNING "TRAB initialization failure\n");
    /* 
     * set up the free-running timer to run at Peripheral Clock/32 
     * which will increment at 1.5MHz
     */
    tcrval = readb(0xffea0006);
    writeb((tcrval & 0xfc) | 0x02, 0xffea0006);

    /* 
     * Default both process and interrupt tracing on
     */
    trace_proc_enabled = 1;
    trace_int_enabled = 1;
}




static inline void trab_trace_execution(struct task_struct* task)
{
    trabbuf_next->pid = (short)task->pid;
    trabbuf_next->timestamp = jiffies;
    trabbuf_next->frc = readw(0xffea0002);
    trabbuf_next = (struct execution_msg*)((unsigned long)trabbuf +
                    (((unsigned long)trabbuf_next + sizeof(struct execution_msg)) & 0x3ffff));
}




static inline void trab_trace_interrupt(unsigned int irq)
{
    trabbuf_next->pid = (short)(-irq);
    trabbuf_next->timestamp = jiffies;
    trabbuf_next->frc = readw(0xffea0002);
    trabbuf_next = (struct execution_msg*)((unsigned long)trabbuf +
                    (((unsigned long)trabbuf_next + sizeof(struct execution_msg)) & 0x3ffff));
}


static inline void trab_lookup_update(pid_t pid, char* name)
{
        if(pid>0 && pid <= TRAB_MAX_PIDS)
        { 
            memcpy(trab_lookup[pid-1].name,name, LOOKUP_NAME_LEN);
        }
}

#if 0
static inline void trab_lookup_add(pid_t pid, char* name)
{
#if 1
    struct trab_lookup_t *tl;
    tl = trab_lookup;
    while (tl < trab_lookup + TRAB_MAX_PIDS) {
	if (tl->pid == 0) {
//		printk("!!!LOOK!!!!\n");
//	printk("tl pid is %lu, previous value is %lu and trab_lookup location is %lx \n",(unsigned long)tl->pid,(unsigned long)(tl-1)->pid, (unsigned long)tl);
            tl->pid = pid;
            /* dont store null terminator, if any. we'll add that in userland */
            memcpy(tl->name, name, LOOKUP_NAME_LEN);
//	    printk(KERN_INFO " Process %s has pid %lu\n", name, (unsigned long) pid);
            break;
        }
        tl ++;
    }

#endif
}

static inline void trab_lookup_remove(pid_t pid)
{
#if 1
    struct trab_lookup_t *tl;
    tl = trab_lookup;
    while (tl < trab_lookup + TRAB_MAX_PIDS) {
        if (tl->pid == pid) {
            tl->pid = 0;
            memset(tl->name, 0, LOOKUP_NAME_LEN);
            break;
        }
        tl ++;
    }
#endif
}

static inline void trab_lookup_update(pid_t pid, char* name)
{
#if 1
    struct trab_lookup_t *tl;
    tl = trab_lookup;
    while (tl < trab_lookup + TRAB_MAX_PIDS) {
        if (tl->pid == pid) {
//	printk("trab_lookup_update is called, pid is %lu, new name is %s \n", (unsigned long) pid, name);
            memcpy(tl->name, name, LOOKUP_NAME_LEN);
            break;
        }
        tl ++;
    }
#endif
}
#endif

#endif /* TRAB_ENABLE */
#endif /* __DELL_TRAB_H */
