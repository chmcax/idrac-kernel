/****************************************************************************

  Copyright (c) 2006, Dell Inc
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution. 

  - Neither the name of Dell Inc nor the names of its contributors
    may be used to endorse or promote products derived from this software 
    without specific prior written permission. 

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 

*****************************************************************************/

/*****************************************************************************
*
*	FILE: util_non_os_mem.h
*
*	This file is needed by u-boot AND the Linux.  In looking over the build  
*	and trying to mod makefiles, create links, copying files, etc, etc.  
*	It was decided to just CHECK THIS FILE INTO TWO PLACES.
*	
*	1) ../src/u-boot_B0/include
*	2) ../src/kernel/linux-2.6.30/include/linux
*	
*****************************************************************************/
#ifndef __UTIL_NON_OS_MEM_H
#define __UTIL_NON_OS_MEM_H


/*
*	In u-boot the CONFIG_PRAM is used by board.c to create the u-boot 
*	"mem" env.  This is passed up on the bootargs cmdline to tell Linux how 
*	much memory that it has available.  The "mem=" points to Non-Linux 
*	memory that can be used by various features.  The "t_non_os_mem" struct 
*	is how this memory can be mapped to avoid hard coding addresses.
*	
*	Early on the kernel has "CONFIG_MEMORY_SIZE=0x0ec00000" or 236MB.  
*	This could have been a built in protection from u-boot sending a bad 
*	"mem" area to make sure it does NOT access the video memory.
*
*	NOTE: The uppermost 16MByte of DDR3 is video frame buffer, this struct 
*	does NOT include it.  KEEP OUT OF THAT 16MB.
*/
#define NON_OS_MEM_AVCT             0x100000
#define NON_OS_TRACEBUF             0x80000
#define NON_OS_PREBOOTLOG           0x80000
#define NON_OS_MEM_AVCT_WHATFOR     0x400000	/*will this go away?? */
/* for u-boot also see idrac_12g.h */


typedef struct {
	unsigned char avct_dma_buf[NON_OS_MEM_AVCT];    /*Avct use, video DMA*/
	unsigned char tracebuf[NON_OS_TRACEBUF];        /*persistant trace*/
	unsigned char prebootlog[NON_OS_PREBOOTLOG];    /*u-boot preboot log*/
	unsigned char avct_whatfor[NON_OS_MEM_AVCT_WHATFOR];  /*????*/
	} t_non_os_mem;


#endif		/*__UTIL_NON_OS_MEM_H*/




