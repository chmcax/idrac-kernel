/*
 * hub.h -- Gadget Hub driver
 *
 * Copyright (C) 2010 Renesas Solutions Corp.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */

/*
 * This header declares the utility functions used by "Gadget Zero", plus
 * interfaces to its two single-configuration function drivers.
 */

#ifndef __F_HUB_H
#define __f_HUB_H

#define USB_HUB_PORTS	6	/* FIXME */

struct hub_dev {
	struct usb_gadget	*gadget;
	spinlock_t		lock;
	struct usb_ep		*ep;
	struct usb_request	*req;
	struct usb_request	*hub_req;
	struct usb_hub_descriptor *hub_desc;
	struct usb_hub_status	*hub_status;
	struct usb_port_status	port_status[USB_HUB_PORTS];
	struct timer_list	timer;

	enum usb_device_speed	speed;
	u8			config;
	u8			device_state;
	u8			hub_resend;
	u16			attach;
	u16			connected;
	u16			detach;

	/* autoresume timer */
	struct timer_list	resume;
};

/* messaging utils */
#define DBG(d, fmt, args...) \
	dev_dbg(&(d)->gadget->dev , fmt , ## args)
#define VDBG(d, fmt, args...) \
	dev_vdbg(&(d)->gadget->dev , fmt , ## args)
#define ERROR(d, fmt, args...) \
	dev_err(&(d)->gadget->dev , fmt , ## args)
#define WARNING(d, fmt, args...) \
	dev_warn(&(d)->gadget->dev , fmt , ## args)
#define INFO(d, fmt, args...) \
	dev_info(&(d)->gadget->dev , fmt , ## args)

#endif /* __F_HUB_H */
