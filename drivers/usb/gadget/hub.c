/*
 * hub.c -- Gadget Hub driver
 *
 * Copyright (C) 2012  Renesas Solutions Corp.
 *
 * This code is based in part on the Gadget Zero driver, which
 * is Copyright (C) 2003-2004 by David Brownell, all rights reserved.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/device.h>
#include <linux/usb.h>
#include <linux/usb/ch9.h>
#include <linux/usb/ch11.h>
#include <linux/usb/gadget.h>
#include <linux/delay.h>

#include "gadget_chips.h"
#include "hub.h"

//#include "usbstring.c"
//#include "config.c"
//#include "epautoconf.c"

/*-------------------------------------------------------------------------*/

#define DRIVER_VERSION		"2017-07-20"

static const char shortname[] = "g_hub";
static const char longname[] = "Gadget USB HUB";

#define DRIVER_VENDOR_NUM	0x413C		//Dell
#define DRIVER_PRODUCT_NUM	0xA001		//Virtual Hub

#define CONFIG_USB_GADGET_DUALSPEED		1

/*-------------------------------------------------------------------------*/

static struct {
	unsigned short  idVendor;
	unsigned short  idProduct;
	unsigned short  bcdDevice;
} mod_data = {
	.idVendor		= DRIVER_VENDOR_NUM,
	.idProduct		= DRIVER_PRODUCT_NUM,
	.bcdDevice		= 0x0000,
};

module_param_named(idVendor, mod_data.idVendor, ushort, S_IRUGO);
MODULE_PARM_DESC(controller, "idVendor field of Device Descriptor");

module_param_named(idProduct, mod_data.idProduct, ushort, S_IRUGO);
MODULE_PARM_DESC(controller, "idProduct field of Device Descriptor");

module_param_named(bcdDevice, mod_data.bcdDevice, ushort, S_IRUGO);
MODULE_PARM_DESC(controller, "bcdDevice field of Device Descriptor");

/*-------------------------------------------------------------------------*/

static struct usb_hub_descriptor hub_device_desc = {
	.bDescLength		= sizeof hub_device_desc,
	.bDescriptorType	= USB_DT_HUB,
	.bNbrPorts		= USB_HUB_PORTS,
	.wHubCharacteristics	= 0x0001,
	.bPwrOn2PwrGood		= 0x32,
	.bHubContrCurrent	= 0x64,
	.u = {
		.hs = {
			.DeviceRemovable[0]	= 0,
			.PortPwrCtrlMask[0]	= 0xff,
		},
	},
};

static struct usb_hub_status hub_status = {
	.wHubStatus	= 0,
	.wHubChange	= 0,
};

/*-------------------------------------------------------------------------*/

static unsigned buflen = 512;

/*-------------------------------------------------------------------------*/

#define STRING_MANUFACTURER		1
#define STRING_PRODUCT			2
#define STRING_SERIAL			3

/*-------------------------------------------------------------------------*/

static struct usb_device_descriptor device_desc = {
	.bLength =		sizeof device_desc,
	.bDescriptorType =	USB_DT_DEVICE,

	.bcdUSB =		__constant_cpu_to_le16(0x0200),
	.bDeviceClass =		USB_CLASS_HUB,
	.bDeviceSubClass =	0x00,
	.bDeviceProtocol =	0x00,

	.idVendor =		__constant_cpu_to_le16(DRIVER_VENDOR_NUM),
	.idProduct =		__constant_cpu_to_le16(DRIVER_PRODUCT_NUM),
	.bcdDevice =		__constant_cpu_to_le16(0x0000),

	.iManufacturer =	STRING_MANUFACTURER,
	.iProduct =		STRING_PRODUCT,
	.iSerialNumber =	STRING_SERIAL,
	.bNumConfigurations =	1,
};

static struct usb_config_descriptor device_config = {
	.bLength =		sizeof device_config,
	.bDescriptorType =	USB_DT_CONFIG,

	.bNumInterfaces =	1,
	.bConfigurationValue =	1,
	.bmAttributes =		USB_CONFIG_ATT_ONE | USB_CONFIG_ATT_SELFPOWER |
				USB_CONFIG_ATT_WAKEUP,
	.bMaxPower =		(100 / 2),	/* self-powered */
};

static struct usb_interface_descriptor device_intf = {
	.bLength =		sizeof device_intf,
	.bDescriptorType =	USB_DT_INTERFACE,

	.bInterfaceNumber =	0,
	.bAlternateSetting =	0,
	.bNumEndpoints =	1,
	.bInterfaceClass =	USB_CLASS_HUB,
	.bInterfaceSubClass =	0x00,
	.bInterfaceProtocol =	0x00,
};

/* for full speed */
static struct usb_endpoint_descriptor fs_device_endp = {
	.bLength =		USB_DT_ENDPOINT_SIZE,
	.bDescriptorType =	USB_DT_ENDPOINT,

	.bEndpointAddress =	USB_DIR_IN,
	.bmAttributes =		USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize =	__constant_cpu_to_le16(1),
	.bInterval =		0x0c,
};

static struct usb_qualifier_descriptor dev_qualifier = {
	.bLength =		sizeof dev_qualifier,
	.bDescriptorType =	USB_DT_DEVICE_QUALIFIER,

	.bcdUSB =		__constant_cpu_to_le16(0x0200),
	.bDeviceClass =		USB_CLASS_PER_INTERFACE,

	.bNumConfigurations =	1,
};

/*-------------------------------------------------------------------------*/

#ifdef CONFIG_USB_GADGET_DUALSPEED
/* high speed */
static struct usb_endpoint_descriptor hs_device_endp = {
	.bLength =		USB_DT_ENDPOINT_SIZE,
	.bDescriptorType =	USB_DT_ENDPOINT,

	.bEndpointAddress =	USB_DIR_IN,
	.bmAttributes =		USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize =	__constant_cpu_to_le16(1),
	.bInterval =		0x0c,
};

static const struct usb_descriptor_header *hs_hub_function[] = {
	(struct usb_descriptor_header *) &device_intf,
	(struct usb_descriptor_header *) &hs_device_endp,
	NULL,
};

static const struct usb_descriptor_header *fs_hub_function[] = {
	(struct usb_descriptor_header *) &device_intf,
	(struct usb_descriptor_header *) &fs_device_endp,
	NULL,
};
#endif /* CONFIG_USB_GADGET_DUALSPEED */

/*-------------------------------------------------------------------------*/

static char	manufacturer[40];
static char	serial[40];

/* static strings, in iso 8859/1 */
static struct usb_string		strings[] = {
	{ STRING_MANUFACTURER, manufacturer, },
	{ STRING_PRODUCT, longname, },
	{ STRING_SERIAL, serial, },
	{  }			/* end of list */
};

static struct usb_gadget_strings	stringtab = {
	.language	= 0x0409,	/* en-us */
	.strings	= strings,
};

static int get_hub_device(u8 *buf)
{
	int len;

	len = sizeof(hub_device_desc);
	memcpy(buf, &hub_device_desc, len);

	return len;
}

static int get_hub_status(u8 *buf)
{
	int len;

	len = sizeof(hub_status);
	memcpy(buf, &hub_status, len);

	return len;
}

static int get_port_status(struct hub_dev *dev,
			   u8 *buf, unsigned short portno, unsigned short req)
{
	struct usb_port_status *port_status = dev->port_status;
	int len;

	len = sizeof(port_status[portno]);
	memcpy(buf, &port_status[portno], len);

	return len;
}

static struct usb_request *alloc_ep_req(struct usb_ep *ep, unsigned length)
{
	struct usb_request      *req;

	req = usb_ep_alloc_request(ep, GFP_ATOMIC);
	if (req) {
		req->length = length;
		req->buf = kmalloc(length, GFP_ATOMIC);
		if (!req->buf) {
			usb_ep_free_request(ep, req);
			req = NULL;
		}
	}
	return req;
}

static void free_ep_req(struct usb_ep *ep, struct usb_request *req)
{
	kfree(req->buf);
	usb_ep_free_request(ep, req);
}

static void send_device_status(struct usb_ep *ep);
static void hub_complete(struct usb_ep *ep, struct usb_request *req)
{
	struct hub_dev	*dev = ep->driver_data;
	u8 old_device_state;
	u8 *bufptr = (u8*)req->buf;
	int reqStatus;
	struct usb_port_status *port_status = dev->port_status;
	u8 state_bit;
	int portno;

	old_device_state = *bufptr;
	reqStatus = req->status;

	if (reqStatus == -EBUSY) {
		/* FIFO not ready */
		printk(KERN_ALERT "%s() %d, %u/%u, req=0x%08X\n", __func__,
			req->status, req->actual, req->length, (u32)req);
		dev->device_state |= old_device_state;
	}

	free_ep_req(ep, req);

	if (!dev)
		return;

	dev->hub_req = NULL;

	if (dev->hub_resend) {
		if (dev->device_state) {
			send_device_status(ep);
			dev->hub_resend = 0;
		}
	} else if (reqStatus == -EBUSY) {
		if (dev->device_state)
			send_device_status(ep);
	} else if (reqStatus == 0) {
		for (portno = 0; portno < USB_HUB_PORTS; portno++) {
			state_bit = 1 << (portno + 1);
			if (!(state_bit & old_device_state))
				continue;
			if (port_status[portno].wPortChange) {
				dev->device_state |= state_bit;
				send_device_status(ep);
				break;
			}
		}
	}
}

static void send_device_status(struct usb_ep *ep)
{
	struct usb_request	*req;
	struct hub_dev		*dev = ep->driver_data;
	int			status, len;
	u8			portno, state_bit;
	struct usb_port_status	*port_status = dev->port_status;

	if (!dev)
		return;
	if (dev->gadget->speed == USB_SPEED_UNKNOWN)
		return;
	if (dev->hub_req) {
		dev->hub_resend = 1;
		return;
	}
	for (portno = 0; portno < USB_HUB_PORTS; portno++) {
		state_bit = 1 << (portno + 1);
		if (!(state_bit & dev->device_state))
			continue;
		if (!port_status[portno].wPortChange)
			dev->device_state &= ~(state_bit);
	}
	if (dev->device_state == 0)
		return;

	dev->hub_resend = 0;

	req = alloc_ep_req(ep, buflen);

	if (!req) {
		printk(KERN_ERR "hub: req error\n");
		return;
	}
	req->complete = hub_complete;
	len = sizeof(dev->device_state);
	memcpy(req->buf, &dev->device_state, len);
	req->length = len;
	dev->hub_req = req;

	status = usb_ep_queue(ep, req, GFP_KERNEL);
	if (status) {
		printk(KERN_ERR "hub: start %s --> %d\n", ep->name, status);
		free_ep_req(ep, req);
		req = NULL;
	}
}

static int hub_set_feature(struct hub_dev *dev,
			   unsigned short portno, unsigned short req)
{
	struct usb_port_status *port_status = dev->port_status;
	u8 sendDeviceStatus = 1;

	switch (req) {
	case USB_PORT_FEAT_CONNECTION:
		dev->device_state |= 0x0001 << (portno + 1);
		port_status[portno].wPortStatus = USB_PORT_STAT_POWER |
						 USB_PORT_STAT_CONNECTION;
		port_status[portno].wPortChange = USB_PORT_STAT_CONNECTION;
		dev->attach &= ~(0x0001 << portno);
		dev->connected |= (0x0001 << (portno));
		if (dev->attach)
			sendDeviceStatus = 0;
		break;
	case USB_PORT_FEAT_ENABLE:
		port_status[portno].wPortStatus |= USB_PORT_STAT_ENABLE;
		break;
	case USB_PORT_FEAT_SUSPEND:
		port_status[portno].wPortStatus |= USB_PORT_STAT_SUSPEND;
		break;
	case USB_PORT_FEAT_RESET:
		dev->device_state |= 0x0001 << (portno + 1);
		port_status[portno].wPortStatus |= USB_PORT_STAT_ENABLE |
						  USB_PORT_STAT_RESET;
		port_status[portno].wPortStatus |=
				(dev->speed == USB_SPEED_HIGH) ?
				USB_PORT_STAT_HIGH_SPEED : 0;
		port_status[portno].wPortStatus &= ~USB_PORT_STAT_SUSPEND;
		port_status[portno].wPortChange |= USB_PORT_STAT_RESET;

		port_status[portno].wPortStatus &= ~USB_PORT_STAT_RESET;
		break;
	case USB_PORT_FEAT_POWER:
		port_status[portno].wPortStatus |= USB_PORT_STAT_POWER;
		port_status[portno].wPortChange = 0;

		if (dev->connected & (0x0001 << portno)) {
			port_status[portno].wPortStatus |= USB_PORT_STAT_CONNECTION;
			port_status[portno].wPortChange |= USB_PORT_STAT_CONNECTION;
		}
		break;
	case USB_PORT_FEAT_OVER_CURRENT:
	case USB_PORT_FEAT_LOWSPEED:
	case USB_PORT_FEAT_C_CONNECTION:
	case USB_PORT_FEAT_C_ENABLE:
	case USB_PORT_FEAT_C_SUSPEND:
	case USB_PORT_FEAT_C_OVER_CURRENT:
	case USB_PORT_FEAT_C_RESET:
		break;
	default:
		printk(KERN_ERR "hub: unkown command\n");
		return -EINVAL;
	}

	if (dev->device_state && sendDeviceStatus)
		send_device_status(dev->ep);
	return 0;
}

static int hub_clear_feature(struct hub_dev *dev,
			     unsigned short portno, unsigned short req)
{
	struct usb_port_status *port_status = dev->port_status;
	u8 sendDeviceStatus = 1;

	switch (req) {
	case USB_PORT_FEAT_CONNECTION:
		port_status[portno].wPortStatus = USB_PORT_STAT_POWER;
		port_status[portno].wPortChange = USB_PORT_STAT_CONNECTION;
		dev->device_state |= 0x0001 << (portno + 1);
		dev->connected &= ~(0x0001 << portno);
		break;
	case USB_PORT_FEAT_ENABLE:
		port_status[portno].wPortStatus &= ~USB_PORT_STAT_ENABLE;
		break;
	case USB_PORT_FEAT_SUSPEND:
		port_status[portno].wPortStatus &= ~USB_PORT_STAT_SUSPEND;
		port_status[portno].wPortChange |= USB_PORT_STAT_SUSPEND;
		dev->device_state |= 0x0001 << (portno + 1);
		break;
	case USB_PORT_FEAT_POWER:
		port_status[portno].wPortStatus = 0;
		port_status[portno].wPortChange = 0;
		break;
	case USB_PORT_FEAT_C_CONNECTION:
		port_status[portno].wPortChange &= ~USB_PORT_STAT_CONNECTION;
		sendDeviceStatus = 0;
		break;
	case USB_PORT_FEAT_C_ENABLE:
		port_status[portno].wPortChange &= ~USB_PORT_STAT_ENABLE;
		break;
	case USB_PORT_FEAT_C_SUSPEND:
		port_status[portno].wPortChange &= ~USB_PORT_STAT_SUSPEND;
		break;
	case USB_PORT_FEAT_C_OVER_CURRENT:
		port_status[portno].wPortChange &= ~USB_PORT_STAT_OVERCURRENT;
		break;
	case USB_PORT_FEAT_C_RESET:
		port_status[portno].wPortChange &= ~USB_PORT_STAT_RESET;
		break;
	case USB_PORT_FEAT_OVER_CURRENT:
	case USB_PORT_FEAT_RESET:
	case USB_PORT_FEAT_LOWSPEED:
		break;
	default:
		printk(KERN_ERR "hub: unkown command\n");
		return -EINVAL;
	}

	if ((1 << (portno + 1)) & dev->device_state &&
	    port_status[portno].wPortChange == 0) {
		if (dev->hub_req) {
			mdelay(10);
			usb_ep_dequeue(dev->ep, dev->hub_req);
		}
		dev->device_state &= ~(0x0001 << (portno + 1));
	}

	if (dev->device_state && sendDeviceStatus)
		send_device_status(dev->ep);

	return 0;
}

static void hub_setup_complete(struct usb_ep *ep, struct usb_request *req)
{
	if (req->status || req->actual != req->length)
		printk(KERN_DEBUG "setup complete --> %d, %d/%d\n",
			req->status, req->actual, req->length);
}

/*-------------------------------------------------------------------------*/
static int hub_class_vendor_request(struct usb_gadget *gadget,
				    const struct usb_ctrlrequest *ctrl)
{
	struct hub_dev		*dev = get_gadget_data(gadget);
	struct usb_request	*req = dev->req;
	int			value = -EOPNOTSUPP;
	unsigned short		w_index = le16_to_cpu(ctrl->wIndex);
	unsigned short		w_length = le16_to_cpu(ctrl->wLength);
	unsigned short		w_value = le16_to_cpu(ctrl->wValue);

	req->zero = 0;
	dev->speed = gadget->speed;

	switch (ctrl->bRequestType & 0x7f) {
	case USB_RT_HUB:
		switch (ctrl->bRequest) {
		case USB_REQ_GET_STATUS:		/* GetHubStatus */
			value = get_hub_status(req->buf);
			break;
		case USB_REQ_GET_DESCRIPTOR:	/* GetHubDescriptor */
			value = get_hub_device(req->buf);
			break;
		default:
			printk(KERN_ERR "hub: hub unknown command\n");
			break;
		}
		if (value >= 0)
			value = min(w_length, (u16) value);
		break;
	case USB_RT_PORT:
		switch (ctrl->bRequest) {
		case USB_REQ_GET_STATUS:		/* GetPortStatus */
			value = get_port_status(dev, req->buf, w_index - 1,
						w_value);
			if (value >= 0)
				value = min(w_length, (u16) value);
			break;
		case USB_REQ_CLEAR_FEATURE:
			value = hub_clear_feature(dev, w_index - 1, w_value);
			break;
		case USB_REQ_SET_FEATURE:
			value = hub_set_feature(dev, w_index - 1, w_value);
			break;
		default:
			printk(KERN_ERR "hub: port unknown command\n");
			break;
		}
		break;
	case (USB_TYPE_VENDOR | USB_RECIP_OTHER):
		switch (ctrl->bRequest) {
		case USB_REQ_CLEAR_FEATURE:
			hub_clear_feature(dev, w_index - 1, w_value);
			break;
		case USB_REQ_SET_FEATURE:
			hub_set_feature(dev, w_index - 1, w_value);
			break;
		case 0x99:
			dev->attach |= (0x0001 << (w_index - 1));
			break;
		}
		return 0;
	default:
		break;
	}

	/* respond with data transfer before status phase? */
	if (value >= 0) {
		req->length = value;
		req->zero = value < w_length
			&& (value % gadget->ep0->maxpacket) == 0;
		value = usb_ep_queue(gadget->ep0, req, GFP_ATOMIC);
		if (value < 0) {
			printk(KERN_DEBUG "ep_queue --> %d\n", value);
			req->status = 0;
			hub_setup_complete(gadget->ep0, req);
		}
	}

	return value;

}

static int hub_config_buf(struct usb_gadget *gadget, u8 *buf, u8 type,
			  unsigned index)
{
	int len;
	const struct usb_descriptor_header **function;
#ifdef CONFIG_USB_GADGET_DUALSPEED
	int hs = (gadget->speed == USB_SPEED_HIGH);
#endif

	/* two configurations will always be index 0 and index 1 */
	if (index > 1)
		return -EINVAL;

#ifdef CONFIG_USB_GADGET_DUALSPEED
	if (type == USB_DT_OTHER_SPEED_CONFIG)
		hs = !hs;
	if (hs)
		function = hs_hub_function;
	else
#endif
		function = fs_hub_function;

	len = usb_gadget_config_buf(&device_config, buf, buflen, function);
	if (len < 0)
		return len;
	((struct usb_config_descriptor *) buf)->bDescriptorType = type;
	return len;
}

static int hub_set_alt(struct usb_gadget *gadget)
{
	struct hub_dev	*dev = get_gadget_data(gadget);
	int ret;

	if (gadget->speed == USB_SPEED_HIGH)
		dev->ep->desc = &hs_device_endp;
	else
		dev->ep->desc = &fs_device_endp;
	ret = usb_ep_enable(dev->ep);
	if (ret == 0)
		dev->ep->driver_data = dev;

	dev->config = 1;

	return 0;
}

static int hub_reset_config(struct usb_gadget *gadget)
{
	struct hub_dev *dev = get_gadget_data(gadget);
	int i;

	/*  Dequeue pending xfer */
	dev->hub_resend = 0;
	if (dev->hub_req) {
		mdelay(10);
		usb_ep_dequeue(dev->ep, dev->hub_req);
	}

	usb_ep_disable(dev->ep);
	dev->ep->driver_data = NULL;

	dev->config = 0;
	dev->device_state = 0;

	/* Clear all port status because the hub is not configured */
	for (i = 0; i < USB_HUB_PORTS; i++) {
		dev->port_status[i].wPortStatus = 0x0000;
		dev->port_status[i].wPortChange = 0x0000;
	}

	return 0;
}


static int hub_set_config(struct usb_gadget *gadget, unsigned number)
{
	int result = 0;
	struct hub_dev		*dev = get_gadget_data(gadget);
	char *speed;

	if (dev->config)
		hub_reset_config(gadget);

	hub_set_alt(gadget);

	switch (gadget->speed) {
	case USB_SPEED_LOW:
		speed = "low";
		break;
	case USB_SPEED_FULL:
		speed = "full";
		break;
	case USB_SPEED_HIGH:
		speed = "high";
		break;
	default:
		speed = "?";
		break;
	}

	dev->config = number;

	INFO(dev, "%s speed connected\n", speed);

	return result;
}

static int hub_standard_request(struct usb_gadget *gadget,
				const struct usb_ctrlrequest *ctrl)
{
	struct hub_dev		*dev = get_gadget_data(gadget);
	struct usb_request	*req = dev->req;
	int			value = -EOPNOTSUPP;
	u16			w_index = le16_to_cpu(ctrl->wIndex);
	u16			w_value = le16_to_cpu(ctrl->wValue);
	u16			w_length = le16_to_cpu(ctrl->wLength);

	req->zero = 0;
	req->complete = hub_setup_complete;

	switch (ctrl->bRequest) {
	/* we handle all standard USB descriptors */
	case USB_REQ_GET_DESCRIPTOR:
		if (ctrl->bRequestType != USB_DIR_IN)
			goto unknown;
		switch (w_value >> 8) {

		case USB_DT_DEVICE:
			value = min_t(unsigned short, w_length,
				      sizeof(device_desc));
			memcpy(req->buf, &device_desc, value);
			break;
		case USB_DT_DEVICE_QUALIFIER:
			if (!gadget_is_dualspeed(gadget))
				break;
			value = min_t(unsigned short, w_length,
				sizeof(struct usb_qualifier_descriptor));
			memcpy(req->buf, &dev_qualifier, value);
			break;
		case USB_DT_OTHER_SPEED_CONFIG:
			if (!gadget_is_dualspeed(gadget))
				break;
			/* FALLTHROUGH */
		case USB_DT_CONFIG:
			value = hub_config_buf(gadget, req->buf, w_value >> 8,
						w_value & 0xff);
			if (value >= 0)
				value = min_t(unsigned short, w_length, value);
			break;
		case USB_DT_STRING:
			value = usb_gadget_get_string(&stringtab,
						w_value & 0xff, req->buf);
			if (value >= 0)
				value = min_t(unsigned short, w_length, value);
			break;
		}
		break;

	/* any number of configs can work */
	case USB_REQ_SET_CONFIGURATION:
		if (ctrl->bRequestType != 0)
			goto unknown;
		if (gadget_is_otg(gadget)) {
			if (gadget->a_hnp_support)
				DBG(dev, "HNP available\n");
			else if (gadget->a_alt_hnp_support)
				DBG(dev, "HNP on another port\n");
			else
				VDBG(dev, "HNP inactive\n");
		}
		spin_lock(&dev->lock);
		value = hub_set_config(gadget, w_value);
		spin_unlock(&dev->lock);
		break;
	case USB_REQ_GET_CONFIGURATION:
		if (ctrl->bRequestType != USB_DIR_IN)
			goto unknown;
		*(u8 *)req->buf = dev->config;
		value = min_t(unsigned short, w_length, 1);
		break;

	case USB_REQ_SET_INTERFACE:
		if (ctrl->bRequestType != USB_RECIP_INTERFACE)
			goto unknown;
		if (!dev->config || w_index != 0)
			break;
		value = hub_set_alt(gadget);
		break;
	case USB_REQ_GET_INTERFACE:
		if (ctrl->bRequestType != (USB_DIR_IN | USB_RECIP_INTERFACE))
			goto unknown;
		if (!dev->config || w_index != 0)
			break;
		*((u8 *)req->buf) = 0;	/* always 0 */
		value = min_t(unsigned short, w_length, 1);
		break;
	default:
unknown:
		VDBG(dev,
			"non-core control req%02x.%02x v%04x i%04x l%d\n",
			ctrl->bRequestType, ctrl->bRequest,
			w_value, w_index, w_length);

		goto done;
	}

	/* respond with data transfer before status phase? */
	if (value >= 0) {
		req->length = value;
		req->zero = value < w_length;
		value = usb_ep_queue(gadget->ep0, req, GFP_ATOMIC);
		if (value < 0) {
			DBG(dev, "ep_queue --> %d\n", value);
			req->status = 0;
			hub_setup_complete(gadget->ep0, req);
		}
	}

done:
	/* device either stalls (value < 0) or reports success */
	return value;
}

static int hub_setup(struct usb_gadget *gadget,
			const struct usb_ctrlrequest *ctrl)
{
	struct hub_dev *dev = get_gadget_data(gadget);
	int value;

	VDBG(dev, "%s: %02x %02x %04x %04x %04x\n", __func__,
		ctrl->bRequestType, ctrl->bRequest, ctrl->wValue,
		ctrl->wIndex, ctrl->wLength);

	if ((ctrl->bRequestType & USB_TYPE_MASK) != USB_TYPE_STANDARD)
		value = hub_class_vendor_request(gadget, ctrl);
	else
		value = hub_standard_request(gadget, ctrl);

	return value;
}

static void __exit hub_unbind(struct usb_gadget *gadget)
{
	struct hub_dev *dev = get_gadget_data(gadget);

	if (dev->req)
	{
		if (dev->req->buf)
			kfree(dev->req->buf);
		usb_ep_free_request(gadget->ep0, dev->req);
	}

	dev->device_state = 0x00;
	kfree(dev);
	set_gadget_data(gadget, NULL);
}

static int __init hub_bind(struct usb_gadget *gadget)
{
	struct hub_dev *dev;
	struct usb_ep *ep;
	int i;

	dev = kzalloc(sizeof *dev, GFP_KERNEL);
	if (!dev)
		return -ENOMEM;
	spin_lock_init(&dev->lock);
	set_gadget_data(gadget, dev);
	dev->gadget = gadget;

	usb_ep_autoconfig_reset(gadget);

	ep = usb_ep_autoconfig(gadget, &fs_device_endp);
	if (!ep) {
		printk(KERN_ERR "%s: can't autoconfigure\n", shortname);
		return -ENODEV;
	}

	dev->req = usb_ep_alloc_request(gadget->ep0, GFP_KERNEL);
	if (!dev->req) {
		printk(KERN_ERR "%s: ep_alloc_request fail.\n", shortname);
		return -ENOMEM;
	}
	dev->req->buf = kmalloc(buflen, GFP_KERNEL);
	if (!dev->req->buf) {
		usb_ep_free_request(gadget->ep0, dev->req);
		printk(KERN_ERR "%s: kmalloc fail.\n", shortname);
		return -ENOMEM;
	}
	gadget->ep0->driver_data = dev;

#ifdef CONFIG_USB_GADGET_DUALSPEED
	hs_device_endp.bEndpointAddress = fs_device_endp.bEndpointAddress;
#endif
	ep->driver_data = dev;
	dev->ep = ep;
	dev->device_state = 0x00;
	dev->attach = 0x0000;
	dev->connected = 0x0000;
	dev->detach = 0x0000;
	dev->speed = USB_SPEED_UNKNOWN;
	dev->hub_req = NULL;
	dev->hub_resend = 0;

	for (i = 0; i < USB_HUB_PORTS; i++) {
		dev->port_status[i].wPortStatus = 0x0000;
		dev->port_status[i].wPortChange = 0x0000;
	}

	dev->hub_desc = &hub_device_desc;
	dev->hub_status = &hub_status;

	/* Set device descriptor idVendor, idProduct, bcdDevice */
	device_desc.idVendor  = cpu_to_le16(mod_data.idVendor);
	device_desc.idProduct = cpu_to_le16(mod_data.idProduct);
	device_desc.bcdDevice = cpu_to_le16(mod_data.bcdDevice);

	device_desc.bMaxPacketSize0 = gadget->ep0->maxpacket;
	dev_qualifier.bMaxPacketSize0 = gadget->ep0->maxpacket;

	usb_gadget_set_selfpowered(gadget);
	usb_gadget_connect(gadget);	/* default = pullup */

	printk(KERN_INFO "%s, version: " DRIVER_VERSION "\n", longname);

	return 0;
}

static void hub_disconnect(struct usb_gadget *gadget)
{
	hub_reset_config(gadget);
}

static void hub_resume(struct usb_gadget *gadget)
{
	struct hub_dev		*dev = get_gadget_data(gadget);

	dev->device_state = 0x00;
	send_device_status(dev->ep);
}

static struct usb_gadget_driver hub_driver = {
	.max_speed	= USB_SPEED_HIGH,
	.function	= (char *)longname,
	.bind		= hub_bind,
	.unbind		= hub_unbind,
	.disconnect	= hub_disconnect,
	.setup		= hub_setup,
	.suspend	= NULL,
	.resume		= hub_resume,

	.driver		= {
		.name		= (char *)shortname,
		.owner		= THIS_MODULE,
	},
};

static int __init hub_interface_init(void)
{
	strlcpy(manufacturer, "no manufacturer", sizeof manufacturer);
	strlcpy(serial, "0123456789", sizeof serial);

	return usb_gadget_probe_driver(&hub_driver);
}
module_init(hub_interface_init);

static void __exit hub_interface_cleanup(void)
{
	usb_gadget_unregister_driver(&hub_driver);
}
module_exit(hub_interface_cleanup);

MODULE_AUTHOR("Renesas Solutions Corp.");
MODULE_LICENSE("GPL");

