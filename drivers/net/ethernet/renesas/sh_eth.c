/*
 *  SuperH Ethernet device driver
 *
 *  Copyright (C) 2006-2012 Nobuhiro Iwamatsu
 *  Copyright (C) 2008-2012 Renesas Solutions Corp.
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms and conditions of the GNU General Public License,
 *  version 2, as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *  more details.
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  The full GNU General Public License is included in this distribution in
 *  the file called "COPYING".
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/etherdevice.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/mdio-bitbang.h>
#include <linux/netdevice.h>
#include <linux/phy.h>
#include <linux/cache.h>
#include <linux/io.h>
#include <linux/pm_runtime.h>
#include <linux/slab.h>
#include <linux/ethtool.h>
#include <linux/if_vlan.h>
#include <linux/clk.h>
#include <linux/sh_eth.h>

#define IDRAC_SUPPORT 1

#ifdef IDRAC_SUPPORT /* { */
#include <linux/aess_debugfs.h>
/** Ethtool support: Start*/
#include <linux/ethtool.h>
/** Ethtool support: End*/

#define POLL_LINK_STATUS
#define POLL_TIMEOUT        (10)

#define CFG_PROJ_ETHERCLONE 1

#ifdef CFG_PROJ_ETHERCLONE /* { */
#include <linux/if_ether.h>
#include <linux/if_vlan.h>
#endif /* } CFG_PROJ_ETHERCLONE */
#define SYS_TYPE_MONOLITHIC     0
#define SYS_TYPE_BLADE          1
static unsigned int system_type = SYS_TYPE_MONOLITHIC;

#ifdef CONFIG_NCSI
#include "../../ncsi/ncsi_protocol.h"
/** add extern API */
extern int ncsi_rx_handler(struct sk_buff *skb);
#endif
#endif /* } IDRAC_SUPPORT */

#include "sh_eth.h"

#ifdef IDRAC_SUPPORT /* { */
static int DNIC = 0;
static struct platform_device *temp_pdev;
static struct sh_eth_plat_data *temp_pd;

static int sh_mdio_release(struct net_device *ndev);
static int sh_mdio_init(struct net_device *ndev, int id, struct sh_eth_plat_data *pd);

#ifdef CFG_PROJ_ETHERCLONE /* { */
static int clone_open(struct net_device *ndev);
static int clone_close(struct net_device *ndev);
void clone_set_multicast_list(struct net_device *clone_dev);
static int clone_start_xmit(struct sk_buff *skb, struct net_device *dev);
static int Dell_get_system_type(void);
#endif /* } CFG_PROJ_ETHERCLONE */
extern unsigned int idrac_type;

static char *driver_name="aess_nicdrv"; /**< NIC driver name */

#define HW_WDG_RESET_COUNT   1000 /** < The threshold of interrupt for reseting watchdog.*/
#define MAX_MAC_NO     3       /**< NIC interface name for MAC0*/
#define SET_MAC_IF (SIOCDEVPRIVATE) /**< It is a NIC ioctl command. Command number is SIOCDEVPRIVATE*/
#define IF_NONE      0x0    /**< Network interface: There is no physical interface connected to MAC.*/
#define IF_RMII_PHY  0x01   /**< Network interface: MAC is connected to an external PHY(RMII).*/
#define IF_MII_PHY   0x02   /**< Network interface: MAC is connected to an external PHY(MII).*/
#define IF_GMII_PHY  0x04   /**< Network interface: MAC is connected to an external PHY(GMII).*/
#define IF_NCSI      0x10   /**< Network interface: MAC is a shared-NIC.*/
#define IF_RMII_PHY_NCSI  (IF_RMII_PHY|IF_NCSI) /**< Network interface: MAC is connected to an external PHY(RMII) and This PHY is connected to a NCSI EVB.*/
#define IF_MII_PHY_NCSI   (IF_MII_PHY|IF_NCSI) /**< Network interface: MAC is connected to an external PHY(MII) and This PHY is connected to a NCSI EVB.*/
#define IF_GMII_PHY_NCSI  (IF_GMII_PHY|IF_NCSI) /**< Network interface: MAC is connected to an external PHY(GMII) and This PHY is connected to a NCSI EVB.*/
#define IS_IF_VALID(a) (!(IF_NONE==a)) /**< Check if the interface is IF_NONE.*/

static unsigned char mac1[6] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x1};
static unsigned char mac2[6] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x2};
static unsigned char mac3[6] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x3};

struct net_device *pnetwork_dev[MAX_MAC_NO] = {NULL,NULL,NULL};

#ifdef BROADCAST_PROTECTION /* { */
static int nBroadStormPackets = 5000;
static int nBroadStormInterval = 100; // The unit is 10ms
static int nBroadStormPacketsActive = 500;
static int nBroadStormIntervalActive  = 10; // The unit is 10ms
static int nBroadStormClearIntervals = 50; //times
#endif  /* } BROADCAST_PROTECTION */

#ifdef UNICAST_PROTECTION
static unsigned char UnicastStormMsgDump = 0;
static unsigned char UnicastStormCount = 20;	/* unit: 1 weight (32 packets) */
static unsigned char UnicastStormDelay = 1;		/* unit: 10 ms */
static unsigned int  UnicastStormPacket = 120;	/* number of packets per jiffies */
static unsigned char UnicastStormMaxCount = 0;
static unsigned int  UnicastStormMaxPacket = 0;
#endif

#ifdef PHY_LAST_FORCE_SETTING
static struct ethtool_cmd phy_last_cmd;
#endif
static int aess_wdt_keepalive(struct file *file){ return 0; };

#ifdef ETH_DEBUG_MSG
static unsigned char OnlyRxMsgDump = 0;
static unsigned char OnlyTxMsgDump = 0;
static unsigned char EnableMsgDump = 0;
static unsigned char NoNCSIMsgDump = 0;
static unsigned char OnlyNCSIMsgDump = 1;
static unsigned char DecodeNCSIMsgDump = 1;
static unsigned char FilterNCSIMsgDump1 = 0xff;
static unsigned char FilterNCSIMsgDump2 = 0xff;
static unsigned char NoLinkMsgDump = 1;
static unsigned char NoGetParametersMsgDump = 1;
static unsigned char NoOEMMsgDump = 1;
#define NCSI_CMD_HISTORY_PAGE_SIZE 128
#define NCSI_CMD_HISTORY_BUFFER_SIZE 1024
static unsigned char EnableNCSICmdHistory = 0;
static unsigned char NCSICmdHistorytPage = 0;
static unsigned int  NCSICmdHistorytIndex = 0;
static unsigned char NCSICmdHistorytData[NCSI_CMD_HISTORY_BUFFER_SIZE];
static unsigned long NCSICmdHistoryTimestamp[NCSI_CMD_HISTORY_BUFFER_SIZE];
#endif

#ifdef PHY_DEBUG
static unsigned char PHYDebug = 0;
static unsigned char PHYReg = 0x00;
static unsigned char PHYShadow = 0x00;
static u16 PHYData = 0;
#endif

static unsigned char NetIfMsgSwitch = 0;
#endif /* } IDRAC_SUPPORT */

#define SH_ETH_DEF_MSG_ENABLE \
        (NETIF_MSG_LINK | \
        NETIF_MSG_TIMER | \
        NETIF_MSG_RX_ERR| \
        NETIF_MSG_TX_ERR)

#ifdef IDRAC_SUPPORT /* { */
static unsigned long u32BufSize = 0;
/** Sysfs support: Start*/
/*
struct device_driver aess_nic_device_driver = {
                .name = "aess_nicdrv",
                .bus = &platform_bus_type,
                .owner = THIS_MODULE
                };
*/

struct platform_driver aess_nic_device_driver = {
	.driver = {
                .name = "aess_nicdrv",
                .owner = THIS_MODULE,
	}
};


#define SYS_PLANAR_TYPE_12G     0
#define SYS_PLANAR_TYPE_13G     1
static unsigned int planar_type = SYS_PLANAR_TYPE_12G;
#define MIN_SYS_PLANAR_ID_13G   0x50
#define PANTERA_PLANAR_ID       0x62
#define PANTERA_IDRAC           1
static unsigned int planar_id = 0;

static int Dell_get_system_type(void)
{
#define ADDRESS_OF_SYSTEM_TYPE  0x14000006
#define SYSTEM_TYPE_BIT_POSITION 3
#define MASK_OF_SYSTEM_TYPE     0x08
#define LEN_OF_SYSTEM_TYPE      1

    int ret = 0;
    unsigned int tmp_type = 0xffffff;
    struct resource     *tmp_mem;
    void __iomem        *tmp_ioaddr;
    static int get_system_type = 1;

    if(!get_system_type) {
        return 0;
    }

    get_system_type = 0;

    /** - Request the memory region for AMEA present */
    tmp_mem = request_mem_region(ADDRESS_OF_SYSTEM_TYPE,
                                 LEN_OF_SYSTEM_TYPE, "sh7757_eth");
    if (!tmp_mem) {
        printk(KERN_ERR "%s: failed to request io memory region 0x%x.\n",
                __FUNCTION__, ADDRESS_OF_SYSTEM_TYPE);
        return 0;
    }
    /** - Remap the memory region */
    tmp_ioaddr = ioremap_nocache(ADDRESS_OF_SYSTEM_TYPE, 1);
    if (0 == tmp_ioaddr) {
        release_mem_region(ADDRESS_OF_SYSTEM_TYPE, LEN_OF_SYSTEM_TYPE);
        printk(KERN_ERR "%s: failed to ioremap_nocache() io memory region.\n",
                __FUNCTION__);
        return 0;
    }
    /** - Read the status */
    tmp_type = (ioread8(tmp_ioaddr) & MASK_OF_SYSTEM_TYPE) >> SYSTEM_TYPE_BIT_POSITION;

    if (tmp_type == SYS_TYPE_BLADE)
        system_type = SYS_TYPE_BLADE;
    else
        system_type = SYS_TYPE_MONOLITHIC;

    if (system_type == SYS_TYPE_BLADE)
        printk(KERN_EMERG"\nBlade\n");
    else
        printk(KERN_EMERG"\nMonolithic/DRB\n");
    /** - Unmap the memory region */
    iounmap(tmp_ioaddr);

    /** - Release the memory region */
    release_mem_region(ADDRESS_OF_SYSTEM_TYPE, LEN_OF_SYSTEM_TYPE);
    /** - Return the status */
    return ret;
}

static int Dell_get_planar_type(void)
{
#define	ADDRESS_OF_PLANAR_TYPE	0x14000003
#define LEN_OF_PLANAR_TYPE		2

	int	ret = 0;
	unsigned int tmp_type = 0xffffff;
	struct resource		*tmp_mem;
	void __iomem 		*tmp_ioaddr;

	/** - Request the memory region for AMEA present */
	tmp_mem = request_mem_region(ADDRESS_OF_PLANAR_TYPE,
								 LEN_OF_PLANAR_TYPE, "sh7757_eth");
	if (!tmp_mem) {
		printk(KERN_ERR "%s: failed to request io memory region 0x%x.\n",
				__FUNCTION__, ADDRESS_OF_PLANAR_TYPE);
		return 0;
	}
	/** - Remap the memory region */
	tmp_ioaddr = ioremap_nocache(ADDRESS_OF_PLANAR_TYPE, LEN_OF_PLANAR_TYPE);
	if (0 == tmp_ioaddr) {
		release_mem_region(ADDRESS_OF_PLANAR_TYPE, LEN_OF_PLANAR_TYPE);
		printk(KERN_ERR "%s: failed to ioremap_nocache() io memory region.\n",
				__FUNCTION__);
		return 0;
	}
	/** - Read the status */

	tmp_type = ioread16(tmp_ioaddr);
    tmp_type = htons(tmp_type);
    planar_id = ((tmp_type & 0x0f) | ((tmp_type & 0x0f00) >> 4));
    printk("Dell_get_planar_type: id %x\n",planar_id);

    if(planar_id >= MIN_SYS_PLANAR_ID_13G)
    {
       planar_type = SYS_PLANAR_TYPE_13G;
       printk("13G System\n");
    }
    else
    {
        planar_type = SYS_PLANAR_TYPE_12G;
        printk("12G System\n");
    }

	/** - Unmap the memory region */
	iounmap(tmp_ioaddr);

	/** - Release the memory region */
	release_mem_region(ADDRESS_OF_PLANAR_TYPE, LEN_OF_PLANAR_TYPE);
	/** - Return the status */
	return ret;
}

static ssize_t set_value(struct device_driver *drv, const char *buf, size_t count)
{
    /** @scope */
    /** - Do nothing currently.*/
    u32BufSize = simple_strtoul(&buf[0],NULL,0);
    return (strlen(buf));
}

static ssize_t show_value(struct device_driver *drv, char *buf)
{

  char p[3072], *s;
  int i = 0;
  struct net_device *pndev = NULL;
    u32 ioaddr = 0;
    struct sh_eth_private *mdp=NULL;

  /** @scope */
  s = p;

  /** - Show values of MAC registers.*/
      s += sprintf(s, "MAC  Registers\n");
      s += sprintf(s, "=================\n");
    for (i = 0; i < MAX_MAC_NO; i++)
    {

    pndev = (struct net_device *)pnetwork_dev[i];
    mdp = netdev_priv(pndev);
    ioaddr = pndev->base_addr;
    s += sprintf(s, "    EDMR%d  0x%x\n",i,ctrl_inl(ioaddr + EDMR));
    s += sprintf(s, "   EDTRR%d  0x%x\n",i,ctrl_inl(ioaddr + EDTRR));
    s += sprintf(s, "   EDRRR%d  0x%x\n",i,ctrl_inl(ioaddr + EDRRR));
    s += sprintf(s, "   TDLAR%d  0x%x\n",i,ctrl_inl(ioaddr + TDLAR));
    s += sprintf(s, "   RDLAR%d  0x%x\n",i,ctrl_inl(ioaddr + RDLAR));
    s += sprintf(s, "    EESR%d  0x%x\n",i,ctrl_inl(ioaddr + EESR));
    s += sprintf(s, "  EESIPR%d  0x%x\n",i,ctrl_inl(ioaddr + EESIPR));
    s += sprintf(s, "  TRSCER%d  0x%x\n",i,ctrl_inl(ioaddr + TRSCER));
    s += sprintf(s, "   RMFCR%d  0x%x\n",i,ctrl_inl(ioaddr + RMFCR));
    s += sprintf(s, "    TFTR%d  0x%x\n",i,ctrl_inl(ioaddr + TFTR));
    s += sprintf(s, "     FDR%d  0x%x\n",i,ctrl_inl(ioaddr + FDR));
    s += sprintf(s, "    RMCR%d  0x%x\n",i,ctrl_inl(ioaddr + RMCR));
    s += sprintf(s, "   FCFTR%d  0x%x\n",i,ctrl_inl(ioaddr + FCFTR));
    s += sprintf(s, "  RPADIR%d  0x%x\n",i,ctrl_inl(ioaddr + RPADIR));
    s += sprintf(s, "   TRIMD%d  0x%x\n",i,ctrl_inl(ioaddr + TRIMD));
    s += sprintf(s, "   RBWAR%d  0x%x\n",i,ctrl_inl(ioaddr + RBWAR));
    s += sprintf(s, "   RDFAR%d  0x%x\n",i,ctrl_inl(ioaddr + RDFAR));
    s += sprintf(s, "   TBRAR%d  0x%x\n",i,ctrl_inl(ioaddr + TBRAR));
    s += sprintf(s, "   TDFAR%d  0x%x\n",i,ctrl_inl(ioaddr + TDFAR));
    s += sprintf(s, "    ECMR%d  0x%x\n",i,ctrl_inl(ioaddr + ECMR));
    s += sprintf(s, "    RFLR%d  0x%x\n",i,ctrl_inl(ioaddr + RFLR));
    s += sprintf(s, "    ECSR%d  0x%x\n",i,ctrl_inl(ioaddr + ECSR));
    s += sprintf(s, "  ECSIPR%d  0x%x\n",i,ctrl_inl(ioaddr + ECSIPR));
    s += sprintf(s, "     PIR%d  0x%x\n",i,ctrl_inl(ioaddr + PIR));
    s += sprintf(s, "   RDMLR%d  0x%x\n",i,ctrl_inl(ioaddr + RDMLR));
    s += sprintf(s, "    IPGR%d  0x%x\n",i,ctrl_inl(ioaddr + IPGR));
    s += sprintf(s, "     APR%d  0x%x\n",i,ctrl_inl(ioaddr + APR));
    s += sprintf(s, "     MPR%d  0x%x\n",i,ctrl_inl(ioaddr + MPR));
    s += sprintf(s, " TPAUSER%d  0x%x\n",i,ctrl_inl(ioaddr + TPAUSER));
    s += sprintf(s, "    MAHR%d  0x%x\n",i,ctrl_inl(ioaddr + MAHR));
    s += sprintf(s, "    MALR%d  0x%x\n",i,ctrl_inl(ioaddr + MALR));
    s += sprintf(s, "   TROCR%d  0x%x\n",i,ctrl_inl(ioaddr + TROCR));
    s += sprintf(s, "    CDCR%d  0x%x\n",i,ctrl_inl(ioaddr + CDCR));
    s += sprintf(s, "    LCCR%d  0x%x\n",i,ctrl_inl(ioaddr + LCCR));
    s += sprintf(s, "   CNDCR%d  0x%x\n",i,ctrl_inl(ioaddr + CNDCR));
    s += sprintf(s, "   CEFCR%d  0x%x\n",i,ctrl_inl(ioaddr + CEFCR));
    s += sprintf(s, "   FRECR%d  0x%x\n",i,ctrl_inl(ioaddr + FRECR));
    s += sprintf(s, "  TSFRCR%d  0x%x\n",i,ctrl_inl(ioaddr + TSFRCR));
    s += sprintf(s, "  TLFRCR%d  0x%x\n",i,ctrl_inl(ioaddr + TLFRCR));
    s += sprintf(s, "    RFCR%d  0x%x\n",i,ctrl_inl(ioaddr + RFCR));
    s += sprintf(s, "   MAFCR%d  0x%x\n",i,ctrl_inl(ioaddr + MAFCR));
    s += sprintf(s, "  RTRATE%d  0x%x\n",i,ctrl_inl(ioaddr + RTRATE));
    }

  return sprintf(buf, "%s", p);
}


#ifdef ETH_DEBUG_MSG
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_history_set_value
******************************************************************************/
/**
 *  @brief      Ued for sysfs writing, do nothing right now.
 *
 *  @return     Size of buffer.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Static function\n
 *
 *****************************************************************************/
static ssize_t ncsi_cmd_history_set_value(
                                           /** Linux device driver. */
                                           struct device_driver *drv,

                                           /** User space buffer. */
                                           const char *buf,

                                           /** Buffer size*/
                                           size_t count
                                         )
{
    unsigned char page;
    /** @scope */
    /** - Do nothing currently.*/
    page = simple_strtoul(&buf[0], NULL, 0) & 0xff;

    if (page > ((NCSI_CMD_HISTORY_BUFFER_SIZE / NCSI_CMD_HISTORY_PAGE_SIZE) - 1))
        NCSICmdHistorytPage = 0;
    else
        NCSICmdHistorytPage = page;

    return (strlen(buf));
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_history_show_value
******************************************************************************/
/**
 *  @brief      Ued for sysfs reading, showing values of NCSI command history.
 *
 *  @return     Size of buffer.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Static function\n
 *
 *****************************************************************************/
static ssize_t ncsi_cmd_history_show_value(
                                            /** Linux device driver. */
                                            struct device_driver *drv,

                                            /** Buffer to user space. */
                                            char *buf
                                          )
{
    char p[3096], *s;
    int i, j;
    int offset;

    if (NCSICmdHistorytPage > ((NCSI_CMD_HISTORY_BUFFER_SIZE / NCSI_CMD_HISTORY_PAGE_SIZE) - 1))
        NCSICmdHistorytPage = 0;

    offset = NCSICmdHistorytPage * NCSI_CMD_HISTORY_PAGE_SIZE;

    s = p;

    s += sprintf(s, "Index.Time.Data\n");

    for (i = 0; i < (NCSI_CMD_HISTORY_PAGE_SIZE / 4); i++)
    {
        for (j = 0; j < 4; j++)
        {
            s += sprintf(s,
                         "%04ld.%08lx.%04x ",
                         offset + i + ((NCSI_CMD_HISTORY_PAGE_SIZE / 4) * j),
                         NCSICmdHistoryTimestamp[offset + i + ((NCSI_CMD_HISTORY_PAGE_SIZE / 4) * j)],
                         NCSICmdHistorytData[offset + i + ((NCSI_CMD_HISTORY_PAGE_SIZE / 4) * j)]);
        }
        s += sprintf(s, "\n");
    }

    return sprintf(buf, "%s", p);
}
#endif

struct driver_attribute drv_attr_regvalues =
{
    .attr = {.name = "MACRegisterValues" ,
             .mode = S_IWUSR | S_IRUGO },
    .show   = show_value,
    .store  = set_value,
};

#ifdef ETH_DEBUG_MSG
struct driver_attribute drv_attr_ncsi_cmd_history =
{
    .attr = {.name = "NCSICmdHistory" ,
             .mode = S_IWUSR | S_IRUGO },
    .show   = ncsi_cmd_history_show_value,
    .store  = ncsi_cmd_history_set_value,
};
#endif

/******************************************************************************
*   FUNCTION        :   packet_counter
******************************************************************************/
/**
 *  @brief      Protect F/W stuck when network storm comes.
 *
 *  @return     void
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Static function\n
 *
 *****************************************************************************/
static void packet_counter(void)
{
    static volatile unsigned long interrupt_count = 0;
    /** @scope */

    /** - Caculate the number of interrupt. If the number > 1000, then reset watchdog.*/
    interrupt_count ++;
    if ( interrupt_count > HW_WDG_RESET_COUNT )
    {
       aess_wdt_keepalive(NULL);
       interrupt_count = 0;
    }
}
#endif /* } IDRAC_SUPPORT */

/* There is CPU dependent code */
#if defined(CONFIG_CPU_SUBTYPE_SH7724)
#define SH_ETH_RESET_DEFAULT    1
static void sh_eth_set_duplex(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    if (mdp->duplex) /* Full */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_DM, ECMR);
    else        /* Half */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) & ~ECMR_DM, ECMR);
}

static void sh_eth_set_rate(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    switch (mdp->speed) {
    case 10: /* 10BASE */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) & ~ECMR_RTM, ECMR);
        break;
    case 100:/* 100BASE */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_RTM, ECMR);
        break;
    default:
        break;
    }
}

/* SH7724 */
static struct sh_eth_cpu_data sh_eth_my_cpu_data = {
    .set_duplex = sh_eth_set_duplex,
    .set_rate   = sh_eth_set_rate,

    .ecsr_value = ECSR_PSRTO | ECSR_LCHNG | ECSR_ICD,
    .ecsipr_value   = ECSIPR_PSRTOIP | ECSIPR_LCHNGIP | ECSIPR_ICDIP,
    .eesipr_value   = DMAC_M_RFRMER | DMAC_M_ECI | 0x01ff009f,

    .tx_check   = EESR_FTC | EESR_CND | EESR_DLC | EESR_CD | EESR_RTO,
    .eesr_err_check = EESR_TWB | EESR_TABT | EESR_RABT | EESR_RDE |
              EESR_RFRMER | EESR_TFE | EESR_TDE | EESR_ECI | EESR_RFE,
    .tx_error_check = EESR_TWB | EESR_TABT | EESR_TDE | EESR_TFE,

    .apr        = 1,
    .mpr        = 1,
    .tpauser    = 1,
    .hw_swap    = 1,
    .rpadir     = 1,
    .rpadir_value   = 0x00020000, /* NET_IP_ALIGN assumed to be 2 */
};
#elif defined(CONFIG_CPU_SUBTYPE_SH7757)
#define SH_ETH_HAS_BOTH_MODULES 1
#define SH_ETH_HAS_TSU  1
static int sh_eth_check_reset(struct net_device *ndev);

static void sh_eth_set_duplex(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    if (mdp->duplex) /* Full */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_DM, ECMR);
    else        /* Half */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) & ~ECMR_DM, ECMR);
}

static void sh_eth_set_rate(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    switch (mdp->speed) {
    case 10: /* 10BASE */
        sh_eth_write(ndev, 0, RTRATE);
        break;
    case 100:/* 100BASE */
        sh_eth_write(ndev, 1, RTRATE);
        break;
    default:
        break;
    }
}

#ifdef BROADCAST_PROTECTION /* { */
static void sh_eth_set_broadcast_cnt(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    sh_eth_write(ndev, mdp->cd->bcfrr_value, BCFRR);
    //printk(KERN_DEBUG "%s: The value of the BCFRR register is %x \n",__FUNCTION__, (unsigned int)sh_eth_read(ndev, BCFRR));
}

static int sh_eth_get_broadcast_rst_cnt(struct net_device *ndev)
{
    //printk(KERN_DEBUG "%s: The value of the CRBCFRR register is %x \n",__FUNCTION__,(unsigned int)sh_eth_read(ndev, CRBCFRR));
    return (sh_eth_read(ndev, CRBCFRR));
}

static void sh_eth_clear_broadcast_rst_cnt(struct net_device *ndev)
{
    sh_eth_write(ndev, 0 , CRBCFRR);
}
#endif  /* } BROADCAST_PROTECTION */

/* SH7757 */
static struct sh_eth_cpu_data sh_eth_my_cpu_data = {
    .set_duplex     = sh_eth_set_duplex,
    .set_rate       = sh_eth_set_rate,
#ifdef BROADCAST_PROTECTION /* { */
  .set_broadcast_cnt        = sh_eth_set_broadcast_cnt,
  .get_broadcast_rst_cnt    = sh_eth_get_broadcast_rst_cnt,
  .clear_broadcast_rst_cnt  = sh_eth_clear_broadcast_rst_cnt,
#endif  /* } BROADCAST_PROTECTION */

    .eesipr_value   = DMAC_M_RFRMER | DMAC_M_ECI | 0x003fffff,
    .rmcr_value = 0x00000001,
#ifdef BROADCAST_PROTECTION /* { */
    .bcfrr_value = 0x00000000,
    .broadcast_storm_clear_cnt = 0x00000000,
    .receive_packet_cnt = 0x00000000,
    .network_storm_threshold = 1000,
#endif  /* } BROADCAST_PROTECTION */

    .tx_check   = EESR_FTC | EESR_CND | EESR_DLC | EESR_CD | EESR_RTO,
    .eesr_err_check = EESR_TWB | EESR_TABT | EESR_RABT | EESR_RDE |
              EESR_RFRMER | EESR_TFE | EESR_TDE | EESR_ECI | EESR_RFE | EESR_CELF,
    .tx_error_check = EESR_TWB | EESR_TABT | EESR_TDE | EESR_TFE,

    .apr        = 1,
    .mpr        = 1,
    .tpauser    = 1,
    .hw_swap    = 1,
    .no_ade     = 1,
    .rpadir     = 1,
    .rpadir_value   = 2 << 16,
};

#define SH_GIGA_ETH_BASE    0xfee00000
#define GIGA_MALR(port)     (SH_GIGA_ETH_BASE + 0x800 * (port) + 0x05c8)
#define GIGA_MAHR(port)     (SH_GIGA_ETH_BASE + 0x800 * (port) + 0x05c0)
static void sh_eth_chip_reset_giga(struct net_device *ndev)
{
    int i;
    unsigned long mahr[2], malr[2];

    /* save MAHR and MALR */
    for (i = 0; i < 2; i++) {
        malr[i] = ioread32((void *)GIGA_MALR(i));
        mahr[i] = ioread32((void *)GIGA_MAHR(i));
    }

    /* reset device */
    iowrite32(ARSTR_ARSTR, (void *)(SH_GIGA_ETH_BASE + 0x1800));
    mdelay(1);

    /* restore MAHR and MALR */
    for (i = 0; i < 2; i++) {
        iowrite32(malr[i], (void *)GIGA_MALR(i));
        iowrite32(mahr[i], (void *)GIGA_MAHR(i));
    }
}

static int sh_eth_is_gether(struct sh_eth_private *mdp);
static int sh_eth_reset(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int ret = 0;

    if (sh_eth_is_gether(mdp)) {
        sh_eth_write(ndev, 0x03, EDSR);
        sh_eth_write(ndev, sh_eth_read(ndev, EDMR) | EDMR_SRST_GETHER,
                EDMR);

        ret = sh_eth_check_reset(ndev);
        if (ret)
            goto out;

        /* Table Init */
        sh_eth_write(ndev, 0x0, TDLAR);
        sh_eth_write(ndev, 0x0, TDFAR);
        sh_eth_write(ndev, 0x0, TDFXR);
        sh_eth_write(ndev, 0x0, TDFFR);
        sh_eth_write(ndev, 0x0, RDLAR);
        sh_eth_write(ndev, 0x0, RDFAR);
        sh_eth_write(ndev, 0x0, RDFXR);
        sh_eth_write(ndev, 0x0, RDFFR);
    } else {
        sh_eth_write(ndev, sh_eth_read(ndev, EDMR) | EDMR_SRST_ETHER,
                EDMR);
        mdelay(3);
        sh_eth_write(ndev, sh_eth_read(ndev, EDMR) & ~EDMR_SRST_ETHER,
                EDMR);
    }

out:
    return ret;
}

static void sh_eth_set_duplex_giga(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    if (mdp->duplex) /* Full */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_DM, ECMR);
    else        /* Half */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) & ~ECMR_DM, ECMR);
}

static void sh_eth_set_rate_giga(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    switch (mdp->speed) {
    case 10: /* 10BASE */
        sh_eth_write(ndev, 0x00000000, GECMR);
        break;
    case 100:/* 100BASE */
        sh_eth_write(ndev, 0x00000010, GECMR);
        break;
    case 1000: /* 1000BASE */
        sh_eth_write(ndev, 0x00000020, GECMR);
        break;
    default:
        break;
    }
}

#ifdef BROADCAST_PROTECTION /* { */
static void sh_eth_set_broadcast_cnt_giga(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    sh_eth_write(ndev, mdp->cd->bcfrr_value, BCFRR);
    //printk(KERN_DEBUG "%s:The value of the BCFRR register is %x \n",__FUNCTION__,(unsigned int)sh_eth_read(ndev, BCFRR));
}

static int sh_eth_get_broadcast_rst_cnt_giga(struct net_device *ndev)
{
    //printk(KERN_DEBUG "%s:The value of the CRBCFRR register is %x \n",__FUNCTION__, (unsigned int)sh_eth_read(ndev, CRBCFRR));
    return (sh_eth_read(ndev, CRBCFRR));

}

static void sh_eth_clear_broadcast_rst_cnt_giga(struct net_device *ndev)
{
    sh_eth_write(ndev, 0, CRBCFRR);
}
#endif  /* } BROADCAST_PROTECTION */

/* SH7757(GETHERC) */
static struct sh_eth_cpu_data sh_eth_my_cpu_data_giga = {
    .chip_reset = sh_eth_chip_reset_giga,
    .set_duplex = sh_eth_set_duplex_giga,
    .set_rate   = sh_eth_set_rate_giga,
#ifdef BROADCAST_PROTECTION /* { */
  .set_broadcast_cnt        = sh_eth_set_broadcast_cnt_giga,
  .get_broadcast_rst_cnt    = sh_eth_get_broadcast_rst_cnt_giga,
  .clear_broadcast_rst_cnt  = sh_eth_clear_broadcast_rst_cnt_giga,
#endif  /* } BROADCAST_PROTECTION */

    .ecsr_value = ECSR_ICD | ECSR_MPD,
    .ecsipr_value   = ECSIPR_LCHNGIP | ECSIPR_ICDIP | ECSIPR_MPDIP,
    .eesipr_value   = DMAC_M_RFRMER | DMAC_M_ECI | 0x003fffff,

    .tx_check   = EESR_TC1 | EESR_FTC,
    .eesr_err_check = EESR_TWB1 | EESR_TWB | EESR_TABT | EESR_RABT | \
              EESR_RDE | EESR_RFRMER | EESR_TFE | EESR_TDE | \
              EESR_ECI | EESR_RFE | EESR_CELF,
    .tx_error_check = EESR_TWB1 | EESR_TWB | EESR_TABT | EESR_TDE | \
              EESR_TFE,
    .fdr_value  = 0x0000072f,
    .rmcr_value = 0x00000001,
#ifdef BROADCAST_PROTECTION /* { */
    .bcfrr_value = 0x00000000,
    .broadcast_storm_clear_cnt = 0x00000000,
    .receive_packet_cnt = 0x00000000,
    .network_storm_threshold = 1000,
#endif  /* } BROADCAST_PROTECTION */

    .apr        = 1,
    .mpr        = 1,
    .tpauser    = 1,
    .bculr      = 1,
    .hw_swap    = 1,
    .rpadir     = 1,
    .rpadir_value   = 2 << 16,
    .no_trimd   = 1,
    .no_ade     = 1,
    .tsu        = 1,
};

static struct sh_eth_cpu_data *sh_eth_get_cpu_data(struct sh_eth_private *mdp)
{
    if (sh_eth_is_gether(mdp))
        return &sh_eth_my_cpu_data_giga;
    else
        return &sh_eth_my_cpu_data;
}

#elif defined(CONFIG_CPU_SUBTYPE_SH7734) || defined(CONFIG_CPU_SUBTYPE_SH7763)
#define SH_ETH_HAS_TSU  1
static int sh_eth_check_reset(struct net_device *ndev);
static void sh_eth_reset_hw_crc(struct net_device *ndev);
static void sh_eth_chip_reset(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    /* reset device */
    sh_eth_tsu_write(mdp, ARSTR_ARSTR, ARSTR);
    mdelay(1);
}

static int sh_eth_reset(struct net_device *ndev)
{
    int ret = 0;

    sh_eth_write(ndev, EDSR_ENALL, EDSR);
    sh_eth_write(ndev, sh_eth_read(ndev, EDMR) | EDMR_SRST_GETHER, EDMR);

    ret = sh_eth_check_reset(ndev);
    if (ret)
        goto out;

    /* Table Init */
    sh_eth_write(ndev, 0x0, TDLAR);
    sh_eth_write(ndev, 0x0, TDFAR);
    sh_eth_write(ndev, 0x0, TDFXR);
    sh_eth_write(ndev, 0x0, TDFFR);
    sh_eth_write(ndev, 0x0, RDLAR);
    sh_eth_write(ndev, 0x0, RDFAR);
    sh_eth_write(ndev, 0x0, RDFXR);
    sh_eth_write(ndev, 0x0, RDFFR);

    /* Reset HW CRC register */
    sh_eth_reset_hw_crc(ndev);
}

static void sh_eth_set_duplex(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    if (mdp->duplex) /* Full */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_DM, ECMR);
    else        /* Half */
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) & ~ECMR_DM, ECMR);
}

static void sh_eth_set_rate(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    switch (mdp->speed) {
    case 10: /* 10BASE */
        sh_eth_write(ndev, GECMR_10, GECMR);
        break;
    case 100:/* 100BASE */
        sh_eth_write(ndev, GECMR_100, GECMR);
        break;
    case 1000: /* 1000BASE */
        sh_eth_write(ndev, GECMR_1000, GECMR);
        break;
    default:
        break;
    }
}

/* sh7763 */
static struct sh_eth_cpu_data sh_eth_my_cpu_data = {
    .chip_reset = sh_eth_chip_reset,
    .set_duplex = sh_eth_set_duplex,
    .set_rate   = sh_eth_set_rate,

    .ecsr_value = ECSR_ICD | ECSR_MPD,
    .ecsipr_value   = ECSIPR_LCHNGIP | ECSIPR_ICDIP | ECSIPR_MPDIP,
    .eesipr_value   = DMAC_M_RFRMER | DMAC_M_ECI | 0x003fffff,

    .tx_check   = EESR_TC1 | EESR_FTC,
    .eesr_err_check = EESR_TWB1 | EESR_TWB | EESR_TABT | EESR_RABT | \
              EESR_RDE | EESR_RFRMER | EESR_TFE | EESR_TDE | \
              EESR_ECI | EESR_RFE,
    .tx_error_check = EESR_TWB1 | EESR_TWB | EESR_TABT | EESR_TDE | \
              EESR_TFE,

    .apr        = 1,
    .mpr        = 1,
    .tpauser    = 1,
    .bculr      = 1,
    .hw_swap    = 1,
    .no_trimd   = 1,
    .no_ade     = 1,
    .tsu        = 1,
#if defined(CONFIG_CPU_SUBTYPE_SH7734)
    .hw_crc     = 1,
#endif
};

static void sh_eth_reset_hw_crc(struct net_device *ndev)
{
    if (sh_eth_my_cpu_data.hw_crc)
        sh_eth_write(ndev, 0x0, CSMR);
}

#elif defined(CONFIG_CPU_SUBTYPE_SH7619)
#define SH_ETH_RESET_DEFAULT    1
static struct sh_eth_cpu_data sh_eth_my_cpu_data = {
    .eesipr_value   = DMAC_M_RFRMER | DMAC_M_ECI | 0x003fffff,

    .apr        = 1,
    .mpr        = 1,
    .tpauser    = 1,
    .hw_swap    = 1,
};
#elif defined(CONFIG_CPU_SUBTYPE_SH7710) || defined(CONFIG_CPU_SUBTYPE_SH7712)
#define SH_ETH_RESET_DEFAULT    1
#define SH_ETH_HAS_TSU  1
static struct sh_eth_cpu_data sh_eth_my_cpu_data = {
    .eesipr_value   = DMAC_M_RFRMER | DMAC_M_ECI | 0x003fffff,
    .tsu        = 1,
};
#endif

static void sh_eth_set_default_cpu_data(struct sh_eth_cpu_data *cd)
{
    if (!cd->ecsr_value)
        cd->ecsr_value = DEFAULT_ECSR_INIT;

    if (!cd->ecsipr_value)
        cd->ecsipr_value = DEFAULT_ECSIPR_INIT;

    if (!cd->fcftr_value)
        cd->fcftr_value = DEFAULT_FIFO_F_D_RFF | \
                  DEFAULT_FIFO_F_D_RFD;

    if (!cd->fdr_value)
        cd->fdr_value = DEFAULT_FDR_INIT;

    if (!cd->rmcr_value)
        cd->rmcr_value = DEFAULT_RMCR_VALUE;

    if (!cd->tx_check)
        cd->tx_check = DEFAULT_TX_CHECK;

    if (!cd->eesr_err_check)
        cd->eesr_err_check = DEFAULT_EESR_ERR_CHECK;

    if (!cd->tx_error_check)
        cd->tx_error_check = DEFAULT_TX_ERROR_CHECK;
}

#if defined(SH_ETH_RESET_DEFAULT)
/* Chip Reset */
static int  sh_eth_reset(struct net_device *ndev)
{
    sh_eth_write(ndev, sh_eth_read(ndev, EDMR) | EDMR_SRST_ETHER, EDMR);
    mdelay(3);
    sh_eth_write(ndev, sh_eth_read(ndev, EDMR) & ~EDMR_SRST_ETHER, EDMR);

    return 0;
}
#else
static int sh_eth_check_reset(struct net_device *ndev)
{
    int ret = 0;
    int cnt = 100;

    while (cnt > 0) {
        if (!(sh_eth_read(ndev, EDMR) & 0x3))
            break;
        mdelay(1);
        cnt--;
    }
    if (cnt < 0) {
        printk(KERN_ERR "Device reset fail\n");
        ret = -ETIMEDOUT;
    }
    return ret;
}
#endif

#if defined(CONFIG_CPU_SH4)
static void sh_eth_set_receive_align(struct sk_buff *skb)
{
    int reserve;

    reserve = SH4_SKB_RX_ALIGN - ((u32)skb->data & (SH4_SKB_RX_ALIGN - 1));
    if (reserve)
        skb_reserve(skb, reserve);
}
#else
static void sh_eth_set_receive_align(struct sk_buff *skb)
{
    skb_reserve(skb, SH2_SH3_SKB_RX_ALIGN);
}
#endif


/* CPU <-> EDMAC endian convert */
static inline __u32 cpu_to_edmac(struct sh_eth_private *mdp, u32 x)
{
    switch (mdp->edmac_endian) {
    case EDMAC_LITTLE_ENDIAN:
        return cpu_to_le32(x);
    case EDMAC_BIG_ENDIAN:
        return cpu_to_be32(x);
    }
    return x;
}

static inline __u32 edmac_to_cpu(struct sh_eth_private *mdp, u32 x)
{
    switch (mdp->edmac_endian) {
    case EDMAC_LITTLE_ENDIAN:
        return le32_to_cpu(x);
    case EDMAC_BIG_ENDIAN:
        return be32_to_cpu(x);
    }
    return x;
}

/*
 * Program the hardware MAC address from dev->dev_addr.
 */
static void update_mac_address(struct net_device *ndev)
{
    sh_eth_write(ndev,
        (ndev->dev_addr[0] << 24) | (ndev->dev_addr[1] << 16) |
        (ndev->dev_addr[2] << 8) | (ndev->dev_addr[3]), MAHR);
    sh_eth_write(ndev,
        (ndev->dev_addr[4] << 8) | (ndev->dev_addr[5]), MALR);
}

/*
 * Get MAC address from SuperH MAC address register
 *
 * SuperH's Ethernet device doesn't have 'ROM' to MAC address.
 * This driver get MAC address that use by bootloader(U-boot or sh-ipl+g).
 * When you want use this device, you must set MAC address in bootloader.
 *
 */
static void read_mac_address(struct net_device *ndev, unsigned char *mac)
{
    if (mac[0] || mac[1] || mac[2] || mac[3] || mac[4] || mac[5]) {
        memcpy(ndev->dev_addr, mac, 6);
    } else {
        ndev->dev_addr[0] = (sh_eth_read(ndev, MAHR) >> 24);
        ndev->dev_addr[1] = (sh_eth_read(ndev, MAHR) >> 16) & 0xFF;
        ndev->dev_addr[2] = (sh_eth_read(ndev, MAHR) >> 8) & 0xFF;
        ndev->dev_addr[3] = (sh_eth_read(ndev, MAHR) & 0xFF);
        ndev->dev_addr[4] = (sh_eth_read(ndev, MALR) >> 8) & 0xFF;
        ndev->dev_addr[5] = (sh_eth_read(ndev, MALR) & 0xFF);
    }
}

static int sh_eth_is_gether(struct sh_eth_private *mdp)
{
    if (mdp->reg_offset == sh_eth_offset_gigabit)
        return 1;
    else
        return 0;
}

static unsigned long sh_eth_get_edtrr_trns(struct sh_eth_private *mdp)
{
    if (sh_eth_is_gether(mdp))
        return EDTRR_TRNS_GETHER;
    else
        return EDTRR_TRNS_ETHER;
}

struct bb_info {
    void (*set_gate)(void *addr);
    struct mdiobb_ctrl ctrl;
    void *addr;
    u32 mmd_msk;/* MMD */
    u32 mdo_msk;
    u32 mdi_msk;
    u32 mdc_msk;
};

/* PHY bit set */
static void bb_set(void *addr, u32 msk)
{
    iowrite32(ioread32(addr) | msk, addr);
}

/* PHY bit clear */
static void bb_clr(void *addr, u32 msk)
{
    iowrite32((ioread32(addr) & ~msk), addr);
}

/* PHY bit read */
static int bb_read(void *addr, u32 msk)
{
    return (ioread32(addr) & msk) != 0;
}

/* Data I/O pin control */
static void sh_mmd_ctrl(struct mdiobb_ctrl *ctrl, int bit)
{
    struct bb_info *bitbang = container_of(ctrl, struct bb_info, ctrl);

    if (bitbang->set_gate)
        bitbang->set_gate(bitbang->addr);

    if (bit)
        bb_set(bitbang->addr, bitbang->mmd_msk);
    else
        bb_clr(bitbang->addr, bitbang->mmd_msk);
}

/* Set bit data*/
static void sh_set_mdio(struct mdiobb_ctrl *ctrl, int bit)
{
    struct bb_info *bitbang = container_of(ctrl, struct bb_info, ctrl);

    if (bitbang->set_gate)
        bitbang->set_gate(bitbang->addr);

    if (bit)
        bb_set(bitbang->addr, bitbang->mdo_msk);
    else
        bb_clr(bitbang->addr, bitbang->mdo_msk);
}

/* Get bit data*/
static int sh_get_mdio(struct mdiobb_ctrl *ctrl)
{
    struct bb_info *bitbang = container_of(ctrl, struct bb_info, ctrl);

    if (bitbang->set_gate)
        bitbang->set_gate(bitbang->addr);

    return bb_read(bitbang->addr, bitbang->mdi_msk);
}

/* MDC pin control */
static void sh_mdc_ctrl(struct mdiobb_ctrl *ctrl, int bit)
{
    struct bb_info *bitbang = container_of(ctrl, struct bb_info, ctrl);

    if (bitbang->set_gate)
        bitbang->set_gate(bitbang->addr);

    if (bit)
        bb_set(bitbang->addr, bitbang->mdc_msk);
    else
        bb_clr(bitbang->addr, bitbang->mdc_msk);
}

/* mdio bus control struct */
static struct mdiobb_ops bb_ops = {
    .owner = THIS_MODULE,
    .set_mdc = sh_mdc_ctrl,
    .set_mdio_dir = sh_mmd_ctrl,
    .set_mdio_data = sh_set_mdio,
    .get_mdio_data = sh_get_mdio,
};


#ifdef ETH_DEBUG_MSG
/* msg_type = 0 (Rx), 1 (Tx) */
static int sh_eth_msg_dump(int msg_type, struct sk_buff *skb, struct net_device *ndev)
{
	int i;
	int is_ncsi;
	int is_response;
	unsigned char cmd;
	unsigned char sub_cmd;

	if ((EnableMsgDump == 0) && (EnableNCSICmdHistory == 0))
	{
		return 0;
	}
	else
	{
		/* Rx checking */
		if ((msg_type == 0) && (OnlyTxMsgDump != 0))
			return 0;
		/* Tx checking */
		else if ((msg_type == 1) && (OnlyRxMsgDump != 0))
			return 0;
	}

	if ((skb->data[12] == 0x88) && (skb->data[13] == 0xf8))
		is_ncsi = 1;
	else
		is_ncsi = 0;

	if ((NoNCSIMsgDump && is_ncsi) || (OnlyNCSIMsgDump && !is_ncsi))
		return 0;

	if (is_ncsi && DecodeNCSIMsgDump)
	{
		if ((NoLinkMsgDump == 1) && ((skb->data[18] & 0x7f) == 0x0A))
			return 0;

		if ((NoGetParametersMsgDump == 1) && ((skb->data[18] & 0x7f) == 0x17))
			return 0;

		if ((NoOEMMsgDump == 1) && ((skb->data[18] & 0x7f) == 0x50))
			return 0;

		if ((FilterNCSIMsgDump1 != 0xff) && (FilterNCSIMsgDump2 != 0xff))
		{
			if (((skb->data[18] & 0x7f) != FilterNCSIMsgDump1) &&
				((skb->data[18] & 0x7f) != FilterNCSIMsgDump2))
				return 0;
		}
		else
		{
			if ((FilterNCSIMsgDump1 != 0xff) && ((skb->data[18] & 0x7f) != FilterNCSIMsgDump1))
				return 0;

			if ((FilterNCSIMsgDump2 != 0xff) && ((skb->data[18] & 0x7f) != FilterNCSIMsgDump2))
				return 0;
		}
	}

	if (EnableMsgDump != 0)
	{
		for (i = 0; i < skb->len; i++)
		{
			if (i == 0)
			{
				if (msg_type)
					printk("Tx:");
				else
					printk("Rx:");
			}

			if ((i != 0) && ((i % 32) == 0))
				printk("\n   ");

			if ((i == 0) || (i == 6) || (i == 12))
				printk("[ ");

			printk("%02x ", skb->data[i]);

			if ((i == 5) || (i == 11) || (i == 13))
				printk("] ");
		}

		if (i != 0)
			printk("\n");

		if (is_ncsi && DecodeNCSIMsgDump)
		{
			printk("   %05ld - IID:%02x PkgID:%02x ChID:%02x Cmd:", jiffies & 0xFFFF, skb->data[17], skb->data[19] >> 5, skb->data[19] & 0x1F);

			if ((skb->data[18] != 0xff) && ((skb->data[18] & 0x80) == 0x80))
			{
				printk("Re-");
				cmd = skb->data[18] & 0x7F;
				is_response = 1;
			}
			else
			{
				cmd = skb->data[18];
				is_response = 0;
			}

			switch (cmd)
			{
				case 0x00:
					printk("Clear Initial State");
					break;

				case 0x01:
					printk("Select Package");
					break;

				case 0x02:
					printk("Deselect Package");
					break;

				case 0x03:
					printk("Enable Channel");
					break;

				case 0x04:
					printk("Disable Channel");
					break;

				case 0x05:
					printk("Reset Channel");
					break;

				case 0x06:
					printk("Enable Channel Network TX");
					break;

				case 0x07:
					printk("Disable Channel Network TX");
					break;

				case 0x08:
					printk("AEN Enable");
					break;

				case 0x09:
					printk("Set Link");
					break;

				case 0x0A:
					printk("Get Link Status");
					break;

				case 0x0B:
					printk("Set VLAN Fliter");
					break;

				case 0x0C:
					printk("Enable VLAN");
					break;

				case 0x0D:
					printk("Disable VLAN");
					break;

				case 0x0E:
					printk("Set MAC Address");
					break;

				case 0x10:
					printk("Enable Broadcast Filtering");
					break;

				case 0x11:
					printk("Disable Broadcast Filtering");
					break;

				case 0x12:
					printk("Enable Global Multicast Filtering");
					break;

				case 0x13:
					printk("Disable Global Multicast Filtering");
					break;

				case 0x14:
					printk("Set NC-SI Flow Control");
					break;

				case 0x15:
					printk("Get Version ID");
					break;

				case 0x16:
					printk("Get Capabilities");
					break;

				case 0x17:
					printk("Get Parameters");
					break;

				case 0x18:
					printk("Get Controller Packet Statistics");
					break;

				case 0x19:
					printk("Get NC-SI Statistics");
					break;

				case 0x1A:
					printk("Get NC0SI Passthrough Statistics");
					break;

				case 0x50:
					printk("OEM - ");

					/* Dell Command ID offset */
					if (is_response)
					{
						sub_cmd = skb->data[39];
					}
					else
					{
						sub_cmd = skb->data[35];
					}

					switch(sub_cmd)
					{
						case 0x00:
							printk("Get Inventory");
							break;

						case 0x01:
							printk("Get Extended Capabilities");
							break;

						case 0x02:
							printk("Get partition Information");
							break;

						case 0x03:
							printk("Get FCoE Capabilities");
							break;

						case 0x04:
							printk("Get Virtual Link");
							break;

						case 0x05:
							printk("Get LAN Statistics");
							break;

						case 0x06:
							printk("Get FCoE Statistics");
							break;

						case 0x07:
							printk("Set Address");
							break;

						case 0x08:
							printk("Get Address");
							break;

						case 0x09:
							printk("Set License");
							break;

						case 0x0A:
							printk("Get License");
							break;

						case 0x0B:
							printk("Set Passthru Control");
							break;

						case 0x0C:
							printk("Get Passthru Control");
							break;

						case 0x0D:
							printk("Set Partition TX Bandwidth");
							break;

						case 0x0E:
							printk("Get Partition TX Bandwidth");
							break;

						case 0x0F:
							printk("Set MC IP Address");
							break;

						case 0x10:
							printk("Gete Teaming Information");
							break;

						case 0x11:
							printk("Enable Ports");
							break;

						case 0x12:
							printk("Disable Ports");
							break;

						case 0x13:
							printk("Get Temperature");
							break;

						case 0x14:
							printk("Set Link Tuning");
							break;

						case 0x15:
							printk("Enable OutOfBox WOL");
							break;

						case 0x16:
							printk("Disable OutOfBox WOL");
							break;

						case 0x17:
							printk("Get FC Statistics");
							break;

						case 0x18:
							printk("Get FC Link Status");
							break;

						case 0x19:
							printk("Host Port Control");
							break;

						case 0x1A:
							printk("Get Supported Payload Version");
							break;

						case 0x1B:
							printk("Get iSCSI Offload Statistics");
							break;

						case 0x1C:
							printk("Get OS Driver Version Command");
							break;

						case 0x1D:
							printk("Get iSCSI Boot Initiator Config");
							break;

						case 0x1E:
							printk("Set iSCSI Boot Initiator Config");
							break;

						case 0x1F:
							printk("Get iSCSI Boot Target Config");
							break;

						case 0x20:
							printk("Set iSCSI Boot Target Config");
							break;

						case 0x21:
							printk("Get FC/FCoE Boot Target Config");
							break;

						case 0x22:
							printk("Set FC/FCoE Boot Target Config");
							break;

						case 0x23:
							printk("NVRAM Commit");
							break;

						case 0x24:
							printk("NVRAM Commit Status");
							break;

						case 0x25:
							printk("Get Link Tuning");
							break;

						case 0x26:
							printk("Get PF Assignment");
							break;

						case 0x27:
							printk("Get RDMA Statistics");
							break;

						case 0x50:
							printk("Get FCoE Boot Order");
							break;

						case 0x51:
							printk("Set FCoE Boot Order");
							break;

						case 0x52:
							printk("Get iSCSI Boot Initiator Config-S1");
							break;

						case 0x53:
							printk("Set iSCSI Boot Initiator Config-S1");
							break;

						case 0x54:
							printk("Get iSCSI Boot Initiator Config-S2");
							break;

						case 0x55:
							printk("Set iSCSI Boot Initiator Config-S2");
							break;

						case 0x56:
							printk("Get iSCSI Boot Initiator Config-S3");
							break;

						case 0x57:
							printk("Set iSCSI Boot Initiator Config-S3");
							break;

						case 0x58:
							printk("Get iSCSI Boot Target Config-S1");
							break;

						case 0x59:
							printk("Set iSCSI Boot Target Config-S1");
							break;

						case 0x5A:
							printk("Get iSCSI Boot Target Config-S2");
							break;

						case 0x5B:
							printk("Set iSCSI Boot Target Config-S2");
							break;

						case 0x5C:
							printk("Get iSCSI Boot Target Config-S3");
							break;

						case 0x5D:
							printk("Set iSCSI Boot Target Config-S3");
							break;

						case 0x5E:
							printk("Get iSCSI Boot Target Config-S4");
							break;

						case 0x5F:
							printk("Set iSCSI Boot Target Config-S4");
							break;

						case 0x60:
							printk("Get iSCSI Boot Target Config-S5");
							break;

						case 0x61:
							printk("Set iSCSI Boot Target Config-S5");
							break;

						case 0x62:
							printk("Send Dell LLDP");
							break;

						default:
							printk("%0x2", sub_cmd);
							break;
					}
					break;

				default:
					printk("Unknown");
					break;
			}

			printk("\n\n");
		}
	}

	if (EnableNCSICmdHistory != 0)
	{
		NCSICmdHistorytData[NCSICmdHistorytIndex] = skb->data[18];
		NCSICmdHistoryTimestamp[NCSICmdHistorytIndex] = jiffies;
		NCSICmdHistorytIndex++;

		if (NCSICmdHistorytIndex >= NCSI_CMD_HISTORY_BUFFER_SIZE)
			NCSICmdHistorytIndex = 0;
	}

	return 0;
}
#endif

/* free skb and descriptor buffer */
static void sh_eth_ring_free(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int i;

    /* Free Rx skb ringbuffer */
    if (mdp->rx_skbuff) {
        for (i = 0; i < mdp->num_rx_ring; i++) {
            if (mdp->rx_skbuff[i])
                dev_kfree_skb(mdp->rx_skbuff[i]);
        }
    }
    kfree(mdp->rx_skbuff);
    mdp->rx_skbuff = NULL;

    /* Free Tx skb ringbuffer */
    if (mdp->tx_skbuff) {
        for (i = 0; i < mdp->num_tx_ring; i++) {
            if (mdp->tx_skbuff[i])
                dev_kfree_skb(mdp->tx_skbuff[i]);
        }
    }
    kfree(mdp->tx_skbuff);
    mdp->tx_skbuff = NULL;
}

/* format skb and descriptor buffer */
static void sh_eth_ring_format(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int i;
    struct sk_buff *skb;
    struct sh_eth_rxdesc *rxdesc = NULL;
    struct sh_eth_txdesc *txdesc = NULL;
    int rx_ringsize = sizeof(*rxdesc) * mdp->num_rx_ring;
    int tx_ringsize = sizeof(*txdesc) * mdp->num_tx_ring;

    mdp->cur_rx = mdp->cur_tx = 0;
    mdp->dirty_rx = mdp->dirty_tx = 0;

    memset(mdp->rx_ring, 0, rx_ringsize);

    /* build Rx ring buffer */
    for (i = 0; i < mdp->num_rx_ring; i++) {
        /* skb */
        mdp->rx_skbuff[i] = NULL;
        skb = netdev_alloc_skb(ndev, mdp->rx_buf_sz);
        mdp->rx_skbuff[i] = skb;
        if (skb == NULL)
            break;
        dma_map_single(&ndev->dev, skb->data, mdp->rx_buf_sz,
                DMA_FROM_DEVICE);
        sh_eth_set_receive_align(skb);

        /* RX descriptor */
        rxdesc = &mdp->rx_ring[i];
        rxdesc->addr = virt_to_phys(PTR_ALIGN(skb->data, 4));
        rxdesc->status = cpu_to_edmac(mdp, RD_RACT | RD_RFP);

        /* The size of the buffer is 16 byte boundary. */
        rxdesc->buffer_length = ALIGN(mdp->rx_buf_sz, 16);
        /* Rx descriptor address set */
        if (i == 0) {
            sh_eth_write(ndev, mdp->rx_desc_dma, RDLAR);
            if (sh_eth_is_gether(mdp))
                sh_eth_write(ndev, mdp->rx_desc_dma, RDFAR);
        }
    }

    mdp->dirty_rx = (u32) (i - mdp->num_rx_ring);

    /* Mark the last entry as wrapping the ring. */
    rxdesc->status |= cpu_to_edmac(mdp, RD_RDEL);

    memset(mdp->tx_ring, 0, tx_ringsize);

    /* build Tx ring buffer */
    for (i = 0; i < mdp->num_tx_ring; i++) {
        mdp->tx_skbuff[i] = NULL;
        txdesc = &mdp->tx_ring[i];
        txdesc->status = cpu_to_edmac(mdp, TD_TFP);
        txdesc->buffer_length = 0;
        if (i == 0) {
            /* Tx descriptor address set */
            sh_eth_write(ndev, mdp->tx_desc_dma, TDLAR);
            if (sh_eth_is_gether(mdp))
                sh_eth_write(ndev, mdp->tx_desc_dma, TDFAR);
        }
    }

    txdesc->status |= cpu_to_edmac(mdp, TD_TDLE);
}

/* Get skb and descriptor buffer */
static int sh_eth_ring_init(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int rx_ringsize, tx_ringsize, ret = 0;

    /*
     * +26 gets the maximum ethernet encapsulation, +7 & ~7 because the
     * card needs room to do 8 byte alignment, +2 so we can reserve
     * the first 2 bytes, and +16 gets room for the status word from the
     * card.
     */
    mdp->rx_buf_sz = (ndev->mtu <= 1492 ? PKT_BUF_SZ :
              (((ndev->mtu + 26 + 7) & ~7) + 2 + 16));
    if (mdp->cd->rpadir)
        mdp->rx_buf_sz += NET_IP_ALIGN;

    /* Allocate RX and TX skb rings */
    mdp->rx_skbuff = kmalloc(sizeof(*mdp->rx_skbuff) * mdp->num_rx_ring,
                GFP_KERNEL);
    if (!mdp->rx_skbuff) {
        dev_err(&ndev->dev, "Cannot allocate Rx skb\n");
        ret = -ENOMEM;
        return ret;
    }

    mdp->tx_skbuff = kmalloc(sizeof(*mdp->tx_skbuff) * mdp->num_tx_ring,
                GFP_KERNEL);
    if (!mdp->tx_skbuff) {
        dev_err(&ndev->dev, "Cannot allocate Tx skb\n");
        ret = -ENOMEM;
        goto skb_ring_free;
    }

    /* Allocate all Rx descriptors. */
    rx_ringsize = sizeof(struct sh_eth_rxdesc) * mdp->num_rx_ring;
    mdp->rx_ring = dma_alloc_coherent(NULL, rx_ringsize, &mdp->rx_desc_dma,
            GFP_KERNEL);

    if (!mdp->rx_ring) {
        dev_err(&ndev->dev, "Cannot allocate Rx Ring (size %d bytes)\n",
            rx_ringsize);
        ret = -ENOMEM;
        goto desc_ring_free;
    }

    mdp->dirty_rx = 0;

    /* Allocate all Tx descriptors. */
    tx_ringsize = sizeof(struct sh_eth_txdesc) * mdp->num_tx_ring;
    mdp->tx_ring = dma_alloc_coherent(NULL, tx_ringsize, &mdp->tx_desc_dma,
            GFP_KERNEL);
    if (!mdp->tx_ring) {
        dev_err(&ndev->dev, "Cannot allocate Tx Ring (size %d bytes)\n",
            tx_ringsize);
        ret = -ENOMEM;
        goto desc_ring_free;
    }
    return ret;

desc_ring_free:
    /* free DMA buffer */
    dma_free_coherent(NULL, rx_ringsize, mdp->rx_ring, mdp->rx_desc_dma);

skb_ring_free:
    /* Free Rx and Tx skb ring buffer */
    sh_eth_ring_free(ndev);
    mdp->tx_ring = NULL;
    mdp->rx_ring = NULL;

    return ret;
}

static void sh_eth_free_dma_buffer(struct sh_eth_private *mdp)
{
    int ringsize;

    if (mdp->rx_ring) {
        ringsize = sizeof(struct sh_eth_rxdesc) * mdp->num_rx_ring;
        dma_free_coherent(NULL, ringsize, mdp->rx_ring,
                  mdp->rx_desc_dma);
        mdp->rx_ring = NULL;
    }

    if (mdp->tx_ring) {
        ringsize = sizeof(struct sh_eth_txdesc) * mdp->num_tx_ring;
        dma_free_coherent(NULL, ringsize, mdp->tx_ring,
                  mdp->tx_desc_dma);
        mdp->tx_ring = NULL;
    }
}

static int sh_eth_dev_init(struct net_device *ndev, bool start)
{
    int ret = 0;
    struct sh_eth_private *mdp = netdev_priv(ndev);
    u32 val;

    /* Soft Reset */
    ret = sh_eth_reset(ndev);
    if (ret)
        goto out;

    /* Descriptor format */
    sh_eth_ring_format(ndev);
    if (mdp->cd->rpadir)
        sh_eth_write(ndev, mdp->cd->rpadir_value, RPADIR);

    /* all sh_eth int mask */
    sh_eth_write(ndev, 0, EESIPR);

#if defined(__LITTLE_ENDIAN)
    if (mdp->cd->hw_swap)
        sh_eth_write(ndev, EDMR_EL, EDMR);
    else
#endif
        sh_eth_write(ndev, 0, EDMR);

    /* FIFO size set */
    sh_eth_write(ndev, mdp->cd->fdr_value, FDR);
    sh_eth_write(ndev, 0, TFTR);

    /* Frame recv control */
    sh_eth_write(ndev, mdp->cd->rmcr_value, RMCR);

    sh_eth_write(ndev, DESC_I_RINT8 | DESC_I_RINT5 | DESC_I_TINT2, TRSCER);

    if (mdp->cd->bculr)
        sh_eth_write(ndev, 0x800, BCULR);   /* Burst sycle set */

    sh_eth_write(ndev, mdp->cd->fcftr_value, FCFTR);

    if (!mdp->cd->no_trimd)
        sh_eth_write(ndev, 0, TRIMD);

    /* Recv frame limit set register */
    sh_eth_write(ndev, ndev->mtu + ETH_HLEN + VLAN_HLEN + ETH_FCS_LEN,
             RFLR);

    sh_eth_write(ndev, sh_eth_read(ndev, EESR), EESR);
    if (start)
        sh_eth_write(ndev, mdp->cd->eesipr_value, EESIPR);

    /* PAUSE Prohibition */
    val = (sh_eth_read(ndev, ECMR) & ECMR_DM) |
        ECMR_ZPF | (mdp->duplex ? ECMR_DM : 0) | ECMR_TE | ECMR_RE;

#ifdef IDRAC_SUPPORT /* { */
    if ((mdp->nic_if.interface_mode & IF_NCSI) != 0)
    {
        val = val | ECMR_PRM ;
    }
#endif /* } IDRAC_SUPPORT */

    sh_eth_write(ndev, val, ECMR);

    if (mdp->cd->set_rate)
        mdp->cd->set_rate(ndev);

    /* E-MAC Status Register clear */
    sh_eth_write(ndev, mdp->cd->ecsr_value, ECSR);

    /* E-MAC Interrupt Enable register */
    if (start)
        sh_eth_write(ndev, mdp->cd->ecsipr_value, ECSIPR);

    /* Set MAC address */
    update_mac_address(ndev);

    /* mask reset */
    if (mdp->cd->apr)
        sh_eth_write(ndev, APR_AP, APR);
    if (mdp->cd->mpr)
        sh_eth_write(ndev, MPR_MP, MPR);
    if (mdp->cd->tpauser)
        sh_eth_write(ndev, TPAUSER_UNLIMITED, TPAUSER);

#ifdef BROADCAST_PROTECTION /* { */
    /* Set the "Broadcast Frame Receive Count Set Register" */
    sh_eth_write(ndev, mdp->cd->bcfrr_value, BCFRR);
#endif  /* } BROADCAST_PROTECTION */

    if (start) {
        /* Setting the Rx mode will start the Rx process. */
        sh_eth_write(ndev, EDRRR_R, EDRRR);

        netif_start_queue(ndev);
    }

out:
    return ret;
}

/* free Tx skb function */
static int sh_eth_txfree(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    struct sh_eth_txdesc *txdesc;
    int freeNum = 0;
    int entry = 0;

    for (; mdp->cur_tx - mdp->dirty_tx > 0; mdp->dirty_tx++) {
        entry = mdp->dirty_tx % mdp->num_tx_ring;
        txdesc = &mdp->tx_ring[entry];
        if (txdesc->status & cpu_to_edmac(mdp, TD_TACT))
            break;
        /* Free the original skb. */
        if (mdp->tx_skbuff[entry]) {
            dma_unmap_single(&ndev->dev, txdesc->addr,
                     txdesc->buffer_length, DMA_TO_DEVICE);
            dev_kfree_skb_irq(mdp->tx_skbuff[entry]);
            mdp->tx_skbuff[entry] = NULL;
            freeNum++;
        }
        txdesc->status = cpu_to_edmac(mdp, TD_TFP);
        if (entry >= mdp->num_tx_ring - 1)
            txdesc->status |= cpu_to_edmac(mdp, TD_TDLE);

        ndev->stats.tx_packets++;
        ndev->stats.tx_bytes += txdesc->buffer_length;
    }
    return freeNum;
}

/* Packet receive function */
static int sh_eth_rx(struct net_device *ndev, u32 intr_status, int *work,
             int budget)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    struct sh_eth_rxdesc *rxdesc;

    int entry = mdp->cur_rx % mdp->num_rx_ring;
    int boguscnt = (mdp->dirty_rx + mdp->num_rx_ring) - mdp->cur_rx;
    struct sk_buff *skb;
    u16 pkt_len = 0;
    u32 desc_status;

#ifdef CFG_PROJ_ETHERCLONE /* { */
    struct vlan_hdr *vhdr;
	struct sh_eth_private *lp = netdev_priv(ndev);
	struct sh_eth_private *clone_priv = NULL;

	if (lp->clone_dev)
		clone_priv = netdev_priv(lp->clone_dev);
#endif /* } CFG_PROJ_ETHERCLONE */

    rxdesc = &mdp->rx_ring[entry];
    while (!(rxdesc->status & cpu_to_edmac(mdp, RD_RACT)) &&
           *work < budget) {
        desc_status = edmac_to_cpu(mdp, rxdesc->status);
        pkt_len = rxdesc->frame_length;

#ifdef IDRAC_SUPPORT /* { */
       /** - Protect F/W stuck when network storm comes.*/
       packet_counter();

#ifdef BROADCAST_PROTECTION /* { */
        mdp->cd->receive_packet_cnt++;
#endif  /* } BROADCAST_PROTECTION */
#endif /* } IDRAC_SUPPORT */
        if (--boguscnt < 0)
            break;

        if (!(desc_status & RDFEND))
            ndev->stats.rx_length_errors++;

        if (desc_status & (RD_RFS1 | RD_RFS2 | RD_RFS3 | RD_RFS4 |
                   RD_RFS5 | RD_RFS6 | RD_RFS10)) {
            ndev->stats.rx_errors++;
            if (desc_status & RD_RFS1)
                ndev->stats.rx_crc_errors++;
            if (desc_status & RD_RFS2)
                ndev->stats.rx_frame_errors++;
            if (desc_status & RD_RFS3)
                ndev->stats.rx_length_errors++;
            if (desc_status & RD_RFS4)
                ndev->stats.rx_length_errors++;
            if (desc_status & RD_RFS6)
                ndev->stats.rx_missed_errors++;
            if (desc_status & RD_RFS10)
                ndev->stats.rx_over_errors++;
        } else {
            if (!mdp->cd->hw_swap)
                sh_eth_soft_swap(
                    phys_to_virt(ALIGN(rxdesc->addr, 4)),
                    pkt_len + 2);
            skb = mdp->rx_skbuff[entry];
            mdp->rx_skbuff[entry] = NULL;
            if (mdp->cd->rpadir)
                skb_reserve(skb, NET_IP_ALIGN);
            skb_put(skb, pkt_len);
#ifdef IDRAC_SUPPORT /* { */
#ifdef MULTICAST_PROTECTION
            //*****RFS bit definition is different with SH7757 spec.
            if(desc_status & (RD_RFS8))
            {
                if(!(ndev->flags & IFF_MULTICAST))
                {
                    dev_kfree_skb(skb);
                    mdp->stats.rx_dropped++;
                    entry = (++mdp->cur_rx) % mdp->num_rx_ring;
                    rxdesc = &mdp->rx_ring[entry];
            //      printk(KERN_EMERG "Drop the multicast packets!! \n");
                    (*work)++;
                    continue;
                }
            }
#endif /* } MULTICAST_PROTECTION */

			if (mdp->poll_enter)
			{
                    dev_kfree_skb(skb);
                    mdp->stats.rx_dropped++;
                    entry = (++mdp->cur_rx) % mdp->num_rx_ring;
                    rxdesc = &mdp->rx_ring[entry];
            //      printk(KERN_EMERG "Drop the all packets!! \n");
                    (*work)++;
                    continue;
			}

#ifdef CFG_PROJ_ETHERCLONE /* { */
            if (clone_priv) {
                if ((((struct ethhdr *)(skb->data))->h_proto) == ntohs(ETH_P_8021Q)) {
                    vhdr = (struct vlan_hdr *)(skb->data + ETH_HLEN);
                    if ((ntohs(vhdr->h_vlan_TCI) & VLAN_VID_MASK ) == 4003) {
                        skb->dev = lp->clone_dev;
                        clone_priv->stats.rx_packets++;
                        clone_priv->stats.rx_bytes += pkt_len;
                    }
                }
            }
#endif /* } CFG_PROJ_ETHERCLONE */

#ifdef ETH_DEBUG_MSG
			sh_eth_msg_dump(0, skb, ndev);
#endif
            if (ncsi_rx_handler(skb) == NET_RX_DROP) {
                mdp->stats.rx_dropped++;
            } else {
               ndev->stats.rx_packets++;
               ndev->stats.rx_bytes += pkt_len;
            }
#else
            skb->protocol = eth_type_trans(skb, ndev);
#ifdef CFG_PROJ_ETHERCLONE /* { */
			if (lp->clone_dev)
			{
				if (skb->protocol == htons(ETH_P_8021Q))
				{
					struct vlan_hdr *vhdr = (struct vlan_hdr *)(skb->data);
					if ( ( ntohs(vhdr->h_vlan_TCI) & VLAN_VID_MASK ) == 4003)
					{
						skb->dev = lp->clone_dev;
						if (clone_priv)
						{
							clone_priv->stats.rx_packets++;
							clone_priv->stats.rx_bytes += pkt_len;
		        		}
					}
				}
 			}
#endif /* } CFG_PROJ_ETHERCLONE */
            netif_receive_skb(skb);
            ndev->stats.rx_packets++;
            ndev->stats.rx_bytes += pkt_len;
#endif /* } IDRAC_SUPPORT */
        }
        entry = (++mdp->cur_rx) % mdp->num_rx_ring;
        rxdesc = &mdp->rx_ring[entry];
        (*work)++;
    }

    /* Refill the Rx ring buffers. */
    for (; mdp->cur_rx - mdp->dirty_rx > 0; mdp->dirty_rx++) {
        entry = mdp->dirty_rx % mdp->num_rx_ring;
        rxdesc = &mdp->rx_ring[entry];
        /* The size of the buffer is 16 byte boundary. */
        rxdesc->buffer_length = ALIGN(mdp->rx_buf_sz, 16);

        if (mdp->rx_skbuff[entry] == NULL) {
            skb = netdev_alloc_skb(ndev, mdp->rx_buf_sz);
            mdp->rx_skbuff[entry] = skb;
            if (skb == NULL)
                break;  /* Better luck next round. */
            dma_map_single(&ndev->dev, skb->data, mdp->rx_buf_sz,
                    DMA_FROM_DEVICE);
            sh_eth_set_receive_align(skb);

            skb_checksum_none_assert(skb);
            rxdesc->addr = virt_to_phys(PTR_ALIGN(skb->data, 4));
        }
        if (entry >= mdp->num_rx_ring - 1)
            rxdesc->status |=
                cpu_to_edmac(mdp, RD_RACT | RD_RFP | RD_RDEL);
        else
            rxdesc->status |=
                cpu_to_edmac(mdp, RD_RACT | RD_RFP);
    }

    /* Restart Rx engine if stopped. */
    /* If we don't need to check status, don't. -KDU */
    if (*work < budget && !(sh_eth_read(ndev, EDRRR) & EDRRR_R)) {
        /* fix the values for the next receiving if RDE is set */
        if (intr_status & EESR_RDE)
            mdp->cur_rx = mdp->dirty_rx =
                (sh_eth_read(ndev, RDFAR) -
                 sh_eth_read(ndev, RDLAR)) >> 4;
        sh_eth_write(ndev, EDRRR_R, EDRRR);
    }

    return 0;
}

static void sh_eth_rcv_snd_disable(struct net_device *ndev)
{
    /* disable tx and rx */
    sh_eth_write(ndev, sh_eth_read(ndev, ECMR) &
        ~(ECMR_RE | ECMR_TE), ECMR);
}

static void sh_eth_rcv_snd_enable(struct net_device *ndev)
{
    /* enable tx and rx */
    sh_eth_write(ndev, sh_eth_read(ndev, ECMR) |
        (ECMR_RE | ECMR_TE), ECMR);
}

/* error control function */
static void sh_eth_error(struct net_device *ndev, int intr_status)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    u32 felic_stat;
    u32 link_stat;
    u32 mask;

    if (intr_status & EESR_ECI) {
        felic_stat = sh_eth_read(ndev, ECSR);
        sh_eth_write(ndev, felic_stat, ECSR);   /* clear int */
        if (felic_stat & ECSR_ICD)
            ndev->stats.tx_carrier_errors++;
        if (felic_stat & ECSR_LCHNG) {
            /* Link Changed */
            if (mdp->cd->no_psr || mdp->no_ether_link) {
                if (mdp->link == PHY_DOWN)
                    link_stat = 0;
                else
                    link_stat = PHY_ST_LINK;
            } else {
                link_stat = (sh_eth_read(ndev, PSR));
                if (mdp->ether_link_active_low)
                    link_stat = ~link_stat;
            }
            if (!(link_stat & PHY_ST_LINK))
                sh_eth_rcv_snd_disable(ndev);
            else {
                /* Link Up */
                sh_eth_write(ndev, sh_eth_read(ndev, EESIPR) &
                      ~DMAC_M_ECI, EESIPR);
                /*clear int */
                sh_eth_write(ndev, sh_eth_read(ndev, ECSR),
                      ECSR);
                sh_eth_write(ndev, sh_eth_read(ndev, EESIPR) |
                      DMAC_M_ECI, EESIPR);
                /* enable tx and rx */
                sh_eth_rcv_snd_enable(ndev);
            }
        }
    }

    if (intr_status & EESR_TWB) {
        /* Write buck end. unused write back interrupt */
        if (intr_status & EESR_TABT)    /* Transmit Abort int */
            ndev->stats.tx_aborted_errors++;
            if (NetIfMsgSwitch && netif_msg_tx_err(mdp))
                dev_err(&ndev->dev, "Transmit Abort\n");
    }

    if (intr_status & EESR_RABT) {
        /* Receive Abort int */
        if (intr_status & EESR_RFRMER) {
            /* Receive Frame Overflow int */
            ndev->stats.rx_frame_errors++;
            if (NetIfMsgSwitch && netif_msg_rx_err(mdp))
                dev_err(&ndev->dev, "Receive Abort\n");
        }
    }

    if (intr_status & EESR_TDE) {
        /* Transmit Descriptor Empty int */
        ndev->stats.tx_fifo_errors++;
        if (NetIfMsgSwitch && netif_msg_tx_err(mdp))
            dev_err(&ndev->dev, "Transmit Descriptor Empty\n");
    }

    if (intr_status & EESR_TFE) {
        /* FIFO under flow */
        ndev->stats.tx_fifo_errors++;
        if (NetIfMsgSwitch && netif_msg_tx_err(mdp))
            dev_err(&ndev->dev, "Transmit FIFO Under flow\n");
    }

    if (intr_status & EESR_RDE) {
        /* Receive Descriptor Empty int */
        ndev->stats.rx_over_errors++;

        if (NetIfMsgSwitch && netif_msg_rx_err(mdp))
            dev_err(&ndev->dev, "Receive Descriptor Empty\n");
    }

    if (intr_status & EESR_RFE) {
        /* Receive FIFO Overflow int */
        ndev->stats.rx_fifo_errors++;
        if (NetIfMsgSwitch && netif_msg_rx_err(mdp))
            dev_err(&ndev->dev, "Receive FIFO Overflow\n");
    }

    if (!mdp->cd->no_ade && (intr_status & EESR_ADE)) {
        /* Address Error */
        ndev->stats.tx_fifo_errors++;
        if (NetIfMsgSwitch && netif_msg_tx_err(mdp))
            dev_err(&ndev->dev, "Address Error\n");
    }

    mask = EESR_TWB | EESR_TABT | EESR_ADE | EESR_TDE | EESR_TFE;
    if (mdp->cd->no_ade)
        mask &= ~EESR_ADE;
    if (intr_status & mask) {
        /* Tx error */
        u32 edtrr = sh_eth_read(ndev, EDTRR);
        /* dmesg */
        dev_err(&ndev->dev, "TX error. status=%8.8x cur_tx=%8.8x ",
                intr_status, mdp->cur_tx);
        dev_err(&ndev->dev, "dirty_tx=%8.8x state=%8.8x EDTRR=%8.8x.\n",
                mdp->dirty_tx, (u32) ndev->state, edtrr);
        /* dirty buffer free */
        sh_eth_txfree(ndev);

        /* SH7712 BUG */
        if (edtrr ^ sh_eth_get_edtrr_trns(mdp)) {
            /* tx dma start */
            sh_eth_write(ndev, sh_eth_get_edtrr_trns(mdp), EDTRR);
        }
        /* wakeup */
#ifdef IDRAC_SUPPORT /* { */
        if(IS_IF_VALID(mdp->u8Interface))
        {
#endif /* } IDRAC_SUPPORT */
           netif_wake_queue(ndev);
#ifdef IDRAC_SUPPORT /* { */
        }
#endif /* } IDRAC_SUPPORT */
    }
}

static irqreturn_t sh_eth_interrupt(int irq, void *netdev)
{
    struct net_device *ndev = netdev;
    struct sh_eth_private *mdp = netdev_priv(ndev);
    struct sh_eth_cpu_data *cd = mdp->cd;
    irqreturn_t ret = IRQ_NONE;
    u32 intr_status = 0;
    u32 ie_status = 0;

    spin_lock(&mdp->lock);

    /* Get interrpt stat */
    intr_status = sh_eth_read(ndev, EESR);
    ie_status = sh_eth_read(ndev, EESIPR);
    if ((intr_status & (EESR_FRC | EESR_RMAF | EESR_RRF |
            EESR_RTLF | EESR_RTSF | EESR_PRE | EESR_CERF |
            cd->tx_check | cd->eesr_err_check)) && (ie_status!=0)) {
        if (napi_schedule_prep(&mdp->napi)) {
            /* Disable interrupts of the channel */
            sh_eth_write(ndev, 0, EESIPR);
            __napi_schedule(&mdp->napi);
        }
        else
        {
            /* Disable interrupts of the channel */
            sh_eth_write(ndev, 0, EESIPR);
        }
        ret = IRQ_HANDLED;
    }

    spin_unlock(&mdp->lock);

    return ret;
}

static int sh_eth_poll(struct napi_struct *napi, int budget)
{
    struct sh_eth_private *mdp = container_of(napi, struct sh_eth_private,
                          napi);
    struct net_device *ndev = mdp->ndev;
    struct sh_eth_cpu_data *cd = mdp->cd;
    int work_done = 0, txfree_num;
    u32 intr_status = sh_eth_read(ndev, EESR);

#ifdef UNICAST_PROTECTION
	int adjusted_budget;

	if (mdp->poll_enter)
	{
		adjusted_budget = SH_ETH_NAPI_WEIGHT / 2;
		if (UnicastStormMsgDump)
			printk("adjusted_budget=%d\n", adjusted_budget);
	}
	else
	{
		adjusted_budget = budget;
	}
#endif

    /* Clear interrupt flags */
    sh_eth_write(ndev, intr_status, EESR);

    /* check txdesc */
    txfree_num = sh_eth_txfree(ndev);
    if (txfree_num) {
        netif_tx_lock(ndev);
        if (netif_queue_stopped(ndev))
            netif_wake_queue(ndev);
        netif_tx_unlock(ndev);
    }

    /* check rxdesc */
#ifdef UNICAST_PROTECTION
    sh_eth_rx(ndev, intr_status, &work_done, adjusted_budget);
#else
    sh_eth_rx(ndev, intr_status, &work_done, budget);
#endif

    /* check error flags */
    if (intr_status & cd->eesr_err_check)
        sh_eth_error(ndev, intr_status);

    /* get current interrupt flags */
    intr_status = sh_eth_read(ndev, EESR);

#ifdef UNICAST_PROTECTION
	/* check whether this driver should call napi_complete() */
	if (work_done < budget) {
		napi_complete(napi);

		if (mdp->poll_enter)
		{
			mdp->rx_delay_timer.expires = jiffies + (UnicastStormDelay * (HZ / 100)); /* 10 ms */

			if (UnicastStormMsgDump)
				printk("add_timer jiffies=%x\n", jiffies);

			add_timer(&mdp->rx_delay_timer);

			mdp->poll_enter = 0;
		}
		else
		{
			/* Enable all interrupts */
			sh_eth_write(ndev, cd->eesipr_value, EESIPR);
		}

		mdp->poll_count = 0;
	}
	else
	{
		mdp->poll_count++;

		if (UnicastStormMaxCount < mdp->poll_count)
			UnicastStormMaxCount = mdp->poll_count;

		if (mdp->poll_count > UnicastStormCount)
		{
			if (UnicastStormMsgDump)
				printk("count > UnicastStormCount\n");

			mdp->poll_enter = 1;
		}
	}

	/* check number of packets per jiffies */
	if (mdp->poll_previous_jiffies == jiffies)
	{
		mdp->poll_packet_count += work_done;

		if (UnicastStormMaxPacket < mdp->poll_packet_count)
			UnicastStormMaxPacket = mdp->poll_packet_count;

		if (mdp->poll_packet_count > UnicastStormPacket)
		{
			if (UnicastStormMsgDump)
				printk("poll_packet_count=%d\n", mdp->poll_packet_count);

			mdp->poll_enter = 1;
			mdp->poll_packet_count = 0;
		}
	}
	else
	{
		/* if jiffies is change, reset count and save the current jiffies */
		mdp->poll_packet_count = 0;
		mdp->poll_previous_jiffies = jiffies;
	}
#else
	if (work_done < budget) {
		napi_complete(napi);
		/* Enable all interrupts */
		sh_eth_write(ndev, cd->eesipr_value, EESIPR);
	}
#endif

    return work_done;
}

#ifdef IDRAC_SUPPORT /* { */
static void sh_eth_timer(unsigned long data)
{
    struct net_device *ndev = (struct net_device *)data;
    struct sh_eth_private *mdp = netdev_priv(ndev);

    mod_timer(&mdp->timer, jiffies + (10 * HZ));
}
#endif /* } IDRAC_SUPPORT */

#ifdef BROADCAST_PROTECTION /* { */
static void sh_eth_broadcast_timer(unsigned long data)
{
   struct net_device *ndev = (struct net_device *)data;
   struct sh_eth_private *mdp = netdev_priv(ndev);

   //printk(KERN_DEBUG "%s: mdp->cd->receive_packet_cnt = %d \n",__FUNCTION__,(int)mdp->cd->receive_packet_cnt);

   if( mdp->cd->receive_packet_cnt <= mdp->cd->network_storm_threshold ) {
      //printk(KERN_DEBUG "%s: NO OVERFLOW! Start receiving the Broadcast packets! \n",__FUNCTION__);

      //There is no overflow event!
      if (mdp->cd->set_broadcast_cnt) {
         //wait for counter to reset before disabling the storm prevention
         if( mdp->cd->broadcast_storm_clear_cnt == 0 ) {
             //reset the BCFRR
             mdp->cd->bcfrr_value = 0;
             mdp->cd->set_broadcast_cnt(ndev);
         }
         if( mdp->cd->broadcast_storm_clear_cnt != 0 ) {
             //only for debug messages
             //printk(KERN_DEBUG "%s: broadcast_storm_clear_cnt = %d \n",__FUNCTION__,(int)mdp->cd->broadcast_storm_clear_cnt);
         }
         if ( mdp->cd->broadcast_storm_clear_cnt > 0) {
            mdp->cd->network_storm_threshold = nBroadStormPacketsActive;
         } else {
            mdp->cd->network_storm_threshold = nBroadStormPackets;
         }
      }
#ifdef MULTICAST_PROTECTION /* { */
      //Enable receiving Multicast packets
      if (!(ndev->flags & IFF_MULTICAST)) {
        //printk(KERN_DEBUG "%s: Start reciving the multicast packets!  \n", __FUNCTION__);
        ndev->flags |= IFF_MULTICAST;
        dev_set_rx_mode(ndev);
      }
#endif  /* } MULTICAST_PROTECTION */
      if ( mdp->cd->broadcast_storm_clear_cnt > 0) {
         mdp->cd->broadcast_storm_clear_cnt --;
         mod_timer(&mdp->broadcast_timer, jiffies + (nBroadStormIntervalActive * (HZ/100)));
      } else {
         //Set the Time interval to default value
         mod_timer(&mdp->broadcast_timer, jiffies + (nBroadStormInterval * (HZ/100)));
      }
   } else {
      mdp->cd->broadcast_storm_clear_cnt = nBroadStormClearIntervals;
      //printk(KERN_DEBUG "%s: Overflow Event Occurs! Broadcast storm prevention mechanism starts!  \n", __FUNCTION__);

      //There are overflow events! Broadcast storm prevention mechanism starts.
      if (mdp->cd->set_broadcast_cnt) {
         //set the BCFRR
         mdp->cd->bcfrr_value = 1;
         mdp->cd->set_broadcast_cnt(ndev);
      }
#ifdef MULTICAST_PROTECTION /* { */
      //Multicast Protection
      if (!(ndev->flags & IFF_MULTICAST)) {
         //do nothing
      } else {
         //printk(KERN_DEBUG "%s: Disable receiving multicast packets!  \n", __FUNCTION__);
         ndev->flags &= ~IFF_MULTICAST;
         dev_set_rx_mode(ndev);
      }
#endif  /* } MULTICAST_PROTECTION */
      mod_timer(&mdp->broadcast_timer, jiffies + (nBroadStormIntervalActive * (HZ/100)));
   }
   mdp->cd->receive_packet_cnt = 0;
}
#endif  /* } BROADCAST_PROTECTION */


#ifdef UNICAST_PROTECTION
static void sh_eth_rx_delay_timer(unsigned long data)
{
	struct net_device *ndev = (struct net_device *)data;
	struct sh_eth_private *mdp = netdev_priv(ndev);
    struct sh_eth_cpu_data *cd = mdp->cd;

	if (UnicastStormMsgDump)
		printk("sh_eth_rx_delay_timer enter jiffies=%x\n", jiffies);

    if (napi_schedule_prep(&mdp->napi)) {
        __napi_schedule(&mdp->napi);

		if (UnicastStormMsgDump)
			printk("__napi_schedule%x\n");
    }

	return;
}
#endif


/* PHY state control function */
static void sh_eth_adjust_link(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    struct phy_device *phydev = mdp->phydev;
    int new_state = 0;

#ifdef IDRAC_SUPPORT /* { */
  if (phydev) {
#endif /* } IDRAC_SUPPORT */
    if (phydev->link != PHY_DOWN) {
        if (phydev->duplex != mdp->duplex) {
            new_state = 1;
            mdp->duplex = phydev->duplex;
            if (mdp->cd->set_duplex)
                mdp->cd->set_duplex(ndev);
        }

        if (phydev->speed != mdp->speed) {
            new_state = 1;
            mdp->speed = phydev->speed;
            if (mdp->cd->set_rate)
                mdp->cd->set_rate(ndev);
        }
        if (mdp->link == PHY_DOWN) {
            sh_eth_write(ndev,
                (sh_eth_read(ndev, ECMR) & ~ECMR_TXF), ECMR);
#ifdef IDRAC_SUPPORT /* { */
#ifdef PHY_LAST_FORCE_SETTING /* { */
            if ((system_type != SYS_TYPE_BLADE) && (phydev->autoneg == AUTONEG_DISABLE)) {
                if (new_state) {
                    phy_print_status(phydev);
                    new_state = 0;
                }
                mdp->link = phydev->link;
                phydev->speed = phy_last_cmd.speed;
                phydev->duplex = phy_last_cmd.duplex;
                phydev->state = PHY_UP;
            } else {
                new_state = 1;
                mdp->link = phydev->link;
            }
#endif /* } PHY_LAST_FORCE_SETTING */
#endif /* } IDRAC_SUPPORT */
        }
    } else if (mdp->link) {
        new_state = 1;
        mdp->link = PHY_DOWN;
        mdp->speed = 0;
        mdp->duplex = -1;
    }

    if (new_state && netif_msg_link(mdp)) {
#ifdef IDRAC_SUPPORT /* { */
       phydev->state = PHY_CHANGELINK;
#endif /* } IDRAC_SUPPORT */
       phy_print_status(phydev);
    }
#ifdef IDRAC_SUPPORT /* { */
  } else {
     DRV_WARN("%s: phydev is NULL\n",__FUNCTION__);
  }
#endif /* } IDRAC_SUPPORT */
}

/* PHY init function */
static int sh_eth_phy_init(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    char phy_id[MII_BUS_ID_SIZE + 3];
    struct phy_device *phydev = NULL;
#ifdef IDRAC_SUPPORT /* { */
    int i;
#endif /* } IDRAC_SUPPORT */

    snprintf(phy_id, sizeof(phy_id), PHY_ID_FMT,
        mdp->mii_bus->id , mdp->phy_id);

    mdp->link = PHY_DOWN;
    mdp->speed = 0;
    mdp->duplex = -1;

    /* Try connect to PHY */
    phydev = phy_connect(ndev, phy_id, sh_eth_adjust_link, mdp->phy_interface);
    if (IS_ERR(phydev)) {
        dev_err(&ndev->dev, "phy_connect failed\n");

#ifdef IDRAC_SUPPORT /* { */
        for (i = 0; i < 32; i++) {
            snprintf(phy_id, sizeof(phy_id), PHY_ID_FMT, mdp->mii_bus->id , i);

            mdp->link = PHY_DOWN;
            mdp->speed = 0;
            mdp->duplex = -1;

            /* Try connect to PHY */
            phydev = phy_connect(ndev, phy_id, &sh_eth_adjust_link,
                        PHY_INTERFACE_MODE_MII);
            if (IS_ERR(phydev)) {
                continue;
            }  else {
                dev_info(&ndev->dev, "phy_connect autoscan succeeded - %d", i);
                mdp->phy_id = i;
                break;
            }
        }

        if (i == 32) {
           dev_err(&ndev->dev, "phy_connect autoscan failed\n");
#endif /* } IDRAC_SUPPORT */
           return PTR_ERR(phydev);
#ifdef IDRAC_SUPPORT /* { */
        }
#endif /* } IDRAC_SUPPORT */
    }

    dev_info(&ndev->dev, "attached phy %i to driver %s\n",
        phydev->addr, phydev->drv->name);

    mdp->phydev = phydev;

    return 0;
}

/* PHY control start function */
static int sh_eth_phy_start(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int ret;
    int reg;

    ret = sh_eth_phy_init(ndev);
    if (ret)
        return ret;

#ifdef IDRAC_SUPPORT /* { */
    if (mdp->phydev) {
#endif /* } IDRAC_SUPPORT */
       /* reset phy - this also wakes it from PDOWN */
       phy_write(mdp->phydev, MII_BMCR, BMCR_RESET);
       if(planar_type != SYS_PLANAR_TYPE_13G)
       {
           // Disable EEE mode
           printk("Disabling EEE mode\n");
           mdelay(10);
           phy_write(mdp->phydev, 0x0d, 0x07);
           phy_write(mdp->phydev, 0x0e, 0x803d);
           phy_write(mdp->phydev, 0x0d, 0x4007);
           phy_write(mdp->phydev, 0x0e, 0x0);
       }
       else
       {
           printk("Not disabling EEE mode\n");
           mdelay(10);
           phy_write(mdp->phydev, 0x17, 0x0000); // Ensure we are on page zero
           unsigned int id = (unsigned int) phy_read(mdp->phydev, 0x02); // read PHY ID MSB
           id <<= 16;
           id |= (unsigned int) phy_read(mdp->phydev, 0x03); // read PHY ID LSB
           id &= 0xFFFFFFF0; // mask off revision bits (3:0)
           printk("PHY ID Masked: %x\n", id);

           //Check PHY is BMC54612
           if(id == 0x03625e60)
           {
              // Disable Broadcom AutogrEEEn mode
              printk("Disabling AutogrEEEn\n");
              phy_write(mdp->phydev, 0x17, 0x0d40); // change page
              phy_write(mdp->phydev, 0x15, 0x0000); // disable AutogrEEEn feature
              phy_write(mdp->phydev, 0x17, 0x0000); // change page back to default
           }
       }
       if(idrac_type == PANTERA_IDRAC)
       {
           printk("Enabling 125mhz phy clock\n");
           phy_write(mdp->phydev, 0x17, 0x0d34);
           phy_write(mdp->phydev, 0x15, ((phy_read(mdp->phydev, 0x15)) | 0x02));
       }


       phy_start(mdp->phydev);
#ifdef IDRAC_SUPPORT /* { */
    }
#endif /* } IDRAC_SUPPORT */

    return 0;
}

static int sh_eth_get_settings(struct net_device *ndev,
            struct ethtool_cmd *ecmd)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    unsigned long flags;
    int ret;

#ifdef IDRAC_SUPPORT /* { */
    if(!mdp->phydev)
        return -ENODEV;
#endif /* } IDRAC_SUPPORT */

#ifdef PHY_DEBUG
    if (PHYDebug)
    {
        if (PHYShadow)
        {
            printk("PHY: Reg=%x Data=%lx\n", PHYReg, (PHYShadow << 10));
            phy_write(mdp->phydev, PHYReg, (PHYShadow << 10));
            printk("PHY: Read=%lx\n", phy_read(mdp->phydev, PHYReg));
            return 0;
        }
        else
        {
            printk("PHY: Reg=%x\n", PHYReg);
            printk("PHY: Read=%lx\n", phy_read(mdp->phydev, PHYReg));
            return 0;
        }
    }
#endif

    spin_lock_irqsave(&mdp->lock, flags);
    ret = phy_ethtool_gset(mdp->phydev, ecmd);
    spin_unlock_irqrestore(&mdp->lock, flags);

    return ret;
}

static int sh_eth_set_settings(struct net_device *ndev,
        struct ethtool_cmd *ecmd)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    unsigned long flags;
    int ret;

#ifdef IDRAC_SUPPORT /* { */
    if(!mdp->phydev)
       return -ENODEV;
#endif /* } IDRAC_SUPPORT */

#ifdef PHY_DEBUG
    if (PHYDebug)
    {
        if (PHYShadow)
        {
            printk("Reg=%x Data=%lx\n", PHYReg, 0x8000 | (PHYShadow << 10) | PHYData);
            phy_write(mdp->phydev, PHYReg, 0x8000 | (PHYShadow << 10) | PHYData);
            return 0;
        }
        else
        {
            printk("Reg=%x Data=%lx\n", PHYReg, PHYData);
            phy_write(mdp->phydev, PHYReg, PHYData);
            return 0;
        }
    }
#endif

    spin_lock_irqsave(&mdp->lock, flags);

    /* disable tx and rx */
    sh_eth_rcv_snd_disable(ndev);

    ret = phy_ethtool_sset(mdp->phydev, ecmd);
    if (ret)
        goto error_exit;

#ifdef IDRAC_SUPPORT /* { */
        if ((system_type == SYS_TYPE_BLADE) && ((strncmp(ndev->name, "eth0", 4) == 0)))
        {
            mdp->phydev->state = PHY_CHANGELINK;
        }
#ifdef PHY_LAST_FORCE_SETTING
        else
        {
            if (ecmd->autoneg == AUTONEG_DISABLE)
            {
                memcpy(&phy_last_cmd, ecmd, sizeof(phy_last_cmd));
            }
        }
#endif

#if 0 /* sh_eth_adjust_link() function would do the change */
    if (ecmd->duplex == DUPLEX_FULL)
        mdp->duplex = 1;
    else
        mdp->duplex = 0;

    if (mdp->cd->set_duplex)
        mdp->cd->set_duplex(ndev);
#endif

#endif /* } IDRAC_SUPPORT */

error_exit:
    mdelay(1);

    /* enable tx and rx */
    sh_eth_rcv_snd_enable(ndev);

    spin_unlock_irqrestore(&mdp->lock, flags);

    return ret;
}

static int sh_eth_nway_reset(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    unsigned long flags;
    int ret;

    spin_lock_irqsave(&mdp->lock, flags);
    ret = phy_start_aneg(mdp->phydev);
    spin_unlock_irqrestore(&mdp->lock, flags);

    return ret;
}

static u32 sh_eth_get_msglevel(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    return mdp->msg_enable;
}

static void sh_eth_set_msglevel(struct net_device *ndev, u32 value)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    mdp->msg_enable = value;
}

static const char sh_eth_gstrings_stats[][ETH_GSTRING_LEN] = {
    "rx_current", "tx_current",
    "rx_dirty", "tx_dirty",
};
#define SH_ETH_STATS_LEN  ARRAY_SIZE(sh_eth_gstrings_stats)

static int sh_eth_get_sset_count(struct net_device *netdev, int sset)
{
    switch (sset) {
    case ETH_SS_STATS:
        return SH_ETH_STATS_LEN;
    default:
        return -EOPNOTSUPP;
    }
}

static void sh_eth_get_ethtool_stats(struct net_device *ndev,
            struct ethtool_stats *stats, u64 *data)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int i = 0;

    /* device-specific stats */
    data[i++] = mdp->cur_rx;
    data[i++] = mdp->cur_tx;
    data[i++] = mdp->dirty_rx;
    data[i++] = mdp->dirty_tx;
}

static void sh_eth_get_strings(struct net_device *ndev, u32 stringset, u8 *data)
{
    switch (stringset) {
    case ETH_SS_STATS:
        memcpy(data, *sh_eth_gstrings_stats,
                    sizeof(sh_eth_gstrings_stats));
        break;
    }
}

static void sh_eth_get_ringparam(struct net_device *ndev,
                 struct ethtool_ringparam *ring)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    ring->rx_max_pending = RX_RING_MAX;
    ring->tx_max_pending = TX_RING_MAX;
    ring->rx_pending = mdp->num_rx_ring;
    ring->tx_pending = mdp->num_tx_ring;
}

static int sh_eth_set_ringparam(struct net_device *ndev,
                struct ethtool_ringparam *ring)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int ret;

    if (ring->tx_pending > TX_RING_MAX ||
        ring->rx_pending > RX_RING_MAX ||
        ring->tx_pending < TX_RING_MIN ||
        ring->rx_pending < RX_RING_MIN)
        return -EINVAL;
    if (ring->rx_mini_pending || ring->rx_jumbo_pending)
        return -EINVAL;

    if (netif_running(ndev)) {
        netif_tx_disable(ndev);
        /* Disable interrupts by clearing the interrupt mask. */
        sh_eth_write(ndev, 0x0000, EESIPR);
        /* Stop the chip's Tx and Rx processes. */
        sh_eth_write(ndev, 0, EDTRR);
        sh_eth_write(ndev, 0, EDRRR);
        napi_disable(&mdp->napi);
        synchronize_irq(ndev->irq);
    }

    /* Free all the skbuffs in the Rx queue. */
    sh_eth_ring_free(ndev);
    /* Free DMA buffer */
    sh_eth_free_dma_buffer(mdp);

    /* Set new parameters */
    mdp->num_rx_ring = ring->rx_pending;
    mdp->num_tx_ring = ring->tx_pending;

    ret = sh_eth_ring_init(ndev);
    if (ret < 0) {
        dev_err(&ndev->dev, "%s: sh_eth_ring_init failed.\n", __func__);
        return ret;
    }
    ret = sh_eth_dev_init(ndev, false);
    if (ret < 0) {
        dev_err(&ndev->dev, "%s: sh_eth_dev_init failed.\n", __func__);
        return ret;
    }

    if (netif_running(ndev)) {
        napi_enable(&mdp->napi);
        sh_eth_write(ndev, mdp->cd->eesipr_value, EESIPR);
        /* Setting the Rx mode will start the Rx process. */
        sh_eth_write(ndev, EDRRR_R, EDRRR);
        netif_wake_queue(ndev);
    }

    return 0;
}

static const struct ethtool_ops sh_eth_ethtool_ops = {
    .get_settings   = sh_eth_get_settings,
    .set_settings   = sh_eth_set_settings,
    .nway_reset = sh_eth_nway_reset,
    .get_msglevel   = sh_eth_get_msglevel,
    .set_msglevel   = sh_eth_set_msglevel,
    .get_link   = ethtool_op_get_link,
    .get_strings    = sh_eth_get_strings,
    .get_ethtool_stats  = sh_eth_get_ethtool_stats,
    .get_sset_count     = sh_eth_get_sset_count,
    .get_ringparam  = sh_eth_get_ringparam,
    .set_ringparam  = sh_eth_set_ringparam,
};

/* network device open function */
static int sh_eth_open(struct net_device *ndev)
{
    int ret = 0;
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int ringsize;

#ifdef IDRAC_SUPPORT /* { */
    int bmcr = 0;
    int Count=0;

    if (strncmp(ndev->name, "eth0", 4) == 0) {
        sh_mdio_release(ndev);
        sh_mdio_init(ndev, temp_pdev->id, temp_pd);
    }

    /* work around - assume eth1=NC-SI */
    if ((strncmp(ndev->name, "eth1", 4) == 0) ||
        ((system_type != SYS_TYPE_BLADE) &&
         (strncmp(ndev->name, "eth2", 4) == 0)))
    {
        mdp->nic_if.interface_mode = IF_NCSI;
        mdp->u8Interface = IF_NCSI;
    }
#endif /* } IDRAC_SUPPORT */

#ifdef IDRAC_SUPPORT /* { */
	if (IS_IF_VALID(mdp->u8Interface)) {
#endif /* } IDRAC_SUPPORT */

    pm_runtime_get_sync(&mdp->pdev->dev);

    napi_enable(&mdp->napi);

    ret = request_irq(ndev->irq, sh_eth_interrupt,
#if defined(CONFIG_CPU_SUBTYPE_SH7763) || \
    defined(CONFIG_CPU_SUBTYPE_SH7764) || \
    defined(CONFIG_CPU_SUBTYPE_SH7757)
                IRQF_SHARED,
#else
                0,
#endif
                ndev->name, ndev);
    if (ret) {
        dev_err(&ndev->dev, "failed to request irq\n");
        goto out_free_napi;
    }

    /* Descriptor set */
    ret = sh_eth_ring_init(ndev);
    if (ret) {
    	dev_err(&ndev->dev, "failed to init ring\n");
        goto out_free_irq;
    }

    /* device init */
    //delay device start until BROADCAST_PROTECTION up 
    //ret = sh_eth_dev_init(ndev, true);
    ret = sh_eth_dev_init(ndev, false);

    if (ret) {
    	dev_err(&ndev->dev, "failed to init dev\n");
        goto out_free_ring;
    }

#ifdef IDRAC_SUPPORT /* { */
    /**MAC I/F*/
    if (IF_NCSI != mdp->u8Interface) {
#endif /* } IDRAC_SUPPORT */

        /* PHY control start*/
        ret = sh_eth_phy_start(ndev);
        if (ret) {
        	dev_err(&ndev->dev, "failed to init phy\n");
            goto out_free_dev;
        }
#ifdef IDRAC_SUPPORT /* { */
    }
#endif /* } IDRAC_SUPPORT */

#ifdef IDRAC_SUPPORT /* { */
    if ((IF_RMII_PHY_NCSI == mdp->u8Interface)
        || (IF_MII_PHY_NCSI == mdp->u8Interface)
        || (IF_GMII_PHY_NCSI == mdp->u8Interface)) {
        // if PHY support
        while(1) {
            if (Count > 3) {
                printk(KERN_EMERG"PHY_INIT count %d, bmcr is %d\n",(Count+1),bmcr);
                break;
            }
            phy_start_aneg(mdp->phydev);
            mdelay(2000);

            bmcr = phy_read(mdp->phydev, MII_BMCR);
            if (bmcr>0) {
                break;
            } else {
                printk(KERN_EMERG"PHY_INIT count %d, bmcr is %d\n",(Count+1),bmcr);
            }
            Count++;
        }

        if (bmcr > 0) {
            mdp->link = PHY_UP;
            mdp->phydev->link = PHY_UP;
            if (bmcr & BMCR_FULLDPLX) {
                mdp->phydev->duplex = DUPLEX_FULL;
                mdp->duplex = DUPLEX_FULL;
                sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_DM, ECMR);
            } else {
                mdp->phydev->duplex = DUPLEX_HALF;
                mdp->duplex = DUPLEX_HALF;
                sh_eth_write(ndev, sh_eth_read(ndev, ECMR) & ~ECMR_DM, ECMR);
            }

            if (bmcr & BMCR_SPEED100) {
                 mdp->phydev->speed = SPEED_100;
                 mdp->speed = 100;
                 sh_eth_write(ndev, 1, RTRATE);
            } else {
                 mdp->phydev->speed = SPEED_10;
                 mdp->speed = 10;
                 sh_eth_write(ndev, 0, RTRATE);
            }

            if(planar_type != SYS_PLANAR_TYPE_13G)
            {
                // Disable EEE mode
                printk("Disabling EEE mode.\n");
                mdelay(10);
                phy_write(mdp->phydev, 0x0d, 0x07);
                phy_write(mdp->phydev, 0x0e, 0x803d);
                phy_write(mdp->phydev, 0x0d, 0x4007);
                phy_write(mdp->phydev, 0x0e, 0x0);
            }
            else
            {
                printk("Not disabling EEE mode.\n");
            }

            if(idrac_type == PANTERA_IDRAC)
            {
                printk("Enabling 125mhz phy clock\n");
                phy_write(mdp->phydev, 0x17, 0x0d34);
                phy_write(mdp->phydev, 0x15, ((phy_read(mdp->phydev, 0x15)) | 0x02));
            }

            netif_carrier_on(ndev);
        }
    } //if((IF_RMII_PHY_NCSI==mdp->u8Interface)......)
    else if (IF_NCSI == mdp->u8Interface) {
        sh_eth_write(ndev, 1, RTRATE);
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_DM, ECMR);

        netif_carrier_on(ndev);
    }

    //sync status of PHY
    if (mdp->phydev) {
        mdp->phydev->state = PHY_CHANGELINK;
    }

    //fix blade server's eth0 with 100/Full (speed/duplex)
    if ((system_type == SYS_TYPE_BLADE) && ((strncmp(ndev->name, "eth0", 4) == 0))) {
        mdp->phydev->advertising = ADVERTISE_100FULL;
        mdp->phydev->autoneg = AUTONEG_DISABLE;

        // Set duplex
        mdp->phydev->duplex = DUPLEX_FULL;
        mdp->duplex = DUPLEX_FULL;
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_DM, ECMR);

        // Set Speed
        mdp->phydev->speed = SPEED_100;
        mdp->speed = SPEED_100;
        sh_eth_write(ndev, 1, RTRATE);

        bmcr = phy_read(mdp->phydev, MII_BMCR);
        bmcr |= 0x00001 <<13;    // set PHY devce speed to 100M
        bmcr |= 0x00001 <<8;     // set PHY device duplex to full
        bmcr &= ~(0x00001 <<12); // disable PHY device auto negotiation
        phy_write(mdp->phydev, MII_BMCR, bmcr);
    }

    /* Enable interrupts */
    //delay interrupts until BROADCAST_PROTECTION up 
    //sh_eth_write(ndev, mdp->cd->eesipr_value, EESIPR);
    
    /* Set the timer to check for link beat. */
    init_timer(&mdp->timer);
    mdp->timer.expires = jiffies + ((24 * HZ) / 10);/* 2.4 sec. */
    setup_timer(&mdp->timer, sh_eth_timer, (unsigned long)ndev);
#endif /* } IDRAC_SUPPORT */

#ifdef BROADCAST_PROTECTION /* { */
    /* Set the timer to check the broadcast status */
    init_timer(&mdp->broadcast_timer);
    mdp->broadcast_timer.expires = jiffies + ((3 * HZ));/* 3 sec. */
    setup_timer(&mdp->broadcast_timer, sh_eth_broadcast_timer, (unsigned long)ndev);
    add_timer(&mdp->broadcast_timer);
#endif  /* } BROADCAST_PROTECTION */

#ifdef UNICAST_PROTECTION
	init_timer(&mdp->rx_delay_timer);
	mdp->rx_delay_timer.expires = jiffies + (HZ / 100);	/* 10 ms */
	setup_timer(&mdp->rx_delay_timer, sh_eth_rx_delay_timer, (unsigned long)ndev);
	mdp->poll_count = 0;
	mdp->poll_enter = 0;
	mdp->poll_packet_count = 0;
	mdp->poll_previous_jiffies = 0;
#endif

    /* E-MAC Interrupt Enable register */
    sh_eth_write(ndev, mdp->cd->ecsipr_value, ECSIPR);
    /* Enable interrupts */
    sh_eth_write(ndev, mdp->cd->eesipr_value, EESIPR);

    /* Setting the Rx mode will start the Rx process. */
    sh_eth_write(ndev, EDRRR_R, EDRRR);
    netif_start_queue(ndev);


#ifdef IDRAC_SUPPORT /* { */
	} else {
		ret = -ENETDOWN;
	}
#endif /* } IDRAC_SUPPORT */

    return ret;

out_free_dev:
    netif_stop_queue(ndev);

    /* Stop the chip's Rx processe. */
    sh_eth_write(ndev, 0, EDRRR);

out_free_ring:
    /* Free all the skbuffs in the Rx queue. */
    sh_eth_ring_free(ndev);

    /* free DMA buffer */
    ringsize = sizeof(struct sh_eth_rxdesc) * RX_RING_SIZE;
    dma_free_coherent(NULL, ringsize, mdp->rx_ring, mdp->rx_desc_dma);

    /* free DMA buffer */
    ringsize = sizeof(struct sh_eth_txdesc) * TX_RING_SIZE;
    dma_free_coherent(NULL, ringsize, mdp->tx_ring, mdp->tx_desc_dma);

out_free_irq:
    free_irq(ndev->irq, ndev);
    pm_runtime_put_sync(&mdp->pdev->dev);

out_free_napi:
	napi_disable(&mdp->napi);

	return ret;
}

/* Timeout function */
static void sh_eth_tx_timeout(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    struct sh_eth_rxdesc *rxdesc;
    int i;

    netif_stop_queue(ndev);

    if (netif_msg_timer(mdp))
        dev_err(&ndev->dev, "%s: transmit timed out, status %8.8x,"
           " resetting...\n", ndev->name, (int)sh_eth_read(ndev, EESR));

    /* tx_errors count up */
    ndev->stats.tx_errors++;

#ifdef IDRAC_SUPPORT /* { */
    /* timer off */
    del_timer_sync(&mdp->timer);
#endif /* } IDRAC_SUPPORT */

    /* Free all the skbuffs in the Rx queue. */
    for (i = 0; i < mdp->num_rx_ring; i++) {
        rxdesc = &mdp->rx_ring[i];
        rxdesc->status = 0;
        rxdesc->addr = 0xBADF00D0;
        if (mdp->rx_skbuff[i])
            dev_kfree_skb(mdp->rx_skbuff[i]);
        mdp->rx_skbuff[i] = NULL;
    }
    for (i = 0; i < mdp->num_tx_ring; i++) {
        if (mdp->tx_skbuff[i])
            dev_kfree_skb(mdp->tx_skbuff[i]);
        mdp->tx_skbuff[i] = NULL;
    }

#ifdef IDRAC_SUPPORT /* { */
    /**MAC I/F*/
    if (IS_IF_VALID(mdp->u8Interface)) {
#endif /* } IDRAC_SUPPORT */
       /* device init */
       sh_eth_dev_init(ndev, true);

#ifdef IDRAC_SUPPORT /* { */
       /* timer on */
       mdp->timer.expires = jiffies + ((24 * HZ) / 10 );/* 2.4 sec. */
       add_timer(&mdp->timer);
    }
#endif /* } IDRAC_SUPPORT */
}

/* Packet transmit function */
static int sh_eth_start_xmit(struct sk_buff *skb, struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    struct sh_eth_txdesc *txdesc;
    u32 entry;

    if (skb->len < ETHERSMALL) {
        if (skb_pad (skb, ETHERSMALL - skb->len) != 0) {
            printk (KERN_ERR "sh_eth_start_xmt: Padding failed\n");
            return NETDEV_TX_BUSY; //skb has been freed
        }
        skb->len = ETHERSMALL;
    }
    entry = mdp->cur_tx % mdp->num_tx_ring;
    mdp->tx_skbuff[entry] = skb;
    txdesc = &mdp->tx_ring[entry];
    /* soft swap. */
    if (!mdp->cd->hw_swap)
        sh_eth_soft_swap(phys_to_virt(ALIGN(txdesc->addr, 4)),
                 skb->len + 2);
    txdesc->addr = dma_map_single(&ndev->dev, skb->data, skb->len,
                      DMA_TO_DEVICE);
    txdesc->buffer_length = skb->len;

    if (entry >= mdp->num_tx_ring - 1)
        txdesc->status |= cpu_to_edmac(mdp, TD_TACT | TD_TDLE);
    else
        txdesc->status |= cpu_to_edmac(mdp, TD_TACT);

    mdp->cur_tx++;

    if (!(sh_eth_read(ndev, EDTRR) & sh_eth_get_edtrr_trns(mdp)))
        sh_eth_write(ndev, sh_eth_get_edtrr_trns(mdp), EDTRR);

    if ((mdp->cur_tx - mdp->dirty_tx) >= (mdp->num_tx_ring - 4)) {
        if (netif_msg_tx_queued(mdp))
            dev_warn(&ndev->dev, "TxFD exhausted.\n");
        netif_stop_queue(ndev);
    }

#ifdef ETH_DEBUG_MSG
	sh_eth_msg_dump(1, skb, ndev);
#endif

    return NETDEV_TX_OK;
}

/* device close function */
static int sh_eth_close(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    netif_stop_queue(ndev);

    /* Disable interrupts by clearing the interrupt mask. */
    sh_eth_write(ndev, 0x0000, EESIPR);

#ifdef IDRAC_SUPPORT /* { */
    /** - Ethernet Software reset*/
    sh_eth_reset(ndev);
#endif /* } IDRAC_SUPPORT */

    /* Stop the chip's Tx and Rx processes. */
    sh_eth_write(ndev, 0, EDTRR);
    sh_eth_write(ndev, 0, EDRRR);

    /* PHY Disconnect */
    if (mdp->phydev) {
        phy_stop(mdp->phydev);
        phy_disconnect(mdp->phydev);
#ifdef IDRAC_SUPPORT /* { */
        mdp->phydev = NULL;
#endif /* } IDRAC_SUPPORT */
    }

    napi_disable(&mdp->napi);

    free_irq(ndev->irq, ndev);

    del_timer_sync(&mdp->timer);

#ifdef BROADCAST_PROTECTION /* { */
    del_timer_sync(&mdp->broadcast_timer);
#endif  /* } BROADCAST_PROTECTION */

#ifdef UNICAST_PROTECTION
    del_timer_sync(&mdp->rx_delay_timer);
#endif

    /* Free all the skbuffs in the Rx queue. */
    sh_eth_ring_free(ndev);

    /* free DMA buffer */
    sh_eth_free_dma_buffer(mdp);

    pm_runtime_put_sync(&mdp->pdev->dev);

    return 0;
}

static struct net_device_stats *sh_eth_get_stats(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);

    pm_runtime_get_sync(&mdp->pdev->dev);
    ndev->stats.tx_dropped += sh_eth_read(ndev, TROCR);
    sh_eth_write(ndev, 0, TROCR);   /* (write clear) */
    ndev->stats.collisions += sh_eth_read(ndev, CDCR);
    sh_eth_write(ndev, 0, CDCR);    /* (write clear) */
    ndev->stats.tx_carrier_errors += sh_eth_read(ndev, LCCR);
    sh_eth_write(ndev, 0, LCCR);    /* (write clear) */
    if (sh_eth_is_gether(mdp)) {
        ndev->stats.tx_carrier_errors += sh_eth_read(ndev, CERCR);
        sh_eth_write(ndev, 0, CERCR);   /* (write clear) */
        ndev->stats.tx_carrier_errors += sh_eth_read(ndev, CEECR);
        sh_eth_write(ndev, 0, CEECR);   /* (write clear) */
    } else {
        ndev->stats.tx_carrier_errors += sh_eth_read(ndev, CNDCR);
        sh_eth_write(ndev, 0, CNDCR);   /* (write clear) */
    }
    pm_runtime_put_sync(&mdp->pdev->dev);

    return &ndev->stats;
}

/* ioctl to device function */
static int sh_eth_do_ioctl(struct net_device *ndev, struct ifreq *rq,
                int cmd)
{
#ifdef IDRAC_SUPPORT /* { */
#define ENABLE_PHY_IOCTL_SETTING 1

    struct sh_eth_private *mdp = netdev_priv(ndev);

#if ENABLE_PHY_IOCTL_SETTING
    struct phy_device *phydev = mdp->phydev;
#endif

    u32 ioaddr = ndev->base_addr;
    u8 *pmacif= (u8 *)rq->ifr_ifru.ifru_data;
    int rc = 0;

    /** - Parse I/O Control command. Only SET_MAC_IF command is supported, currently.*/
    switch(cmd)
    {
        /**Implement user defined IOCtrl commands here*/
      case SET_MAC_IF:
            /** - Handling SET_MAC_IF command*/
            dev_info(&ndev->dev, "SET_MAC_IF_STATUS ioctl command\n");

                /** - =>Check if networ is running, if yes, return -EBUSY*/
            if (netif_running(ndev))
                 return -EBUSY;

                /** - =>Check if network device is valid, if no, return -EOPNOTSUPP.*/
            if (strncmp(ndev->name, "eth0", 4) == 0) {
                dev_info(&ndev->dev, "eth0: Interface\n");
                dev_info(&ndev->dev, "Interface: %x\n", pmacif[0]);
            } else if(strncmp(ndev->name, "eth1", 4) == 0) {
                dev_info(&ndev->dev, "eth1: Interface and status\n");
                dev_info(&ndev->dev, "Interface: %x\n", pmacif[0]);
            } else if(strncmp(ndev->name, "eth2", 4) == 0) {
                dev_info(&ndev->dev, "eth2: Interface and status\n");
                dev_info(&ndev->dev, "Interface: %x\n", pmacif[0]);
            } else {
                printk(KERN_EMERG"Not valid nic name:%s\n",ndev->name);
                return -EOPNOTSUPP;
                break;
            }

            if((NULL != mdp) && (NULL != pmacif)) {
              /** - =>Update network interface.*/
              mdp->nic_if.interface_mode = pmacif[0];
              mdp->u8Interface = pmacif[0];

              switch(mdp->nic_if.interface_mode /*mdp->u8Interface*/)
              {
                /**There are 8 I/Fs for each MAC.
                   1.IF_NONE, 2. IF_RMII_PHY, 3.IF_MII_PHY, 4. IF_GMII_PHY,
                   5.IF_NCSI, 6. IF_RMII_PHY_NCSI, 7.IF_MII_PHY_NCSI,
                   8.IF_GMII_PHY_NCSI
                 From firmware's point of view, behavior of accessing MII, RMII, GMII,
                 IF_RMII_PHY_NCSI, IF_MII_PHY_NCSI, and IF_GMII_PHY_NCSI are the same.
                 So there are only three different behaviors in firmware
                 a. IF_NONE,
                 b. PHY,
                 c. NCSI
                 */
                case IF_NCSI:
                case IF_NONE:
                case IF_RMII_PHY_NCSI:
                case IF_RMII_PHY:
                case IF_MII_PHY:
                case IF_GMII_PHY:
                case IF_MII_PHY_NCSI:
                case IF_GMII_PHY_NCSI:


                    break;
                default:
                    /**Invalid interface*/
                    return -EBADRQC;
                    /**If interface is neither IF_NONE nor IF_NCSI,
                       we will used default settings(MAC0/MAC1 are both connected RMII).*/
                    break;
              }
            } else {
              return -ENODATA;
            }
            return rc;
          break;
      default:
//          dev_err(&ndev->dev, "IOCTL cmd(0x%x) is not supported\n",cmd);
          return -EOPNOTSUPP;
    }

    if (mdp->nic_if.interface_mode & IF_NCSI) {
        /**The interface is IF_RMII_PHY_NCSI|IF_MII_PHY_NCSI|IF_GMII_PHY_NCSI|IF_NCSI*/
        ndev->flags |= IFF_PROMISC;
        sh_eth_write(ndev, sh_eth_read(ndev, ECMR) | ECMR_PRM, ECMR);
        dev_info(&ndev->dev, "\nmdp->nic_if.interface_mode & IF_NCSI !=0  ctrl_inl(ioaddr + ECMR)=%x \n",ctrl_inl(ioaddr + ECMR) );
    }
#endif /* } IDRAC_SUPPORT */

    if (!netif_running(ndev))
        return -EINVAL;

    if (!phydev)
        return -ENODEV;

    return phy_mii_ioctl(phydev, rq, cmd);
}

#if defined(SH_ETH_HAS_TSU)
/* For TSU_POSTn. Please refer to the manual about this (strange) bitfields */
static void *sh_eth_tsu_get_post_reg_offset(struct sh_eth_private *mdp,
                        int entry)
{
    return sh_eth_tsu_get_offset(mdp, TSU_POST1) + (entry / 8 * 4);
}

static u32 sh_eth_tsu_get_post_mask(int entry)
{
    return 0x0f << (28 - ((entry % 8) * 4));
}

static u32 sh_eth_tsu_get_post_bit(struct sh_eth_private *mdp, int entry)
{
    return (0x08 >> (mdp->port << 1)) << (28 - ((entry % 8) * 4));
}

static void sh_eth_tsu_enable_cam_entry_post(struct net_device *ndev,
                         int entry)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    u32 tmp;
    void *reg_offset;

    reg_offset = sh_eth_tsu_get_post_reg_offset(mdp, entry);
    tmp = ioread32(reg_offset);
    iowrite32(tmp | sh_eth_tsu_get_post_bit(mdp, entry), reg_offset);
}

static bool sh_eth_tsu_disable_cam_entry_post(struct net_device *ndev,
                          int entry)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    u32 post_mask, ref_mask, tmp;
    void *reg_offset;

    reg_offset = sh_eth_tsu_get_post_reg_offset(mdp, entry);
    post_mask = sh_eth_tsu_get_post_mask(entry);
    ref_mask = sh_eth_tsu_get_post_bit(mdp, entry) & ~post_mask;

    tmp = ioread32(reg_offset);
    iowrite32(tmp & ~post_mask, reg_offset);

    /* If other port enables, the function returns "true" */
    return tmp & ref_mask;
}

static int sh_eth_tsu_busy(struct net_device *ndev)
{
    int timeout = SH_ETH_TSU_TIMEOUT_MS * 100;
    struct sh_eth_private *mdp = netdev_priv(ndev);

    while ((sh_eth_tsu_read(mdp, TSU_ADSBSY) & TSU_ADSBSY_0)) {
        udelay(10);
        timeout--;
        if (timeout <= 0) {
            dev_err(&ndev->dev, "%s: timeout\n", __func__);
            return -ETIMEDOUT;
        }
    }

    return 0;
}

static int sh_eth_tsu_write_entry(struct net_device *ndev, void *reg,
                  const u8 *addr)
{
    u32 val;

    val = addr[0] << 24 | addr[1] << 16 | addr[2] << 8 | addr[3];
    iowrite32(val, reg);
    if (sh_eth_tsu_busy(ndev) < 0)
        return -EBUSY;

    val = addr[4] << 8 | addr[5];
    iowrite32(val, reg + 4);
    if (sh_eth_tsu_busy(ndev) < 0)
        return -EBUSY;

    return 0;
}

static void sh_eth_tsu_read_entry(void *reg, u8 *addr)
{
    u32 val;

    val = ioread32(reg);
    addr[0] = (val >> 24) & 0xff;
    addr[1] = (val >> 16) & 0xff;
    addr[2] = (val >> 8) & 0xff;
    addr[3] = val & 0xff;
    val = ioread32(reg + 4);
    addr[4] = (val >> 8) & 0xff;
    addr[5] = val & 0xff;
}


static int sh_eth_tsu_find_entry(struct net_device *ndev, const u8 *addr)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    void *reg_offset = sh_eth_tsu_get_offset(mdp, TSU_ADRH0);
    int i;
    u8 c_addr[ETH_ALEN];

    for (i = 0; i < SH_ETH_TSU_CAM_ENTRIES; i++, reg_offset += 8) {
        sh_eth_tsu_read_entry(reg_offset, c_addr);
        if (memcmp(addr, c_addr, ETH_ALEN) == 0)
            return i;
    }

    return -ENOENT;
}

static int sh_eth_tsu_find_empty(struct net_device *ndev)
{
    u8 blank[ETH_ALEN];
    int entry;

    memset(blank, 0, sizeof(blank));
    entry = sh_eth_tsu_find_entry(ndev, blank);
    return (entry < 0) ? -ENOMEM : entry;
}

static int sh_eth_tsu_disable_cam_entry_table(struct net_device *ndev,
                          int entry)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    void *reg_offset = sh_eth_tsu_get_offset(mdp, TSU_ADRH0);
    int ret;
    u8 blank[ETH_ALEN];

    sh_eth_tsu_write(mdp, sh_eth_tsu_read(mdp, TSU_TEN) &
             ~(1 << (31 - entry)), TSU_TEN);

    memset(blank, 0, sizeof(blank));
    ret = sh_eth_tsu_write_entry(ndev, reg_offset + entry * 8, blank);
    if (ret < 0)
        return ret;
    return 0;
}

static int sh_eth_tsu_add_entry(struct net_device *ndev, const u8 *addr)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    void *reg_offset = sh_eth_tsu_get_offset(mdp, TSU_ADRH0);
    int i, ret;

    if (!mdp->cd->tsu)
        return 0;

    i = sh_eth_tsu_find_entry(ndev, addr);
    if (i < 0) {
        /* No entry found, create one */
        i = sh_eth_tsu_find_empty(ndev);
        if (i < 0)
            return -ENOMEM;
        ret = sh_eth_tsu_write_entry(ndev, reg_offset + i * 8, addr);
        if (ret < 0)
            return ret;

        /* Enable the entry */
        sh_eth_tsu_write(mdp, sh_eth_tsu_read(mdp, TSU_TEN) |
                 (1 << (31 - i)), TSU_TEN);
    }

    /* Entry found or created, enable POST */
    sh_eth_tsu_enable_cam_entry_post(ndev, i);

    return 0;
}

static int sh_eth_tsu_del_entry(struct net_device *ndev, const u8 *addr)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int i, ret;

    if (!mdp->cd->tsu)
        return 0;

    i = sh_eth_tsu_find_entry(ndev, addr);
    if (i) {
        /* Entry found */
        if (sh_eth_tsu_disable_cam_entry_post(ndev, i))
            goto done;

        /* Disable the entry if both ports was disabled */
        ret = sh_eth_tsu_disable_cam_entry_table(ndev, i);
        if (ret < 0)
            return ret;
    }
done:
    return 0;
}

static int sh_eth_tsu_purge_all(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int i, ret;

    if (unlikely(!mdp->cd->tsu))
        return 0;

    for (i = 0; i < SH_ETH_TSU_CAM_ENTRIES; i++) {
        if (sh_eth_tsu_disable_cam_entry_post(ndev, i))
            continue;

        /* Disable the entry if both ports was disabled */
        ret = sh_eth_tsu_disable_cam_entry_table(ndev, i);
        if (ret < 0)
            return ret;
    }

    return 0;
}

static void sh_eth_tsu_purge_mcast(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    u8 addr[ETH_ALEN];
    void *reg_offset = sh_eth_tsu_get_offset(mdp, TSU_ADRH0);
    int i;

    if (unlikely(!mdp->cd->tsu))
        return;

    for (i = 0; i < SH_ETH_TSU_CAM_ENTRIES; i++, reg_offset += 8) {
        sh_eth_tsu_read_entry(reg_offset, addr);
        if (is_multicast_ether_addr(addr))
            sh_eth_tsu_del_entry(ndev, addr);
    }
}

/* Multicast reception directions set */
static void sh_eth_set_multicast_list(struct net_device *ndev)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    u32 ecmr_bits;
    int mcast_all = 0;
    unsigned long flags;

    spin_lock_irqsave(&mdp->lock, flags);
    /*
     * Initial condition is MCT = 1, PRM = 0.
     * Depending on ndev->flags, set PRM or clear MCT
     */
    ecmr_bits = (sh_eth_read(ndev, ECMR) & ~ECMR_PRM) | ECMR_MCT;

    if (!(ndev->flags & IFF_MULTICAST)) {
        sh_eth_tsu_purge_mcast(ndev);
        mcast_all = 1;
    }
    if (ndev->flags & IFF_ALLMULTI) {
        sh_eth_tsu_purge_mcast(ndev);
        ecmr_bits &= ~ECMR_MCT;
        mcast_all = 1;
    }

    if (ndev->flags & IFF_PROMISC) {
        sh_eth_tsu_purge_all(ndev);
        ecmr_bits = (ecmr_bits & ~ECMR_MCT) | ECMR_PRM;
    } else if (mdp->cd->tsu) {
        struct netdev_hw_addr *ha;
        netdev_for_each_mc_addr(ha, ndev) {
            if (mcast_all && is_multicast_ether_addr(ha->addr))
                continue;

            if (sh_eth_tsu_add_entry(ndev, ha->addr) < 0) {
                if (!mcast_all) {
                    sh_eth_tsu_purge_mcast(ndev);
                    ecmr_bits &= ~ECMR_MCT;
                    mcast_all = 1;
                }
            }
        }
    } else {
        /* Normal, unicast/broadcast-only mode. */
        ecmr_bits = (ecmr_bits & ~ECMR_PRM) | ECMR_MCT;
    }

    /* update the ethernet mode */
    sh_eth_write(ndev, ecmr_bits, ECMR);

    spin_unlock_irqrestore(&mdp->lock, flags);
}

static int sh_eth_get_vtag_index(struct sh_eth_private *mdp)
{
    if (!mdp->port)
        return TSU_VTAG0;
    else
        return TSU_VTAG1;
}

static int sh_eth_vlan_rx_add_vid(struct net_device *ndev, u16 vid)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int vtag_reg_index = sh_eth_get_vtag_index(mdp);

    if (unlikely(!mdp->cd->tsu))
        return -EPERM;

    /* No filtering if vid = 0 */
    if (!vid)
        return 0;

    mdp->vlan_num_ids++;

    /*
     * The controller has one VLAN tag HW filter. So, if the filter is
     * already enabled, the driver disables it and the filte
     */
    if (mdp->vlan_num_ids > 1) {
        /* disable VLAN filter */
        sh_eth_tsu_write(mdp, 0, vtag_reg_index);
        return 0;
    }

    sh_eth_tsu_write(mdp, TSU_VTAG_ENABLE | (vid & TSU_VTAG_VID_MASK),
             vtag_reg_index);

    return 0;
}

static int sh_eth_vlan_rx_kill_vid(struct net_device *ndev, u16 vid)
{
    struct sh_eth_private *mdp = netdev_priv(ndev);
    int vtag_reg_index = sh_eth_get_vtag_index(mdp);

    if (unlikely(!mdp->cd->tsu))
        return -EPERM;

    /* No filtering if vid = 0 */
    if (!vid)
        return 0;

    mdp->vlan_num_ids--;
    sh_eth_tsu_write(mdp, 0, vtag_reg_index);

    return 0;
}
#endif /* SH_ETH_HAS_TSU */

/* SuperH's TSU register init function */
static void sh_eth_tsu_init(struct sh_eth_private *mdp)
{
    sh_eth_tsu_write(mdp, 0, TSU_FWEN0);    /* Disable forward(0->1) */
    sh_eth_tsu_write(mdp, 0, TSU_FWEN1);    /* Disable forward(1->0) */
    sh_eth_tsu_write(mdp, 0, TSU_FCM);  /* forward fifo 3k-3k */
    sh_eth_tsu_write(mdp, 0xc, TSU_BSYSL0);
    sh_eth_tsu_write(mdp, 0xc, TSU_BSYSL1);
    sh_eth_tsu_write(mdp, 0, TSU_PRISL0);
    sh_eth_tsu_write(mdp, 0, TSU_PRISL1);
    sh_eth_tsu_write(mdp, 0, TSU_FWSL0);
    sh_eth_tsu_write(mdp, 0, TSU_FWSL1);
    sh_eth_tsu_write(mdp, TSU_FWSLC_POSTENU | TSU_FWSLC_POSTENL, TSU_FWSLC);
    if (sh_eth_is_gether(mdp)) {
        sh_eth_tsu_write(mdp, 0, TSU_QTAG0);    /* Disable QTAG(0->1) */
        sh_eth_tsu_write(mdp, 0, TSU_QTAG1);    /* Disable QTAG(1->0) */
    } else {
        sh_eth_tsu_write(mdp, 0, TSU_QTAGM0);   /* Disable QTAG(0->1) */
        sh_eth_tsu_write(mdp, 0, TSU_QTAGM1);   /* Disable QTAG(1->0) */
    }
    sh_eth_tsu_write(mdp, 0, TSU_FWSR); /* all interrupt status clear */
    sh_eth_tsu_write(mdp, 0, TSU_FWINMK);   /* Disable all interrupt */
    sh_eth_tsu_write(mdp, 0, TSU_TEN);  /* Disable all CAM entry */
    sh_eth_tsu_write(mdp, 0, TSU_POST1);    /* Disable CAM entry [ 0- 7] */
    sh_eth_tsu_write(mdp, 0, TSU_POST2);    /* Disable CAM entry [ 8-15] */
    sh_eth_tsu_write(mdp, 0, TSU_POST3);    /* Disable CAM entry [16-23] */
    sh_eth_tsu_write(mdp, 0, TSU_POST4);    /* Disable CAM entry [24-31] */
}

/* MDIO bus release function */
static int sh_mdio_release(struct net_device *ndev)
{
    struct mii_bus *bus = dev_get_drvdata(&ndev->dev);

    /* unregister mdio bus */
    mdiobus_unregister(bus);

    /* remove mdio bus info from net_device */
    dev_set_drvdata(&ndev->dev, NULL);

    /* free interrupts memory */
    kfree(bus->irq);

    /* free bitbang info */
    free_mdio_bitbang(bus);

    return 0;
}

/* MDIO bus init function */
static int sh_mdio_init(struct net_device *ndev, int id,
            struct sh_eth_plat_data *pd)
{
    int ret, i;
    struct bb_info *bitbang;
    struct sh_eth_private *mdp = netdev_priv(ndev);

    /* create bit control struct for PHY */
    bitbang = kzalloc(sizeof(struct bb_info), GFP_KERNEL);
    if (!bitbang) {
        ret = -ENOMEM;
        goto out;
    }

    /* bitbang init */
    bitbang->addr = mdp->addr + mdp->reg_offset[PIR];
    bitbang->set_gate = pd->set_mdio_gate;
    bitbang->mdi_msk = 0x08;
    bitbang->mdo_msk = 0x04;
    bitbang->mmd_msk = 0x02;/* MMD */
    bitbang->mdc_msk = 0x01;
    bitbang->ctrl.ops = &bb_ops;

    /* MII controller setting */
    mdp->mii_bus = alloc_mdio_bitbang(&bitbang->ctrl);
    if (!mdp->mii_bus) {
        ret = -ENOMEM;
        goto out_free_bitbang;
    }

    /* Hook up MII support for ethtool */
    mdp->mii_bus->name = "sh_mii";
    mdp->mii_bus->parent = &ndev->dev;
    snprintf(mdp->mii_bus->id, MII_BUS_ID_SIZE, "%s-%x",
        mdp->pdev->name, id);

    /* PHY IRQ */
    mdp->mii_bus->irq = kmalloc(sizeof(int)*PHY_MAX_ADDR, GFP_KERNEL);
    if (!mdp->mii_bus->irq) {
        ret = -ENOMEM;
        goto out_free_bus;
    }

    for (i = 0; i < PHY_MAX_ADDR; i++)
        mdp->mii_bus->irq[i] = PHY_POLL;

    /* regist mdio bus */
    ret = mdiobus_register(mdp->mii_bus);
    if (ret)
        goto out_free_irq;

    dev_set_drvdata(&ndev->dev, mdp->mii_bus);

    return 0;

out_free_irq:
    kfree(mdp->mii_bus->irq);

out_free_bus:
    free_mdio_bitbang(mdp->mii_bus);

out_free_bitbang:
    kfree(bitbang);

out:
    return ret;
}

static const u16 *sh_eth_get_register_offset(int register_type)
{
    const u16 *reg_offset = NULL;

    switch (register_type) {
    case SH_ETH_REG_GIGABIT:
        reg_offset = sh_eth_offset_gigabit;
        break;
    case SH_ETH_REG_FAST_SH4:
        reg_offset = sh_eth_offset_fast_sh4;
        break;
    case SH_ETH_REG_FAST_SH3_SH2:
        reg_offset = sh_eth_offset_fast_sh3_sh2;
        break;
    default:
        printk(KERN_ERR "Unknown register type (%d)\n", register_type);
        break;
    }

    return reg_offset;
}

static const struct net_device_ops sh_eth_netdev_ops = {
    .ndo_open       = sh_eth_open,
    .ndo_stop       = sh_eth_close,
    .ndo_start_xmit     = sh_eth_start_xmit,
    .ndo_get_stats      = sh_eth_get_stats,
#if defined(SH_ETH_HAS_TSU)
    .ndo_set_rx_mode    = sh_eth_set_multicast_list,
    .ndo_vlan_rx_add_vid    = sh_eth_vlan_rx_add_vid,
    .ndo_vlan_rx_kill_vid   = sh_eth_vlan_rx_kill_vid,
#endif
    .ndo_tx_timeout     = sh_eth_tx_timeout,
    .ndo_do_ioctl       = sh_eth_do_ioctl,
    .ndo_validate_addr  = eth_validate_addr,
    .ndo_set_mac_address    = eth_mac_addr,
    .ndo_change_mtu     = eth_change_mtu,
#ifdef CONFIG_NET_POLL_CONTROLLER
    .ndo_poll_controller = &poll_tulip,
#endif
};

static int sh_set_mac_address(
                                     /** network device */
                                     struct net_device *ndev,

                                     /** MAC address from user space*/
                                     void *addr
                                     )
{
    DRV_MSG("%s\n",__FUNCTION__);
    /** @scope */

    /** - Check if network interface is running. If yes, return -EBUSY.*/
    if(netif_running(ndev))
    {
        DRV_WARN("Net device is busy now. It can't set MAC address.\n");
        return -EBUSY;
    }

    /** - Set MAC address to following devices: */
    /** - 1. net_device structure of kernel,    */
    /** - 2. SOC MAC register,                  */
    /** - 3. global variables if necessary      */

    /**The first two bytes of addr is address family [AF_xxx],
       the third byte to eighth byte MACaddress */
    memcpy(ndev->dev_addr,addr+2,ETH_ALEN);

    /*Set MAC address to the register*/
    update_mac_address(ndev);

    if(!strncmp(ndev->name,"eth0",4))
    {
        memcpy(mac1,ndev->dev_addr,ETH_ALEN);/*Set MAC address to a static variable*/
        DRV_MSG("Set eth0 Mac\n");
    }else if (!strncmp(ndev->name,"eth1",4))
    {
        memcpy(mac2,ndev->dev_addr,ETH_ALEN);/*Set MAC address to a static variable*/
        DRV_MSG("Set eth1 Mac\n");
    }else if (!strncmp(ndev->name,"eth2",4))
    {
        memcpy(mac3,ndev->dev_addr,ETH_ALEN);/*Set MAC address to a static variable*/
        DRV_MSG("Set eth2 Mac\n");
    }else
    {
        DRV_ERROR("There is no matched NIC name.\n");
        return -EINVAL;
    }

    DRV_INFO("\nSet %s MaC Address %02x:%02x:%02x:%02x:%02x:%02x\n",ndev->name,\
            ndev->dev_addr[0],\
            ndev->dev_addr[1],\
            ndev->dev_addr[2],\
            ndev->dev_addr[3],\
            ndev->dev_addr[4],\
            ndev->dev_addr[5]);
    return 0;
}

#ifdef POLL_LINK_STATUS /* { */
#ifdef CFG_PROJ_ETHERCLONE /* { */
static const struct net_device_ops sh_eth_clone_netdev_ops = {
	.ndo_open		= clone_open,
	.ndo_stop		= clone_close,
	.ndo_start_xmit		= clone_start_xmit,
	.ndo_get_stats		= sh_eth_get_stats,
#if defined(SH_ETH_HAS_TSU)
    .ndo_set_rx_mode    = clone_set_multicast_list,
    .ndo_vlan_rx_add_vid    = sh_eth_vlan_rx_add_vid,
    .ndo_vlan_rx_kill_vid   = sh_eth_vlan_rx_kill_vid,
#endif
	.ndo_tx_timeout		= sh_eth_tx_timeout,
	.ndo_do_ioctl		= sh_eth_do_ioctl,
    .ndo_set_mac_address    = sh_set_mac_address,
	.ndo_change_mtu		= eth_change_mtu,
#ifdef CONFIG_NET_POLL_CONTROLLER
    .ndo_poll_controller = &poll_tulip,
#endif
};

static void clone_link_timer(unsigned long arg)
{
	struct net_device * dev =(struct net_device *)arg;
	struct sh_eth_private * priv= netdev_priv(dev);
	if (!netif_carrier_ok( priv->real_dev))
	{
		if (netif_carrier_ok(dev))
		{
			printk("%s: Link clone Down\n",dev->name);
			netif_carrier_off(dev);
		}
	}
	else
	{
		if (!netif_carrier_ok(dev))
		{
			printk("%s: Link clone Up\n",dev->name);
			netif_carrier_on(dev);
		}
	}

	mod_timer(&priv->timer, jiffies +POLL_TIMEOUT*HZ);
}

#endif /* } CFG_PROJ_ETHERCLONE */
#endif /* } POLL_LINK_STATUS */


#ifdef CFG_PROJ_ETHERCLONE /* { */
static int clone_open(struct net_device *dev)
{
  	struct sh_eth_private * priv = netdev_priv(dev);
	local_bh_disable();

	dev->trans_start = jiffies;
	netif_start_queue(dev);

	/* Start Link Monitor */
	(*priv->timer.function)((unsigned long)dev);
	local_bh_enable();
    return 0;
}
static int  clone_close(struct net_device *dev)
{
	struct sh_eth_private *priv= netdev_priv(dev);
	local_bh_disable();
	__hw_addr_flush(&priv->mc_list);
	del_timer_sync(&priv->timer);
	netif_stop_queue(dev);
	local_bh_enable();

	return 0;
}

static int clone_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct sh_eth_private *priv = netdev_priv(dev);
    unsigned int len = skb->len;
    ++dev->stats.tx_packets;
    dev->stats.tx_bytes += len;
    skb->dev = priv->real_dev;
    dev_queue_xmit(skb);
    /* do not return an error - it will cause the skb to be freed twice */
    return 0;
}


static int clone_probe(struct net_device *real_dev)
{
	struct sh_eth_private *clone_priv;
	struct sh_eth_private *real_priv;
	struct net_device *clone_dev = NULL;
	int retval;
#ifdef SH_CLONE_DEBUG
printk(KERN_EMERG"clone_probe\n", ioaddr);
#endif
	clone_dev = alloc_etherdev(sizeof(struct sh_eth_private));
	if (!clone_dev)
		return -ENOMEM;

    ether_setup(clone_dev);

	clone_priv = netdev_priv(clone_dev);
	real_priv = netdev_priv(real_dev);
	clone_priv->regset = real_priv->regset;
	clone_priv->real_dev = real_dev;
	real_priv->clone_dev = clone_dev;

    clone_priv->pdev = real_priv->pdev;
    clone_priv->addr = real_priv->addr;
    clone_priv->reg_offset = real_priv->reg_offset;

    __hw_addr_init(&clone_priv->mc_list);

	/* Modify by Avct */
    clone_priv->u8Interface = real_priv->u8Interface;
    clone_priv->nic_if.interface_mode = real_priv->nic_if.interface_mode;
	/* Modify by Avct End */


	// Temp Assignment
	sprintf(clone_dev->name, "eth2");

	clone_dev->base_addr = real_dev->base_addr;

	clone_dev->irq = real_dev->irq;
	clone_dev->dma = real_dev->dma;

	/* set function */
	clone_dev->netdev_ops = &sh_eth_clone_netdev_ops;
	clone_dev->watchdog_timeo = TX_TIMEOUT;

	/* ipv6 shared card related stuff */
	clone_dev->dev_id = real_dev->dev_id;
    memcpy(clone_dev->dev_addr, real_dev->dev_addr, ETH_ALEN);
	memcpy(clone_dev->broadcast, real_dev->broadcast, clone_dev->addr_len);

    netif_carrier_off(clone_dev);
    netif_stop_queue(clone_dev);

	retval = register_netdev(clone_dev);

	if (retval)
	{
		return retval;
	}

#ifdef POLL_LINK_STATUS
	init_timer(&clone_priv->timer);
	clone_priv->timer.data = (unsigned long) clone_dev;
	clone_priv->timer.function = clone_link_timer;	/* timer handler */
#endif

	/* Modify by Avct */
    /** - Export interface of NIC in debugfs(/sys/kernel/debug/aess_nicdrv/eth#. # is the NIC number)*/
    aess_debugfs_create_file(driver_name, clone_dev->name, (u8 *)&clone_priv->u8Interface, DBG_TYPE8, 0);
	/* Modify by Avct End */

	return 0;

}

static bool clone_addr_in_mc_list(unsigned char *addr, struct netdev_hw_addr_list *list, int addrlen)
{
	struct netdev_hw_addr *ha;

	netdev_hw_addr_list_for_each(ha, list)
		if (!memcmp(ha->addr, addr, addrlen))
            return true;

	return false;
}

/** Taken from Gleb + Lennert's VLAN code, and modified... */
void clone_set_multicast_list(struct net_device *clone_dev)
{
	struct sh_eth_private *dev = netdev_priv(clone_dev);
	struct netdev_hw_addr *ha;
	struct net_device *real_dev;
	int inc;
	bool found;

	read_lock(&dev->lock);

	real_dev = dev->real_dev;

	/* compare the current promiscuity to the last promisc we had.. */
	inc = clone_dev->promiscuity - dev->old_promiscuity;
	if (inc) {
		printk(KERN_INFO "%s: dev_set_promiscuity(master, %d)\n",
		       clone_dev->name, inc);
		dev_set_promiscuity(real_dev, inc); /* found in dev.c */
		dev->old_promiscuity = clone_dev->promiscuity;
	}

	inc = clone_dev->allmulti - dev->old_allmulti;
	if (inc) {
		printk(KERN_INFO "%s: dev_set_allmulti(master, %d)\n",
		       clone_dev->name, inc);
		dev_set_allmulti(real_dev, inc); /* dev.c */
		dev->old_allmulti = clone_dev->allmulti;
	}

	/* looking for addresses to add to master's list */
	netdev_for_each_mc_addr(ha, clone_dev) {
		found = clone_addr_in_mc_list(ha->addr, &dev->mc_list, clone_dev->addr_len);
		if (!found) {
			dev_mc_add(real_dev, ha->addr);
			printk(KERN_DEBUG "%s: add %.2x:%.2x:%.2x:%.2x:%.2x:%.2x mcast address to master interface\n",
			       clone_dev->name, ha->addr[0], ha->addr[1], ha->addr[2], ha->addr[3], ha->addr[4], ha->addr[5]);
        }
	}

	/* looking for addresses to delete from master's list */
	netdev_hw_addr_list_for_each(ha, &dev->mc_list) {
		found = clone_addr_in_mc_list(ha->addr, &clone_dev->mc, clone_dev->addr_len);
		if (!found) {
			dev_mc_del(real_dev, ha->addr);
			printk(KERN_DEBUG "%s: del %.2x:%.2x:%.2x:%.2x:%.2x:%.2x mcast address from master interface\n",
			       clone_dev->name, ha->addr[0], ha->addr[1], ha->addr[2], ha->addr[3], ha->addr[4], ha->addr[5]);
        }
	}

	/* save master's multicast list */
	__hw_addr_flush(&dev->mc_list);
	__hw_addr_add_multiple(&dev->mc_list, &clone_dev->mc,
			       clone_dev->addr_len, NETDEV_HW_ADDR_T_MULTICAST);
	read_unlock(&dev->lock);
}
#endif /* } CFG_PROJ_ETHERCLONE */

static int sh_eth_drv_probe(struct platform_device *pdev)
{
    int ret, devno = 0;
    struct resource *res;
    struct net_device *ndev = NULL;
    struct sh_eth_private *mdp = NULL;
    struct sh_eth_plat_data *pd;

#ifdef IDRAC_SUPPORT /* { */
    int result =0;
    static u8 DebugfsFlag = 0;

    Dell_get_system_type();
    Dell_get_planar_type();
#endif /* } IDRAC_SUPPORT */

    /* get base addr */
    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    if (unlikely(res == NULL)) {
        dev_err(&pdev->dev, "invalid resource\n");
        ret = -EINVAL;
        goto out;
    }

    ndev = alloc_etherdev(sizeof(struct sh_eth_private));
    if (!ndev) {
        ret = -ENOMEM;
        goto out;
    }

    /* The sh Ether-specific entries in the device structure. */
    ndev->base_addr = res->start;
    devno = pdev->id;
    if (devno < 0)
        devno = 0;

    ndev->dma = -1;
    ret = platform_get_irq(pdev, 0);
    if (ret < 0) {
        ret = -ENODEV;
        goto out_release;
    }
    ndev->irq = ret;

    SET_NETDEV_DEV(ndev, &pdev->dev);

    /* Fill in the fields of the device structure with ethernet values. */
    ether_setup(ndev);

    mdp = netdev_priv(ndev);
    mdp->num_tx_ring = TX_RING_SIZE;
    mdp->num_rx_ring = RX_RING_SIZE;
    mdp->addr = ioremap(res->start, resource_size(res));
    if (mdp->addr == NULL) {
        ret = -ENOMEM;
        dev_err(&pdev->dev, "ioremap failed.\n");
        goto out_release;
    }

    spin_lock_init(&mdp->lock);
    mdp->pdev = pdev;
    pm_runtime_enable(&pdev->dev);
    pm_runtime_resume(&pdev->dev);

    pd = (struct sh_eth_plat_data *)(pdev->dev.platform_data);
    /* get PHY ID */
    mdp->phy_id = pd->phy;
    mdp->phy_interface = pd->phy_interface;
    /* EDMAC endian */
    mdp->edmac_endian = pd->edmac_endian;
    mdp->no_ether_link = pd->no_ether_link;
    mdp->ether_link_active_low = pd->ether_link_active_low;
    mdp->reg_offset = sh_eth_get_register_offset(pd->register_type);

    /* set cpu data */
#if defined(SH_ETH_HAS_BOTH_MODULES)
    mdp->cd = sh_eth_get_cpu_data(mdp);
#else
    mdp->cd = &sh_eth_my_cpu_data;
#endif
    sh_eth_set_default_cpu_data(mdp->cd);

    mdp->ndev = ndev;
    netif_napi_add(ndev, &mdp->napi, sh_eth_poll, SH_ETH_NAPI_WEIGHT);

    /* set function */
    ndev->netdev_ops = &sh_eth_netdev_ops;
    SET_ETHTOOL_OPS(ndev, &sh_eth_ethtool_ops);
    ndev->watchdog_timeo = TX_TIMEOUT;

    /* debug message level */
    mdp->msg_enable = SH_ETH_DEF_MSG_ENABLE;

    /* read and set MAC address */
    read_mac_address(ndev, pd->mac_addr);

    /* ioremap the TSU registers */
    if (mdp->cd->tsu) {
        struct resource *rtsu;
        rtsu = platform_get_resource(pdev, IORESOURCE_MEM, 1);
        if (!rtsu) {
            dev_err(&pdev->dev, "Not found TSU resource\n");
            goto out_release;
        }
        mdp->tsu_addr = ioremap(rtsu->start,
                    resource_size(rtsu));
        mdp->port = devno % 2;
        ndev->features = NETIF_F_HW_VLAN_CTAG_FILTER;
    }

    /* initialize first or needed device */
    if (!devno || pd->needs_init) {
        if (mdp->cd->chip_reset)
            mdp->cd->chip_reset(ndev);

        if (mdp->cd->tsu) {
            /* TSU init (Init only)*/
            sh_eth_tsu_init(mdp);
        }
    }

    /* network device register */
    ret = register_netdev(ndev);
    if (ret)
        goto out_release;

#ifdef IDRAC_SUPPORT /* { */
    /* save temp data */
    if (0 == DNIC) {
        temp_pdev = pdev;
        temp_pd = pd;
        DNIC = 1;
    }
#endif /* } IDRAC_SUPPORT */

    /* mdio bus init */
    ret = sh_mdio_init(ndev, pdev->id, pd);
    if (ret)
        goto out_unregister;

    /* print device information */
    pr_info("Base address at 0x%x, %pM, IRQ %d.\n",
           (u32)ndev->base_addr, ndev->dev_addr, ndev->irq);

    platform_set_drvdata(pdev, ndev);

#ifdef IDRAC_SUPPORT /* { */
    if(devno < MAX_MAC_NO)
        pnetwork_dev[devno] = ndev;

    /** Ethtool support: Start*/
    /** - Register ethtool operations*/
    SET_ETHTOOL_OPS(ndev, &sh_eth_ethtool_ops);

    /**MAC I/F*/
    mdp->nic_if.interface_mode = IF_RMII_PHY;
    mdp->u8Interface = IF_RMII_PHY;

    if(0==DebugfsFlag) {
        aess_debugfs_default_create(driver_name,NULL,0,0);
        //result = driver_register(&aess_nic_device_driver);
        result = platform_driver_register(&aess_nic_device_driver);

        if (result) {
            printk(KERN_ERR "NIC driver: can't register nic driver\n");
            return result;
        }

#ifdef BROADCAST_PROTECTION /* { */
        aess_debugfs_create_file(driver_name,"nBroadStormPackets",(int *)&nBroadStormPackets, DBG_TYPE32, 0);
        aess_debugfs_create_file(driver_name,"nBroadStormInterval",(int *)&nBroadStormInterval, DBG_TYPE32, 0);
        aess_debugfs_create_file(driver_name,"nBroadStormPacketsActive",(int *)&nBroadStormPacketsActive, DBG_TYPE32, 0);
        aess_debugfs_create_file(driver_name,"nBroadStormIntervalActive",(int *)&nBroadStormIntervalActive, DBG_TYPE32, 0);
        aess_debugfs_create_file(driver_name,"nBroadStormClearIntervals",(int *)&nBroadStormClearIntervals, DBG_TYPE32, 0);
#endif  /* } BROADCAST_PROTECTION */

#ifdef UNICAST_PROTECTION
		aess_debugfs_create_file(driver_name,"UnicastStormMsgDump",(int *)&UnicastStormMsgDump, DBG_TYPE8, 0);
		aess_debugfs_create_file(driver_name,"UnicastStormCount",(int *)&UnicastStormCount, DBG_TYPE8, 0);
		aess_debugfs_create_file(driver_name,"UnicastStormDelay",(int *)&UnicastStormDelay, DBG_TYPE8, 0);
		aess_debugfs_create_file(driver_name,"UnicastStormPacket",(int *)&UnicastStormPacket, DBG_TYPE32, 0);
		aess_debugfs_create_file(driver_name,"UnicastStormMaxCount",(int *)&UnicastStormMaxCount, DBG_TYPE8, 0);
		aess_debugfs_create_file(driver_name,"UnicastStormMaxPacket",(int *)&UnicastStormMaxPacket, DBG_TYPE32, 0);
#endif

#ifdef ETH_DEBUG_MSG
        aess_debugfs_create_file(driver_name,"OnlyRxMsgDump",(unsigned char *)&OnlyRxMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"OnlyTxMsgDump",(unsigned char *)&OnlyTxMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"EnableMsgDump",(unsigned char *)&EnableMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"NoNCSIMsgDump",(unsigned char *)&NoNCSIMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"OnlyNCSIMsgDump",(unsigned char *)&OnlyNCSIMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"DecodeMsgDump",(unsigned char *)&DecodeNCSIMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"FilterMsgDump1",(unsigned char *)&FilterNCSIMsgDump1, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"FilterMsgDump2",(unsigned char *)&FilterNCSIMsgDump2, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"NoLinkMsgDump",(unsigned char *)&NoLinkMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"NoGetParametersMsgDump",(unsigned char *)&NoGetParametersMsgDump, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"NoOEMMsgDump",(unsigned char *)&NoOEMMsgDump, DBG_TYPE8, 0);

        aess_debugfs_create_file(driver_name,"EnableNCSICmdHistory",(unsigned char *)&EnableNCSICmdHistory, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"NCSICmdHistorytPage",(unsigned char *)&NCSICmdHistorytPage, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"NCSICmdHistorytIndex",(unsigned int *)&NCSICmdHistorytIndex, DBG_TYPE32, 0);
#endif

#ifdef PHY_DEBUG
        aess_debugfs_create_file(driver_name,"PHYDebug",(unsigned char *)&PHYDebug, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"PHYReg",(unsigned char *)&PHYReg, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"PHYShadow",(unsigned char *)&PHYShadow, DBG_TYPE8, 0);
        aess_debugfs_create_file(driver_name,"PHYData",(unsigned char *)&PHYData, DBG_TYPE16, 0);
#endif

        aess_debugfs_create_file(driver_name,"NetIfMsgSwitch",(unsigned char *)&NetIfMsgSwitch, DBG_TYPE8, 0);

        result = driver_create_file(&(aess_nic_device_driver.driver), &drv_attr_regvalues);

        if (result)
        {
            printk(KERN_ERR "Fail to create sysfs attribute(register values)\n");
            return result;
        }

#ifdef ETH_DEBUG_MSG
        result = driver_create_file(&(aess_nic_device_driver.driver), &drv_attr_ncsi_cmd_history);

        if (result)
        {
            printk(KERN_ERR "Fail to create sysfs attribute(drv_attr_ncsi_cmd_history values)\n");
            return result;
        }
#endif

        DebugfsFlag = 1;
    }
    /** - Export interface of NIC in debugfs(/sys/kernel/debug/aess_nicdrv/eth#. # is the NIC number)*/
    aess_debugfs_create_file(driver_name,ndev->name,(u8 *)&mdp->u8Interface,DBG_TYPE8,0);

#ifdef CFG_PROJ_ETHERCLONE /* { */
    if ((system_type == SYS_TYPE_BLADE) && ((strncmp(ndev->name, "eth0", 4) == 0))) {
        printk("clone_probe --> ndev->name=%s\n", ndev->name);
        clone_probe(ndev);
    }
#endif /* } CFG_PROJ_ETHERCLONE */
#endif /* } IDRAC_SUPPORT */

    return ret;

out_unregister:
    unregister_netdev(ndev);

out_release:
    /* net_dev free */
    if (mdp && mdp->addr)
        iounmap(mdp->addr);
    if (mdp && mdp->tsu_addr)
        iounmap(mdp->tsu_addr);
    if (ndev)
        free_netdev(ndev);

out:
    return ret;
}

static int sh_eth_drv_remove(struct platform_device *pdev)
{
    struct net_device *ndev = platform_get_drvdata(pdev);
    struct sh_eth_private *mdp = netdev_priv(ndev);

    if (mdp->cd->tsu)
        iounmap(mdp->tsu_addr);
    sh_mdio_release(ndev);
    unregister_netdev(ndev);
    pm_runtime_disable(&pdev->dev);
    iounmap(mdp->addr);
    free_netdev(ndev);
    platform_set_drvdata(pdev, NULL);

#ifdef IDRAC_SUPPORT /* { */
    aess_debugfs_remove(driver_name,NULL,0,0);
    driver_remove_file(&(aess_nic_device_driver.driver), &drv_attr_regvalues);
    //driver_unregister(&(aess_nic_device_driver));
    platform_driver_unregister(&(aess_nic_device_driver));
    //platform_driver_unregister(&sh_eth_driver);
#endif /* } IDRAC_SUPPORT */
    return 0;
}

static int sh_eth_runtime_nop(struct device *dev)
{
    /*
     * Runtime PM callback shared between ->runtime_suspend()
     * and ->runtime_resume(). Simply returns success.
     *
     * This driver re-initializes all registers after
     * pm_runtime_get_sync() anyway so there is no need
     * to save and restore registers here.
     */
    return 0;
}

static struct dev_pm_ops sh_eth_dev_pm_ops = {
    .runtime_suspend = sh_eth_runtime_nop,
    .runtime_resume = sh_eth_runtime_nop,
};

static struct platform_driver sh_eth_driver = {
    .probe = sh_eth_drv_probe,
    .remove = sh_eth_drv_remove,
    .driver = {
           .name = CARDNAME,
           .pm = &sh_eth_dev_pm_ops,
    },
};

module_platform_driver(sh_eth_driver);

MODULE_AUTHOR("Nobuhiro Iwamatsu, Yoshihiro Shimoda");
MODULE_DESCRIPTION("Renesas SuperH Ethernet driver");
MODULE_LICENSE("GPL v2");
