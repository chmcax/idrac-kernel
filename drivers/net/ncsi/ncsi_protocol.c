/*
 *
 * Copyright (C) 2009-2017 Avocent Corporation
 *
 * This file is subject to the terms and conditions of the GNU
 * General Public License Version 2. This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License Version 2 for more details.
 *
 *----------------------------------------------------------------------------\n
 *  MODULES     linux driver
 *----------------------------------------------------------------------------\n
 *  @file   ncsi_protocol.c
 *  @brief  This is NC-SI device driver
 *
 *  @internal
 *----------------------------------------------------------------------------*/

/******************************************************************************
 * Content
 * ----------------
 * ncsi_cmd_error_handler()       - Handle error code and reason code of ncsi
 *                                  response packets.
 * ncsi_cmd_clear_initial_state() - Issue the NC-SI command "Clear Initial
 *                                  State".
 * ncsi_cmd_select_package()      - Issue the NC-SI command "Select package".
 * ncsi_cmd_deselect_package()    - Issue the NC-SI command "Deselect package".
 * ncsi_cmd_enable_channel()      - Issue the NC-SI command "Enable Channel".
 * ncsi_cmd_disable_channel()     - Issue the NC-SI command "Disable Channel".
 * ncsi_cmd_reset_channel()       - Issue the NC-SI command "Reset Channel".
 * ncsi_cmd_enable_channel_network_tx()  - Issue the NC-SI command "Enable
 *                                         Channel Network Tx".
 * ncsi_cmd_disable_channel_network_tx() - Issue the NC-SI command "Disable
 *                                         Channel Network Tx".
 * ncsi_cmd_aen_enable()          - Issue the NC-SI command "AEN Enable".
 * ncsi_cmd_set_link()            - Issue the NC-SI command "Set Link".
 * ncsi_cmd_get_link_status()     - Issue the NC-SI command "Get Link".
 * ncsi_cmd_set_vlan_filters()    - Issue the NC-SI command "Set VLAN filters".
 * ncsi_cmd_enable_vlan()         - Issue the NC-SI command "Enable VLAN".
 * ncsi_cmd_disable_vlan()        - Issue the NC-SI command "Disable VLAN".
 * ncsi_cmd_set_mac_address()     - Issue the NC-SI command "Set MAC Address".
 * ncsi_cmd_enable_broadcast_filtering()  - Issue the NC-SI command "Enable
 *                                          broadcast filtering".
 * ncsi_cmd_disable_broadcast_filtering() - Issue the NC-SI command "Disable
 *                                          broadcast filtering".
 * ncsi_cmd_enable_global_multicast_filtering()  - Issue the NC-SI command
 *                                          "Enable Global Multicast Filtering".
 * ncsi_cmd_disable_global_multicast_filtering() - Issue the NC-SI command
 *                                          "Disable Global Multicast Filtering".
 * ncsi_cmd_set_ncsi_flow_control() - Issue the NC-SI command "Set NCSI flow
 *                                    control".
 * ncsi_cmd_get_version_id()        - Issue the NC-SI command "Get Version ID".
 * ncsi_cmd_get_capabilities()      - Issue the NC-SI command "Get Capabilities".
 * ncsi_cmd_get_parameters()        - Issue the NC-SI command "Get Parameters".
 * ncsi_cmd_get_controller_packet_statistics() - Issue the NC-SI command "Get
 *                                               controller packet statistics".
 * ncsi_cmd_get_ncsi_statistics()   - Issue the NC-SI command "Get NCSI statistics".
 * ncsi_cmd_get_ncsi_passthrough_statistics()  - Issue the NC-SI command "Get NCSI
 *                                               passthrough statistics".
 * ncsi_aen_link_status_change()    - Handle "Link Status Change" AEN.
 * ncsi_aen_reconfig_required()     - Handle "Reconfig Required" AEN
 * ncsi_aen_os_driver_change()      - Handle "OS Driver Running Status Change" AEN.
 * ncsi_queue_read()                - Take the socket buffer from the response queue.
 * ncsi_queue_write()               - Enqueue the socket buffer to the response queue.
 * ncsi_rx_aen_packet()             - Dispatch the received AEN packets to the related
 *                                    handling functions.
 * ncsi_rx_response_packet()        - Receive the response packet and enqueue to the
 *                                    response queue.
 * ncsi_wait_response_packet()      - Wait for the response packets.
 * ncsi_eth_header()                - prepare the ethernet header of the packet.
 * ncsi_header_payload()            - prepare the ethernet header and payload of the
 *                                    packet.
 * ncsi_send_packet()               - Send NC-SI packet.
 * ncsi_send_and_wait()             - Send NC-SI command and wait the response.
 * ncsi_open()                      - NC-SI open function.
 * ncsi_close()                     - NC-SI close function.
 * ncsi_init()                      - NC-SI init function.
 * ncsi_error_recovery()            - Handling ncsi error recovery.
 * ncsi_get_link_status_polling()   - Polling link status of NC-SI.
 * ncsi_get_parameters_polling()    - Polling get parameters of NC-SI.
 * ncsi_command_handler()           - Handle the switch channel and error recovery.
 * ncsi_notifier()                  - Notify the status of NIC interface.
 * ncsi_ioctl()                     - NC-SI device's ioctl function.
 * ncsi_set_mac_address()           - NC-SI device set mac address function.
 * ncsi_detect_device()             - NC-SI device auto detect function.
 * ncsi_new_device()                - Create the NC-SI device.
 * ncsi_free_device()               - Free the NC-SI device from list tree.
 * ncsi_virtual_open()              - open function for NC-SI virtual devices.
 * ncsi_virtual_close()             - close function for NC-SI virtual devices.
 * ncsi_virtual_xmit()              - Transmit function for NC-SI virtual devices.
 * ncsi_proto_init()                - Init function for NC-SI protocol.
 * ncsi_proto_exit()                - NC-SI protocol exit function.
 * ncsi_cmd_get_link_status_with_rt() - Issue the NC-SI command "Get Link" with \n
 *                                      retries and timeout parameters.
 * ncsi_cmd_get_version_id_with_rt()  - Issue the NC-SI command "Get Version ID" \n
 *                                      with timeout and retries parameters.
 * ncsi_cmd_get_capabilities_with_rt() - Issue the NC-SI command "Get Capabilities" \n
 *                                       with retries and timeout parameters.
 * ncsi_cmd_SendOEM50()             - Issue the NC-SI command "OEM Command".
 * ncsi_cmd_get_ncsi_passthrough_statistics - Issue the NC-SI command \n
 *                                            "Get NCSI passthrough statistics".
 * __ncsi_send_and_wait()           - Send NC-SI command and wait the response with \n
 *                                    configurable retries and timeout.
 * ncsi_ioctl_sendcommand()         - Pass through any NC-SI command.
 ******************************************************************************/
#define AESSNCSI_C
#include <linux/aess_debugfs.h>
#include <linux/version.h>
#include <linux/bitops.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/ethtool.h>
#include <linux/etherdevice.h>
#include <linux/inet.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/inetdevice.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/mii.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/platform_device.h>
#include <linux/rcupdate.h>
#include <linux/route.h>
#include <linux/sched.h>
#include <linux/skbuff.h>
#include <linux/socket.h>
#include <linux/sockios.h>
#include <linux/spinlock.h>
#include <linux/stat.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/udp.h>
#include <linux/wireless.h>
#include <linux/kthread.h>
#include <net/inet_common.h>
#include <net/udp.h>
#include <net/ip.h>
#include <net/sock.h>
#include <asm/uaccess.h>
#include "ncsi_protocol.h"
#include "../bonding/bonding.h"

#define CONFIG_ETH_NAPI 1

#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)
#include <linux/sched.h>
#endif

/* To show what NC-SI control packet send out.
   It will disable get_link_status polling.  */
//#define DEBUG_SHOW_SEND_NCSI_CONTROL_PACKET
//#define DEBUG_NOT_SHOW_GET_LINK_PACKET

#define NCSI_DETECT_RETRY_TIMES 3
#define NCSI_DETECT_INTERVAL  100


#define NCSI_PROTOCOL 0x88F8

/* If support command checksum */
//#define NCSI_SUPPORT_CHECKSUM

/* For NC-SI evb use. */
/* This flag is for Renesas EVB to use;
   since the EVB doesn't have NC-SI chip and need to connect to another NC-SI board to work.
   The DRB still need to do some code modification, and cannot just enable this flag to use.
*/
//#define CONFIG_EVB_WORKAROUND


#define ENABLE_ERROR_RECOVERY 0
#define DISABLE_ERROR_RECOVERY 1

static u8 au8NCSIDriverVersion [] = "0.1.3.7";
static u8 total_package_no[NCSI_MAX_MII_NUM] = {0, 0, 0};

static volatile sNCSI_MII_INFO mii_if[NCSI_MAX_MII_NUM] =
{
   {"eth0",0,0,0}
  ,{"eth1",1,0,0}
  ,{"eth2",2,0,0}
#if 0
  ,{"eth3",3,0,0}
#endif
};

static sNCSI_IF_Info array_ncsi_if[NCSI_MAX_MII_NUM];
static char ncsi_control_flag[NCSI_MAX_MII_NUM]  = {0,0,0};
static struct semaphore ncsi_control_sem[NCSI_MAX_MII_NUM];
static char nic_interface_mode[NCSI_MAX_MII_NUM] = {0,0,0};
static char ncsi_dev_count = 0;
static struct net_device *active_slave_dev = NULL;

static LIST_HEAD(ncsi_devices);
static struct lock_class_key ncsi_netdev_xmit_lock_key;

#ifdef CONFIG_ETH_NAPI
static int aess_wdt_keepalive(struct file *file){ return 0; };
#else
extern int aess_wdt_keepalive(struct file *file);
#endif

/** - For sysfs information display */
static u32 ncsi_dev_rx_status_error;
static u32 ncsi_dev_rx_status_dnic[NCSI_MAX_MII_NUM][3];
static u32 ncsi_init_flag[NCSI_MAX_MII_NUM] = {0,0,0};
static u32 ncsi_package_status[NCSI_MAX_MII_NUM][CONFIG_NCSI_MAX_PACKAGE_NUMBER];
static u32 ncsi_dev_tx_count[NCSI_MAX_MII_NUM][CONFIG_NCSI_MAX_PACKAGE_NUMBER][2];
static u32 ncsi_dev_rx_status[NCSI_MAX_MII_NUM*CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER][3];

static int ncsi_get_ethtool_settings(struct net_device *dev, struct ethtool_cmd *cmd);
static int ncsi_set_ethtool_settings(struct net_device *dev, struct ethtool_cmd *cmd);
static const char * ncsi_print_ethaddr(const unsigned char *e);
static int ncsi_open(struct net_device *dev);

#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)

#else
static int ncsi_close(struct net_device *dev);
#endif

static void ncsi_init(sNCSI_IF_Info *ncsi_if);
static int ncsi_cmd_clear_initial_state(sNCSI_IF_Info *ncsi_if , int dis_recovery);
static int ncsi_cmd_select_package(sNCSI_IF_Info *ncsi_if , int dis_recovery );
//static int ncsi_cmd_reset_channel(sNCSI_IF_Info *ncsi_if, int dis_recovery );
static int ncsi_cmd_deselect_package(sNCSI_IF_Info *ncsi_if , int dis_recovery);
static int ncsi_cmd_get_capabilities(sNCSI_IF_Info *ncsi_if , int dis_recovery);
static int ncsi_cmd_enable_channel_network_tx(sNCSI_IF_Info *ncsi_if , int dis_recovery);
static int ncsi_cmd_disable_channel_network_tx(sNCSI_IF_Info *ncsi_if , int dis_recovery);
static int ncsi_cmd_enable_channel(sNCSI_IF_Info *ncsi_if);

static int ncsi_cmd_aen_enable(sNCSI_IF_Info *ncsi_if);
static int ncsi_cmd_set_link(sNCSI_IF_Info *ncsi_if);
static int ncsi_cmd_get_link_status(struct net_device *dev);
static int ncsi_cmd_set_vlan_filters(struct net_device *dev);
static int ncsi_cmd_enable_vlan(struct net_device *dev);
static int ncsi_cmd_disable_vlan(struct net_device *dev);
static int ncsi_cmd_set_mac_address(sNCSI_IF_Info *ncsi_if);
static int ncsi_cmd_enable_broadcast_filtering(sNCSI_IF_Info *ncsi_if);
//static int ncsi_cmd_enable_global_multicast_filtering(sNCSI_IF_Info *ncsi_if);
static int ncsi_cmd_get_version_id(sNCSI_IF_Info *ncsi_if);
//static int ncsi_cmd_disable_global_multicast_filtering (sNCSI_IF_Info *ncsi_if);


static int ncsi_cmd_disable_channel(sNCSI_IF_Info *ncsi_if);
//static int ncsi_cmd_disable_broadcast_filtering(sNCSI_IF_Info *ncsi_if);
//static int ncsi_cmd_set_ncsi_flow_control(sNCSI_IF_Info *ncsi_if);
static int ncsi_cmd_get_parameters(sNCSI_IF_Info *ncsi_if);
static int ncsi_cmd_get_controller_packet_statistics(sNCSI_IF_Info *ncsi_if, u32 , u32);
static int ncsi_cmd_get_ncsi_statistics(sNCSI_IF_Info *, u32, u32);
static int ncsi_cmd_get_ncsi_passthrough_statistics(sNCSI_IF_Info *ncsi_if, u32, u32);

static int ncsi_aen_link_status_change(sNCSI_IF_Info *ncsi_if, struct sNCSI_AEN_HDR *ncsi_aenhdr);
static int ncsi_aen_reconfig_required(sNCSI_IF_Info *ncsi_if, struct sNCSI_AEN_HDR *ncsi_aenhdr);
static int ncsi_aen_os_driver_change(sNCSI_IF_Info *ncsi_if, struct sNCSI_AEN_HDR *ncsi_aenhdr);

static int ncsi_queue_read(sNCSI_IF_Info *ncsi_if, struct sk_buff **res_skb,
                           enum ncsi_queue_type type);
static int ncsi_queue_write(sNCSI_IF_Info *ncsi_if, struct sk_buff *skb,
                            enum ncsi_queue_type type);
static void ncsi_rx_aen_packet(sNCSI_IF_Info *ncsi_if, struct sk_buff *aen_skb);
static void ncsi_rx_response_packet(sNCSI_IF_Info *ncsi_if, struct sk_buff *res_skb);
static int ncsi_wait_response_packet(sNCSI_IF_Info *ncsi_if,
                                     struct sk_buff **res_skb, u8 res_cmd);
static void ncsi_eth_header(sNCSI_IF_Info *ncsi_if, struct sk_buff *skb);
static void ncsi_header_payload(sNCSI_IF_Info *ncsi_if, struct sk_buff *req_skb,
                                u8 cmd, u8 *payload, u16 payload_len);
static int ncsi_send_packet(sNCSI_IF_Info *ncsi_if,
                            u8 cmd, u8 *payload, u16 payload_len);
static int __ncsi_send_and_wait(sNCSI_IF_Info *, struct sk_buff **,
                              u8 , u8 *, u16 ,
                              u32, u32 , int );
static int ncsi_send_and_wait(sNCSI_IF_Info *ncsi_if, struct sk_buff **res_skb,
                              u8 cmd, u8 *payload, u16 payload_len , int dis_recovery );

static int ncsi_notifier(struct notifier_block *this,unsigned long msg,void *data);
static int ncsi_ioctl(struct net_device *dev,struct ifreq *ifr,int cmd);
static int ncsi_set_mac_address(struct net_device *dev, void *addr);
static int ncsi_detect_device(struct net_device *dev, int mii_index);
static int ncsi_new_device(struct net_device *edev, unsigned long ncsi_chnl_id, int index, int mii_index);
static void ncsi_free_device(struct net_device *ndev);
static int ncsi_virtual_open(struct net_device *dev);
static int ncsi_virtual_close(struct net_device *dev);
static int ncsi_virtual_xmit(struct sk_buff *skb,struct net_device *dev);

#define NCSI_ETH_HEADER(skb) ((struct ethhdr *)((skb)->data))
#define NCSI_HEADER(skb) ((struct sNCSI_HDR *)((skb)->data + ETH_HLEN))

/* the response packet is already removed the Ethernet header */
#define NCSI_RES_HEADER(skb) ((struct sNCSI_RES_HDR *)((skb)->data))
#define NCSI_AEN_HEADER(skb) ((struct sNCSI_AEN_HDR *)((skb)->data))

#define IS_NCSI_PACKET(ether_type) \
    (NCSI_ETHER_TYPE == ntohs(ether_type))

#define IS_NCSI_GOOD_PACKET(ncsi_hdr) \
    (NCSI_HEADER_REVISION == ncsi_hdr->hdr_rev)

#define IS_NCSI_AEN_PACKET(ncsi_hdr) \
    ((ncsi_hdr->cmd_iid == NCSI_AEN_PACKET_IID) && \
    (ncsi_hdr->cmd == 0xFF))

#define IS_NCSI_RES_PACKET(ncsi_hdr) \
    (((ncsi_hdr->cmd) & NCSI_RES_PACKET_MASK) && \
    (ncsi_hdr->cmd != 0xFF))

#define IS_VALID_NCSI_RES_PACKET(ncsi_reshdr, ncsi_if, res_cmd) \
    (((ncsi_reshdr->ncsi_hdr.chnl_id == ncsi_if->chnl_id)) && \
    (ncsi_reshdr->ncsi_hdr.cmd == res_cmd))
//mark by sonata: follow Bill's comment    (ncsi_reshdr->ncsi_hdr.cmd_iid == ncsi_if->cmd_iid))

#define IS_VALID_QUERY_NCSI_RES_PACKET(ncsi_reshdr, ncsi_if, res_cmd) \
    (((ncsi_reshdr->ncsi_hdr.chnl_id == ncsi_if->chnl_id)) && \
    (ncsi_reshdr->ncsi_hdr.cmd == 0x8A))

#define NCSI_RECEIVE_RES_PACKET_OK    0
#define NCSI_RECEIVE_RES_PACKET_INVALID    1
#define NCSI_NEED_REINIT             0xFE

#define DEBUG_PRINT_01
//#define DEBUG_PRINT_01 do{if(debug_printf)printk("DBG %s(%d) jiffies=%lu\n",__FUNCTION__,__LINE__,jiffies);}while(0)

static u8  u8dumpmsgswitch = 0;
static u32 u32RecoveryCountPre = 0;
static u32 u32RecoveryCount = 0;
static u32 u32RecoveryCount_last = 0;
static u8  u8ScanComplete = 0;
static u8  u8TraceRecovery = 0;
static u8  u8TraceTxEnable = 0;
static u8  u8TraceTxFun = 0;
static u8  u8TraceMsgTimeout = 0;
static u8  u8ForceTXEnable = 0;
static u8  u8TraceAENEnable = 0;
static u8  u8DelayMethod = 1;
static u8  u8TraceSendMessage = 0;

static u8  u8TraceNotifier = 0;

#define NCSI_TX_ENABLE_WORKAROUND           0
#define NCSI_TX_ENABLE_RETRIGGER_INTERVAL   300

static char *ncsi_cmd_str[] =
{
    "CMD_CLEAR_INITIAL_STATE",
    "CMD_SELECT_PACKAGE",
    "CMD_DESELECT_PACKAGE",
    "CMD_ENABLE_CHANNEL",
    "CMD_DISABLE_CHANNEL",
    "CMD_RESET_CHANNEL",
    "CMD_ENABLE_CHANNEL_NETWORK_TX",
    "CMD_DISABLE_CHANNEL_NETWORK_TX",
    "CMD_AEN_ENABLE",
    "CMD_SET_LINK",
    "CMD_GET_LINK_STATUS",
    "CMD_SET_VLAN_FILTERS",
    "CMD_ENABLE_VLAN",
    "CMD_DISABLE_VLAN",
    "CMD_SET_MAC_ADDRESS",
    "CMD_RESERVED_0xF",
    "CMD_ENABLE_BROADCAST_FILTERING",
    "CMD_DISABLE_BROADCAST_FILTERING",
    "CMD_ENABLE_GLOBAL_MULTICAST_FILTERING",
    "CMD_DISABLE_GLOBAL_MULTICAST_FILTERING ",
    "CMD_SET_NCSI_FLOW_CONTROL",
    "CMD_GET_VERSION_ID",
    "CMD_GET_CAPABILITIES",
    "CMD_GET_PARAMETERS",
    "CMD_GET_CONTROLLER_PACKET_STATISTICS",
    "CMD_GET_NCSI_STATISTICS",
    "CMD_GET_NCSI_PASSTHROUGH_STATISTICS",
};

int ncsi_set_recovery_flag(sNCSI_IF_Info *ncsi_if_info, u8 value, const char *function_name, long int function_line)
{
    ncsi_if_info->error_recovery_enable_flag = value;
    if (u8TraceRecovery)
    {
        printk("ncsi_set_recovery_flag(%d) - %s:%ld - %d %d\n", value, function_name, function_line, ncsi_if_info->chnl_id, ncsi_if_info->query_chnl_id);
    }
    return 0;
}


/******************************************************************************
*   FUNCTION        :   ncsi_get_mii_index_with_name
******************************************************************************/
/**
 *  @brief      Get MII index id with MII device name.
 *
 *
 *  @return     MII index id.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
int ncsi_get_mii_index_with_name(
                                    /** MII device name. */
                                    char *dev_name
                                )
{
    int index = 0;
    int mii_index = 0xFF;

    /* Avoid the the name string to be NULL.*/
    if( NULL != mii_if[index].name)
    {
        for (index = 0; index < NCSI_MAX_MII_NUM; index++)
        {
            if ( 0 == strncmp((char *) &mii_if[index].name[0], dev_name , strlen(dev_name)) )
            {
                mii_index = mii_if[index].mii_interface_index ;
                return (mii_index);
            }
        }
    }
    return (mii_index);
}


/******************************************************************************
*   FUNCTION        :   ncsi_cmd_error_handler
******************************************************************************/
/**
 *  @brief     Handle error code and reason code of NC-SI response packets.
 *
 *
 *  @return    0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
int ncsi_cmd_error_handler(
                              /** The pointer with NC-SI interface. */
                              sNCSI_IF_Info *ncsi_if,

                              /** The pointer with NC-SI command response
                                  packet. */
                              struct sNCSI_RES_HDR *ncsi_reshdr
                          )
{
    ncsi_reshdr->response_code = ntohs(ncsi_reshdr->response_code);
    ncsi_reshdr->reason_code = ntohs(ncsi_reshdr->reason_code);

    if(ncsi_reshdr->response_code != NCSI_RES_CODE_CMD_OK)
    {
        if(ncsi_reshdr->reason_code == NCSI_REASON_CODE_INTERFACE_INIT_REQUIRED)
        {
            ncsi_set_recovery_flag(ncsi_if, 1, __FUNCTION__, __LINE__);
            DRV_ERROR("NCSI_REASON_CODE_INTERFACE_INIT_REQUIRED \n");
            return(NCSI_NEED_REINIT);
        }
        DRV_ERROR("NCSI COMMAND ERROR (%x, %x)\n",ncsi_reshdr->response_code,
                  ncsi_reshdr->reason_code);
        return (ncsi_reshdr->reason_code);
    }
    /* Using the definition to replace magic number. */
    return NCSI_RES_CODE_CMD_OK;
}

#if 0
/******************************************************************************
*   FUNCTION        :   ncsi_do_checksum
******************************************************************************/
/**
 *  @brief      The checksum function for NC-SI device.
 *
 *
 *  @return     checksum.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
static u32 ncsi_do_checksum(
                               /** The data buff. */
                               u8 *buf,

                               /** The data buff length. */
                               /* Using the int to replace the u16 declaration. */
                               int len
                           )
{
    u32 checksum = 0;
    u16 cnt = 0;
    u16 temp = 0;

    while (len > 0)
    {
        temp = htons(*(((u16 *) buf) + cnt));
        checksum += temp;
        cnt++;
        len -= sizeof(u16);
    }

    return (htonl((~checksum) + 1));
}
#endif

/******************************************************************************
*   FUNCTION        :   IS_VALID_NCSI_CHECKSUM
******************************************************************************/
/**
 *  @brief      The checksum function for NC-SI device.
 *
 *
 *  @return     0: Success. 1: Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
static inline int IS_VALID_NCSI_CHECKSUM(
                                            /** The pointer with NC-SI command
                                                response packet. */
                                            struct sNCSI_RES_HDR *ncsi_reshdr
                                        )
{
/*  Add a flag to handle support checksum process and not support checksum process */
#ifdef NCSI_SUPPORT_CHECKSUM
    u32 reschecksum = *(u32 *) (((u8 *) ncsi_reshdr) + NCSI_HLEN +
                                ncsi_reshdr->ncsi_hdr.payload_len);
    u32 expchecksum = 0;

    expchecksum = ncsi_do_checksum((unsigned char *)ncsi_reshdr,
                                               NCSI_HLEN + ncsi_reshdr->ncsi_hdr.payload_len);

    DRV_ERROR("CHECKSUM: RES = 0x%lx EXP = 0x%lx\n",
              (long unsigned int) reschecksum, (long unsigned int) expchecksum);

    if (reschecksum == expchecksum)
    {
        return (1);
    }
    else
    {
        return (0);
    }
#else
        return (0);
#endif
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_clear_initial_state
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Clear Initial State".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_clear_initial_state(
                                           /** pointer to NC-SI interface. */
                                           sNCSI_IF_Info *ncsi_if,

                                           int dis_recovery
                                       )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_CLEAR_INITIAL_STATE,
                         NULL, CMD_CLEAR_INITIAL_STATE_PAYLOAD_LEN , dis_recovery );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_CLEAR_INITIAL_STATE]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            if( dis_recovery )
            {
                ncsi_reshdr->response_code = ntohs(ncsi_reshdr->response_code);
                ncsi_reshdr->reason_code = ntohs(ncsi_reshdr->reason_code);
                DRV_INFO("NCSI COMMAND ERROR (%x, %x)", ncsi_reshdr->response_code, ncsi_reshdr->reason_code);
            }
            else
            {
                res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
            }
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n",(unsigned int)res_skb);
        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }
    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_select_package
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Select package".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_select_package(
                                      /** pointer to NC-SI interface. */
                                      sNCSI_IF_Info *ncsi_if ,

                                      int dis_recovery

                                  )
{
    int res;
    struct sk_buff *res_skb = NULL;
    u32 payload;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    payload = htonl(ncsi_if->ncsi_hardware_arb);

    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_SELECT_PACKAGE,
                 (u8 *)&payload, CMD_SELECT_PACKAGE_PAYLOAD_LEN , dis_recovery );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_SELECT_PACKAGE]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        DRV_ERROR("chnl_id = 0x%lx, cmd = 0x%lx, cmd_iid = 0x%lx, hdr_rev = 0x%lx, mc_id = 0x%lx, payload_len = 0x%lx, reason_code = 0x%lx, response_code = 0x%lx\n",
        (long unsigned int) ncsi_reshdr->ncsi_hdr.chnl_id,
        (long unsigned int) ncsi_reshdr->ncsi_hdr.cmd,
        (long unsigned int) ncsi_reshdr->ncsi_hdr.cmd_iid,
        (long unsigned int) ncsi_reshdr->ncsi_hdr.hdr_rev,
        (long unsigned int) ncsi_reshdr->ncsi_hdr.mc_id,
        (long unsigned int) ncsi_reshdr->ncsi_hdr.payload_len,
        (long unsigned int) ncsi_reshdr->reason_code,
        (long unsigned int) ncsi_reshdr->response_code);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            ncsi_reshdr->response_code = ntohs(ncsi_reshdr->response_code);
            ncsi_reshdr->reason_code = ntohs(ncsi_reshdr->reason_code);
            DRV_INFO("NCSI COMMAND ERROR (%x, %x)", ncsi_reshdr->response_code, ncsi_reshdr->reason_code);
        }
        /* free res_skb when it received response packet */
        if (res_skb)
        {
            DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

            dev_kfree_skb(res_skb);
            res_skb = NULL;
        }
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_deselect_package
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Deselect package".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_deselect_package(
                                       /** pointer to NC-SI interface. */
                                       sNCSI_IF_Info *ncsi_if ,

                                       int dis_recovery
                                    )
{
    int res;
      struct sk_buff *res_skb = NULL;
      struct sNCSI_RES_HDR *ncsi_reshdr;

      /* Issue the command */
      res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_DESELECT_PACKAGE,
                             NULL, CMD_DESELECT_PACKAGE_PAYLOAD_LEN , dis_recovery);

      if (NCSI_RECEIVE_RES_PACKET_OK == res)
      {
            /* proces the command */
            DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_DESELECT_PACKAGE]);

            ncsi_reshdr = NCSI_RES_HEADER(res_skb);

            if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
            {
                  DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
            }
            else
            {
              ncsi_reshdr->response_code = ntohs(ncsi_reshdr->response_code);
              ncsi_reshdr->reason_code = ntohs(ncsi_reshdr->reason_code);
                  DRV_INFO("NCSI COMMAND ERROR (%x, %x)", ncsi_reshdr->response_code, ncsi_reshdr->reason_code);
            }
      }

      if (res_skb)
      {
            DRV_INFO("NCSI Free res_skb = 0x%X\n",(unsigned int) res_skb);
            dev_kfree_skb(res_skb);
            res_skb = NULL;
      }

      return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_enable_channel
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Enable Channel".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_enable_channel(
                                      /** pointer to NC-SI interface. */
                                      sNCSI_IF_Info *ncsi_if
                                  )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    /* Using the definition to replace magic number.*/
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_ENABLE_CHANNEL,
                         NULL, CMD_ENABLE_CHANNEL_PAYLOAD_LEN , ENABLE_ERROR_RECOVERY );


    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_ENABLE_CHANNEL]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
        DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
              res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n",(unsigned int) res_skb);
        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_disable_channel
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Disable Channel".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_disable_channel(
                                       /** pointer to NC-SI interface. */
                                       sNCSI_IF_Info *ncsi_if
                                   )
{
    int res;
      struct sk_buff *res_skb = NULL;
      struct sNCSI_RES_HDR *ncsi_reshdr;
      u32 payload;

      /* Issue the command */
      payload = CMD_DISABLE_CHANNEL_PAYLOAD_KEEP_LINK_UP;
      res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_DISABLE_CHANNEL,
                             (u8 *)&payload, CMD_DISABLE_CHANNEL_PAYLOAD_LEN , ENABLE_ERROR_RECOVERY );

      if (NCSI_RECEIVE_RES_PACKET_OK == res)
      {
            /* proces the command */
            DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_DISABLE_CHANNEL]);

            ncsi_reshdr = NCSI_RES_HEADER(res_skb);

            if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
            {
                  DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
            }
            else
            {
                  res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
            }
      }

      if (res_skb)
      {
            DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

            dev_kfree_skb(res_skb);
            res_skb = NULL;
      }

      return (res);
}

#if 0
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_reset_channel
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Reset Channel".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_reset_channel(
                                     /** pointer to NC-SI interface. */
                                     sNCSI_IF_Info *ncsi_if ,

                                     int dis_recovery
                                 )
{
    int res;
    u32 payload;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    payload = 0;
    /* Issue the command */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_RESET_CHANNEL,
                             (u8 *)&payload, CMD_RESET_CHANNEL_PAYLOAD_LEN ,dis_recovery );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_RESET_CHANNEL]);
        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n",(unsigned int) res_skb);
        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }
    return (res);
}
#endif

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_enable_channel_network_tx
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Enable Channel Network Tx".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_enable_channel_network_tx(
                                                 /** pointer to NC-SI interface. */
                                                 sNCSI_IF_Info *ncsi_if,

                                                 int dis_recovery
                                             )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

	DRV_MSG("TX enable  - %x\n", ncsi_if->chnl_id);

    /* Issue the command */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_ENABLE_CHANNEL_NETWORK_TX,
                     NULL, CMD_ENABLE_CHANNEL_NETWORK_TX_PAYLOAD_LEN , dis_recovery);

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_ENABLE_CHANNEL_NETWORK_TX]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
              DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
              DRV_INFO("NCSI_RES_CODE_CMD_OK != ncsi_reshdr->response_code \n");
              res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);
        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }
    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_disable_channel_network_tx
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Disable Channel Network Tx".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_disable_channel_network_tx(
                                                  /** pointer to NC-SI interface. */
                                                  sNCSI_IF_Info *ncsi_if,

                                                  int dis_recovery
                                              )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

	DRV_MSG("TX disable - %x\n", ncsi_if->chnl_id);

    /* Issue the command */
    /* Using the definition to replace magic number.*/
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_DISABLE_CHANNEL_NETWORK_TX,
                     NULL, CMD_DISABLE_CHANNEL_NETWORK_TX_PAYLOAD_LEN ,dis_recovery );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_DISABLE_CHANNEL_NETWORK_TX]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK != ncsi_reshdr->response_code \n");
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);
        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }
    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_aen_enable
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "AEN Enable".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_aen_enable(
                                  /** pointer to NC-SI interface. */
                                  sNCSI_IF_Info *ncsi_if
                              )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    u32 payload[2];

    payload[0] = htonl( (u32) NCSI_MC_ID);
    payload[1] = htonl(ncsi_if->ncsi_aen_ctrl);
    /* Issue the command */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_AEN_ENABLE,
                         (u8 *)payload, CMD_AEN_ENABLE_PAYLOAD_LEN , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_AEN_ENABLE]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_set_link
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Set Link".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_set_link(
                                /** pointer to NC-SI interface. */
                                sNCSI_IF_Info *ncsi_if
                            )
{
    int res = 0 ;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    u32 payload[2];
    payload[0] = htonl((u32) ncsi_if->ncsi_chnl_parameters.link_setting.link_settings);
    payload[1] = 0;

    /* Issue the command */
    /* Using the definition to replace magic number.*/
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_SET_LINK,
                             (u8 *)payload, CMD_SET_LINK_PAYLOAD_LEN ,ENABLE_ERROR_RECOVERY );
    /*Add error handling process.*/
    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_SET_LINK]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }
    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_set_ethtool_settings
******************************************************************************/
/**
 *  @brief      The ethtool set setting function for NC-SI device.
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_set_ethtool_settings(
                                        /** pointer to NC-SI interface. */
                                        struct net_device *dev,

                                        /** */
                                        struct ethtool_cmd *cmd
                                    )
{
    int err;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);

    #ifdef CONFIG_EVB_WORKAROUND
    if( IANA_BROADCOM == ncsi_priv->ncsi_version.iana )
    {
        /* Correct the typo. */
        printk(KERN_EMERG "Does not support setting link command");
        return 0 ;
    }
    #endif

    if(cmd->autoneg)
    {
        /* Using the definition to replace magic number. */
        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings =
                                                                CMD_SET_LINK_PAYLOAD_AUTO_NEGO_ENABLE;
        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_10M;
        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_100M;
        //array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |= (1 <<11);
        #ifdef NCSI_SPEED_1000M_AUTONEGOTIATION_SUPPORT
        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_1G ;
        #else
        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_10G;
        #endif

        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_HALF_DUPLEX;
        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_FULL_DUPLEX;
    }
    else
    {
        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings &=
                                                                CMD_SET_LINK_PAYLOAD_AUTO_NEGO_DISABLE;

        switch(cmd->speed)
        {
            case SPEED_10:
                array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_10M;
                break;
            case SPEED_100:
                array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_100M;
                break;
            case SPEED_1000:
                array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_1G;
                break;
            case SPEED_10000:
                array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_10G ;
                break;

            default:

                switch(ncsi_priv->ncsi_link_status.speed_and_duplex)
                {
                    /** The forced link mode to speed 10Mb and half duplex */
                    case 1:
                    case 2:
                        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_10M;
                        break;
                    /** The forced link mode to speed 100Mb and half duplex */
                    case 3:
                    case 4:
                    case 5:
                        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_100M;
                        break;
                    /** The forced link mode to speed 1Gb and half duplex */
                    case 6:
                    case 7:
                        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_1G;
                        break;
                    /** The forced link mode to speed 10Gb and full duplex */
                    case 8:
                        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_LINK_SPEED_10G;                         break;
                    default:
                        DRV_ERROR("Invalid SPEED parameter!\n");
                        return 1;
                        break;
                }

                break;
        }
        switch(cmd->duplex)
        {
            case DUPLEX_HALF:
                array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_HALF_DUPLEX;
                break;
            case DUPLEX_FULL:
                array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_FULL_DUPLEX;
                break;
            default:
                /** To check NC-SI device's link mode */
                switch(ncsi_priv->ncsi_link_status.speed_and_duplex)
                {
                    /** The forced link mode to speed 10Mb and half duplex */
                    case 1:
                    case 3:
                    case 6:
                        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_HALF_DUPLEX;
                        break;
                    /** The forced link mode to speed 10Mb and full duplex */
                    case 2:
                    case 4:
                    case 5:
                    case 7:
                    case 8:
                        array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings |=
                                                                CMD_SET_LINK_PAYLOAD_FULL_DUPLEX;
                        break;
                    default :
                        printk(KERN_ERR"Invalid DUPLEX parameter!\n");
                        return 1;
                        break;
                }

                break;
        }

    }
    #ifdef DEBUG_SHOW_SEND_NCSI_CONTROL_PACKET
    printk(KERN_EMERG"ncsi_cmd_set_link -cmd->auto=%d speed=%d, duplex=%d \n",cmd->autoneg,cmd->speed, cmd->duplex);
    printk(KERN_EMERG"ncsi_cmd_set_link - link_settings= 0x%x\n",array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.link_setting.link_settings);
    #endif
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].query_chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
    err = ncsi_cmd_set_link((sNCSI_IF_Info *)&array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_INFO("ncsi_cmd_set_link Fail!\n");
        return err;
    }
    DRV_INFO("ncsi_cmd_set_link success!\n");

    return 0;

}

/******************************************************************************
*   FUNCTION        :   ncsi_get_ethtool_settings
******************************************************************************/
/**
 *  @brief      The ethtool get setting function for NC-SI device.
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_get_ethtool_settings(
                                        /** pointer to NC-SI interface. */
                                        struct net_device *dev,

                                        /** */
                                        struct ethtool_cmd *cmd
                                    )
{
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);

    cmd->cmd = ETHTOOL_GSET ;
    /** To check NC-SI device's link mode */
    switch(ncsi_priv->ncsi_link_status.speed_and_duplex)
    {
        /** The forced link mode to speed 10Mb and half duplex */
          case 1:
               cmd->speed = SPEED_10;
               cmd->duplex = DUPLEX_HALF;
               break;
        /** The forced link mode to speed 10Mb and full duplex */
          case 2:
               cmd->speed = SPEED_10;
               cmd->duplex = DUPLEX_FULL;
               break;
        /** The forced link mode to speed 100Mb and half duplex */
        case 3:
               cmd->speed = SPEED_100;
               cmd->duplex = DUPLEX_HALF;
               break;
        /** The forced link mode to speed 100Mb and full duplex */
          case 4:
               cmd->speed = SPEED_100;
               cmd->duplex = DUPLEX_FULL;
               break;
        /** The forced link mode to speed 100Mb and full duplex */
          case 5:
               cmd->speed = SPEED_100;
               cmd->duplex = DUPLEX_FULL;
               break;
        /** The forced link mode to speed 1Gb and half duplex */
          case 6:
               cmd->speed = SPEED_1000;
               cmd->duplex = DUPLEX_HALF;
               break;
        /** The forced link mode to speed 1Gb and full duplex */
          case 7:
               cmd->speed = SPEED_1000;
               cmd->duplex = DUPLEX_FULL;
               break;
        /** The forced link mode to speed 10000Mb and full duplex */
          case 8:
               cmd->speed = SPEED_10000;
               cmd->duplex = DUPLEX_FULL;
               break;
          default:
               cmd->speed = 0;
               cmd->duplex = 0;
               break;
    }

    if ( ncsi_priv->ncsi_link_status.auto_negotiate_flag == 1 &&
         ncsi_priv->ncsi_link_status.auto_negotiate_complete == 1
       )
    {
        cmd->supported = 0;
        cmd->advertising = 0;

        if(ncsi_priv->ncsi_link_status.link_partner_advertised_1000TFD)
        {
          cmd->supported |= SUPPORTED_1000baseT_Full;
          cmd->advertising |= ADVERTISED_1000baseT_Full;
        }

        if(ncsi_priv->ncsi_link_status.link_partner_advertised_1000THD)
        {
          cmd->supported |= SUPPORTED_1000baseT_Half;
          cmd->advertising |= ADVERTISED_1000baseT_Half;
        }

        if(ncsi_priv->ncsi_link_status.link_partner_advertised_100TXFD)
        {
          cmd->supported |= SUPPORTED_100baseT_Full;
          cmd->advertising |= ADVERTISED_100baseT_Full;
        }

        if(ncsi_priv->ncsi_link_status.link_partner_advertised_100TXHD)
        {
          cmd->supported |= SUPPORTED_100baseT_Half;
          cmd->advertising |= ADVERTISED_100baseT_Half;
        }

        if(ncsi_priv->ncsi_link_status.link_partner_advertised_10TFD)
        {
          cmd->supported |= SUPPORTED_10baseT_Full;
          cmd->advertising |= ADVERTISED_10baseT_Full;
        }

        if(ncsi_priv->ncsi_link_status.link_partner_advertised_10THD)
        {
          cmd->supported |= SUPPORTED_10baseT_Half;
          cmd->advertising |= ADVERTISED_10baseT_Half;
        }
    }
    cmd->supported |= SUPPORTED_MII | SUPPORTED_Autoneg;
    cmd->advertising |= ADVERTISED_Autoneg;
    /** Which connector port */
    cmd->port = PORT_MII;
    cmd->phy_address = 0x00;
    /** Which transceiver to use */
    cmd->transceiver = XCVR_INTERNAL;
    /** Enable or disable autonegotiation */
    cmd->autoneg = ncsi_priv->ncsi_link_status.auto_negotiate_flag;

    return 0;
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_link_status_with_rt
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get Link" with retries and timeout parameters.
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_link_status_with_rt(
                                            /** pointer to NC-SI interface. */
                                            struct net_device *dev,
                                            /** Times for retry. */
                                            u32 u32retry,
                                            /** Timeout.*/
                                            u32 u32timeout
                                           )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].query_chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = __ncsi_send_and_wait((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index],
                             &res_skb, CMD_GET_LINK_STATUS,NULL,
                             CMD_GET_LINK_STATUS_PAYLOAD_LEN,
                             u32retry, u32timeout , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_GET_LINK_STATUS]);
        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        /* Save response code and return code. */
        ncsi_priv->ncsi_link_status.response_code
            = ntohs(ncsi_reshdr->response_code);
        ncsi_priv->ncsi_link_status.reason_code
            = ntohs(ncsi_reshdr->reason_code);

        /* If success, get return data. */
        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            if( ncsi_reshdr->ncsi_hdr.payload_len > 4 )
            {
                memcpy((u8 *)&array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status,
                       (u8 *)&ncsi_reshdr->payload_data, (ncsi_reshdr->ncsi_hdr.payload_len - 4));
                array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status
                    = ntohl(array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status);
                ncsi_priv->ncsi_link_status.link_flag
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00000001;
                ncsi_priv->ncsi_link_status.speed_and_duplex
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x0000001E) >> 1;
                ncsi_priv->ncsi_link_status.auto_negotiate_flag
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00000020) >> 5;
                ncsi_priv->ncsi_link_status.auto_negotiate_complete
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00000040) >> 6;
                ncsi_priv->ncsi_link_status.parallel_detection_flag
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00000080) >> 7;
                ncsi_priv->ncsi_link_status.link_partner_advertised_1000TFD
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00000200) >> 9;
                ncsi_priv->ncsi_link_status.link_partner_advertised_1000THD
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00000400) >> 10;
                ncsi_priv->ncsi_link_status.link_partner_advertised_100T4
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00000800) >> 11;
                ncsi_priv->ncsi_link_status.link_partner_advertised_100TXFD
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00001000) >> 12;
                ncsi_priv->ncsi_link_status.link_partner_advertised_100TXHD
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00002000) >> 13;
                ncsi_priv->ncsi_link_status.link_partner_advertised_10TFD
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00004000) >> 14;
                ncsi_priv->ncsi_link_status.link_partner_advertised_10THD
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00008000) >> 15;
                ncsi_priv->ncsi_link_status.tx_flow_control_flag
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00010000) >> 16;
                ncsi_priv->ncsi_link_status.rx_flow_control_flag
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00020000) >> 17;
                ncsi_priv->ncsi_link_status.link_partner_advertised_flow_control
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x000C0000) >> 18;
                ncsi_priv->ncsi_link_status.serdes_link
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00100000) >> 20;
                ncsi_priv->ncsi_link_status.oem_link_speed_valid
                    = (array_ncsi_if[ncsi_priv->mii_index].ncsi_link_status.link_status & 0x00100000) >> 21;

                if(ncsi_priv->ncsi_link_status.link_flag)
                {
                    if (!netif_carrier_ok(dev))
                    {
                        netif_carrier_on(dev);
                        netif_wake_queue(dev);
                        DRV_INFO("Query channel %x is carrier on!\n", array_ncsi_if[ncsi_priv->mii_index].query_chnl_id);
                    }
                }
                else
                {
                    if (netif_carrier_ok(dev))
                    {
                        netif_carrier_off(dev);
                        netif_stop_queue(dev);
                        DRV_INFO("Query channel %x is carrier off!\n", array_ncsi_if[ncsi_priv->mii_index].query_chnl_id);
                    }
                }
            }
            else
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
                DRV_INFO("Get Wrong Payload Length of Set Link Status Command!\n");
            }
        }
        else
        {
            DRV_INFO("%s: command issue error!\n", ncsi_cmd_str[CMD_GET_LINK_STATUS]);
            if ((ntohs(ncsi_reshdr->reason_code) & 0xFF) == 0x01)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
                DRV_INFO("Set Link Host OS/Driver Conflict!\n");
            }
            else if ((ntohs(ncsi_reshdr->reason_code) & 0xFF) == 0x02)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
                DRV_INFO("Set Link Media Conflict!\n");
            }
            else if ((ntohs(ncsi_reshdr->reason_code) & 0xFF) == 0x03)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
                DRV_INFO("Set Link Parameter Conflict!\n");
            }
            else if ((ntohs(ncsi_reshdr->reason_code) & 0xFF) == 0x04)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
                DRV_INFO("Set Link Power Mode Conflict!\n");
            }
            else if ((ntohs(ncsi_reshdr->reason_code) & 0xFF) == 0x05)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
                DRV_INFO("Set Link Speed Conflict!\n");
            }
            else if ((ntohs(ncsi_reshdr->reason_code) & 0xFF) == 0x06)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
                DRV_INFO("Link Command Failed-Hardware Access Error!\n");
            }
            res = ncsi_cmd_error_handler((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index],ncsi_reshdr);
        }
    }
    else
    {
        /* If no response, set reason code and response code. */
        ncsi_priv->ncsi_link_status.response_code
            = NCSI_RES_CODE_CMD_FAILED;
        ncsi_priv->ncsi_link_status.reason_code
            = NCSI_REASON_CODE_NO_ERROR;
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);
        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_link_status
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get Link".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_link_status(
                                       /** pointer to NC-SI interface. */
                                       struct net_device *dev
                                   )
{
    return (ncsi_cmd_get_link_status_with_rt(dev,
                                            (NCSI_ERROR_RETRY_COUNT * 5),
                                            (NCSI_ERROR_RETRY_DELAY * 5)));
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_set_vlan_filters
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Set VLAN filters".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_set_vlan_filters(
                                        /** pointer to NC-SI interface. */
                                        struct net_device *dev
                                    )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    u32 payload[2];

    array_ncsi_if[ncsi_priv->mii_index].ncsi_vlan_info.vlan_id = ((ncsi_priv->vlan_config.UserPriority & 0x07) << 13) +
                                       ((ncsi_priv->vlan_config.CFI & 0x01) << 12) +
                                       (ncsi_priv->vlan_config.VID & 0x0FFF) ;
    array_ncsi_if[ncsi_priv->mii_index].ncsi_vlan_info.selector_enable = ((ncsi_priv->vlan_config.FilterSelector & 0x0F) << 8) +
                                              0x01;

    payload[0] = htonl(array_ncsi_if[ncsi_priv->mii_index].ncsi_vlan_info.vlan_id);
    payload[1] = htonl(array_ncsi_if[ncsi_priv->mii_index].ncsi_vlan_info.selector_enable);

    /* Issue the command */
    /* Using the definition to replace magic number. */
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
    res = ncsi_send_and_wait((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index], &res_skb, CMD_SET_VLAN_FILTERS,
                             (u8 *)payload, CMD_SET_VLAN_FILTERS_PAYLOAD_LEN , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_SET_VLAN_FILTERS]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index],ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_enable_vlan
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Enable VLAN".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_enable_vlan(
                                /** pointer to NC-SI interface. */
                                 struct net_device *dev
                               )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    u32 payload;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);;

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
    array_ncsi_if[ncsi_priv->mii_index].ncsi_vlan_mode = ncsi_priv->vlan_config.VMode;

    payload = htonl(array_ncsi_if[ncsi_priv->mii_index].ncsi_vlan_mode);


    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = ncsi_send_and_wait((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index], &res_skb, CMD_ENABLE_VLAN,
                             (u8 *)&payload, CMD_ENABLE_VLAN_PAYLOAD_LEN , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_ENABLE_VLAN]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index],ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);
        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_disable_vlan
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Disable VLAN".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_disable_vlan(
                                 /** pointer to NC-SI interface. */
                                 struct net_device *dev
                                )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = ncsi_send_and_wait((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index], &res_skb, CMD_DISABLE_VLAN,
                             NULL, CMD_DISABLE_VLAN_PAYLOAD_LEN , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_DISABLE_VLAN]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index],ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_set_mac_address
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Set MAC Address".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_set_mac_address(
                                    /** pointer to NC-SI interface */
                                    sNCSI_IF_Info *ncsi_if
                                   )
{
    int res;
    int i;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    u8 payload[CMD_SET_MAC_ADDRESS_PAYLOAD_LEN];

    for(i=0;i<6;i++)
    {
        payload[i] = ncsi_if->ncsi_mac_info.mac_addr[i];
    }
    payload[6] = ncsi_if->ncsi_mac_info.mac_num;
    payload[7] = ncsi_if->ncsi_mac_info.addr_type_enable;

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_SET_MAC_ADDRESS,
                             payload, CMD_SET_MAC_ADDRESS_PAYLOAD_LEN ,ENABLE_ERROR_RECOVERY);

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_SET_MAC_ADDRESS]);
        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_enable_broadcast_filtering
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Enable broadcast filtering".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_enable_broadcast_filtering(
                                               /** pointer to NC-SI interface. */
                                               sNCSI_IF_Info *ncsi_if
                                              )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    u32 payload;

    payload = htonl(ncsi_if->ncsi_chnl_caps.bcast_filter_flag);

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_ENABLE_BROADCAST_FILTERING,
                             (u8 *)&payload, CMD_ENABLE_BROADCAST_FILTERING_PAYLOAD_LEN, ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_ENABLE_BROADCAST_FILTERING]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

#if 0
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_disable_broadcast_filtering
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Disable broadcast filtering".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_disable_broadcast_filtering(
                                                /** pointer to NC-SI interface. */
                                                sNCSI_IF_Info *ncsi_if
                                               )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_DISABLE_BROADCAST_FILTERING,
                         NULL, CMD_DISABLE_BROADCAST_FILTERING_PAYLOAD_LEN,ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_DISABLE_BROADCAST_FILTERING]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
              DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
              res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}
#endif
#if 0
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_enable_global_multicast_filtering
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Enable Global Multicast Filtering".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_enable_global_multicast_filtering(
                                                      /** pointer to NC-SI interface. */
                                                      sNCSI_IF_Info *ncsi_if
                                                     )
{
    int res = 0;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    u32 payload;

//    payload = htonl(ncsi_if->ncsi_chnl_caps.mcast_filter_flag);
    payload = 0;   //filter out all multicast packets

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_ENABLE_GLOBAL_MULTICAST_FILTERING,
                             (u8 *)&payload, CMD_ENABLE_GLOBAL_MULTICAST_FILTERING_PAYLOAD_LEN,ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_ENABLE_GLOBAL_MULTICAST_FILTERING]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}
#endif
#if 0
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_disable_global_multicast_filtering
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Disable Global Multicast Filtering".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_disable_global_multicast_filtering(
                                                       /** pointer to NC-SI interface. */
                                                       sNCSI_IF_Info *ncsi_if
                                                      )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_DISABLE_GLOBAL_MULTICAST_FILTERING,
                             NULL, CMD_DISABLE_GLOBAL_MULTICAST_FILTERING_PAYLOAD_LEN ,ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_DISABLE_GLOBAL_MULTICAST_FILTERING]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}
#endif

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_set_ncsi_flow_control
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Set NC-SI flow control".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
//static int ncsi_cmd_set_ncsi_flow_control(
//                                          /** pointer to NC-SI interface */
//                                          sNCSI_IF_Info *ncsi_if
//                                         )
//{
//    int res;
//      struct sk_buff *res_skb = NULL;
//      struct sNCSI_RES_HDR *ncsi_reshdr;
//
//      /* Issue the command */
//      res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_SET_NCSI_FLOW_CONTROL,
//                             (u8 *)&ncsi_if->ncsi_flow_ctrl, CMD_SET_NCSI_FLOW_CONTROL_PAYLOAD_LEN, 0 );
//
//      if (NCSI_RECEIVE_RES_PACKET_OK == res)
//      {
//            /* proces the command */
//            DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_SET_NCSI_FLOW_CONTROL]);
//
//            ncsi_reshdr = NCSI_RES_HEADER(res_skb);
//
//            if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
//            {
//                  DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
//            }
//            else
//            {
//                  res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
//            }
//      }
//
//      if (res_skb)
//      {
//            DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);
//
//            dev_kfree_skb(res_skb);
//            res_skb = NULL;
//      }
//
//      return (res);
//}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_version_id_with_rt
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get Version ID" with timeout and retries parameters.
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_version_id_with_rt (
                                      /** pointer to NC-SI interface. */
                                      sNCSI_IF_Info *ncsi_if,
                                      /** Times for retry. */
                                      u32 u32retry,
                                      /** Timeout.*/
                                      u32 u32timeout,
                                      /** Flag that indicate to transform reading or not. */
                                      u8  u8transformreading
                                  )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    /* Using the definition to replace magic number. */
    u8 tmpstr[FIRMWARE_NAME_STRING_LENGTH+1];

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = __ncsi_send_and_wait(ncsi_if, &res_skb, CMD_GET_VERSION_ID,
                               NULL, CMD_GET_VERSION_ID_PAYLOAD_LEN,
                               u32retry, u32timeout ,ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_GET_VERSION_ID]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        /* Save response code and reason code. */
        ncsi_if->ncsi_version.response_code
            = ntohs(ncsi_reshdr->response_code);
        ncsi_if->ncsi_version.reason_code
            = ntohs(ncsi_reshdr->reason_code);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            if( ncsi_reshdr->ncsi_hdr.payload_len > 4 )
            {
                memcpy((u8 *)&ncsi_if->ncsi_version,
                       (u8 *)&ncsi_reshdr->payload_data,
                       (ncsi_reshdr->ncsi_hdr.payload_len - 4));

                /* The reading is BCD-encoded, the u8transformreading decides to transform the read to human reading or not. */
                if (u8transformreading)
                {
                    DRV_URGENT ("\n===== Transform to human reading =====\n");
                    if((ncsi_if->ncsi_version.ncsi_version[0]&0xF0) == 0xf0)
                    {
                        ncsi_if->ncsi_version.ncsi_version[0] = ncsi_if->ncsi_version.ncsi_version[0] & 0x0F;
                    }
                    if((ncsi_if->ncsi_version.ncsi_version[1]&0xF0) == 0xf0)
                    {
                        ncsi_if->ncsi_version.ncsi_version[1] = ncsi_if->ncsi_version.ncsi_version[1] & 0x0F;
                    }
                    if((ncsi_if->ncsi_version.ncsi_version[2]&0xF0) == 0xf0)
                    {
                        ncsi_if->ncsi_version.ncsi_version[2] = ncsi_if->ncsi_version.ncsi_version[2] & 0x0F;
                    }
                }
                DRV_URGENT ("\n===== NCSI Version =====\n");
                DRV_URGENT ( "NCSI Version: %d%d.%d%d.%d%d.%c\n",
                           (ncsi_if->ncsi_version.ncsi_version[0] & 0xF0) >> 4,
                           (ncsi_if->ncsi_version.ncsi_version[0] & 0x0F),
                           (ncsi_if->ncsi_version.ncsi_version[1] & 0xF0) >> 4,
                           (ncsi_if->ncsi_version.ncsi_version[1] & 0x0F),
                           (ncsi_if->ncsi_version.ncsi_version[2] & 0xF0) >> 4,
                           (ncsi_if->ncsi_version.ncsi_version[2] & 0x0F),
                           ncsi_if->ncsi_version.ncsi_version[3]
                         );
               /* Using the definition to replace magic number. */
                memset(tmpstr, '\0', sizeof(tmpstr));
                memcpy(tmpstr, ncsi_if->ncsi_version.fw_name, FIRMWARE_NAME_STRING_LENGTH );

                DRV_URGENT ("FW Name String: %s \n", tmpstr);
                DRV_URGENT ("FW Version:%02X.%02X.%02X.%02X\n",
                        *(((u8 *)(&(ncsi_if->ncsi_version.fw_version))) + 0),
                        *(((u8 *)(&(ncsi_if->ncsi_version.fw_version))) + 1),
                        *(((u8 *)(&(ncsi_if->ncsi_version.fw_version))) + 2),
                        *(((u8 *)(&(ncsi_if->ncsi_version.fw_version))) + 3));

                ncsi_if->ncsi_version.pci_did = ntohs(ncsi_if->ncsi_version.pci_did);
                ncsi_if->ncsi_version.pci_vid = ntohs(ncsi_if->ncsi_version.pci_vid);
                ncsi_if->ncsi_version.pci_ssid = ntohs(ncsi_if->ncsi_version.pci_ssid);
                ncsi_if->ncsi_version.pci_svid = ntohs(ncsi_if->ncsi_version.pci_svid);

                DRV_URGENT ("PCI ID Infromation: DID: 0x%X, VID: 0x%X, SSID: 0x%X, SVID: 0x%X\n",
                          ncsi_if->ncsi_version.pci_did,
                          ncsi_if->ncsi_version.pci_vid,
                          ncsi_if->ncsi_version.pci_ssid,
                          ncsi_if->ncsi_version.pci_svid);

                ncsi_if->ncsi_version.iana = ntohl(ncsi_if->ncsi_version.iana);
                    DRV_URGENT ("IANA: 0x%X\n\n", ncsi_if->ncsi_version.iana);
            }
            else
            {
                ncsi_set_recovery_flag(ncsi_if, 1, __FUNCTION__, __LINE__);
                DRV_INFO("Get Wrong Payload Length of Get Version Command!\n");
            }
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }
    else
    {
        /* If no response, set a response code and reason code. */
        ncsi_if->ncsi_version.response_code
            = NCSI_RES_CODE_CMD_FAILED;
        ncsi_if->ncsi_version.reason_code
            = NCSI_REASON_CODE_NO_ERROR;
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_version_id
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get Version ID".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_version_id(
                                      /** pointer to NC-SI interface. */
                                      sNCSI_IF_Info *ncsi_if
                                  )
{
    return (ncsi_cmd_get_version_id_with_rt (ncsi_if, NCSI_ERROR_RETRY_COUNT,
                                             NCSI_ERROR_RETRY_DELAY, true));
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_capabilities_with_rt
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get Capabilities" with retries and timeout parameters.
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_capabilities_with_rt (
                                               /** pointer to NC-SI interface. */
                                               sNCSI_IF_Info *ncsi_if,
                                               /** Times for retry. */
                                               u32 u32retry,
                                               /** Timeout.*/
                                               u32 u32timeout,

                                               int dis_recovery
                                             )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    res = __ncsi_send_and_wait(ncsi_if, &res_skb, CMD_GET_CAPABILITIES,
                               NULL, CMD_GET_CAPABILITIES_PAYLOAD_LEN,
                               u32retry, u32timeout , dis_recovery );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_GET_CAPABILITIES]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        /* Save response code and reason code. */
        ncsi_if->ncsi_chnl_caps.response_code
            = ntohs(ncsi_reshdr->response_code);
        ncsi_if->ncsi_chnl_caps.reason_code
            = ntohs(ncsi_reshdr->reason_code);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            if (ncsi_reshdr->ncsi_hdr.payload_len > 4)
            {
                /* If success, save return data. */
                memcpy((u8 *)&ncsi_if->ncsi_chnl_caps,
                       (u8 *)&ncsi_reshdr->payload_data,
                       (ncsi_reshdr->ncsi_hdr.payload_len - 4));
                ncsi_if->ncsi_chnl_caps.capabilities_flags = ntohl(ncsi_if->ncsi_chnl_caps.capabilities_flags);
                ncsi_if->ncsi_chnl_caps.bcast_filter_flag = ntohl(ncsi_if->ncsi_chnl_caps.bcast_filter_flag);
                ncsi_if->ncsi_chnl_caps.mcast_filter_flag = ntohl(ncsi_if->ncsi_chnl_caps.mcast_filter_flag);
                ncsi_if->ncsi_chnl_caps.buf_cap = ntohl(ncsi_if->ncsi_chnl_caps.buf_cap);
                ncsi_if->ncsi_chnl_caps.aen_ctrl = ntohl(ncsi_if->ncsi_chnl_caps.aen_ctrl);

                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.capabilities_flags = %x   \n", ncsi_if->ncsi_chnl_caps.capabilities_flags);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.bcast_filter_flag = %x\n", ncsi_if->ncsi_chnl_caps.bcast_filter_flag);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.mcast_filter_flag = %x\n", ncsi_if->ncsi_chnl_caps.mcast_filter_flag);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.buf_cap = %x\n", ncsi_if->ncsi_chnl_caps.buf_cap);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.aen_ctrl = %x\n", ncsi_if->ncsi_chnl_caps.aen_ctrl);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.vlan_filter_cnt = %x\n", ncsi_if->ncsi_chnl_caps.vlan_filter_cnt);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.mixed_filter_cnt = %x\n", ncsi_if->ncsi_chnl_caps.mixed_filter_cnt);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.mcast_filter_cnt = %x\n", ncsi_if->ncsi_chnl_caps.mcast_filter_cnt);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.ucast_filter_cnt = %x\n", ncsi_if->ncsi_chnl_caps.ucast_filter_cnt);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.reserved2 = %x\n", ncsi_if->ncsi_chnl_caps.reserved2);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.vlan_mode = %x\n", ncsi_if->ncsi_chnl_caps.vlan_mode);
                DRV_URGENT(" ncsi_if->ncsi_chnl_caps.chnl_cnt = %x\n", ncsi_if->ncsi_chnl_caps.chnl_cnt);
            }
            else
            {
                if (!dis_recovery)
                {
                    ncsi_set_recovery_flag(ncsi_if, 1, __FUNCTION__, __LINE__);
                }
                DRV_INFO("Get Wrong Payload Length of Get Capabilities Command!\n");
            }
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }
    else
    {
        /* If no response, set a response code and reason code. */
        ncsi_if->ncsi_chnl_caps.response_code
            = NCSI_RES_CODE_CMD_FAILED;
        ncsi_if->ncsi_chnl_caps.reason_code
            = NCSI_REASON_CODE_NO_ERROR;
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}
/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_capabilities
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get Capabilities".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_capabilities(
                                     /** pointer to NC-SI interface. */
                                     sNCSI_IF_Info *ncsi_if ,

                                     int dis_recovery
                                    )
{
    return (ncsi_cmd_get_capabilities_with_rt (ncsi_if,
                                               NCSI_ERROR_RETRY_COUNT,
                                               NCSI_ERROR_RETRY_DELAY,
                                               dis_recovery ));
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_parameters
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get Parameters".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_parameters(
                                   /** pointer to NC-SI interface. */
                                   sNCSI_IF_Info *ncsi_if
                                  )
{
    int res;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    res = ncsi_send_and_wait(ncsi_if, &res_skb, CMD_GET_PARAMETERS,
                             NULL, CMD_GET_PARAMETERS_PAYLOAD_LEN , DISABLE_ERROR_RECOVERY);

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_GET_PARAMETERS]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            if (ncsi_reshdr->ncsi_hdr.payload_len > (sizeof(struct sNCSI_CHNL_PARAMETER) + 4))
            {
                memcpy((u8 *)&ncsi_if->ncsi_chnl_parameters,
                             (u8 *)&ncsi_reshdr->payload_data,
                             sizeof(struct sNCSI_CHNL_PARAMETER));

                ncsi_if->ncsi_chnl_parameters.mac_addr_cnt_flag = ntohl(ncsi_if->ncsi_chnl_parameters.mac_addr_cnt_flag);
                ncsi_if->ncsi_chnl_parameters.vlan_tag_cnt_flag = ntohl(ncsi_if->ncsi_chnl_parameters.vlan_tag_cnt_flag);
                ncsi_if->ncsi_chnl_parameters.link_setting.link_settings = ntohl(ncsi_if->ncsi_chnl_parameters.link_setting.link_settings);
                ncsi_if->ncsi_chnl_parameters.link_setting.oem_link_settings = ntohl(ncsi_if->ncsi_chnl_parameters.link_setting.oem_link_settings);
                ncsi_if->ncsi_chnl_parameters.bcast_filter_flag = ntohl(ncsi_if->ncsi_chnl_parameters.bcast_filter_flag);
                ncsi_if->ncsi_chnl_parameters.config_flag = ntohl(ncsi_if->ncsi_chnl_parameters.config_flag);
                ncsi_if->ncsi_chnl_parameters.aen_ctrl = ntohl(ncsi_if->ncsi_chnl_parameters.aen_ctrl);

                DRV_INFO("ncsi_if->ncsi_chnl_parameters.mac_addr_cnt_flag = %x\n", ncsi_if->ncsi_chnl_parameters.mac_addr_cnt_flag);
                DRV_INFO("ncsi_if->ncsi_chnl_parameters.vlan_tag_cnt_flag = %x\n", ncsi_if->ncsi_chnl_parameters.vlan_tag_cnt_flag);
                DRV_INFO("ncsi_if->ncsi_chnl_parameters.link_setting.link_settings = %x\n", ncsi_if->ncsi_chnl_parameters.link_setting.link_settings);
                DRV_INFO("ncsi_if->ncsi_chnl_parameters.link_setting.oem_link_settings = %x\n", ncsi_if->ncsi_chnl_parameters.link_setting.oem_link_settings);
                DRV_INFO("ncsi_if->ncsi_chnl_parameters.bcast_filter_flag = %x\n", ncsi_if->ncsi_chnl_parameters.bcast_filter_flag);
                DRV_INFO("ncsi_if->ncsi_chnl_parameters.config_flag = %x\n", ncsi_if->ncsi_chnl_parameters.config_flag);
                DRV_INFO("ncsi_if->ncsi_chnl_parameters.aen_ctrl = %x\n", ncsi_if->ncsi_chnl_parameters.aen_ctrl);
            }
            else
            {
                DRV_INFO("Get Wrong Payload Length of Set Link Status Command!\n");
            }
        }
        else
        {
              res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }

    if (res_skb)
    {
          DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

          dev_kfree_skb(res_skb);
          res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_controller_packet_statistics
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get controller packet statistics".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_controller_packet_statistics(
                                                     /** pointer to NC-SI interface */
                                                     sNCSI_IF_Info *ncsi_if,
                                                     /** Times for retry. */
                                                     u32 u32retry,
                                                     /** Timeout.*/
                                                     u32 u32timeout
                                                    )
{
    int res;
    int i;
    u32 *pu32payloadptr = NULL;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    DRV_URGENT("Enter %s()\n",__FUNCTION__);

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = __ncsi_send_and_wait(ncsi_if, &res_skb, CMD_GET_CONTROLLER_PACKET_STATISTICS,
                               NULL, CMD_GET_CONTROLLER_PACKET_STATISTICS_PAYLOAD_LEN,
                               u32retry, u32timeout , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        /* Save response code and reason code. */
        ncsi_if->control_packet_statistics.response_code
            = ntohs(ncsi_reshdr->response_code);
        ncsi_if->control_packet_statistics.reason_code
            = ntohs(ncsi_reshdr->reason_code);

        DRV_URGENT("%s():response_code=0x%x\n",__FUNCTION__,
                ncsi_if->control_packet_statistics.response_code);
        DRV_URGENT("%s():reason_code=0x%x\n",__FUNCTION__,
                ncsi_if->control_packet_statistics.reason_code);
        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
            DRV_URGENT("%s():payload_len=%d\n",__FUNCTION__, ncsi_reshdr->ncsi_hdr.payload_len);

            pu32payloadptr = (u32 *) &ncsi_reshdr->payload_data;

            /* Prints received data. */
            for (i=0;i<ncsi_reshdr->ncsi_hdr.payload_len;i++)
            {
                DRV_URGENT("%s():payload[%d]=0x%x\n",__FUNCTION__, i,((u8 *)&ncsi_reshdr->payload_data)[i]);
            }

            /* Save received data in internal data structure. */
            i = 0;
            ncsi_if->control_packet_statistics.counters_cleared_from_last_read_ms
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.counters_cleared_from_last_read_ls
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.total_bytes_received
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);

            /* 'total_bytes_received' is 64-bit width, we need add offset i by 2 to retrieve next record.*/
            i+=2;
            ncsi_if->control_packet_statistics.total_bytes_transmitted
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.total_unicast_packets_received
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.total_multicast_packets_received
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.total_broadcast_packets_received
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.total_unicast_packets_transmitted
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.total_multicast_packets_transmitted
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.total_broadcast_packets_transmitted
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.fcs_receive_errors
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.alignment_errors
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.false_carrier_detections
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.runt_packets_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.jabber_packets_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.pause_xon_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.pause_xoff_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.pause_xon_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.pause_xoff_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.single_collision_transmit_frames
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.multiple_collision_transmit_frames
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.late_collision_frames
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.excessive_collision_frames
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.control_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_64_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_65_127_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_128_255_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_256_511_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_512_1023_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_1024_1522_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_1523_9022_frames_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_64_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_65_127_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_128_255_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_256_511_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_512_1023_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_1024_1522_frames_transmitted
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.byte_1523_9022_frames_transmitted
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.valid_bytes_received
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->control_packet_statistics.error_runt_packets_received
                = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->control_packet_statistics.error_jabber_packets_received
                = be32_to_cpu(pu32payloadptr[i++]);
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
            DRV_URGENT("%s():ncsi_cmd_error_handler(),res=0x%x\n",__FUNCTION__,res);
        }
    }
    else
    {
        /* If no response, set a response code and reason code. */
        ncsi_if->control_packet_statistics.response_code
            = NCSI_RES_CODE_CMD_FAILED;
        ncsi_if->control_packet_statistics.reason_code
            = NCSI_REASON_CODE_NO_ERROR;
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);
        DRV_URGENT("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_ncsi_statistics
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get NCSI statistics".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_ncsi_statistics(
                                        /** pointer to NC-SI interface */
                                        sNCSI_IF_Info *ncsi_if,
                                        /** Times for retry. */
                                        u32 u32retry,
                                        /** Timeout.*/
                                        u32 u32timeout
                                       )
{
    int res;
    int i;
    u32 *pu32payloadptr = NULL;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command */
    /* Using the definition to replace magic number. */
    res = __ncsi_send_and_wait(ncsi_if, &res_skb, CMD_GET_NCSI_STATISTICS,
                                NULL, CMD_GET_NCSI_STATISTICS_PAYLOAD_LEN,
                                u32retry, u32timeout , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_URGENT("%s: Start to proces the command\n", ncsi_cmd_str[CMD_GET_NCSI_STATISTICS]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        /* Save response code and reason code. */
        ncsi_if->ncsi_statistics.response_code
            = ntohs(ncsi_reshdr->response_code);
        ncsi_if->ncsi_statistics.reason_code
            = ntohs(ncsi_reshdr->reason_code);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_URGENT("%s():NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n", __FUNCTION__);
            DRV_URGENT("%s():payload_len=%d\n",__FUNCTION__, ncsi_reshdr->ncsi_hdr.payload_len);
            pu32payloadptr = (u32 *) &ncsi_reshdr->payload_data;

            /* Prints received data. */
            for (i=0;i<ncsi_reshdr->ncsi_hdr.payload_len;i++)
            {
                DRV_URGENT("%s():payload[%d]=0x%x\n",__FUNCTION__, i,((u8 *)&ncsi_reshdr->payload_data)[i]);
            }

            /* Save received data in data structure. */
            i = 0;
            ncsi_if->ncsi_statistics.commands_received       = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->ncsi_statistics.control_packets_dropped = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->ncsi_statistics.command_type_errors     = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->ncsi_statistics.command_checksum_errors = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->ncsi_statistics.receive_packets         = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->ncsi_statistics.transmit_packets        = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->ncsi_statistics.aens_sent               = be32_to_cpu(pu32payloadptr[i++]);
        }
        else
        {
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }
    else
    {
        /* If no response, set a response code and reason code. */
        ncsi_if->ncsi_statistics.response_code
            = NCSI_RES_CODE_CMD_FAILED;
        ncsi_if->ncsi_statistics.reason_code
            = NCSI_REASON_CODE_NO_ERROR;
    }

    if (res_skb)
    {
        DRV_URGENT("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_SendOEM50
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "OEM Command".
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_SendOEM50 (
                                /** pointer to NC-SI interface */
                                sNCSI_IF_Info *ncsi_if,
                                /** Input data buffer. */
                                void *inputData,
                                /** Input data size. */
                                int inputDataSize,
                                /** Output data buffer. */
                                void *outputData,
                                /** Output data size. */
                                int *outputDataSize,
                                /** Times for retry. */
                                u32 u32retry,
                                /** Timeout.*/
                                u32 u32timeout
                              )
{
    int res;
    int i;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;

    /* Issue the command. */
    /* Using the definition to replace magic number. */
    res = __ncsi_send_and_wait(ncsi_if, &res_skb, CMD_OEM_COMMAND,
                                inputData, inputDataSize,
                                u32retry, u32timeout , ENABLE_ERROR_RECOVERY );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* Process the command. */
        DRV_URGENT("%s(): Start to proces the OEM command\n", __FUNCTION__);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_URGENT("%s():NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n", __FUNCTION__);
            DRV_URGENT("%s():payload_len=%d\n",__FUNCTION__, ncsi_reshdr->ncsi_hdr.payload_len);

            /* First 4 bytes is response code and reason code. */
            *(u16*)(outputData) = ncsi_reshdr->response_code;
            *(u16*)(outputData+2) = ncsi_reshdr->reason_code;

            /* *outputDataSize is the size given by application, ncsi_reshdr->ncsi_hdr.payload_len is actual received size.
             * In order to don't over-write the buffer, we pick the min one. */
            memcpy ((u8 *)(outputData+4),
                   (u8 *)&ncsi_reshdr->payload_data, min(*outputDataSize,ncsi_reshdr->ncsi_hdr.payload_len));

            DRV_URGENT("%s():copy_to_user finish\n",__FUNCTION__);

            /* Tells application the acutual received size. */
            *outputDataSize = ncsi_reshdr->ncsi_hdr.payload_len;

            /* Prints received data. */
            for (i=0;i<min(*outputDataSize,ncsi_reshdr->ncsi_hdr.payload_len);i++)
            {
                DRV_URGENT("%s():payload[%d]=0x%x\n",__FUNCTION__, i,((u8 *)&ncsi_reshdr->payload_data)[i]);
            }
        }
        else
        {
            /* Save response code and reason code. */
            *(u16*)(outputData) = ncsi_reshdr->response_code;
            *(u16*)(outputData+2) = ncsi_reshdr->reason_code;
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }
    else
    {
        /* If no response, set a response code and reason code. */
        ncsi_reshdr = NCSI_RES_HEADER(res_skb);
        *(u16*)(outputData) = NCSI_RES_CODE_CMD_FAILED;
        *(u16*)(outputData+2) = NCSI_REASON_CODE_NO_ERROR;
        *outputDataSize = 4;
    }

    if (res_skb)
    {
        DRV_URGENT("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_cmd_get_ncsi_passthrough_statistics
******************************************************************************/
/**
 *  @brief      Issue the NC-SI command "Get NCSI passthrough statistics".
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_cmd_get_ncsi_passthrough_statistics(
                                                    /** pointer to NC-SI interface */
                                                    sNCSI_IF_Info *ncsi_if,
                                                    /** Times for retry. */
                                                    u32 u32retry,
                                                    /** Timeout.*/
                                                    u32 u32timeout
                                                   )
{
    int res;
    int i = 0;
    struct sk_buff *res_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    u32 *pu32payloadptr = NULL;

    /* Issue the command */
    res = __ncsi_send_and_wait(ncsi_if, &res_skb, CMD_GET_NCSI_PASSTHROUGH_STATISTICS,
                                NULL, CMD_GET_NCSI_PASSTHROUGH_STATISTICS_PAYLOAD_LEN,
                                u32retry, u32timeout , 0 );

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* proces the command */
        DRV_INFO("%s: Start to proces the command\n", ncsi_cmd_str[CMD_GET_NCSI_PASSTHROUGH_STATISTICS]);
        DRV_URGENT("%s: Start to proces the command\n", ncsi_cmd_str[CMD_GET_NCSI_PASSTHROUGH_STATISTICS]);

        ncsi_reshdr = NCSI_RES_HEADER(res_skb);

        ncsi_if->passthrough_statistics.response_code
            = ntohs(ncsi_reshdr->response_code);
        ncsi_if->passthrough_statistics.reason_code
            = ntohs(ncsi_reshdr->reason_code);

        if ((NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code))
        {
            DRV_INFO("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
            DRV_URGENT("NCSI_RES_CODE_CMD_OK == ncsi_reshdr->response_code \n");
            pu32payloadptr = (u8 *)&ncsi_reshdr->payload_data;

            /* Prints received data. */
            for (i=0;i<ncsi_reshdr->ncsi_hdr.payload_len;i++)
            {
                DRV_URGENT("%s():payload[%d]=0x%x\n",__FUNCTION__, i,((u8 *)&ncsi_reshdr->payload_data)[i]);
            }

            /* Save received data. */
            i = 0;
            ncsi_if->passthrough_statistics.pt_tx_pkts_rcved_on_ncsi_rx_interface
                = be64_to_cpu(*(u64 *)&pu32payloadptr[i]);
            i+=2;
            ncsi_if->passthrough_statistics.pt_tx_pkts_dropped = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.pt_tx_pkt_chn_state_errors = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.pt_tx_pkt_undersized_errors = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.pt_tx_pkt_oversized_errors = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.pt_rx_pkts_rcved_on_lan_interface = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.total_pt_rx_pkts_dropped = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.pt_rx_pkt_chn_state_errors = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.pt_rx_pkt_undersized_errors = be32_to_cpu(pu32payloadptr[i++]);
            ncsi_if->passthrough_statistics.pt_rx_pkt_oversized_errors = be32_to_cpu(pu32payloadptr[i++]);
        }
        else
        {
            DRV_URGENT("%s():NCSI_RES_CODE_CMD_OK != ncsi_reshdr->response_code \n",__FUNCTION__);
            res = ncsi_cmd_error_handler(ncsi_if,ncsi_reshdr);
        }
    }
    else
    {

        DRV_URGENT("%s():NCSI_RES_CODE_CMD_NORESPONSE == res \n",__FUNCTION__);
        /* If no response, set a response code and reason code. */
        ncsi_if->passthrough_statistics.response_code
            = NCSI_RES_CODE_CMD_FAILED;
        ncsi_if->passthrough_statistics.reason_code
            = NCSI_REASON_CODE_NO_ERROR;
    }

    if (res_skb)
    {
        DRV_INFO("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_aen_link_status_change
******************************************************************************/
/**
 *  @brief      Handle "Link Status Change" AEN for NC-SI device.
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_aen_link_status_change(
                                        /** pointer to NC-SI interface. */
                                        sNCSI_IF_Info *ncsi_if,
                                        /** pointer to the AEN packet header */
                                        struct sNCSI_AEN_HDR *ncsi_aenhdr
                                      )
{
    int res = 0;
    u32    link_stat;
    u8 chnl_id;
    struct ncsi_aen_link_hdr *aen_link_hdr;
    struct net_device *dev = NULL;
    struct vir_dev_priv *ncsi_priv;

    aen_link_hdr = (struct ncsi_aen_link_hdr *)ncsi_aenhdr;
    link_stat = ntohl(aen_link_hdr->link_stat);
    chnl_id = aen_link_hdr->aenh.ncsi_hdr.chnl_id ;

    list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
    {
        if (ncsi_if->dev == ncsi_priv->parent_dev)
        {
              if (ncsi_priv->ncsi_chnl_id == chnl_id)
              {
                  dev = ncsi_priv->ncsidev;
                  break;
              }
        }
    }
    if (NULL == dev)
    {
      return -1;
    }

    if (0 == (link_stat & BNCSI_LINK_UP)) /* link down */
    {
        DRV_INFO("Link down AEN\n");
        if (netif_carrier_ok(dev))  /* if current state is up */
        {
            DRV_INFO("Calling netif_carrier_off()\n");
            netif_carrier_off(dev);
        }
    }
    else
    {
        DRV_INFO("Link up AEN\n");
        if (!netif_carrier_ok(dev)) /* if current state is down */
        {
            netif_carrier_on(dev);
            netif_wake_queue(dev);
        }
        else
        {
            DRV_INFO("The [%s] is already link up!\n",dev->name);
        }
    }

    return res;
}

/******************************************************************************
*   FUNCTION        :   ncsi_aen_reconfig_required
******************************************************************************/
/**
 *  @brief      Handle "Reconfig Required" AEN for NC-SI device.
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_aen_reconfig_required(
                                         /** pointer to NC-SI interface */
                                         sNCSI_IF_Info *ncsi_if,

                                         /** pointer to the AEN packet header */
                                         struct sNCSI_AEN_HDR *ncsi_aenhdr
                                     )
{
    int err = 0;

    /** - We got the reconfig required,
          so we need to set flag to enabled error recovery */
    ncsi_set_recovery_flag(ncsi_if, 1, __FUNCTION__, __LINE__);

    return err;
}

/******************************************************************************
*   FUNCTION        :   ncsi_aen_os_driver_change
******************************************************************************/
/**
 *  @brief      Handle "OS Driver Running Status Change" AEN for NC-SI device
 *
 *
 *  @return     res: response code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_aen_os_driver_change(
                                     /** pointer to NC-SI interface */
                                     sNCSI_IF_Info *ncsi_if,

                                     /** pointer to the AEN packet header */
                                     struct sNCSI_AEN_HDR *ncsi_aenhdr
                                    )
{
    int res = 0;

    /* TBD: */
    return (res);
}

/******************************************************************************
*   FUNCTION        :   ncsi_queue_read
******************************************************************************/
/**
 *  @brief      Take the socket buffer from the response queue.
 *
 *
 *  @return     0: Success -EINVAL: Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_queue_read(
                              /** pointer to NC-SI interface */
                              sNCSI_IF_Info *ncsi_if,

                              /** pointer to response socket buffer*/
                              struct sk_buff **res_skb,

                              /** queue type */
                              enum ncsi_queue_type type
                          )
{
    struct skb_queue *skbq_res;
    /* Using the   private data(u32rcsf defined in sNCSI_IF_Info structure) to replace
       global vriable. The u32rcsf will be protected by semaphore when command transcation. */
    int delay = NCSI_ERROR_RETRY_DELAY;

    if (unlikely(in_atomic()) || test_bit(RCSF_BUSY_WAIT, &(ncsi_if->u32rcsf)))
    {
        do{
            mdelay(1);
            if (test_bit(RCSF_PACKET_RECEIVED, &(ncsi_if->u32rcsf) ) )
                break;

        }while( delay--);
         clear_bit(RCSF_BUSY_WAIT, &(ncsi_if->u32rcsf));
    }
    else
    {
        /* timeout sets to 5*jiffies - 50ms */
        wait_event_interruptible_timeout(ncsi_if->inq, test_bit(RCSF_PACKET_RECEIVED, &(ncsi_if->u32rcsf)), 5);
    }

    clear_bit(RCSF_PACKET_RECEIVED, &(ncsi_if->u32rcsf));

    skbq_res = &ncsi_if->ncsi_res_quene;

    if (skb_queue_len(&skbq_res->ncsi_queue) == 0)
    {
        DRV_INFO("ncsi_queue_read():skb_queue_len = 0\n");
        return NCSI_RES_CODE_CMD_NORESPONSE;
    }
    else
    {
        DRV_INFO("ncsi_queue_read():ok\n");
        *res_skb = skb_dequeue(&skbq_res->ncsi_queue);
    }

    return NCSI_RES_CODE_CMD_OK;
}

/******************************************************************************
*   FUNCTION        :   ncsi_queue_write
******************************************************************************/
/**
 *  @brief      Enqueue the socket buffer to the response queue.
 *
 *
 *  @return     0: Success -EINVAL: Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_queue_write(
                               /** pointer to NC-SI interface */
                               sNCSI_IF_Info *ncsi_if,

                               /** pointer to response socket buffer*/
                               struct sk_buff *skb,

                               /** queue type */
                               enum ncsi_queue_type type
                           )
{
    struct skb_queue *skbq;
    u8 queue_len;

    /* Using the private data(u32rcsf defined in sNCSI_IF_Info structure) to replace
       global vriable. The u32rcsf will be protected by semaphore when command transcation. */
    skbq = &ncsi_if->ncsi_res_quene;
    queue_len = NCSI_RES_PACKET_QUEUE_LEN;

    /* if queue is full, just return */
    if (skb_queue_len(&skbq->ncsi_queue) >= queue_len)
    {
		if (u8TraceMsgTimeout)
			printk("MTO: skb queue full %d\n", skb_queue_len(&skbq->ncsi_queue));

        DRV_WARN("ncsi_queue_write: The response queue is error! \n");
        wake_up_interruptible(&(ncsi_if->inq));
        /** the respond packet shound be dropped when the queue is full */
        return -ENOSPC ;
    }

    skb_queue_tail(&skbq->ncsi_queue, skb);

    set_bit(RCSF_PACKET_RECEIVED, &(ncsi_if->u32rcsf));
    wake_up_interruptible(&(ncsi_if->inq));

    return 0;
}

/******************************************************************************
*   FUNCTION        :   ncsi_rx_aen_packet
******************************************************************************/
/**
 *  @brief      Dispatch the received AEN packets to the related handling
 *              functions.
 *
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static void ncsi_rx_aen_packet(
                                  /** pointer to NC-SI interface */
                                  sNCSI_IF_Info *ncsi_if,

                                  /** pointer to aen socket buffer*/
                                  struct sk_buff *aen_skb
                              )
{
    struct sNCSI_AEN_HDR    *ncsi_aenhdr;

    if (aen_skb != NULL)
    {
        /** read the AEN packet in */
        ncsi_aenhdr = NCSI_AEN_HEADER(aen_skb);

        /** start to process AEN command */
        switch (ncsi_aenhdr->aen_type)
        {
            case AEN_LINK_STATUS_CHANGE:
                {
                    DRV_INFO("\n Receive a AEN_LINK_STATUS_CHANGE packet !! \n");

                    if (u8TraceAENEnable)
                        printk("\n Receive a AEN_LINK_STATUS_CHANGE packet !! \n");

                    ncsi_aen_link_status_change(ncsi_if, ncsi_aenhdr);
                    break;
                }

            case AEN_RECONFIG_REQUIRED:
                {
                    DRV_INFO("\n Receive a AEN_RECONFIG_REQUIRED packet !! \n");

                    if (u8TraceAENEnable)
                        printk("\n Receive a AEN_RECONFIG_REQUIRED packet !! \n");

                    ncsi_aen_reconfig_required(ncsi_if, ncsi_aenhdr);
                    break;
                    }

            case AEN_OS_DRIVER_CHANGE:
                {
                    DRV_INFO("\n Receive a AEN_OS_DRIVER_CHANGE packet !! \n");

                    if (u8TraceAENEnable)
                        printk("\n Receive a AEN_OS_DRIVER_CHANGE packet !! \n");

                    ncsi_aen_os_driver_change(ncsi_if, ncsi_aenhdr);
                    break;
                }

            default:
                    DRV_INFO("\n Receive a unKnown AEN packet !! \n");
                    break;
        }
        DRV_INFO("NCSI Free aen_skb = 0x%X\n",(unsigned int) aen_skb);
        dev_kfree_skb(aen_skb);
        aen_skb = NULL;

    }
    else
    {
        DRV_INFO("\n ncsi_rx_aen_packet() aen_skb == NULL !! \n");
    }
    return;
}

/******************************************************************************
*   FUNCTION        :   ncsi_rx_response_packet
******************************************************************************/
/**
 *  @brief      Receive the response packet and enqueue to the response queue.
 *
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static void ncsi_rx_response_packet(
                                       /** pointer to NC-SI interface */
                                       sNCSI_IF_Info *ncsi_if,

                                       /** pointer to response socket buffer*/
                                       struct sk_buff *res_skb
                                   )
{
    int res;

    res = ncsi_queue_write(ncsi_if, res_skb, NCSI_RES_QUEUE);

    /*Add error handling process.	*/
    if (res < 0)
    {
        dev_kfree_skb(res_skb);
        res_skb = NULL;
        DRV_WARN("NC-SI response packet en-queue error (errno=%d)!!\n", res);
    }

}


/******************************************************************************
*   FUNCTION        :   ncsi_wait_response_packet
******************************************************************************/
/**
 *  @brief      Wait for the response packets.
 *
 *
 *  @return     NCSI_RECEIVE_RES_PACKET_OK:      Success.
 *              NCSI_RECEIVE_RES_PACKET_INVALID: Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_wait_response_packet(
                                        /** pointer to NC-SI interface */
                                        sNCSI_IF_Info *ncsi_if,

                                        /** pointer to response socket buffer*/
                                        struct sk_buff **res_skb,

                                        /** response command number */
                                        u8 res_cmd
                                    )
{
    struct sNCSI_RES_HDR *ncsi_reshdr;
    int res;
	int i;

    res = ncsi_queue_read(ncsi_if, res_skb, NCSI_RES_QUEUE);

    DRV_INFO("ncsi_wait_response_packet: Finish queue read! \n");

    if (unlikely(res == NCSI_RES_CODE_CMD_NORESPONSE))
    {
        /* faid to get the response packet */
        DRV_ERROR("ncsi_wait_response_packet: Fail to get the response packet\n");
        return res;
    }
    else
    {
        if (*res_skb != NULL)
        {
            DRV_INFO("ncsi_wait_response_packet: res_skb != NULL \n");
            ncsi_reshdr = NCSI_RES_HEADER(*res_skb);

            /* Is a valid response packet and have the vaild checksum*/
            if (IS_VALID_NCSI_RES_PACKET(ncsi_reshdr, ncsi_if, res_cmd))
            //sonata: don't do checksum check now && IS_VALID_NCSI_CHECKSUM(ncsi_reshdr))
            {
                DRV_INFO("IS_VALID_NCSI_RES_PACKET\n");
                return (NCSI_RECEIVE_RES_PACKET_OK);
            }
            else
            {
                if (IS_VALID_QUERY_NCSI_RES_PACKET(ncsi_reshdr, ncsi_if, res_cmd))
                {
                    DRV_INFO("IS_VALID_QUERY_NCSI_RES_PACKET \n");
                    return (NCSI_RECEIVE_RES_PACKET_OK);
                }
                else
                {
                    DRV_ERROR("NCSI_RECEIVE_RES_PACKET_INVALID\n");

					if (u8TraceMsgTimeout)
					{
						printk("MTO: ");

						for (i = 0; i < 32; i++)
							printk("%02x ", *(((u8 *) ncsi_reshdr) + i));

						printk("\n");
					}

                    return (NCSI_RECEIVE_RES_PACKET_INVALID);
                }
            }
        }
        else
        {
            DRV_ERROR("ncsi_wait_response_packet res_skb is NULL\n");
            return (NCSI_RECEIVE_RES_PACKET_INVALID);
        }
    }
}

/******************************************************************************
*   FUNCTION        :   ncsi_eth_header
******************************************************************************/
/**
 *  @brief      prepare the ethernet header of the packet.
 *
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static void ncsi_eth_header(
                               /** pointer to NC-SI interface */
                               sNCSI_IF_Info *ncsi_if,

                               /** pointer to socket buffer */
                               struct sk_buff *skb
                           )
{
    static const unsigned char ncsi_dest_mac[ETH_ALEN] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    struct ethhdr *eth = NCSI_ETH_HEADER(skb);
    int i;

    for(i=0;i<ETH_ALEN;i++)
    {
        eth->h_source[i] = ncsi_if->dev->dev_addr[i];
    }
    memcpy(eth->h_dest, ncsi_dest_mac, 6);

    eth->h_proto = htons(NCSI_ETHER_TYPE);
    return;
}

/******************************************************************************
*   FUNCTION        :   ncsi_header_payload
******************************************************************************/
/**
 *  @brief      prepare the ethernet header of the packet.
 *
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static void ncsi_header_payload(
                                   /** pointer to NC-SI interface */
                                   sNCSI_IF_Info *ncsi_if,

                                   /** pointer to response packet */
                                   struct sk_buff *req_skb,

                                   /** ncsi command number */
                                   u8 cmd,

                                   /** pointer to the payload */
                                   u8 *payload,

                                   /** payload length */
                                   u16 payload_len
                               )
{
    struct sNCSI_HDR *ncsi_hdr;

    u8 *ncsi_payload_start;
    u32 checksum = 0;
    u8 u8cmd_iid = ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)] ;

    /* build NC-SI header */
    ncsi_hdr = NCSI_HEADER(req_skb);

    ncsi_hdr->mc_id = NCSI_MC_ID;
    ncsi_hdr->hdr_rev = NCSI_HEADER_REVISION;

    ncsi_hdr->cmd_iid = u8cmd_iid;

    ncsi_hdr->cmd = cmd;

    if(ncsi_hdr->cmd == CMD_GET_LINK_STATUS)
    {
        ncsi_hdr->chnl_id = ncsi_if->query_chnl_id;
    }
    else
    {
        ncsi_hdr->chnl_id = ncsi_if->chnl_id;
    }

    ncsi_hdr->payload_len = payload_len;

    /* build NC-SI payload */
    ncsi_payload_start = (req_skb->data + ETH_HLEN + NCSI_HLEN);

    if (0 != ncsi_hdr->payload_len)
    {
        /* the payload is always in big-endian byte order */
        memcpy(ncsi_payload_start, payload, ncsi_hdr->payload_len);
    }

    if (CMD_OEM_COMMAND != cmd)
    {
//mark by sonata: keep checksum to zero
//      checksum = ncsi_do_checksum((unsigned char *)eth,
//      ETH_HLEN+NCSI_HLEN + ncsi_hdr->payload_len);

        /* add 2's complement checksum */
        *((u32 *) (ncsi_payload_start + ncsi_hdr->payload_len)) = checksum;
    }

    return;
}

/******************************************************************************
*   FUNCTION        :   ncsi_send_packet
******************************************************************************/
/**
 *  @brief      Send NCSI packet.
 *
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_send_packet(
                               /** pointer to NC-SI interface */
                               sNCSI_IF_Info *ncsi_if,

                               /** ncsi command number */
                               u8 cmd,

                               /** pointer to the payload */
                               u8 *payload,

                               /** payload length */
                               u16 payload_len
                           )
{
    u32 ncsi_packet_len;
    int i;
    /* the payload length should be the 32-bit boundary */
    #define PAYLOAD_PAD(len)    (4 - (len % 4))

    struct sk_buff *req_skb = NULL;
    unsigned int skb_len;

    if (!netif_running(ncsi_if->dev))
    {
        DRV_ERROR("ncsi_send_packet() error! %s is down!\n",ncsi_if->dev->name);
        return STATUS_FAIL;
    }

    if (CMD_OEM_COMMAND == cmd)
    {
        skb_len = ETH_HLEN + NCSI_HLEN + payload_len ;
    }
    else
    {
        skb_len = ETH_HLEN + NCSI_HLEN +
                  payload_len + PAYLOAD_PAD(payload_len) + NCSI_CHECKSUM_LEN;
        if (skb_len < ETH_ZLEN)
        {
            skb_len = ETH_ZLEN;
        }
    }

    ncsi_packet_len = skb_len;

    req_skb = alloc_skb(skb_len + NET_IP_ALIGN, GFP_ATOMIC);

    DRV_INFO("NCSI allocate req_skb = 0x%X\n", (unsigned int)req_skb);

    if (likely(req_skb != NULL))
    {
        req_skb->dev = ncsi_if->dev;
        req_skb->len = skb_len;
        skb_reserve(req_skb, NET_IP_ALIGN);
        memset(req_skb->data, 0, skb_len);
        ncsi_eth_header(ncsi_if, req_skb);
        ncsi_header_payload(ncsi_if, req_skb, cmd, payload, payload_len);

        if ( u8dumpmsgswitch != 0)
        {
            DRV_MSG("Tx: \n");

            for(i=0;i<ncsi_packet_len;i+=4)
            {
                if((ncsi_packet_len-i)>=4)
                {
                    DRV_MSG("%2x %2x %2x %2x \n",*(req_skb->data+i),*(req_skb->data+i+1),
                    *(req_skb->data+i+2),*(req_skb->data+i+3));
                }
                else if((ncsi_packet_len%4)==3)
                {
                    DRV_MSG("%2x %2x %2x \n",*(req_skb->data+i),*(req_skb->data+i+1),
                    *(req_skb->data+i+2));
                }
                else if((ncsi_packet_len%4)==2)
                {
                    DRV_MSG("%2x %2x \n",*(req_skb->data+i),*(req_skb->data+i+1));
                }
                else if((ncsi_packet_len%4)==1)
                {
                    DRV_MSG("%2x \n",*(req_skb->data+i));
                }
            }
            DRV_MSG("\n");
        }

        if (!netif_carrier_ok(ncsi_if->dev))
        {
            if (req_skb != NULL)
            {
                dev_kfree_skb(req_skb);
                req_skb = NULL;
                DRV_ERROR("ncsi_send_packet() !netif_carrier_ok(%s)\n",ncsi_if->dev->name);
            }
            return STATUS_FAIL;
        }

        /* send NCSI packet */
        if ( 0 == dev_queue_xmit(req_skb))
        {
            DRV_INFO("ncsi_send_packet():dev_queue_xmit ok[%s][%s]!\n",ncsi_if->dev->name,ncsi_cmd_str[cmd]);
            return STATUS_OK;
        }
        else
        {
            DRV_ERROR("ncsi_send_packet():dev_queue_xmit fail!\n");
            return STATUS_FAIL;
        }
    }
    else
    {
        DRV_ERROR("Memory squeeze when alloc_skb in dropping ncsi_send_packet!!\n");
        return -ENOMEM;
    }

    #undef PAYLOAD_PAD
}

/******************************************************************************
*   FUNCTION        :   __ncsi_send_and_wait
******************************************************************************/
/**
 *  @brief      Send NC-SI command and wait the response.
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int __ncsi_send_and_wait(
                                 /** pointer to NC-SI interface */
                                 sNCSI_IF_Info *ncsi_if,

                                 /** pointer to response socket buffer */
                                 struct sk_buff **res_skb,

                                 /** command number */
                                 u8 cmd,

                                 /** pointer to the payload */
                                 u8 *payload,

                                 /** payload length */
                                 u16 payload_len,

                                 /** retry count */
                                 u32 u32retry,

                                 /** timeout */
                                 u32 u32timeout,

                                 int dis_recovery
                               )
{
    int  res;
    u32  retrycnt = 0;
    u8   read_cnt = 0;
    u8   res_cmd = (cmd | NCSI_RES_PACKET_MASK);
    u32  u32retrytimeout;
    u32  start_time;
    u32  end_time;

    #ifdef DEBUG_SHOW_SEND_NCSI_CONTROL_PACKET
    int tmpi =0;
    char tms_1[256] ;
    char tmps[20] ;
    #endif
    struct sNCSI_HDR *pNCSI_HEADER ;

    start_time = jiffies;

    /* We use mdelay() to implement timeout mechanism. */
    if (down_trylock(&ncsi_control_sem[ncsi_if->ncsi_mii_index]))
    {
        mdelay (u32timeout);

        /* Try to get semaphore again. */
        if (down_trylock(&ncsi_control_sem[ncsi_if->ncsi_mii_index]))
        {
            return STATUS_BUSY;
        }
    }

    spin_lock(&ncsi_if->lock);
    ncsi_control_flag[ncsi_if->ncsi_mii_index] = 1;
    spin_unlock(&ncsi_if->lock);

    /* Using the   private data(u32rcsf defined in sNCSI_IF_Info structure) to replace
       global vriable. The u32rcsf will be protected by semaphore when command transcation. */
    //clear_bit( RCSF_PACKET_RECEIVED , &(ncsi_if->u32rcsf) );

    do
    {
        /* send command */
        res = ncsi_send_packet(ncsi_if, cmd, payload, payload_len);

        DRV_INFO("[%d]End ncsi_send_packet\n", res);

        if (res < 0)
        {
            spin_lock(&ncsi_if->lock);
            ncsi_control_flag[ncsi_if->ncsi_mii_index] = 0;
            spin_unlock(&ncsi_if->lock);
            up(&ncsi_control_sem[ncsi_if->ncsi_mii_index]);
            DRV_INFO("Memory squeeze in ncsi_send_and_wait \n\n");
            /* Memory squeeze, return directly */
            return res;
        }

        if (STATUS_FAIL == res)
        {
            spin_lock(&ncsi_if->lock);
            ncsi_control_flag[ncsi_if->ncsi_mii_index] = 0;
            spin_unlock(&ncsi_if->lock);
            up(&ncsi_control_sem[ncsi_if->ncsi_mii_index]);
            DRV_ERROR("ncsi_send_packet fail!\n");
            return res;
        }

        read_cnt = 0;

        u32retrytimeout = u32timeout;

        do
        {
            #ifdef DEBUG_SHOW_SEND_NCSI_CONTROL_PACKET

            #ifdef DEBUG_NOT_SHOW_GET_LINK_PACKET
            if( CMD_GET_LINK_STATUS != cmd )
            #endif
            {
                memset( tms_1, '\0' , sizeof(tms_1));
                for( tmpi = 0 ; tmpi < payload_len ; tmpi++ )
                {
                    sprintf( tmps ,"%02x,",payload[tmpi] );
                    strcat( tms_1 , tmps );
                }
                printk( KERN_EMERG "(ncsi%d)cmd=(%x) %s  --iid=%d \n",ncsi_if->chnl_id, cmd , tms_1 ,ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)]);
            }
            #endif

            /* Using the   private data(u32rcsf defined in sNCSI_IF_Info structure) to replace
               global vriable. The u32rcsf will be protected by semaphore when command transcation. */
            if( (CMD_ENABLE_CHANNEL == cmd ) || CMD_ENABLE_CHANNEL_NETWORK_TX==cmd )
            {
                set_bit( RCSF_BUSY_WAIT , &(ncsi_if->u32rcsf) );
            }

            /* Wait response, if error it will be re-sended, after that return result */
            res = ncsi_wait_response_packet(ncsi_if, res_skb, res_cmd);
            while (res != NCSI_RES_CODE_CMD_NORESPONSE)
            {
                pNCSI_HEADER = (struct sNCSI_HDR *) ((*res_skb)->data);

                if ((res == NCSI_RECEIVE_RES_PACKET_OK) && (ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)] == pNCSI_HEADER->cmd_iid))
                    break;

                /* not match */
                dev_kfree_skb(*res_skb);
                *res_skb = NULL;
                res = ncsi_wait_response_packet(ncsi_if, res_skb, res_cmd);
            }

            if (res == NCSI_RECEIVE_RES_PACKET_OK)
            {
                pNCSI_HEADER = (struct sNCSI_HDR *) ((*res_skb)->data);

                if ((cmd + 0x80) == pNCSI_HEADER->cmd)
                {
                    if (ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)] == pNCSI_HEADER->cmd_iid)
                    {
                        DRV_INFO("ncsi_wait_response_packet() ok with %s\n\n",ncsi_cmd_str[cmd]);
                    }
                    else
                    {
                        DRV_INFO("res iid incorrect - cmd=(%x)%d , res=(%x)%d\n", cmd, ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)], pNCSI_HEADER->cmd, pNCSI_HEADER->cmd_iid);
                    }
                    break;
                }
                else
                {
                    DRV_ERROR("cmd mismatch - cmd=(%x)%d res=(%x)%d\n", cmd, ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)], pNCSI_HEADER->cmd, pNCSI_HEADER->cmd_iid);
                }
            }

            /* Decrease the timeout value. */
            if (u32retrytimeout > NCSI_ERROR_RETRY_DELAY)
            {
                u32retrytimeout -= NCSI_ERROR_RETRY_DELAY;
            }
            else
            {
                u32retrytimeout = 0;
            }

        } while (u32retrytimeout > 0); /* Continue to wait if not timeout. */

        DRV_INFO("[%d]End ncsi_wait_response_packet\n", res);

        if (likely(NCSI_RECEIVE_RES_PACKET_OK == res))
        {
            DRV_INFO("NCSI_RECEIVE_RES_PACKET_OK\n");
            break;
        }
        else
        {
			if (u8TraceMsgTimeout)
			{
				printk("   %05ld - 123-45-6789 - IID:%02x PkgID:%02x ChID:%02x Cmd:", jiffies & 0xFFFF, ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)], PKG_ID(ncsi_if->chnl_id), INTERNAL_CHNL_ID(ncsi_if->chnl_id));

				if ((cmd >= CMD_CLEAR_INITIAL_STATE) && (cmd <= CMD_GET_NCSI_PASSTHROUGH_STATISTICS))
				{
					printk("%s", ncsi_cmd_str[cmd]);
				}
				else if (cmd == CMD_OEM_COMMAND)
				{
					printk("CMD_OEM");
				}
				else
				{
					printk("CMD_Unknown");
				}

				printk("\n\n");
			}

            if (*res_skb)
            {
                DRV_ERROR("NCSI Free wrong res_skb = 0x%X\n",(unsigned int) *res_skb);
                dev_kfree_skb(*res_skb);
                *res_skb = NULL;
            }

#if 0
            /* increase the IID */
            ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)]++;
            if (0xFF == ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)])
            {
                ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)] = 1;
            }
#endif

            /* faid to get the response packet, do the next retry */
            continue;
        }
    } while (retrycnt++ < u32retry);    /* Retry. */

    /* add the IID */
    ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)]++;
    if (0xFF == ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)])
    {
        ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)] = 1;
    }

    end_time = jiffies;

    if (likely(NCSI_RES_CODE_CMD_NORESPONSE == res))
    {
        if( !dis_recovery )
        {
            ncsi_set_recovery_flag(ncsi_if, 1, __FUNCTION__, __LINE__);
            if (u8TraceRecovery)
                printk("cmd=%d s=%u e=%u r=%u t=%u\n", cmd, start_time, end_time, u32retry, u32timeout);
            DRV_URGENT("%s():ncsi_if->error_recovery_enable_flag \n",__FUNCTION__);
        }
        //printk(KERN_EMERG"(ncsi%d)cmd=(%x)-RecoveryFlag=%d \n",ncsi_if->chnl_id, cmd ,ncsi_if->error_recovery_enable_flag );
    }

    spin_lock(&ncsi_if->lock);
    ncsi_control_flag[ncsi_if->ncsi_mii_index] = 0;
    spin_unlock(&ncsi_if->lock);
    up(&ncsi_control_sem[ncsi_if->ncsi_mii_index]);

    return res;
}

/******************************************************************************
*   FUNCTION        :   ncsi_send_and_wait
******************************************************************************/
/**
 *  @brief      Send NC-SI command and wait the response.
 *
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_send_and_wait(
                                 /** pointer to NC-SI interface */
                                 sNCSI_IF_Info *ncsi_if,

                                 /** pointer to response socket buffer */
                                 struct sk_buff **res_skb,

                                 /** command number */
                                 u8 cmd,
                                 /** pointer to the payload */
                                 u8 *payload,
                                 /** payload length */
                                 u16 payload_len,

                                 int dis_recovery

                             )
{
    return (__ncsi_send_and_wait (ncsi_if,
                                  res_skb,
                                  cmd,
                                  payload,
                                  payload_len,
                                  NCSI_ERROR_RETRY_COUNT,
                                  NCSI_ERROR_RETRY_DELAY,
                                  dis_recovery));
}
/******************************************************************************
*   FUNCTION        :   ncsi_tx_handler
******************************************************************************/
/**
 *  @brief      NCSI channel active handler function.
 *
 *
 *  @return     0: success err: error code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_tx_handler(
                              /** pointer to NC-SI device */
                              struct net_device *dev
                          )
{
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    struct vir_dev_priv *sel_ncsi_priv = NULL ;

    struct net_device *parent_dev = ncsi_priv->parent_dev;

    /** NCSI channel number */
    int ncsi_chnl_no  = ncsi_priv->ncsi_chnl_id;

    /** - Get NC-SI device information to check mode */
    list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
    {
        if (parent_dev == ncsi_priv->parent_dev)
        {
            if (ncsi_priv->sync_tx == 1)
                return 1;

			if (ncsi_control_flag[ncsi_priv->mii_index] == 1)
				return 1;

            /** It's same MII interface */

            if (ncsi_chnl_no == ncsi_priv->ncsi_chnl_id)
            {
                sel_ncsi_priv = ncsi_priv ;
            }
            else
            {
                if (ncsi_priv->active_tx)
                {
                    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
                    if (STATUS_BUSY == ncsi_cmd_disable_channel_network_tx((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , DISABLE_ERROR_RECOVERY))
                    {
                        if (u8TraceTxFun)
                            printk("ncsi_tx_handler - TX disable failed\n");
                        return 1;
                    }
                    else
                    {
                        ncsi_priv->channel_enabled = 0;
                        ncsi_priv->active_tx = 0;
                    }

                    if (u8TraceTxEnable)
                        printk("Tx-10\n");
                }
            }
        }
    }
    /** - Get NC-SI device information to check mode */
    if (sel_ncsi_priv != NULL)
    {
        ncsi_priv = sel_ncsi_priv ;
        if (parent_dev == ncsi_priv->parent_dev)
        {
            if ((!ncsi_priv->active_tx)
                || (u32RecoveryCount != u32RecoveryCount_last)
                || (u8ForceTXEnable != 0)
                || ((u8ScanComplete) && ((ncsi_priv->ncsi_chnl_parameters.bcast_filter_flag & CMD_GET_PARAMETERS_CHANNEL_NETWORK_TX_ENABLED) != CMD_GET_PARAMETERS_CHANNEL_NETWORK_TX_ENABLED)))
            {
                array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
                if (STATUS_BUSY == ncsi_cmd_enable_channel_network_tx((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index],DISABLE_ERROR_RECOVERY))
                {
                    if (u8TraceTxFun)
                        printk("ncsi_tx_handler - TX enable failed\n");
                    return 1;
                }
                else
                {
                    u32RecoveryCount_last = u32RecoveryCount;
                    u8ForceTXEnable = 0;
                    ncsi_priv->active_tx = 1;
					u8ScanComplete = 0;
                }

                if (u8TraceTxEnable)
                    printk("Tx-11 at=%d c=%d lc=%d fte=%d f=%u s=%d\n",
                           ncsi_priv->active_tx,
                           u32RecoveryCount,
                           u32RecoveryCount_last,
                           u8ForceTXEnable,
                           ncsi_priv->ncsi_chnl_parameters.bcast_filter_flag,
                           u8ScanComplete);
            }
        }
    }
    ncsi_priv->channel_enabled++;
    return 0;
}


/******************************************************************************
*   FUNCTION        :   ncsi_rx_handler
******************************************************************************/
/**
 *  @brief      NCSI skb data handler function.
 *
 *
 *  @return     0: success err: error code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
int inline ncsi_rx_handler(
                              /** pointer to the soket buffer. */
                              struct sk_buff *skb
                          )
{
    struct vir_dev_priv *ncsi_priv;
    struct ethhdr *eth = NCSI_ETH_HEADER(skb);
    struct sNCSI_RES_HDR *ncsi_reshdr;
    struct sNCSI_HDR *ncsi_hdr;
    struct sk_buff *newskb;
    unsigned int len = skb->len;
    int nret = NET_RX_DROP ;
    struct bonding *bond;
    struct net_device *upper_dev;

    int mii_id = ncsi_get_mii_index_with_name(skb->dev->name);

    if (!netif_running(skb->dev))
    {
        if (skb != NULL)
        {
            dev_kfree_skb(skb);
            skb = NULL;
        }
        return NET_RX_DROP ;
    }

    /** - To check MII interface index ID */
    if (0xFF != mii_id)
    {
        /** - To check DNIC's interface mode */
        if (!(nic_interface_mode[mii_id] & IF_NCSI))
        {
            /** - It's not NC-SI mode */
            /** - To check DNIC in bonding mode or not */
            if ((skb->dev->flags & IFF_SLAVE))
            {
#if 0
                if (rtnl_trylock())
                {
                    upper_dev = netdev_master_upper_dev_get(skb->dev);

                    /* upper_dev would be NULL if there is no upper device */
                    if (upper_dev)
                    {
                        bond = netdev_priv(upper_dev);

                        if (NULL == bond)
                        {
                            DRV_URGENT("upper_dev bonding is NULL (1)\n");
                            __rtnl_unlock();
                            dev_kfree_skb(skb);
                            return NET_RX_DROP;
                        }

                        if (NULL == bond->curr_active_slave)
                        {
                            DRV_URGENT("upper_dev curr_active_slave is NULL (1)\n");
                            __rtnl_unlock();
                            dev_kfree_skb(skb);
                            return NET_RX_DROP;
                        }

                        if (NULL == bond->curr_active_slave->dev)
                        {
                            DRV_URGENT("upper_dev curr_active_slave->dev is NULL (1)\n");
                            __rtnl_unlock();
                            dev_kfree_skb(skb);
                            return NET_RX_DROP;
                        }
                        else
                        {
                            DRV_MSG("DNIC(%s) bonding mode - primary(%s)\n", skb->dev->name, bond->curr_active_slave->dev->name);
                            skb->dev = bond->curr_active_slave->dev;
                        }
                    }
                    else
                    {
                        bond = NULL;
                        DRV_URGENT("upper_dev is NULL (1) - %s\n", skb->dev->name);
                    }

                    __rtnl_unlock();
                }
                else
                {
                    bond = NULL;
                    DRV_URGENT("fail to grab rtnl lock (1)\n");
                }

                if (bond)
                {
                    DRV_URGENT("bond is NULL (1)\n");
                    dev_kfree_skb(skb);
                    return NET_RX_DROP;

                }
#else
                /* only one dedicated NIC; no need to change the active interface */
                DRV_MSG("DNIC(%s) bonding mode\n", skb->dev->name);
#endif
            }
            else
            {
                DRV_MSG("DNIC(%s) non-bonding mode\n", skb->dev->name);
            }

            skb->protocol = eth_type_trans((struct sk_buff *)skb, skb->dev);
#ifdef CONFIG_ETH_NAPI
            nret = netif_receive_skb(skb);
#else
            nret = netif_rx(skb);
            skb->dev->last_rx = jiffies;
#endif
            ncsi_dev_rx_status_dnic[mii_id][0]++;
            return 0;
        }

        if (IS_NCSI_PACKET(eth->h_proto))
        {
            skb->protocol = eth_type_trans((struct sk_buff *)skb,skb->dev);

            ncsi_reshdr = NCSI_RES_HEADER(skb);
            ncsi_hdr = &(ncsi_reshdr->ncsi_hdr);


            #ifdef DEBUG_SHOW_SEND_NCSI_CONTROL_PACKET

            #ifdef DEBUG_NOT_SHOW_GET_LINK_PACKET
            if( 0x8A != skb->data[4] )
            #endif
            {
                printk(KERN_EMERG "res_CMD(%x) repose=%02x%02x, reason=%02x%02x  iid=(%d) \n ", skb->data[4] ,skb->data[16],skb->data[17],skb->data[18],skb->data[19],skb->data[3] );
            }
            #endif

            if (IS_NCSI_GOOD_PACKET(ncsi_hdr))
            {
                /** - It's good NC-SI control packet */
                if (IS_NCSI_RES_PACKET(ncsi_hdr))
                {
                    DRV_INFO("SUCCESS to receive the response packet!! \n");
                    ncsi_rx_response_packet((sNCSI_IF_Info *) &array_ncsi_if[mii_id], skb);
                    return  NET_RX_DROP;
                }
                else if (IS_NCSI_AEN_PACKET(ncsi_hdr))
                {
                    DRV_INFO("SUCCESS to receive the AEN packet!! \n");
                    ncsi_rx_aen_packet((sNCSI_IF_Info *) &array_ncsi_if[mii_id], skb);
                    return  NET_RX_DROP;
                }
                else
                {
                    if(skb != NULL)
                    {
                        dev_kfree_skb(skb);
                        skb = NULL;
                    }
                    /* drop packet directly */
                    DRV_ERROR("unknown NC-SI packet!\n");
                    return NET_RX_DROP;
                }
            }
            else
            {
                /** - It's bad NC-SI control packet */
                /* drop packet directly */
                if(skb != NULL)
                {
                    dev_kfree_skb(skb);
                    skb = NULL;
                }
                DRV_ERROR("unknown NC-SI packet!\n");
                return NET_RX_DROP;
            }
        }
        else
        {
            if( array_ncsi_if[mii_id].error_recovery_enable_flag == 1 )
            {
                if(skb != NULL)
                {
                    dev_kfree_skb(skb);
                    skb = NULL;
                }
                DRV_ERROR("ncsi_rx_handler() error_recovery_enable_flag == 1!\n");
                return NET_RX_DROP;
            }

            /** - Get NC-SI device information to check mode */
            list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
            {
                if (skb->dev == ncsi_priv->parent_dev)
                {
                    if (ncsi_priv->ncsidev->flags & IFF_SLAVE)
                    {
                        if (netif_running(ncsi_priv->ncsidev))
                        {
                            if (0 == memcmp(ncsi_priv->ncsidev->dev_addr, eth->h_dest, ETH_ALEN))
                            {
#if 0
                                if (rtnl_trylock())
                                {
                                    upper_dev = netdev_master_upper_dev_get(ncsi_priv->ncsidev);

                                    /* upper_dev would be NULL if there is no upper device */
                                    if (upper_dev)
                                    {
                                        bond = netdev_priv(upper_dev);

                                        if (NULL == bond)
                                        {
                                            DRV_URGENT("upper_dev bonding is NULL (2)\n");
                                            __rtnl_unlock();
                                            dev_kfree_skb(skb);
                                            return NET_RX_DROP;
                                        }

                                        if (NULL == bond->curr_active_slave)
                                        {
                                            DRV_URGENT("upper_dev curr_active_slave is NULL (2)\n");
                                            __rtnl_unlock();
                                            dev_kfree_skb(skb);
                                            return NET_RX_DROP;
                                        }

                                        if (NULL == bond->curr_active_slave->dev)
                                        {
                                            DRV_URGENT("upper_dev curr_active_slave->dev is NULL (2)\n");
                                            __rtnl_unlock();
                                            dev_kfree_skb(skb);
                                            return NET_RX_DROP;
                                        }
                                        else
                                        {
                                            skb->dev = bond->curr_active_slave->dev;
                                        }
                                    }
                                    else
                                    {
                                        bond = NULL;
                                        DRV_URGENT("upper_dev is NULL (2) - %s\n", skb->dev->name);
                                    }

                                    __rtnl_unlock();
                                }
                                else
                                {
                                    DRV_URGENT("fail to grab rtnl lock (2)\n");
                                    bond = NULL;
                                }

                                if (NULL == bond)
                                {
                                    if (ncsi_priv->active_tx == 1)
                                    {
                                        DRV_URGENT("use active_tx flag (2) - %s\n", ncsi_priv->ncsidev->name);
                                        skb->dev = ncsi_priv->ncsidev;
                                    }
                                    else
                                    {
                                        DRV_URGENT("check for the next network device (2)\n");
                                        continue;
                                    }
                                }
#else
                                if (active_slave_dev == NULL)
                                {
                                    if (ncsi_priv->active_tx == 1)
                                    {
                                        DRV_URGENT("use active_tx flag (2) - %s\n", ncsi_priv->ncsidev->name);
                                        skb->dev = ncsi_priv->ncsidev;
                                    }
                                    else
                                    {
                                        DRV_URGENT("check for the next network device (2)\n");
                                        continue;
                                    }
                                }
                                else
                                {
                                    skb->dev = active_slave_dev;
                                    DRV_URGENT("use active_slave_dev %s (2)\n", skb->dev->name);
                                }
#endif

                                skb->protocol = eth_type_trans((struct sk_buff *)skb, skb->dev);
                                skb->dev->stats.rx_packets++;
                                skb->dev->stats.rx_bytes += len;
#ifdef CONFIG_ETH_NAPI
                                netif_receive_skb(skb);
#else
                                netif_rx(skb);
                                skb->dev->last_rx = jiffies;
#endif
                                ncsi_dev_rx_status[ncsi_priv->dev_index][0]++;
                                DRV_INFO("ncsi_rx_handler: bonding mode pass packet to [%s]\n",ncsi_priv->ncsidev->name);
                                return  NET_RX_DROP;
                            }
                        }
                    }
                }
            }

            /** - Get NC-SI device information to check MAC address */
            list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
            {
                if (0 == memcmp(ncsi_priv->ncsidev->dev_addr, eth->h_dest, ETH_ALEN))
                {
                    if (netif_running(ncsi_priv->ncsidev))
                    {
                        /** - The MAC address is match. */
                        skb->dev = ncsi_priv->ncsidev;
                        skb->protocol = eth_type_trans((struct sk_buff *)skb,skb->dev);
                        skb->dev->stats.rx_packets++;
                        skb->dev->stats.rx_bytes += len;

#ifdef CONFIG_ETH_NAPI
                        netif_receive_skb(skb);
#else
                        netif_rx(skb);
                        skb->dev->last_rx = jiffies;
#endif

                        ncsi_dev_rx_status[ncsi_priv->dev_index][1]++;
                        DRV_INFO("ncsi_rx_handler: mac address is match with [%s]\n",ncsi_priv->ncsidev->name);
                        DRV_INFO("ncsi_rx_handler() [0x%04x][%s]\n",htons(eth->h_proto),ncsi_print_ethaddr(eth->h_dest));
                        DRV_INFO("                          [%s]\n",ncsi_print_ethaddr(ncsi_priv->ncsidev->dev_addr));
                        return  NET_RX_DROP;;
                    }
                }
            }
            aess_wdt_keepalive(NULL);

            /** - Get NC-SI device information to check MAC address */
            list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
            {
                if (netif_running(ncsi_priv->ncsidev))
                {
                    newskb = skb_clone(skb, GFP_ATOMIC);
                    newskb->dev = ncsi_priv->ncsidev;
                    newskb->protocol = eth_type_trans((struct sk_buff *)newskb,newskb->dev);
                    newskb->dev->stats.rx_packets++;
                    newskb->dev->stats.rx_bytes += len;

#ifdef CONFIG_ETH_NAPI
                    netif_receive_skb(newskb);
#else
                    netif_rx(newskb);
                    newskb->dev->last_rx = jiffies;
#endif

                    ncsi_dev_rx_status[ncsi_priv->dev_index][2]++;
                    DRV_INFO("ncsi_rx_handler() [0x%04x][%s]\n",htons(eth->h_proto),ncsi_print_ethaddr(eth->h_dest));
                    DRV_INFO("ncsi_rx_handler: broadcast packet to [%s]\n",ncsi_priv->ncsidev->name);
                }
            }

            if(skb != NULL)
            {
               dev_kfree_skb(skb);
               skb = NULL;
            }
            DRV_INFO("ncsi_rx_handler: MAC address is no match, broadcast packet![0x%04x][%s]\n",htons(eth->h_proto),ncsi_print_ethaddr(eth->h_dest));
        }
    }
    else
    {
        /** - The NIC name is not in MII interface information array. */
        skb->protocol = eth_type_trans((struct sk_buff *)skb, skb->dev);
#ifdef CONFIG_ETH_NAPI
        netif_receive_skb(skb);
#else
        netif_rx(skb);
        skb->dev->last_rx = jiffies;
#endif
        ncsi_dev_rx_status_error++;
        DRV_ERROR("unknown packet from %s!\n", skb->dev->name);
        return 0;
    }

    if(skb != NULL)
    {
        dev_kfree_skb(skb);
        skb = NULL;
    }
    return  NET_RX_DROP;;
}

/******************************************************************************
*   FUNCTION        :   ncsi_open
******************************************************************************/
/**
 *  @brief      NC-SI open function.
 *
 *
 *  @return     0: success err: error code.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_open(
                        /** pointer to NC-SI interface */
                        struct net_device *dev
                    )
{
    int err = 0;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    //struct vir_dev_priv *ncsi_priv_other = NULL;

    if (u8TraceTxEnable)
        printk("O-0\n");

    if (NOT_SUPPORT_HW_ARBITRATION_MODE==array_ncsi_if[ncsi_priv->mii_index].arbitration_mode)
        return -EIO ;

    ncsi_priv->sync_tx = 1;
    ncsi_priv->active_tx = 0;

    array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;

    //if (total_package_no != 1)
    {
        array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id | 0x1f;
        array_ncsi_if[ncsi_priv->mii_index].ncsi_hardware_arb = CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_ENABLE;
        err = ncsi_cmd_select_package((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , 0);
        if(err != 0)
        {
            DRV_ERROR("ncsi_open(): %s ncsi_cmd_select_package() Fail!!\n", dev->name);

            if (u8TraceTxEnable)
                printk("O-1\n");

            ncsi_priv->sync_tx = 0;
            return err;
        }
    }

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;


#if 0
    err = ncsi_cmd_reset_channel((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , 0 );

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_clear_initial_state((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , 0 );
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_clear_initial_state() Fail!!\n", dev->name);
        ncsi_priv->sync_tx = 0;
        return err;
    }
#endif

    err = ncsi_cmd_get_version_id((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_get_version_id() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-2\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }
    memcpy( &(ncsi_priv->ncsi_version) ,&(array_ncsi_if[ncsi_priv->mii_index].ncsi_version), sizeof(array_ncsi_if[ncsi_priv->mii_index].ncsi_version) );
    //printk("(%s)(pci_id=0x%x)(iana=0x%x)",dev->name,ncsi_priv->ncsi_version.pci_vid,ncsi_priv->ncsi_version.iana );
    //DRV_INFO("(%s)(pci_id=0x%x)(iana=0x%x)",dev->name,ncsi_priv->ncsi_version.pci_vid,ncsi_priv->ncsi_version.iana );

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_get_capabilities((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , 0);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_get_capabilities() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-3\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }

    memcpy((u8 *)array_ncsi_if[ncsi_priv->mii_index].ncsi_mac_info.mac_addr, dev->dev_addr, 6);
    DRV_INFO("ncsi_open() set mac address [%s]\n",ncsi_print_ethaddr((unsigned char *)array_ncsi_if[ncsi_priv->mii_index].ncsi_mac_info.mac_addr));
    array_ncsi_if[ncsi_priv->mii_index].ncsi_mac_info.mac_num = 1;
    array_ncsi_if[ncsi_priv->mii_index].ncsi_mac_info.addr_type_enable = CMD_SET_MAC_ADDRESS_PAYLOAD_ADDR_TYPE_UNICAST | CMD_SET_MAC_ADDRESS_PAYLOAD_ENABLE_MAC_ADDR_FILTER;
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_set_mac_address((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_set_mac_address(%s) Fail!!\n", dev->name, ncsi_print_ethaddr(dev->dev_addr));

        if (u8TraceTxEnable)
            printk("O-4\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }

#if 1
    array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.bcast_filter_flag = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.bcast_filter_flag;
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_enable_broadcast_filtering((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_enable_broadcast_filtering() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-5\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }
#else
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_disable_broadcast_filtering((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_enable_broadcast_filtering() Fail!!\n", dev->name);
        ncsi_priv->sync_tx = 0;
        return err;
    }
#endif

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_enable_vlan(dev);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_enable_vlan() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-6\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }

#if 1
//    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
//    if (ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.mcast_filter_flag != 7)
//    {
//        printk("YOUR NCSI NIC DOES NOT SUPPORT THE MULTICAST FILTER FUNCTION, WE WILL DISABLE MULTICAST FILTER FUNCTION IN THIS PLATFORM\n");
//        err = ncsi_cmd_disable_global_multicast_filtering((sNCSI_IF_Info *) &ncsi_if[ncsi_priv->mii_index]);
//        if(err != 0)
//        {
//            DRV_ERROR("ncsi_open(): %s ncsi_cmd_disable_global_multicast_filtering() Fail!!\n", dev->name);
//            ncsi_priv->sync_tx = 0;
//            return err;
//        }
//    }
//    else
//    {
//  	  if( ncsi_if[ncsi_priv->mii_index].error_recovery_enable_flag != 1 )
//    	  {
//            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
//            err = ncsi_cmd_enable_global_multicast_filtering((sNCSI_IF_Info *) &ncsi_if[ncsi_priv->mii_index]);
//            if(err != 0)
//            {
//                DRV_ERROR("ncsi_open(): %s ncsi_cmd_enable_global_multicast_filtering() Fail!!\n", dev->name);
//                ncsi_priv->sync_tx = 0;
//                return err;
//            }
//        }
//    }
#else
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_disable_global_multicast_filtering((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_disable_global_multicast_filtering() Fail!!\n", dev->name);
        ncsi_priv->sync_tx = 0;
        return err;
    }
#endif

    /* NCSI AEN control */
    if (array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.aen_ctrl != 0)
    {
        array_ncsi_if[ncsi_priv->mii_index].ncsi_aen_ctrl = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.aen_ctrl;
        /* Enable AEN */
        array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
        err = ncsi_cmd_aen_enable((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
        if(err != 0)
        {
            DRV_ERROR("ncsi_open(): %s ncsi_cmd_aen_enable() Fail!!\n", dev->name);

            if (u8TraceTxEnable)
                printk("O-7\n");

            ncsi_priv->sync_tx = 0;
            return err;
        }
    }

#if 1
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_enable_channel((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_enable_channel() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-8\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }
    ncsi_priv->active = 1 ;
    mdelay(50);
#endif

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].query_chnl_id = array_ncsi_if[ncsi_priv->mii_index].chnl_id;
    err = ncsi_cmd_get_link_status(dev);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_get_link_status() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-9\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }

#if 0
    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_enable_channel_network_tx((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index], ENABLE_ERROR_RECOVERY );
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_enable_channel_network_tx() Fail!!\n", dev->name);
        ncsi_priv->sync_tx = 0;
        return err;
    }
    ncsi_priv->active_tx = 1;
#endif

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_disable_channel_network_tx((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index], ENABLE_ERROR_RECOVERY );
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_disable_channel_network_tx() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-10\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }
    ncsi_priv->active_tx = 0;
    u8ScanComplete = 0;

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    err = ncsi_cmd_get_parameters((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
    if (err != 0)
    {
        DRV_ERROR("ncsi_open(): %s ncsi_cmd_get_parameters() Fail!!\n", dev->name);

        if (u8TraceTxEnable)
            printk("O-11\n");

        ncsi_priv->sync_tx = 0;
        return err;
    }
    else
    {
        /* store flag for ncsi_tx_handler() */
        ncsi_priv->ncsi_chnl_parameters.bcast_filter_flag = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.bcast_filter_flag;
    }

    ncsi_priv->sync_tx = 0;

    /* Reset error recovery counter. */
    array_ncsi_if[ncsi_priv->mii_index].error_recovery_counter = 0;

    if (u8TraceTxEnable)
        printk("O-12\n");

    return 0;
}

#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)

#else
/******************************************************************************
*   FUNCTION        :   ncsi_close
******************************************************************************/
/**
 *  @brief      NCSI close function
 *
 *
 *  @return     0: Success err: Error code
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_close(
                         /** pointer to ncsi interface */
                        struct net_device *dev
                     )
{

    int err = 0;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);


    ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;

    ncsi_priv->active_tx = 0;

    err = ncsi_cmd_disable_channel_network_tx((sNCSI_IF_Info *) &ncsi_if[ncsi_priv->mii_index]);
    if( err )
    {
        DRV_INFO("ncsi_tx_handler():ncsi_cmd_disable_channel_network_tx(%d) Fail!!\n",(int) ncsi_priv->ncsi_chnl_id);
        return 1 ;
    }
    skb_queue_purge(&ncsi_if->ncsi_res_quene.ncsi_queue);


    return err;
}
#endif
/******************************************************************************
*   FUNCTION        :   ncsi_init
******************************************************************************/
/**
 *  @brief      NC-SI init function.
 *
 *
 *  @return     None.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: void function\n
 *
 *****************************************************************************/
static void ncsi_init(
                         /** pointer to NC-SI interface */
                         sNCSI_IF_Info *ncsi_if
                     )
{
    DRV_INFO("Enter ncsi_init HZ = %d\n", HZ);

#if 0
    /* add the IID */
    ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)]++;
    ncsi_if->cmd_iid[1] = 10 ;
    ncsi_if->cmd_iid[2] = 20 ;
    ncsi_if->cmd_iid[3] = 40 ;
    ncsi_if->cmd_iid[4] = 80 ;
#endif
    if (0xFF == ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)])
    {
        ncsi_if->cmd_iid[PKG_ID(ncsi_if->chnl_id)] = 1;

    }

    sema_init(&ncsi_if->ncsi_res_quene.sem, 1);
    skb_queue_head_init(&ncsi_if->ncsi_res_quene.ncsi_queue);
    init_waitqueue_head(&ncsi_if->ncsi_res_quene.inq);

    init_completion (&ncsi_if->cmd_exited);
}

/******************************************************************************
*   FUNCTION        :   set_ncsiinfo_value
******************************************************************************/
/**
 *  @brief      Used by sysfs to set NC-SI device information value.
 *
 *  @return     0
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private inline function\n
 *
 *****************************************************************************/
static ssize_t set_ncsiinfo_value(
                                     /** Pointer of NC-SI devic driver*/
                                     struct device_driver *drv,

                                     /** String that want to be set. */
                                     const char *buf,

                                     /** Count size.*/
                                     size_t count
                                 )
{
    /** @scope */
    /** - Nothing to do, return 0.*/
    return (0);
}

/******************************************************************************
*   FUNCTION        :   set_ncsi_device_number_value
******************************************************************************/
/**
 *  @brief      Used by sysfs to set value.
 *
 *  @return     0.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: Private inline function\n
 *
 *****************************************************************************/
static ssize_t set_ncsi_device_number_value(
                                               /** Pointer of NC-SI device driver. */
                                               struct device_driver *drv,

                                               /** String that want to be set. */
                                               const char *buf,

                                               /** Count size.*/
                                               size_t count
                                            )
{
    /** @scope */
    /** - Nothing to do, return 0.*/
    return (0);
}

/******************************************************************************
*   FUNCTION        :   ncsi_print_ethaddr
******************************************************************************/
/**
 *  @brief      To get printable MAC address string.
 *
 *  @return     Printable MAC address string.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: Private inline function\n
 *
 *****************************************************************************/
static const char * ncsi_print_ethaddr(
                                          /** Net device's MAC address */
                                          const unsigned char *dev_addr
                                      )
{
    static char buf[18];

    sprintf(buf, "%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X",
    dev_addr[0], dev_addr[1], dev_addr[2], dev_addr[3], dev_addr[4], dev_addr[5]);

    return buf;
}


/******************************************************************************
*   FUNCTION        :   get_ncsi_info
******************************************************************************/
/**
 *  @brief      Get formatted string of NC-SI device information.
 *
 *  @return     Pointer point to the status string.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: Private inline function\n
 *
 *****************************************************************************/
char *get_ncsi_info(
                       /** Buffer to put status string. */
                       char *buffer
                   )
{
    int mii_index = 0;
    int pkg_index = 0;
    int ncsi_index = 0;
    int ncsi_count = 0;
    char *p;
    struct vir_dev_priv *ncsi_priv;

    p = buffer;

    /** - Print 1st line: header. */
    p += sprintf(p, "ID  PID CID NAME   PNAME  MAC Address        A TA RU CR NQ\n");
    p += sprintf(p, "==========================================================\n");
    /** - Print Other line: NCSI Infor. */

    list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
    {
        p += sprintf(p, "%03d %03d %03d %-6s %-6s %s  %d  %d  %d  %d  %d\n",
        (int)ncsi_priv->ncsi_chnl_id,
        (int)PKG_ID(ncsi_priv->ncsi_chnl_id),
        (int)INTERNAL_CHNL_ID(ncsi_priv->ncsi_chnl_id),
        ncsi_priv->ncsidev->name,
        ncsi_priv->parent_dev->name,
        ncsi_print_ethaddr(ncsi_priv->ncsidev->dev_addr),
        (int)ncsi_priv->active,
        (int)ncsi_priv->active_tx,
        (int)netif_running(ncsi_priv->ncsidev),
        (int)netif_carrier_ok(ncsi_priv->ncsidev),
        (int)netif_queue_stopped(ncsi_priv->ncsidev));
      }
    p += sprintf(p, "==========================================================\n");
    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        if (NULL != array_ncsi_if[mii_index].dev)
        {
            p += sprintf(p, "--- --- --- %-6s ------ 00:00:00:00:00:00  -  -  %d  %d  %d\n",
            array_ncsi_if[mii_index].dev->name,
            (int)netif_running(array_ncsi_if[mii_index].dev),
            (int)netif_carrier_ok(array_ncsi_if[mii_index].dev),
            (int)netif_queue_stopped(array_ncsi_if[mii_index].dev));
        }
    }
    p += sprintf(p, "==========================================================\n");
    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "%-11s  Packge0   Packge1   Packge2   Packge3\n",mii_if[mii_index].name);
        p += sprintf(p, "TX_Disabled");
        for (pkg_index = 0; pkg_index < CONFIG_NCSI_MAX_PACKAGE_NUMBER; pkg_index++)
        {
            p += sprintf(p, "[%08d]",ncsi_dev_tx_count[mii_index][pkg_index][0]);
        }
        p += sprintf(p, "\nTX_Enabled ");
        for (pkg_index = 0; pkg_index < CONFIG_NCSI_MAX_PACKAGE_NUMBER; pkg_index++)
        {
            p += sprintf(p, "[%08d]",ncsi_dev_tx_count[mii_index][pkg_index][1]);
        }
        p += sprintf(p, "\n");
    }
    p += sprintf(p, "==========================================================\n");

    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "TYPE");
        for (ncsi_index = ncsi_count; ncsi_index < (CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER); ncsi_index++)
        {
            p += sprintf(p, "   ncsi%02d ",ncsi_index+(mii_index*(CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER)));
        }
        p += sprintf(p, "\nRX_A");
        for (ncsi_index = ncsi_count; ncsi_index < (CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER); ncsi_index++)
        {
            p += sprintf(p, "[%08d]",ncsi_dev_rx_status[ncsi_index+(mii_index*(CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER))][0]);
        }
        p += sprintf(p, "\nRX_B");
        for (ncsi_index = ncsi_count; ncsi_index < (CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER); ncsi_index++)
        {
            p += sprintf(p, "[%08d]",ncsi_dev_rx_status[ncsi_index+(mii_index*(CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER))][1]);
        }
        p += sprintf(p, "\nRX_C");
        for (ncsi_index = ncsi_count; ncsi_index < (CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER); ncsi_index++)
        {
            p += sprintf(p, "[%08d]",ncsi_dev_rx_status[ncsi_index+(mii_index*(CONFIG_NCSI_MAX_PACKAGE_NUMBER*CONFIG_NCSI_MAX_CHANNEL_NUMBER))][2]);
        }
        p += sprintf(p, "\n");
    }
    p += sprintf(p, "==========================================================\n");
    p += sprintf(p, "TYPE    ");
    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "     %-4s ",mii_if[mii_index].name);
    }
    p += sprintf(p, "\n");
    p += sprintf(p, "RX_TYPE1");
    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "[%08d]",ncsi_dev_rx_status_dnic[mii_index][0]);
    }
    p += sprintf(p, "\nRX_TYPE2");
       for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "[%08d]",ncsi_dev_rx_status_dnic[mii_index][1]);
    }
    p += sprintf(p, "\nRX_TYPE3");
    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "[%08d]",ncsi_dev_rx_status_dnic[mii_index][2]);
    }
    p += sprintf(p, "\nERR_RC ");
    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "[%08d]",array_ncsi_if[mii_index].error_recovery_enable_flag);
    }
    p += sprintf(p, "\n==========================================================\n");
    p += sprintf(p, "RX_TYPE_ERROR[%08d]", ncsi_dev_rx_status_error);
    p += sprintf(p, "\n==========================================================\n");
    return buffer;
}

/******************************************************************************
*   FUNCTION        :   show_ncsiinfo_value
******************************************************************************/
/**
 *  @brief      Used by sysfs to show ncsi device info.
 *
 *  @return     String buffer that indicates ncsi device info.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: Private inline function\n
 *
 *****************************************************************************/
static ssize_t show_ncsiinfo_value(
                                      /** Pointer of device_driver. */
                                      struct device_driver *drv,

                                      /** Returned buffer.*/
                                      char *buf
                                  )
{
    /* The all information needs only about 2500 bytes */
    int rc = 0 ;
    char *buffer = NULL ;
    buffer =(char* )kmalloc(2500,GFP_KERNEL);
    if( NULL == buffer )
    {
        DRV_ERROR("kmalloc() fail!");
        return 0;
    }
    rc =  sprintf(buf, "%s\n", get_ncsi_info(buffer));
    kfree( buffer );
    return rc;

}

/******************************************************************************
*   FUNCTION        :   get_ncsi_device_number
******************************************************************************/
/**
 *  @brief      Get formatted string of NC-SI device infor.
 *
 *  @return     Pointer point to the status string.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: Private inline function\n
 *
 *****************************************************************************/
char *get_ncsi_device_number(
                                /** Buffer to put status string. */
                                char *buffer
                            )
{
    int mii_index = 0;
    char *p;

    /** @scope */
    p = buffer;

    p += sprintf(p, "INIT PKG CH HW\n");
    p += sprintf(p, "==============\n");

    /** - Print line: NCSI device number . */
    for (mii_index = 0; mii_index < NCSI_MAX_MII_NUM; mii_index++)
    {
        p += sprintf(p, "%-4d %-3d %-2d %-2d\n",
                     ncsi_init_flag[mii_index],
                     total_package_no[mii_index],
                     mii_if[mii_index].mii_ncsi_dev_count,
                     array_ncsi_if[mii_index].arbitration_mode);
    }

    return buffer;
}

/******************************************************************************
*   FUNCTION        :   show_ncsi_device_number_value
******************************************************************************/
/**
 *  @brief      Used by sysfs to show ncsi device info.
 *
 *  @return     String buffer that indicates NC-SI device info.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: Private inline function\n
 *
 *****************************************************************************/
static ssize_t show_ncsi_device_number_value(
                                                /** Pointer of device_driver. */
                                                struct device_driver *drv,

                                                /** Returned buffer.*/
                                                char *buf
                                            )
{
    char buffer[128];

    return sprintf(buf, "%s\n", get_ncsi_device_number(buffer));
}

/*
struct device_driver ncsi_device_driver =
{
    .name = "ncsi_protocol",
    .bus = &platform_bus_type,
    .owner = THIS_MODULE
};
*/

static struct platform_driver ncsi_device_driver = {
	.driver = {
	 .name	 = "ncsi_protocol",
	 .owner	 = THIS_MODULE,
	}
};

struct driver_attribute drv_attr_ncsi_device_info = {
    .attr = {.name  = "ncsi_device_info" ,
             .mode   = S_IWUSR | S_IRUGO },
    .show   = show_ncsiinfo_value,
    .store  = set_ncsiinfo_value,
};

struct driver_attribute drv_attr_ncsi_device_number = {
    .attr = {.name  = "ncsi_device_number" ,
             .mode   = S_IWUSR | S_IRUGO },
    .show   = show_ncsi_device_number_value,
    .store  = set_ncsi_device_number_value,
};
/**< Sysfs driver attribute structure for ncsi driver. */

/******************************************************************************
*   FUNCTION        :   ncsi_error_recovery
******************************************************************************/
/**
 *  @brief      Handling NC-SI error recovery.
 *
 *
 *  @return     0: Success -1:Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
int ncsi_error_recovery(
                           /** pointer to NC-SI interface. */
                           sNCSI_IF_Info *ncsi_if
                       )
{
    struct vir_dev_priv *ncsi_priv;
    int package_id = 0xff ;
    int err;

    u32RecoveryCountPre++;

    DRV_INFO("NCSI Error Recovery !!\n");
    list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
    {
        if (ncsi_if->ncsi_mii_index == ncsi_priv->mii_index)
        {
            if (!netif_running(ncsi_priv->parent_dev))
            {
                DRV_ERROR("NCSI Error Recovery Fail with %s not running!\n",ncsi_priv->ncsidev->name);
                return -1;
            }
            netif_stop_queue(ncsi_priv->ncsidev);
            netif_tx_lock(ncsi_priv->ncsidev);

#if 1
            if( package_id != (ncsi_priv->ncsi_chnl_id | 0x1f) )
            {
                package_id = (ncsi_priv->ncsi_chnl_id | 0x1f);

                array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
                array_ncsi_if[ncsi_priv->mii_index].chnl_id = package_id;
                array_ncsi_if[ncsi_if->ncsi_mii_index].ncsi_hardware_arb = CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_ENABLE  ;
                //printk(KERN_EMERG"Recover array_ncsi_if[ncsi_if->ncsi_mii_index].chnl_id(%d) !!\n", array_ncsi_if[ncsi_if->ncsi_mii_index].chnl_id);
                err = ncsi_cmd_select_package((sNCSI_IF_Info *) &array_ncsi_if[ncsi_if->ncsi_mii_index] , 0 );
                if (err ==NCSI_RES_CODE_CMD_NORESPONSE)
                {
                    //ncsi_if->error_recovery_enable_flag = 1;
                    //netif_tx_unlock(ncsi_priv->ncsidev);
                    DRV_ERROR("Recover fail! select package(%x) !!\n", package_id);
                    //DRV_INFO("ncsi_detect_device():ncsi_cmd_deselect_package(%d) not found!!\n", package_id);
                    //return -1;
                }

            }

#endif


#if 0 // Do not send reset channel command.
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            err = ncsi_cmd_reset_channel((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index],1 );
            if(err == NCSI_RES_CODE_CMD_NORESPONSE)
            {
                DRV_ERROR("Recovery Fail ncsi_cmd_reset_channel() with %s!\n",ncsi_priv->ncsidev->name);
                //ncsi_if->error_recovery_enable_flag = 1;
                //netif_tx_unlock(ncsi_priv->ncsidev);
                //return -1;
            }
#endif

#if 1
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            err = ncsi_cmd_clear_initial_state((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , 0 );

            if(err == NCSI_RES_CODE_CMD_NORESPONSE)
            {
                DRV_ERROR("Recovery Fail ncsi_cmd_clear_initial_state() with %s!\n",ncsi_priv->ncsidev->name);
                //ncsi_if->error_recovery_enable_flag = 1;
                //netif_tx_unlock(ncsi_priv->ncsidev);
                //return -1;
            }
#endif
            if(ncsi_priv->active)
            {
                //printk("\n NCSI(%s) Error Recovery",ncsi_priv->ncsidev->name);
                if(0 != ncsi_open(ncsi_priv->ncsidev))
                {
                    DRV_ERROR("NCSI Error Recovery Fail at ncsi_open() with %s!\n",ncsi_priv->ncsidev->name);
                    ncsi_set_recovery_flag(ncsi_if, 1, __FUNCTION__, __LINE__);
                    netif_tx_unlock(ncsi_priv->ncsidev);
                    return -1;
                }
                else
                {
                    netif_wake_queue(ncsi_priv->ncsidev);
                    ncsi_priv->channel_enabled = 0 ;
                    DRV_INFO("NCSI Error Recovery OK with %s!\n",ncsi_priv->ncsidev->name);
                }
            }
            else
            {
                array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
                err = ncsi_cmd_disable_channel_network_tx((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index], ENABLE_ERROR_RECOVERY );
                if (err != 0)
                {
                    DRV_ERROR("ncsi_error_recovery(): %s ncsi_cmd_disable_channel_network_tx() Fail!!\n", ncsi_priv->ncsidev->name);
					netif_tx_unlock(ncsi_priv->ncsidev);
                    return -1;
                }
                ncsi_priv->active_tx = 0;

                DRV_INFO("NCSI Error Recovery %s is not active!\n",ncsi_priv->ncsidev->name);
            }
            ncsi_set_recovery_flag(ncsi_if, 0, __FUNCTION__, __LINE__);
            netif_tx_unlock(ncsi_priv->ncsidev);
            //netif_wake_queue(ncsi_priv->ncsidev);
        }
    }

    u32RecoveryCount++;
	if (u8TraceRecovery)
		printk("NCSI Error Recovery u32RecoveryCount=%x\n", u32RecoveryCount);

    return 0;
}


/******************************************************************************
*   FUNCTION        :   ncsi_get_link_status_polling
******************************************************************************/
/**
 *  @brief      Polling link status of NC-SI device.
 *
 *
 *  @return     0: Success -1:Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
int ncsi_get_link_status_polling(
                                    /** pointer to NC-SI interface */
                                    struct net_device *dev
                                )
{
    int err = 0;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    int mii_id = ncsi_get_mii_index_with_name(ncsi_priv->parent_dev->name);

    if ( (0xFF != mii_id) && (ncsi_init_flag[mii_id] == 1) && (ncsi_priv->active == 1))
    {
        if (netif_running(ncsi_priv->parent_dev))
        {
            DRV_INFO("ncsi_get_link_status_polling: the query device is %s \n",dev->name );
            err = ncsi_cmd_get_link_status(dev);
            if (NCSI_NEED_REINIT == err)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
            }
        }
        else
        {
            DRV_INFO("ncsi_get_link_status_polling: %s need re-open!\n",dev->name );
        }
    }
    else
    {
        DRV_INFO("ncsi_get_link_status_polling: %s is not active!\n",dev->name );
    }

    return 0;
}

/******************************************************************************
 *   FUNCTION        :   ncsi_get_parameters_polling
******************************************************************************/
/**
 *  @brief      Polling get parameters of NC-SI device.
 *
 *
 *  @return     0: Success -1:Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
int ncsi_get_parameters_polling(
                                 /** pointer to NC-SI interface */
                                 struct net_device *dev
                               )
{
    int err = 0;
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    int mii_id = ncsi_get_mii_index_with_name(ncsi_priv->parent_dev->name);

    if ((0xFF != mii_id) && (ncsi_init_flag[mii_id] == 1) && (ncsi_priv->active == 1))
    {
        if (netif_running(ncsi_priv->parent_dev))
        {
            DRV_INFO( "%s: the query device is %s \n", __func__, dev->name);
            err = ncsi_cmd_get_parameters((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
            if (NCSI_RECEIVE_RES_PACKET_OK == err)
            {
                /* store flag for ncsi_tx_handler() */
                ncsi_priv->ncsi_chnl_parameters.bcast_filter_flag = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_parameters.bcast_filter_flag;
				u8ScanComplete = 1;
            }
            else if (NCSI_NEED_REINIT == err)
            {
                ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
            }
        }
        else
        {
            DRV_INFO("%s: %s need re-open!\n", __func__, dev->name);
        }
    }
    else
    {
        DRV_INFO("%s: %s is not active!\n", __func__, dev->name);
    }

    return 0;
}

/******************************************************************************
*   FUNCTION        :   ncsi_command_handler
******************************************************************************/
/**
 *  @brief      Handle the switch channel and error recovery.
 *
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
static int ncsi_command_handler(
                                   /** unused pointer. */
                                   void *arg
                               )
{
#define RETRY_INDEFINITELY
#ifdef RETRY_INDEFINITELY
		int retry = 0, interval = 0;
#endif
    sNCSI_IF_Info *ncsi_if = arg;
    struct vir_dev_priv *ncsi_priv = NULL;

    int get_parameter_polling_interval = 0;

#if NCSI_TX_ENABLE_WORKAROUND
    int tx_enable_counter = 0;
#endif

#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)
    static wait_queue_head_t wq;

    int timeout = 1 ;
    init_waitqueue_head(&wq);
    allow_signal(SIGKILL);
#endif
    allow_signal(SIGTERM);

    while (ncsi_if->cmd_exit_flag != 1 )
    {
#if NCSI_TX_ENABLE_WORKAROUND
        tx_enable_counter++;
        if (tx_enable_counter >= NCSI_TX_ENABLE_RETRIGGER_INTERVAL)
        {
            u8ForceTXEnable = 1;
            tx_enable_counter = 0;
        }
#endif

        DRV_INFO("Enter ncsi_command_handler! %d %d\n", ncsi_control_flag[ncsi_if->ncsi_mii_index], ncsi_if->NCSI_transition_flag);

#ifdef RETRY_INDEFINITELY
        if (ncsi_if->error_recovery_counter >= MAX_NCSI_ERR_RECOVERY_TIMES)
        {
            if(interval >= 64)
            {
                interval = 64;
            }
            else
            {
                interval = interval<<1;
            }
        }
        else
        {
            interval = 1;
        }

        for(retry = interval; retry > 0; retry--)
#endif
        {
#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)
        /**Let condition always be false, kernel thread will be waked up when timeout*/
        timeout = wait_event_interruptible_timeout(wq, 0, HZ );

        /* if wait was interrupted by SIGTERM, end thread */
            if (timeout == -ERESTARTSYS)
            {
                ncsi_init_flag[ncsi_if->ncsi_mii_index] = 0;
                complete_and_exit(&ncsi_if->cmd_exited, 0);
                return 0;
            }
#else
            mdelay(500);
#endif
        }
        if (NULL == ncsi_if->dev)
        {
            DRV_INFO("ncsi_command_handler() NULL == ncsi_if->dev\n");
            continue;
        }
        if (ncsi_control_flag[ncsi_if->ncsi_mii_index] == 1)
        {
            DRV_INFO("ncsi_command_handler() ncsi_control_flag[%d] == 1\n",ncsi_if->ncsi_mii_index);
            continue;
        }
        if(ncsi_if->NCSI_transition_flag != 1)
        {
            ncsi_if->NCSI_transition_flag = 1;

            if(ncsi_if->error_recovery_enable_flag == 1)
            {
                DRV_INFO("ncsi_command_handler() ncsi_error_recovery()\n");
                if(0 != ncsi_error_recovery(ncsi_if))
                {
                    ncsi_if->error_recovery_counter++;
                    DRV_URGENT("%s():error counter=%d\n",__FUNCTION__,ncsi_if->error_recovery_counter);
#ifndef RETRY_INDEFINITELY
                    if (ncsi_if->error_recovery_counter > MAX_NCSI_ERR_RECOVERY_TIMES)
                    {
                        DRV_URGENT("%s():close dev\n",__FUNCTION__);
                        ncsi_virtual_close(ncsi_if->dev);
                    }
#endif
                    DRV_INFO("ncsi_command_handler() ncsi_error_recovery() Fail\n");
                }
                else
                {
                    ncsi_if->error_recovery_counter = 0;
                    DRV_INFO("ncsi_command_handler() ncsi_error_recovery() Done\n");
                }
                ncsi_if->NCSI_transition_flag = 0;
                continue;
            }
            else
            {
                DRV_INFO("ncsi_command_handler() list_for_each_entry_rcu()\n");
                list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
                {
                    DRV_INFO("ncsi_command_handler() list_for_each_entry_rcu() %s\n",ncsi_priv->ncsidev->name);
                    if (ncsi_if->dev == ncsi_priv->parent_dev)
                    {
                        if (netif_running(ncsi_priv->parent_dev) && ncsi_priv->active && (!ncsi_priv->sync_tx))
                        {
                            netif_tx_lock(ncsi_priv->ncsidev);
                            ncsi_get_link_status_polling(ncsi_priv->ncsidev);

                            /* every 5 seconds */
                            if (get_parameter_polling_interval >= 5)
                            {
                                ncsi_get_parameters_polling(ncsi_priv->ncsidev);
                            }

                            netif_tx_unlock(ncsi_priv->ncsidev);
                        }
                    }
                }

                /* every 5 seconds */
                if (get_parameter_polling_interval >= 5)
                    get_parameter_polling_interval = 0;
                else
                    get_parameter_polling_interval++;
            }
            ncsi_if->NCSI_transition_flag = 0;
        }
        else
        {
            DRV_INFO("ncsi_command_handler() ncsi_if->NCSI_transition_flag == 1 !\n");
        }
    }
    ncsi_init_flag[ncsi_if->ncsi_mii_index] = 0;
    complete_and_exit(&ncsi_if->cmd_exited, 0);
    return 0;
}

/******************************************************************************
*   FUNCTION        :   ncsi_notifier
******************************************************************************/
/**
 *  @brief      Notify the status of NIC interface.
 *
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
static int ncsi_notifier(
                            /** pointer to the notifier block. */
                            struct notifier_block *this,

                            /** message number. */
                            unsigned long msg,

                            /** pointer to the ethernet device. */
                            void *data
                         )
{
	struct net_device *dev = netdev_notifier_info_to_dev(data);
    struct nic_info_private *info_private;
    int package_id;
    struct vir_dev_priv *ncsi_priv;
    struct bonding *bond;
    int mii_id = 0;
    int i = 0;
    char name[12];

    if (u8TraceNotifier)
    {
        printk("ncsi_notifier - dev->name=%s msg=0x%x(%d) ", dev->name, msg, msg);

        switch (msg)
        {
            case NETDEV_UP:
                printk("NETDEV_UP\n");
                break;
            case NETDEV_DOWN:
                printk("NETDEV_DOWN\n");
                break;
            case NETDEV_REBOOT:
                printk("NETDEV_REBOOT\n");
                break;
            case NETDEV_CHANGE:
                printk("NETDEV_CHANGE\n");
                break;
            case NETDEV_REGISTER:
                printk("NETDEV_REGISTER\n");
                break;
            case NETDEV_UNREGISTER:
                printk("NETDEV_UNREGISTER\n");
                break;
            case NETDEV_CHANGEMTU:
                printk("NETDEV_CHANGEMTU\n");
                break;
            case NETDEV_CHANGEADDR:
                printk("NETDEV_CHANGEADDR\n");
                break;
            case NETDEV_GOING_DOWN:
                printk("NETDEV_GOING_DOWN\n");
                break;
            case NETDEV_CHANGENAME:
                printk("NETDEV_CHANGENAME\n");
                break;
            case NETDEV_FEAT_CHANGE:
                printk("NETDEV_FEAT_CHANGE\n");
                break;
            case NETDEV_BONDING_FAILOVER:
                printk("NETDEV_BONDING_FAILOVER\n");
                break;
            case NETDEV_PRE_UP:
                printk("NETDEV_PRE_UP\n");
                break;
            case NETDEV_PRE_TYPE_CHANGE:
                printk("NETDEV_PRE_TYPE_CHANGE\n");
                break;
            case NETDEV_POST_TYPE_CHANGE:
                printk("NETDEV_POST_TYPE_CHANGE\n");
                break;
            case NETDEV_POST_INIT:
                printk("NETDEV_POST_INIT\n");
                break;
            case NETDEV_UNREGISTER_FINAL:
                printk("NETDEV_UNREGISTER_FINAL\n");
                break;
            case NETDEV_RELEASE:
                printk("NETDEV_RELEASE\n");
                break;
            case NETDEV_NOTIFY_PEERS:
                printk("NETDEV_NOTIFY_PEERS\n");
                break;
            case NETDEV_JOIN:
                printk("NETDEV_JOIN\n");
                break;
            case NETDEV_CHANGEUPPER:
                printk("NETDEV_CHANGEUPPER\n");
                break;
            case NETDEV_RESEND_IGMP:
                printk("NETDEV_RESEND_IGMP\n");
                break;
            case NETDEV_PRECHANGEMTU:
                printk("NETDEV_PRECHANGEMTU\n");
                break;
            case NETDEV_CHANGEINFODATA:
                printk("NETDEV_CHANGEINFODATA\n");
                break;
            case NETDEV_BONDING_INFO:
                printk("NETDEV_BONDING_INFO\n");
                break;
            default:
                printk("\n");
                break;
        }
    }

    switch (msg)
    {
        case NETDEV_REGISTER:  /* A new network device has appeared */
            DRV_INFO("ncsi_notifier: %s device registered\n", dev->name);
            break;
        case NETDEV_UP:
            mii_id = ncsi_get_mii_index_with_name(dev->name);
            if (0xFF == mii_id)
            {
                DRV_INFO("ncsi_notifier open: Virtual Devices opened[%s]! \n",dev->name);

                /* ignore VLAN interface */
                if ((strncmp(dev->name, "bond0", 5) == 0) && (strlen(dev->name) == 5))
                {
                    rcu_read_lock();
                    bond = netdev_priv(dev);

                    if (bond->curr_active_slave != NULL)
                    {
                        if (bond->curr_active_slave->dev != NULL)
                        {
                            if (strncmp(bond->curr_active_slave->dev->name, "ncsi", 4) == 0)
                            {
                                active_slave_dev = bond->curr_active_slave->dev;

                                if (u8TraceNotifier)
                                    printk("active_slave_dev=%s\n", active_slave_dev->name);

                                rcu_read_unlock();
                                break;
                            }
                        }
                    }

                    active_slave_dev = NULL;

                    if (u8TraceNotifier)
                        printk("active_slave_dev=NULL\n");

                    rcu_read_unlock();
                }

                break;
            }
            else
            {
                info_private = netdev_priv(dev);

                DRV_INFO("%s is up, 0x%02x\n",dev->name,info_private->interface_info.interface_mode);
                nic_interface_mode[mii_id] = info_private->interface_info.interface_mode;
                switch (info_private->interface_info.interface_mode)
                {
                    /** There are 8 I/Fs for each MAC.
                        1.IF_NONE, 2. IF_RMII_PHY, 3.IF_MII_PHY, 4. IF_GMII_PHY,
                        5.IF_NCSI, 6. IF_RMII_PHY_NCSI, 7.IF_MII_PHY_NCSI,
                        8.IF_GMII_PHY_NCSI
                        From firmware's point of view, behavior of accessing MII, RMII, GMII,
                        IF_RMII_PHY_NCSI, IF_MII_PHY_NCSI, and IF_GMII_PHY_NCSI are the same.
                        So there are only three different behaviors in firmware
                        a. IF_NONE,
                        b. PHY,
                        c. NCSI
                    */
                    case IF_NONE:
                    DRV_INFO("%s up, mode is IF_NONE\n",dev->name);
                    break;
                    case IF_RMII_PHY:
                    DRV_INFO("%s up, mode is IF_RMII_PHY\n",dev->name);
                    break;
                    case IF_MII_PHY:
                    DRV_INFO("%s up, mode is IF_MII_PHY\n",dev->name);
                    break;
                    case IF_GMII_PHY:
                    DRV_INFO("%s up, mode is IF_GMII_PHY\n",dev->name);
                    break;
                    case IF_RMII_PHY_NCSI:
                    case IF_MII_PHY_NCSI:
                    case IF_GMII_PHY_NCSI:
                    case IF_NCSI:
                        DRV_INFO("ncsi_notifier: %s is NCSI device!!\n",dev->name);
                        array_ncsi_if[mii_id].dev = dev;
                        array_ncsi_if[mii_id].ncsi_mii_index = mii_id;
                        ncsi_init((sNCSI_IF_Info *) &array_ncsi_if[mii_id]);
                        ncsi_init_flag[mii_id] = 1;
                        for (i = 0; i < NCSI_DETECT_RETRY_TIMES; i++)
                        {
                            if (1 == ncsi_detect_device(dev, mii_id))
                            {
                                ncsi_init_flag[mii_id]=1;
                                break ;
                            }
                            mdelay(NCSI_DETECT_INTERVAL);
                        }

                        if ((array_ncsi_if[mii_id].cmd_handler_pid == 0) &&
                            (1 == ncsi_init_flag[mii_id]) &&
                            (array_ncsi_if[mii_id].arbitration_mode != NOT_SUPPORT_HW_ARBITRATION_MODE))
                        {
                            array_ncsi_if[mii_id].cmd_exit_flag = 0;
                            array_ncsi_if[mii_id].ncsi_mii_index = mii_id;
							memset((void *) &name[0], 0, 12);
							sprintf(&name[0], "kncsi/eth%d", mii_id);
							array_ncsi_if[mii_id].thread_task = kthread_run(ncsi_command_handler, (void *) &array_ncsi_if[mii_id], &name[0]);

							if (IS_ERR(array_ncsi_if[mii_id].thread_task))
							{
								array_ncsi_if[mii_id].cmd_handler_pid = 0;
								printk(KERN_EMERG "%s(): thread create failed.\n", __FUNCTION__);
							}
							else
							{
								array_ncsi_if[mii_id].cmd_handler_pid = array_ncsi_if[mii_id].thread_task->pid;
								DRV_INFO("%s(): NC-SI_thread create succes - pid=%d.\n", __FUNCTION__, array_ncsi_if[mii_id].thread_task->pid);
							}
                            ///array_ncsi_if[mii_id].cmd_handler_pid = kernel_thread(ncsi_command_handler, (void *) &array_ncsi_if[mii_id], CLONE_KERNEL);
                            printk(KERN_ALERT "\nGenerate the command process, the pid is %d!\n", array_ncsi_if[mii_id].cmd_handler_pid);
                            //if(array_ncsi_if[mii_id].cmd_handler_pid < 0)
                            //{
                            //    printk(KERN_ALERT "ncsi_notifier: create kernel thread fail! \n");
                            ///}
                        }
                    break;
                    default:
                        DRV_INFO("%s up, mode is not define %d\n",dev->name,info_private->interface_info.interface_mode);
                        break;
                }
            }
            break;

        case NETDEV_DOWN:
            mii_id = ncsi_get_mii_index_with_name(dev->name);
            if (0xFF == mii_id)
            {
                DRV_INFO("ncsi_notifier open: Others Devices closed[%s]! \n",dev->name);

                /* ignore VLAN interface */
                if ((strncmp(dev->name, "bond0", 5) == 0) && (strlen(dev->name) == 5))
                {
                    active_slave_dev = NULL;
                    if (u8TraceNotifier)
                        printk("active_slave_dev=NULL\n");
                }

                break;
            }
            else
            {
                info_private = netdev_priv(dev);
                DRV_INFO("%s is down, 0x%02x\n",dev->name,info_private->interface_info.interface_mode);
                nic_interface_mode[mii_id] = info_private->interface_info.interface_mode;
                switch(info_private->interface_info.interface_mode)
                {
                    /**There are 8 I/Fs for each MAC.
                       1.IF_NONE, 2. IF_RMII_PHY, 3.IF_MII_PHY, 4. IF_GMII_PHY,
                       5.IF_NCSI, 6. IF_RMII_PHY_NCSI, 7.IF_MII_PHY_NCSI,
                       8.IF_GMII_PHY_NCSI
                     From firmware's point of view, behavior of accessing MII, RMII, GMII,
                     IF_RMII_PHY_NCSI, IF_MII_PHY_NCSI, and IF_GMII_PHY_NCSI are the same.
                     So there are only three different behaviors in firmware
                     a. IF_NONE,
                     b. PHY,
                     c. NCSI
                     */
                    case IF_NONE:
                        DRV_INFO("%s up, mode is IF_NONE\n",dev->name);
                        break;
                    case IF_RMII_PHY:
                        DRV_INFO("%s up, mode is IF_RMII_PHY\n",dev->name);
                        break;
                    case IF_MII_PHY:
                        DRV_INFO("%s up, mode is IF_MII_PHY\n",dev->name);
                        break;
                    case IF_GMII_PHY:
                        DRV_INFO("%s up, mode is IF_GMII_PHY\n",dev->name);
                        break;
                    case IF_RMII_PHY_NCSI:
                    case IF_MII_PHY_NCSI:
                    case IF_GMII_PHY_NCSI:
                    case IF_NCSI:
                        DRV_INFO("ncsi_notifier: %s device downed\n",dev->name);
                        for (package_id = 0 ; package_id < CONFIG_NCSI_MAX_PACKAGE_NUMBER ; package_id ++ )
                        {
                            ncsi_package_status[mii_id][package_id] = 0;
                        }
                        list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
                        {
                            if (0 == strcmp((char *) mii_if[mii_id].name, ncsi_priv->parent_dev->name))
                            {
                                ncsi_free_device(ncsi_priv->ncsidev);
                            }
                        }

#if LINUX_VERSION_CODE > KERNEL_VERSION(3, 11, 0)
						printk(KERN_ALERT "Kill the command process, the pid is %d!\n",array_ncsi_if[mii_id].cmd_handler_pid);
						array_ncsi_if[mii_id].cmd_exit_flag = 1;
						if (array_ncsi_if[mii_id].cmd_handler_pid)
						{
							kthread_stop(array_ncsi_if[mii_id].thread_task);
							array_ncsi_if[mii_id].cmd_handler_pid = 0;
						}
						array_ncsi_if[mii_id].NCSI_transition_flag = 0;
#else
                        if (array_ncsi_if[mii_id].cmd_handler_pid != 0)
                        {
                            printk(KERN_ALERT "Kill the command process,the pid is %d!\n",array_ncsi_if[mii_id].cmd_handler_pid);
                            array_ncsi_if[mii_id].cmd_exit_flag = 1;
                            #if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)
                                kill_proc_info( SIGTERM ,(struct siginfo *)1 ,array_ncsi_if[mii_id].cmd_handler_pid );
                            #else
                                kill_proc(array_ncsi_if[mii_id].cmd_handler_pid, SIGTERM, 1);
                            #endif
                            wait_for_completion((struct completion *)&array_ncsi_if[mii_id].cmd_exited);

                            skb_queue_purge((struct sk_buff_head *)&array_ncsi_if[mii_id].ncsi_res_quene.ncsi_queue);
                            array_ncsi_if[mii_id].cmd_handler_pid = 0;
                            array_ncsi_if[mii_id].NCSI_transition_flag = 0;
                        }
#endif

                        ncsi_init_flag[mii_id] = 0;
                        break;
                    default:
                        DRV_INFO("%s up, mode is not define %d\n",dev->name,info_private->interface_info.interface_mode);
                        break;
                }
            }
            break;
        case NETDEV_GOING_DOWN:
            mii_id = ncsi_get_mii_index_with_name(dev->name);
            if (0xFF == mii_id)
            {
                DRV_INFO("ncsi_notifier open: Others Devices closed[%s]! \n",dev->name);
                break;
            }
            else
            {
                info_private = netdev_priv(dev);
                DRV_INFO("%s is going down, 0x%02x\n",dev->name,info_private->interface_info.interface_mode);
                nic_interface_mode[mii_id] = info_private->interface_info.interface_mode;
                switch(info_private->interface_info.interface_mode)
                {
                    /**There are 8 I/Fs for each MAC.
                       1.IF_NONE, 2. IF_RMII_PHY, 3.IF_MII_PHY, 4. IF_GMII_PHY,
                       5.IF_NCSI, 6. IF_RMII_PHY_NCSI, 7.IF_MII_PHY_NCSI,
                       8.IF_GMII_PHY_NCSI
                     From firmware's point of view, behavior of accessing MII, RMII, GMII,
                     IF_RMII_PHY_NCSI, IF_MII_PHY_NCSI, and IF_GMII_PHY_NCSI are the same.
                     So there are only three different behaviors in firmware
                     a. IF_NONE,
                     b. PHY,
                     c. NCSI
                     */
                    case IF_NONE:
                        DRV_INFO("%s up, mode is IF_NONE\n",dev->name);
                        break;
                    case IF_RMII_PHY:
                        DRV_INFO("%s up, mode is IF_RMII_PHY\n",dev->name);
                        break;
                    case IF_MII_PHY:
                        DRV_INFO("%s up, mode is IF_MII_PHY\n",dev->name);
                        break;
                    case IF_GMII_PHY:
                        DRV_INFO("%s up, mode is IF_GMII_PHY\n",dev->name);
                        break;
                    case IF_RMII_PHY_NCSI:
                    case IF_MII_PHY_NCSI:
                    case IF_GMII_PHY_NCSI:
                    case IF_NCSI:
                        DRV_INFO("ncsi_notifier: %s device going downed\n",dev->name);
                        list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
                        {
                            if (0 == strcmp((char *) mii_if[mii_id].name, ncsi_priv->parent_dev->name))
                            {
                                netif_stop_queue(ncsi_priv->ncsidev);
                            }
                        }
                        break;
                    default:
                        DRV_INFO("%s going down, mode is not define %d\n",
                              dev->name,info_private->interface_info.interface_mode);
                        break;
                }
            }
            break;
        case NETDEV_UNREGISTER:  /* A network device is being unloaded */
            DRV_INFO("ncsi_notifier: %s device unregistered\n",dev->name);
            break;
        case NETDEV_CHANGE:
            mii_id = ncsi_get_mii_index_with_name(dev->name);
            if (0xFF == mii_id)
            {
                DRV_INFO("ncsi_notifier change: Others Devices change[%s]! \n",dev->name);
                break;
            }
            else
            {
                info_private = netdev_priv(dev);
                DRV_INFO("%s is change, 0x%02x\n",dev->name,info_private->interface_info.interface_mode);
                nic_interface_mode[mii_id] = info_private->interface_info.interface_mode;

                switch(info_private->interface_info.interface_mode)
                {
                    /**There are 8 I/Fs for each MAC.
                       1.IF_NONE, 2. IF_RMII_PHY, 3.IF_MII_PHY, 4. IF_GMII_PHY,
                       5.IF_NCSI, 6. IF_RMII_PHY_NCSI, 7.IF_MII_PHY_NCSI,
                       8.IF_GMII_PHY_NCSI
                     From firmware's point of view, behavior of accessing MII, RMII, GMII,
                     IF_RMII_PHY_NCSI, IF_MII_PHY_NCSI, and IF_GMII_PHY_NCSI are the same.
                     So there are only three different behaviors in firmware
                     a. IF_NONE,
                     b. PHY,
                     c. NCSI
                     */
                    case IF_NONE:
                        DRV_INFO("%s change, mode is IF_NONE\n",dev->name);
                        break;
                    case IF_RMII_PHY:
                        DRV_INFO("%s change, mode is IF_RMII_PHY\n",dev->name);
                        break;
                    case IF_MII_PHY:
                        DRV_INFO("%s change, mode is IF_MII_PHY\n",dev->name);
                        break;
                    case IF_GMII_PHY:
                        DRV_INFO("%s change, mode is IF_GMII_PHY\n",dev->name);
                        break;
                    case IF_RMII_PHY_NCSI:
                    case IF_MII_PHY_NCSI:
                    case IF_GMII_PHY_NCSI:
                    case IF_NCSI:
                        if (!netif_running(dev))
                        {
                            list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
                            {
                                if (0 == strcmp((char *) mii_if[mii_id].name, ncsi_priv->parent_dev->name))
                                {
                                    if (ncsi_priv->active)
                                    {
                                        netif_tx_lock(ncsi_priv->ncsidev);
                                        netif_stop_queue(ncsi_priv->ncsidev);
                                        netif_tx_unlock(ncsi_priv->ncsidev);
                                    }
                                }
                            }
                        }
                        else
                        {
                            dev_change_flags(dev,dev->flags);
                            netif_start_queue(dev);
                            list_for_each_entry_rcu(ncsi_priv, &ncsi_devices, ncsi_list)
                            {
                                netif_tx_lock(ncsi_priv->ncsidev);
                                if (0 == strcmp((char *) mii_if[mii_id].name, ncsi_priv->parent_dev->name))
                                {
                                    if (ncsi_priv->active)
                                    {
                                        ncsi_set_recovery_flag(&array_ncsi_if[mii_id], 1, __FUNCTION__, __LINE__);
                                    }
                                }
                                netif_tx_unlock(ncsi_priv->ncsidev);
                            }
                        }
                        break;
                    default:
                        DRV_INFO("%s change, mode is not define %d\n",dev->name,info_private->interface_info.interface_mode);
                        break;
                }
            }
            break;
        case NETDEV_FEAT_CHANGE:
            break;
        case NETDEV_CHANGEMTU:
            break;
        case NETDEV_BONDING_FAILOVER:
            /* ignore VLAN interface */
            if ((strncmp(dev->name, "bond0", 5) == 0) && (strlen(dev->name) == 5))
            {
                rcu_read_lock();
                bond = netdev_priv(dev);

                if (bond->curr_active_slave != NULL)
                {
                    if (bond->curr_active_slave->dev != NULL)
                    {
                        if (strncmp(bond->curr_active_slave->dev->name, "ncsi", 4) == 0)
                        {
                            active_slave_dev = bond->curr_active_slave->dev;

                            if (u8TraceNotifier)
                                printk("active_slave_dev=%s\n", active_slave_dev->name);

                            rcu_read_unlock();
                            break;
                        }
                    }
                }

                active_slave_dev = NULL;

                if (u8TraceNotifier)
                    printk("active_slave_dev=NULL\n");

                rcu_read_unlock();
            }
            break;
        case NETDEV_BONDING_INFO:
            break;
    }
    return NOTIFY_DONE;
}

static const struct ethtool_ops ncsi_ethtool_ops = {
    .get_settings        = ncsi_get_ethtool_settings,
    .set_settings        = ncsi_set_ethtool_settings,
};

static struct notifier_block ncsi_netdev_notifier = {
    .notifier_call = ncsi_notifier,
};

#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 28)
static const struct net_device_ops ncsi_netdev_ops = {
    .ndo_open		= ncsi_virtual_open,
    .ndo_stop		= ncsi_virtual_close,
    .ndo_start_xmit		= ncsi_virtual_xmit,
    .ndo_do_ioctl		= ncsi_ioctl,
    .ndo_set_mac_address	= ncsi_set_mac_address,

};
#endif


#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)
static const struct header_ops ncsi_header_ops =
{
    .create     =   eth_header ,
    .cache      =   eth_header_cache,
    .cache_update=   eth_header_cache_update,
};
#endif


static void ncsi_setup(struct net_device *dev)
{
    //Kernel 2.6.30 support
#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 28)
	dev->netdev_ops = &ncsi_netdev_ops;
#endif

    //Kernel 2.6.30/2.6.28 support
#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)
	dev->header_ops = &ncsi_header_ops;
#endif

    dev->open                = ncsi_virtual_open;
    dev->stop                = ncsi_virtual_close;
    dev->hard_start_xmit     = ncsi_virtual_xmit;
    dev->set_mac_address     = ncsi_set_mac_address;
    dev->do_ioctl            = ncsi_ioctl;

    // Kernel 2.6.23 only
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 28)
    dev->hard_header         = eth_header;
    dev->hard_header_cache   = eth_header_cache;
    dev->header_cache_update = eth_header_cache_update;
#endif

    dev->hard_header_len     = ETH_HLEN;
    dev->addr_len            = ETH_ALEN;

    dev->type                = ARPHRD_ETHER;
    dev->mtu                 = ETH_DATA_LEN;
    dev->tx_queue_len        = 1000; /* Ethernet wants good queues */
    dev->flags               = IFF_BROADCAST|IFF_MULTICAST;

    dev->ethtool_ops         = &ncsi_ethtool_ops;

    memset(dev->broadcast, 0xFF, ETH_ALEN);
}

/******************************************************************************
*   FUNCTION        :   ncsi_ioctl_sendcommand
******************************************************************************/
/**
 *  @brief      NC-SI device's ioctl function which could send any NC-SI command.
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
static int ncsi_ioctl_sendcommand (
                       /** net_device */
                       struct net_device *dev,
                       /** ifreq */
                       struct ifreq *ifr,
                       /** cmd id */
                       int cmd
                     )
{
    /** @scope */
    void                 *priv_data = NULL;
    struct vir_dev_priv  *ncsi_priv = netdev_priv(dev);
    int                   res;
    struct sk_buff       *res_skb = NULL;
    struct sk_buff       *req_skb = NULL;
    struct sNCSI_RES_HDR *ncsi_reshdr;
    int                   i;
    u32                   retrycnt = 0;
    u8                    read_cnt = 0;
    u32                   u32retrytimeout;
    u32                   u32waittime;
    unsigned int          skb_len;
    u32                   u32retry;
    int                   outputdatasize;
    u8                   *pu8outputdata = NULL;
    u8                   *pu8inputdata = NULL;
    int                   inputdatasize;
    int                   err;
    struct sNCSI_HDR *ncsi_hdr;
    u8 get_respond = 0 ;

    DRV_URGENT("%s(): SEND_COMMAND!\n", __FUNCTION__);

    /** - Check private data to ensure the data from user space is valid. */
    /* The private data can't be NULL. It's type is (struct sNCSILIB_SEND_MESSAGE *). */
    if (NULL == ifr->ifr_ifru.ifru_data)
    {
        DRV_ERROR("%s(): SEND_COMMAND ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);

        if (u8TraceSendMessage)
            printk("SM-0\n");

        return -EINVAL;
    }

    if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_SEND_MESSAGE)))
    {
        DRV_URGENT("Debugfs: %s() SEND_COMMAND buffer access error Exit!\n", __FUNCTION__);

        if (u8TraceSendMessage)
            printk("SM-1\n");

        /* error occur */
        return (-EFAULT);
    }

    priv_data = ifr->ifr_ifru.ifru_data;

    /** - Prepare necessary data. */
    /* Get output data size from user space. */
    get_user(outputdatasize, ((struct sNCSILIB_SEND_MESSAGE *)priv_data)->outputDataSize);

    DRV_URGENT("%s(): Outputdatasize=0x%x\n", __FUNCTION__, outputdatasize);

    /* Allocate a buffer for output data. */
    pu8outputdata = kzalloc(outputdatasize, GFP_KERNEL);
    if (pu8outputdata == NULL)
    {
        DRV_URGENT("Debugfs: %s() SEND_COMMAND Can't allocate memory!\n", __FUNCTION__);

        if (u8TraceSendMessage)
            printk("SM-2\n");

        /* error occur */
        return (-ENOMEM);
    }

    /* Get input data size from user space. */
    get_user(inputdatasize, &(((struct sNCSILIB_SEND_MESSAGE *)priv_data)->inputDataSize));

    DRV_URGENT("%s(): inputdatasize=0x%x\n", __FUNCTION__, inputdatasize);

    /* Allocate a buffer for input data. */
    pu8inputdata = kzalloc(inputdatasize, GFP_KERNEL);
    if (pu8inputdata == NULL)
    {
        DRV_URGENT("Debugfs: %s() SENDOEM50_CMD Can't allocate memory!\n", __FUNCTION__);

        kfree(pu8outputdata);

        if (u8TraceSendMessage)
            printk("SM-3\n");

        /* error occur */
        return (-ENOMEM);
    }

    /* Copy input data to buffer. */
    copy_from_user((void*)pu8inputdata,
          (void*)(((struct sNCSILIB_SEND_MESSAGE *)priv_data)->inputData),
          inputdatasize);

    get_user(u32retrytimeout ,&(((struct sNCSILIB_SEND_MESSAGE *)priv_data)->timeout));

	/* waiting time for grabbing semaphore */
	u32waittime = u32retrytimeout;

	/* We use mdelay() to implement timeout mechanism. */
	if (down_trylock(&ncsi_control_sem[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index]))
	{
		if (u32waittime < NCSI_ERROR_RETRY_DELAY)
			u32waittime = NCSI_ERROR_RETRY_DELAY;

		while (u32waittime)
		{
			if (u8DelayMethod)
				msleep(NCSI_ERROR_RETRY_DELAY);
			else
				mdelay(NCSI_ERROR_RETRY_DELAY);

			if (u32waittime > NCSI_ERROR_RETRY_DELAY)
				u32waittime -= NCSI_ERROR_RETRY_DELAY;
			else
				u32waittime = 0;

			/* Try to get semaphore again. */
			if (down_trylock(&ncsi_control_sem[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index]))
			{
				if (u32waittime == 0)
				{
					kfree(pu8outputdata);
					kfree(pu8inputdata);

					if (u8TraceSendMessage)
						printk("SM-4\n");

					return (-EBUSY);
				}
			}
			else
			{
				break;
			}
		}
	}

    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
    array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;

    DRV_URGENT("%s(): chnl_id=0x%x, mii_index=0x%x\n", __FUNCTION__,
          array_ncsi_if[ncsi_priv->mii_index].chnl_id,
          ncsi_priv->mii_index);

    spin_lock(&array_ncsi_if[ncsi_priv->mii_index].lock);
    ncsi_control_flag[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index] = 1;
    spin_unlock(&array_ncsi_if[ncsi_priv->mii_index].lock);

    get_user(u32retry,&(((struct sNCSILIB_SEND_MESSAGE *)priv_data)->retries));

    do
    {
        /** - Send raw data. */
        if (!netif_running(array_ncsi_if[ncsi_priv->mii_index].dev))
        {
            DRV_ERROR("ncsi_send_packet() error! %s is down!\n", array_ncsi_if[ncsi_priv->mii_index].dev->name);
            //res = STATUS_FAIL;

            spin_lock(&array_ncsi_if[ncsi_priv->mii_index].lock);
            ncsi_control_flag[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index] = 0;
            spin_unlock(&array_ncsi_if[ncsi_priv->mii_index].lock);
            up(&ncsi_control_sem[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index]);

            kfree(pu8outputdata);
            kfree(pu8inputdata);

            if (u8TraceSendMessage)
                printk("SM-5\n");

            return (-EIO);

        }
        else
        {
            skb_len = ETH_HLEN + inputdatasize;

            req_skb = alloc_skb(skb_len + NET_IP_ALIGN, GFP_ATOMIC);

            ncsi_hdr = NCSI_HEADER(req_skb);
            ncsi_hdr->cmd_iid = array_ncsi_if[ncsi_priv->mii_index].cmd_iid[PKG_ID(array_ncsi_if[ncsi_priv->mii_index].chnl_id)];

            if (likely(req_skb != NULL))
            {
                req_skb->dev = array_ncsi_if[ncsi_priv->mii_index].dev;
                req_skb->len = skb_len;
                skb_reserve(req_skb, NET_IP_ALIGN);
                memset(req_skb->data, 0, skb_len);

                ncsi_eth_header(&array_ncsi_if[ncsi_priv->mii_index], req_skb);


                pu8inputdata[3] = array_ncsi_if[ncsi_priv->mii_index].cmd_iid[PKG_ID(array_ncsi_if[ncsi_priv->mii_index].chnl_id)] ;
                pu8inputdata[5] = ncsi_priv->ncsi_chnl_id ;


                memcpy(req_skb->data + ETH_HLEN, pu8inputdata, inputdatasize);

                if (!netif_carrier_ok(array_ncsi_if[ncsi_priv->mii_index].dev))
                {
                    if (req_skb != NULL)
                    {
                        dev_kfree_skb(req_skb);
                        req_skb = NULL;
                        DRV_ERROR("ncsi_send_packet() !netif_carrier_ok(%s)\n",array_ncsi_if[ncsi_priv->mii_index].dev->name);
                    }
                    res = (-EFAULT);
                }
                else
                {
                    /* send NCSI packet */
                    if ( 0 == dev_queue_xmit(req_skb))
                    {
                        DRV_INFO("ncsi_send_packet():dev_queue_xmit ok[%s]!\n",
                                 array_ncsi_if[ncsi_priv->mii_index].dev->name);
                        res = STATUS_OK;
                    }
                    else
                    {
                        DRV_ERROR("ncsi_send_packet():dev_queue_xmit fail!\n");
                        res = (-EIO);
                    }

                    /* add the IID */
                    array_ncsi_if[ncsi_priv->mii_index].cmd_iid[PKG_ID(array_ncsi_if[ncsi_priv->mii_index].chnl_id)]++;
                    if (0xFF == array_ncsi_if[ncsi_priv->mii_index].cmd_iid[PKG_ID(array_ncsi_if[ncsi_priv->mii_index].chnl_id)] )
                    {
                        array_ncsi_if[ncsi_priv->mii_index].cmd_iid[PKG_ID(array_ncsi_if[ncsi_priv->mii_index].chnl_id)] = 1;
                    }
                    DRV_INFO("NCSI_RECEIVE_RES_PACKET_OK\n");

                }
            }
            else
            {
                DRV_ERROR("Memory squeeze when alloc_skb in dropping ncsi_send_packet!!\n");
                res = -ENOMEM;
            }
       }

        DRV_INFO("[%d]End ncsi_send_packet\n", res);

        if (res < 0)
        {
            spin_lock(&array_ncsi_if[ncsi_priv->mii_index].lock);
            ncsi_control_flag[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index] = 0;
            spin_unlock(&array_ncsi_if[ncsi_priv->mii_index].lock);
            up(&ncsi_control_sem[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index]);
            DRV_INFO("Memory squeeze in ncsi_send_and_wait \n\n");

            /* Memory squeeze, return directly */
            kfree(pu8outputdata);
            kfree(pu8inputdata);
            if (req_skb != NULL)
            {
                dev_kfree_skb(req_skb);
                req_skb = NULL;
            }

            if (u8TraceSendMessage)
                printk("SM-6\n");

            return (-EIO) ;
            //return res;
        }

        if (STATUS_OK != res)
        {
            spin_lock(&array_ncsi_if[ncsi_priv->mii_index].lock);
            ncsi_control_flag[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index] = 0;
            spin_unlock(&array_ncsi_if[ncsi_priv->mii_index].lock);
            up(&ncsi_control_sem[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index]);
            DRV_ERROR("ncsi_send_packet fail!\n");
            kfree(pu8outputdata);
            kfree(pu8inputdata);
            if (req_skb != NULL)
            {
                dev_kfree_skb(req_skb);
                req_skb = NULL;
            }

            if (u8TraceSendMessage)
                printk("SM-7\n");

            return (-EIO) ;
        }

        read_cnt = 0;

        /* Usually, we won't received packets immediately, delays a while before checking response. */
        if (u8DelayMethod)
            msleep(NCSI_ERROR_RETRY_DELAY);
        else
            mdelay(NCSI_ERROR_RETRY_DELAY);

        do
        {
            do
            {
                /** Wait response, if error it will be re-sent, after that return result. */
                res = ncsi_queue_read(&array_ncsi_if[ncsi_priv->mii_index], &res_skb, NCSI_RES_QUEUE);
                if ( NCSI_RES_CODE_CMD_OK == res)
                {
                    if( pu8inputdata[3] == ((u8 *)res_skb->data)[3] )
                    {
                        DRV_INFO("ncsi_wait_response_packet() ok with 0x%x\n\n",pu8inputdata[4]);
                        get_respond = 1 ;
                        break;
                    }
                    else
                    {
                        if (res_skb)
                        {
                            DRV_ERROR("NCSI Free wrong res_skb = 0x%X\n",(unsigned int) res_skb);
                            dev_kfree_skb(res_skb);
                            res_skb = NULL;
                        }


                    }
                }
            }while (res != NCSI_RES_CODE_CMD_NORESPONSE);

            if (get_respond)
                break;

            /* Decrease the timeout value. */
            if (u32retrytimeout > NCSI_ERROR_RETRY_DELAY)
            {
                u32retrytimeout -= NCSI_ERROR_RETRY_DELAY;

                if (u8DelayMethod)
                    msleep(NCSI_ERROR_RETRY_DELAY);
                else
                    mdelay(NCSI_ERROR_RETRY_DELAY);
            }
            else
            {
                if (u8DelayMethod)
                    msleep(u32retrytimeout);
                else
                    mdelay(u32retrytimeout);

                u32retrytimeout = 0;
            }
        } while (u32retrytimeout > 0); /* Continue to wait if not timeout. */
        DRV_INFO("[%d]End ncsi_wait_response_packet\n", res);

        if (likely(NCSI_RECEIVE_RES_PACKET_OK == res))
        {
            break;
        }
        else
        {
            if (res_skb)
            {
               DRV_ERROR("NCSI Free wrong res_skb = 0x%X\n",(unsigned int) res_skb);
               dev_kfree_skb(res_skb);
               res_skb = NULL;
            }
            /* faid to get the response packet, do the next retry */
            continue;
        }
    } while (retrycnt++ < u32retry);    /* Retry. */

#if 0
    if (likely(NCSI_RES_CODE_CMD_NORESPONSE == res))
    {
        ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 1, __FUNCTION__, __LINE__);
        DRV_URGENT("%s():array_ncsi_if[ncsi_priv->mii_index].error_recovery_enable_flag \n",__FUNCTION__);
    }
#endif

    spin_lock(&array_ncsi_if[ncsi_priv->mii_index].lock);
    ncsi_control_flag[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index] = 0;
    spin_unlock(&array_ncsi_if[ncsi_priv->mii_index].lock);
    up(&ncsi_control_sem[array_ncsi_if[ncsi_priv->mii_index].ncsi_mii_index]);

    if(res_skb)  //  ADDED IN 13G
       ncsi_reshdr = NCSI_RES_HEADER(res_skb);

    if (NCSI_RECEIVE_RES_PACKET_OK == res)
    {
        /* Process the command. */
        DRV_URGENT("%s(): Start to proces the response of send command\n", __FUNCTION__);

        /* *outputDataSize is the size given by application, ncsi_reshdr->ncsi_hdr.payload_len is actual received size.
         * In order to don't over-write the buffer, we pick the min one. */
        outputdatasize = min(outputdatasize,res_skb->len);
        memcpy ((u8 *)pu8outputdata,
               (u8 *)res_skb->data, outputdatasize);

        if ((NCSI_RES_CODE_CMD_OK != ncsi_reshdr->response_code))
        {
//printk(KERN_EMERG"NCSI_RES_CODE_CMD_OK != ncsi_reshdr->response_code");
            ncsi_cmd_error_handler(&array_ncsi_if[ncsi_priv->mii_index],ncsi_reshdr);
        }

        DRV_URGENT("%s():copy_to_user finish\n",__FUNCTION__);

        /* Prints received data. */
        for (i = 0; i < outputdatasize; i++)
        {
            DRV_URGENT("%s():res_skb->data[%d]=0x%x\n",__FUNCTION__, i,((u8 *)res_skb->data)[i]);
        }
    }
    else
    {
        /* If no response. */
        outputdatasize = 0;

        if (u8TraceSendMessage)
            printk("SM-8\n");

    }
#if 0 // force error for debug
    printk(KERN_EMERG"recover-mii_index=%d",ncsi_priv->mii_index );
    array_ncsi_if[ncsi_priv->mii_index].error_recovery_enable_flag = 1 ;
#endif

    if (res_skb)
    {
        DRV_URGENT("NCSI Free res_skb = 0x%X\n", (unsigned int)res_skb);

        dev_kfree_skb(res_skb);
        res_skb = NULL;
    }

    err = res;

    /* Save the actual output data size to user space. */
    put_user(outputdatasize, (((struct sNCSILIB_SEND_MESSAGE *)priv_data)->outputDataSize));

    if (STATUS_OK == err)
    {
        /** Copy the received data to user space. */
        copy_to_user ((void*)(((struct sNCSILIB_SEND_MESSAGE *)priv_data)->outputData),
              (void*)pu8outputdata,
              *(((struct sNCSILIB_SEND_MESSAGE *)priv_data)->outputDataSize));
        err = 0;
    }
    else if (STATUS_BUSY == err)
    {
        err = (-EBUSY);
    }
    else if (outputdatasize == 0)
    {
        DRV_URGENT("%s(): No response.\n", __FUNCTION__);
        err = (-ENODATA);
    }
    else
    {
        err = (-EIO);
    }
    kfree(pu8outputdata);
    kfree(pu8inputdata);

    if (u8TraceSendMessage)
        printk("SM-9\n");

    return err;
}

/******************************************************************************
*   FUNCTION        :   ncsi_ioctl
******************************************************************************/
/**
 *  @brief      NC-SI device's ioctl function.
 *
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: int function\n
 *
 *****************************************************************************/
static int ncsi_ioctl(
                       /** net_device */
                       struct net_device *dev,
                       /** ifreq */
                       struct ifreq *ifr,
                       /** cmd id */
                       int cmd
                     )
{
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    struct ncsi_vlan_configs *vlan_configs;
    void  *priv_data = NULL;
    int    err;
    int    outputdatasize;
    u8     *pu8outputdata = NULL;
    u8     *pu8inputdata = NULL;
    int    inputdatasize;




    switch (cmd)
    {
        case SET_VLAN_CONFIG_CMD:
        case SET_VLAN_ENABLED_CMD:
        case SET_VLAN_DISABLED_CMD:
            /** - Set VLAN enabled command to NCSI device */
            if (!netif_running(dev))
            {
                printk(KERN_ALERT "Net device is busy now. It can't enabled VLAN.\n");
                return -EBUSY;
            }
        case SENDOEM50_CMD:
        case GET_STATISTICS_CMD:
        case GET_LINK_STATUS_CMD:
        case GET_CONTROLLER_PACKET_STATISTICS_CMD:
        case GET_PASSTHROUGH_STATISTICS_CMD:
        case GET_VERSION_ID_CMD:
        case GET_CAPABILITIES_CMD:
        case SEND_COMMAND:

            if( NOT_SUPPORT_HW_ARBITRATION_MODE == array_ncsi_if[ncsi_priv->mii_index].arbitration_mode)
            {
                array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id | 0x1f;
                array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
                array_ncsi_if[ncsi_priv->mii_index].ncsi_hardware_arb = CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_DISABLE;
                err = ncsi_cmd_select_package((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , 1 );
                if (err == NCSI_RES_CODE_CMD_NORESPONSE)
                {
                    DRV_INFO("ncsi_cmd_select_package() Fail!!\n" );
                }
                array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            }
            break;
    }


    switch (cmd)
    {
        case GET_VLAN_CONFIG_CMD:
            /** - Get VLAN configuration from NCSI device */
            if(!netif_running(dev))
            {
                printk(KERN_INFO "Net device is busy now. It can't get VLAN configuration.\n");
                return -EBUSY;
            }
            DRV_INFO("%s(): GET_VLAN_CONFIG_CMD!\n", __FUNCTION__);
            if (NULL != ifr->ifr_ifru.ifru_data)
            {
                vlan_configs = (struct ncsi_vlan_configs *) ifr->ifr_ifru.ifru_data;
                vlan_configs->VID = ncsi_priv->vlan_config.VID;
                vlan_configs->UserPriority = ncsi_priv->vlan_config.UserPriority;
                vlan_configs->CFI = ncsi_priv->vlan_config.CFI;
                vlan_configs->FilterSelector = ncsi_priv->vlan_config.FilterSelector;
                vlan_configs->VMode = ncsi_priv->vlan_config.VMode;
            }
            else
            {
                DRV_ERROR("%s(): GET_VLAN_CONFIG_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }
            err = 0;
            break;
        case SET_VLAN_CONFIG_CMD:
            /** - Set VLAN configuration to NCSI device */
            if(!netif_running(dev))
            {
                printk(KERN_ALERT "Net device is busy now. It can't set VLAN configuration.\n");
                return -EBUSY;
            }
            DRV_INFO("%s(): SET_VLAN_CONFIG_CMD!\n", __FUNCTION__);
            if (NULL != ifr->ifr_ifru.ifru_data)
            {
                vlan_configs = (struct ncsi_vlan_configs *) ifr->ifr_ifru.ifru_data;
                ncsi_priv->vlan_config.VID = vlan_configs->VID;
                ncsi_priv->vlan_config.UserPriority = vlan_configs->UserPriority;
                ncsi_priv->vlan_config.CFI = vlan_configs->CFI;
                ncsi_priv->vlan_config.FilterSelector = vlan_configs->FilterSelector;
                ncsi_priv->vlan_config.VMode = vlan_configs->VMode;
                DRV_INFO("%s(): ncsi_priv->vlan_config.VID            is 0x%02x",
                      __FUNCTION__, ncsi_priv->vlan_config.VID);
                DRV_INFO("%s(): ncsi_priv->vlan_config.UserPriority   is 0x%02x",
                      __FUNCTION__, ncsi_priv->vlan_config.UserPriority);
                DRV_INFO("%s(): ncsi_priv->vlan_config.CFI            is 0x%02x",
                      __FUNCTION__, ncsi_priv->vlan_config.CFI);
                DRV_INFO("%s(): ncsi_priv->vlan_config.FilterSelector is 0x%02x",
                      __FUNCTION__, ncsi_priv->vlan_config.FilterSelector);
                DRV_INFO("%s(): ncsi_priv->vlan_config.VMode          is 0x%02x",
                      __FUNCTION__, ncsi_priv->vlan_config.VMode);
                if (0 != ncsi_cmd_set_vlan_filters(dev))
                {
                    DRV_ERROR("%s(): NCSI set VLAN filters command error!\n", __FUNCTION__);
                    return -EINVAL;
                }
                else
                {
                    DRV_INFO("%s(): NCSI set VLAN filters command ok!\n", __FUNCTION__);
                }
            }
            else
            {
                DRV_ERROR("%s(): SET_VLAN_CONFIG_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }
            err = 0;
            break;
        case SET_VLAN_ENABLED_CMD:
            /** - Set VLAN enabled command to NCSI device */
            if (!netif_running(dev))
            {
                printk(KERN_ALERT "Net device is busy now. It can't enabled VLAN.\n");
                return -EBUSY;
            }
            DRV_INFO("%s(): SET_VLAN_ENABLED_CMD!\n", __FUNCTION__);
            if (NULL != ifr->ifr_ifru.ifru_data)
            {
                vlan_configs = (struct ncsi_vlan_configs *) ifr->ifr_ifru.ifru_data;
                if (0 != ncsi_cmd_enable_vlan(dev))
                {
                    DRV_ERROR("%s(): NCSI enabled VLAN command error!\n", __FUNCTION__);
                    ncsi_priv->vlan_config.Enabled = 0;
                    return -EINVAL;
                }
                else
                {
                    ncsi_priv->vlan_config.Enabled = 1;
                    DRV_INFO("%s(): NCSI enabled VLAN command ok!\n", __FUNCTION__);
                }
            }
            else
            {
                DRV_ERROR("%s(): SET_VLAN_ENABLED_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }
            err = 0;
            break;
        case SET_VLAN_DISABLED_CMD:
            /** - Set VLAN disabled command to NCSI device */
            if (!netif_running(dev))
            {
                printk(KERN_ALERT "Net device is busy now. It can't disabled VLAN.\n");
                return -EBUSY;
            }
            DRV_INFO("%s(): SET_VLAN_DISABLED_CMD!\n", __FUNCTION__);
            if (NULL != ifr->ifr_ifru.ifru_data)
            {
                vlan_configs = (struct ncsi_vlan_configs *) ifr->ifr_ifru.ifru_data;
                if (0 != ncsi_cmd_disable_vlan(dev))
                {
                    DRV_ERROR("%s(): NCSI disabled VLAN command error!\n", __FUNCTION__);
                }
                else
                {
                    ncsi_priv->vlan_config.Enabled = 0;
                    DRV_INFO("%s(): NCSI disabled VLAN command ok!\n", __FUNCTION__);
                }
            }
            else
            {
                DRV_ERROR("%s(): SET_VLAN_DISABLED_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }
            err = 0;
            break;
        case SIOCSIFHWADDR:
            /** - Set MAC address */
            if (netif_running(dev))
            {
                printk(KERN_ALERT "Net device is busy now. It can't set MAC address.\n");
                return -EBUSY;
            }
            DRV_INFO("%s(): SIOCSIFHWADDR!\n", __FUNCTION__);
            memcpy(dev->dev_addr,ifr->ifr_hwaddr.sa_data,ETH_ALEN);
            err = 0;
            break;
        case SENDOEM50_CMD:
            DRV_URGENT("%s(SENDOEM50_CMD): chnl_id=0x%lx, mii_index=0x%x\n", __FUNCTION__,
                  ncsi_priv->ncsi_chnl_id, ncsi_priv->mii_index);

            /* The private data can't be NULL. It's type is (struct sNCSILIB_OEM_COMMAND *). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): SENDOEM50_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }

            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_OEM_COMMAND)))
            {
                DRV_URGENT("Debugfs: %s() SENDOEM50_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }

            priv_data = ifr->ifr_ifru.ifru_data;

            /* Get output data size from user space. */
            get_user(outputdatasize,(((struct sNCSILIB_OEM_COMMAND *)priv_data)->outputDataSize));

            DRV_URGENT("%s(): Outputdatasize=0x%x\n", __FUNCTION__, outputdatasize);

            /* Allocate a buffer for output data. */
            pu8outputdata = kzalloc(outputdatasize, GFP_KERNEL);
            if (pu8outputdata == NULL)
            {
                DRV_URGENT("Debugfs: %s() SENDOEM50_CMD Can't allocate memory!\n", __FUNCTION__);

                /* error occur */
                return (-ENOMEM);
            }

            /* Get input data size from user space. */
            get_user(inputdatasize, &(((struct sNCSILIB_OEM_COMMAND *)priv_data)->inputDataSize));

            DRV_URGENT("%s(): inputdatasize=0x%x\n", __FUNCTION__, inputdatasize);

            /* Allocate a buffer for input data. */
            pu8inputdata = kzalloc(inputdatasize, GFP_KERNEL);
            if (pu8outputdata == NULL)
            {
                DRV_URGENT("Debugfs: %s() SENDOEM50_CMD Can't allocate memory!\n", __FUNCTION__);

                kfree(pu8outputdata);

                /* error occur */
                return (-ENOMEM);
            }

            /* Copy input data to buffer. */
            copy_from_user((void*)pu8inputdata,
                  (void*)(((struct sNCSILIB_OEM_COMMAND *)priv_data)->inputData),
                  inputdatasize);

            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
            /* Execute OEM command. */
            err = ncsi_cmd_SendOEM50 (&array_ncsi_if[ncsi_priv->mii_index],
                                       pu8inputdata,
                                       inputdatasize,
                                       pu8outputdata,
                                      &outputdatasize,
                                       ((struct sNCSILIB_OEM_COMMAND *)priv_data)->retries,
                                       ((struct sNCSILIB_OEM_COMMAND *)priv_data)->timeout);

            /* Save the actual output data size to user space. */
            put_user(outputdatasize, (((struct sNCSILIB_OEM_COMMAND *)priv_data)->outputDataSize));
            if (STATUS_OK == err)
            {
                /* Copy the received data to user space. */
                copy_to_user ((void*)(((struct sNCSILIB_OEM_COMMAND *)priv_data)->outputData),
                      (void*)pu8outputdata,
                      *(((struct sNCSILIB_OEM_COMMAND *)priv_data)->outputDataSize));
                err = 0;
            }
            else if (STATUS_BUSY == err)
            {
                err = (-EBUSY);
            }
            else if (NCSI_RES_CODE_CMD_NORESPONSE == err)
            {
                DRV_URGENT("%s(): No response.\n", __FUNCTION__);
                err = (-ENODATA);
            }
            else
            {
                err = (-EIO);
            }
            if (err != 0)
            {
                /* copy response code and reason code. */
                copy_to_user ((void*)(((struct sNCSILIB_OEM_COMMAND *)priv_data)->outputData),
                    (void*)pu8outputdata, 4);
            }
            kfree(pu8outputdata);
            kfree(pu8inputdata);
            break;
        case GET_STATISTICS_CMD:
            /* The private data can't be NULL. It's type is (struct sNCSILIB_NCSI_STATISTICS*). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): GET_LINK_STATUS_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }

            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_Link_Status)))
            {
                DRV_URGENT("Debugfs: %s() GET_LINK_STATUS_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }
            priv_data = ifr->ifr_ifru.ifru_data;
            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
            err = ncsi_cmd_get_ncsi_statistics (&array_ncsi_if[ncsi_priv->mii_index],
                                          ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->retries,
                                          ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->timeout);

            ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.reason_code
                  = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.reason_code;
            ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.response_code
                  = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.response_code;

            if (STATUS_OK == err)
            {
                ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.commands_received
                      = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.commands_received;
                ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.control_packets_dropped
                      = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.control_packets_dropped;
                ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.command_type_errors
                      = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.command_type_errors;
                ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.command_checksum_errors
                      = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.command_checksum_errors;
                ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.receive_packets
                      = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.receive_packets;
                ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.transmit_packets
                      = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.transmit_packets;
                ((struct sNCSILIB_NCSI_STATISTICS*)priv_data)->ncsi_statistics.aens_sent
                      = array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.aens_sent;
                DRV_URGENT("%s(): commands received=0x%x\n", __FUNCTION__,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.commands_received);
                DRV_URGENT("%s(): control packets dropped=0x%x\n", __FUNCTION__,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.control_packets_dropped);
                DRV_URGENT("%s(): command type errors=0x%x\n", __FUNCTION__,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.command_type_errors);
                DRV_URGENT("%s(): command checksum errors=0x%x\n", __FUNCTION__,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.command_checksum_errors);
                DRV_URGENT("%s(): receive packets=0x%x\n", __FUNCTION__,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.receive_packets);
                DRV_URGENT("%s(): transmit packets=0x%x\n", __FUNCTION__,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.transmit_packets);
                DRV_URGENT("%s(): aens sent=0x%x\n", __FUNCTION__,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_statistics.aens_sent);
                err = 0;
            }
            else if (STATUS_BUSY == err)
            {
                err = (-EBUSY);
            }
            else if (NCSI_RES_CODE_CMD_NORESPONSE == err)
            {
                DRV_URGENT("%s(): No response.\n", __FUNCTION__);
                err = (-ENODATA);
            }
            else
            {
                err = (-EIO);
            }
            break;
        case GET_LINK_STATUS_CMD:
            /* The private data can't be NULL. It's type is (struct sNCSILIB_Link_Status*). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): GET_LINK_STATUS_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }

            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_Link_Status)))
            {
                DRV_URGENT("Debugfs: %s() GET_LINK_STATUS_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }
            priv_data = ifr->ifr_ifru.ifru_data;
            err = ncsi_cmd_get_link_status_with_rt (dev,
                  ((struct sNCSILIB_Link_Status*)priv_data)->retries,
                  ((struct sNCSILIB_Link_Status*)priv_data)->timeout);

            ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.reason_code
                = ncsi_priv->ncsi_link_status.reason_code;
            ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.response_code
                = ncsi_priv->ncsi_link_status.response_code;
            if (STATUS_OK == err)
            {
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_flag
                   = ncsi_priv->ncsi_link_status.link_flag;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.speed_and_duplex
                   = ncsi_priv->ncsi_link_status.speed_and_duplex;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.auto_negotiate_flag
                   = ncsi_priv->ncsi_link_status.auto_negotiate_flag;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.auto_negotiate_complete
                   = ncsi_priv->ncsi_link_status.auto_negotiate_complete;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.parallel_detection_flag
                   = ncsi_priv->ncsi_link_status.parallel_detection_flag;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_1000TFD
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_1000TFD;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_1000THD
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_1000THD;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_100T4
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_100T4;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_100TXFD
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_100TXFD;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_100TXHD
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_100TXHD;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_10TFD
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_10TFD;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_10THD
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_10THD;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.tx_flow_control_flag
                   = ncsi_priv->ncsi_link_status.tx_flow_control_flag;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.rx_flow_control_flag
                   = ncsi_priv->ncsi_link_status.rx_flow_control_flag;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.link_partner_advertised_flow_control
                   = ncsi_priv->ncsi_link_status.link_partner_advertised_flow_control;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.serdes_link
                   = ncsi_priv->ncsi_link_status.serdes_link;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.reserved1
                   = ncsi_priv->ncsi_link_status.reserved1;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.reserved2
                   = ncsi_priv->ncsi_link_status.reserved2;
                ((struct sNCSILIB_Link_Status*)priv_data)->ncsi_link_status.reserved3
                   = ncsi_priv->ncsi_link_status.reserved3;
                DRV_URGENT("%s(): link flag=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_flag);
                DRV_URGENT("%s(): speed_and_duplex=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.speed_and_duplex);
                DRV_URGENT("%s(): auto_negotiate_flag=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.auto_negotiate_flag);
                DRV_URGENT("%s(): auto_negotiate_complete=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.auto_negotiate_complete);
                DRV_URGENT("%s(): parallel_detection_flag=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.parallel_detection_flag);
                DRV_URGENT("%s(): link_partner_advertised_1000TFD=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_1000TFD);
                DRV_URGENT("%s(): link_partner_advertised_1000THD=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_1000THD);
                DRV_URGENT("%s(): link_partner_advertised_100T4=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_100T4);
                DRV_URGENT("%s(): link_partner_advertised_100TXFD=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_100TXFD);
                DRV_URGENT("%s(): link_partner_advertised_100TXHD=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_100TXHD);
                DRV_URGENT("%s(): link_partner_advertised_10TFD=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_10TFD);
                DRV_URGENT("%s(): link_partner_advertised_10THD=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_10THD);
                DRV_URGENT("%s(): tx_flow_control_flag=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.tx_flow_control_flag);
                DRV_URGENT("%s(): rx_flow_control_flag=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.rx_flow_control_flag);
                DRV_URGENT("%s(): link_partner_advertised_flow_control=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.link_partner_advertised_flow_control);
                DRV_URGENT("%s(): serdes_link=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.serdes_link);
                DRV_URGENT("%s(): reserved1=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.reserved1);
                DRV_URGENT("%s(): reserved2=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.reserved2);
                DRV_URGENT("%s(): reserved3=0x%x\n", __FUNCTION__,
                            ncsi_priv->ncsi_link_status.reserved3);
                err = 0;
            }
            else if (STATUS_BUSY == err)
            {
                err = (-EBUSY);
            }
            else if (NCSI_RES_CODE_CMD_NORESPONSE == err)
            {
                DRV_URGENT("%s(): No response.\n", __FUNCTION__);
                err = (-ENODATA);
            }
            else
            {
                err = (-EIO);
            }
            break;
        case GET_CONTROLLER_PACKET_STATISTICS_CMD:
            DRV_URGENT("Debugfs: %s() GET_CONTROLLER_PACKET_STATISTICS_CMD\n", __FUNCTION__);
            /* The private data can't be NULL. It's type is (struct sNCSILIB_CONTROL_PACKET_STATISTICS*). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): GET_CONTROLLER_PACKET_STATISTICS_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }


            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_CONTROL_PACKET_STATISTICS)))
            {
                DRV_URGENT("Debugfs: %s() GET_CONTROLLER_PACKET_STATISTICS_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }
            priv_data = ifr->ifr_ifru.ifru_data;
            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
            err = ncsi_cmd_get_controller_packet_statistics(&array_ncsi_if[ncsi_priv->mii_index],
                                          ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->retries,
                                          ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->timeout);
            ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.reason_code
                = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.reason_code;
            ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.response_code
                = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.response_code;
            if (STATUS_OK == err)
            {
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.counters_cleared_from_last_read_ms
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.counters_cleared_from_last_read_ms;
                DRV_URGENT("%s(): counters_cleared_from_last_read_ms=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.counters_cleared_from_last_read_ms);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.counters_cleared_from_last_read_ls
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.counters_cleared_from_last_read_ls;
                DRV_URGENT("%s(): counters_cleared_from_last_read_ls=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.counters_cleared_from_last_read_ls);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_bytes_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_bytes_received;
                DRV_URGENT("%s(): total_bytes_received=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_bytes_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_bytes_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_bytes_transmitted;
                DRV_URGENT("%s(): total_bytes_transmitted=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_bytes_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_unicast_packets_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_unicast_packets_received;
                DRV_URGENT("%s(): total_unicast_packets_received=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_unicast_packets_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_multicast_packets_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_multicast_packets_received;
                DRV_URGENT("%s(): total_multicast_packets_received=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_multicast_packets_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_broadcast_packets_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_broadcast_packets_received;
                DRV_URGENT("%s(): total_broadcast_packets_received=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_broadcast_packets_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_unicast_packets_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_unicast_packets_transmitted;
                DRV_URGENT("%s(): total_unicast_packets_transmitted=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_unicast_packets_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_multicast_packets_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_multicast_packets_transmitted;
                DRV_URGENT("%s(): total_multicast_packets_transmitted=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_multicast_packets_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.total_broadcast_packets_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_broadcast_packets_transmitted;
                DRV_URGENT("%s(): total_broadcast_packets_transmitted=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.total_broadcast_packets_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.fcs_receive_errors
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.fcs_receive_errors;
                DRV_URGENT("%s(): fcs_receive_errors=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.fcs_receive_errors);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.alignment_errors
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.alignment_errors;
                DRV_URGENT("%s(): alignment_errors=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.alignment_errors);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.false_carrier_detections
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.false_carrier_detections;
                DRV_URGENT("%s(): false_carrier_detections=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.false_carrier_detections);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.runt_packets_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.runt_packets_received;
                DRV_URGENT("%s(): runt_packets_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.runt_packets_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.jabber_packets_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.jabber_packets_received;
                DRV_URGENT("%s(): jabber_packets_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.jabber_packets_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.pause_xon_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xon_frames_received;
                DRV_URGENT("%s(): pause_xon_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xon_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.pause_xoff_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xoff_frames_received;
                DRV_URGENT("%s(): pause_xoff_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xoff_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.pause_xon_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xon_frames_transmitted;
                DRV_URGENT("%s(): pause_xon_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xon_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.pause_xoff_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xoff_frames_transmitted;
                DRV_URGENT("%s(): pause_xoff_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.pause_xoff_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.single_collision_transmit_frames
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.single_collision_transmit_frames;
                DRV_URGENT("%s(): single_collision_transmit_frames=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.single_collision_transmit_frames);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.multiple_collision_transmit_frames
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.multiple_collision_transmit_frames;
                DRV_URGENT("%s(): multiple_collision_transmit_frames=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.multiple_collision_transmit_frames);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.late_collision_frames
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.late_collision_frames;
                DRV_URGENT("%s(): late_collision_frames=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.late_collision_frames);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.excessive_collision_frames
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.excessive_collision_frames;
                DRV_URGENT("%s(): excessive_collision_frames=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.excessive_collision_frames);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.control_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.control_frames_received;
                DRV_URGENT("%s(): control_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.control_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_64_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_64_frames_received;
                DRV_URGENT("%s(): byte_64_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_64_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_65_127_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_65_127_frames_received;
                DRV_URGENT("%s(): byte_65_127_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_65_127_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_128_255_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_128_255_frames_received;
                DRV_URGENT("%s(): byte_128_255_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_128_255_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_256_511_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_256_511_frames_received;
                DRV_URGENT("%s(): byte_256_511_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_256_511_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_512_1023_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_512_1023_frames_received;
                DRV_URGENT("%s(): byte_512_1023_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_512_1023_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_1024_1522_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1024_1522_frames_received;
                DRV_URGENT("%s(): byte_1024_1522_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1024_1522_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_1523_9022_frames_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1523_9022_frames_received;
                DRV_URGENT("%s(): byte_1523_9022_frames_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1523_9022_frames_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_64_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_64_frames_transmitted;
                DRV_URGENT("%s(): byte_64_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_64_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_65_127_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_65_127_frames_transmitted;
                DRV_URGENT("%s(): byte_65_127_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_65_127_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_128_255_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_128_255_frames_transmitted;
                DRV_URGENT("%s(): byte_128_255_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_128_255_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_256_511_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_256_511_frames_transmitted;
                DRV_URGENT("%s(): byte_256_511_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_256_511_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_512_1023_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_512_1023_frames_transmitted;
                DRV_URGENT("%s(): byte_512_1023_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_512_1023_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_1024_1522_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1024_1522_frames_transmitted;
                DRV_URGENT("%s(): byte_1024_1522_frames_transmitted=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1024_1522_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.byte_1523_9022_frames_transmitted
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1523_9022_frames_transmitted;
                DRV_URGENT("%s(): byte_1523_9022_frames_transmitted=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.byte_1523_9022_frames_transmitted);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.valid_bytes_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.valid_bytes_received;
                DRV_URGENT("%s(): valid_bytes_received=0x%llx\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.valid_bytes_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.error_runt_packets_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.error_runt_packets_received;
                DRV_URGENT("%s(): error_runt_packets_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.error_runt_packets_received);
                ((struct sNCSILIB_CONTROL_PACKET_STATISTICS*)priv_data)->ncsi_control_packet_statistics.error_jabber_packets_received
                      = array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.error_jabber_packets_received;
                DRV_URGENT("%s(): error_jabber_packets_received=0x%x\n",
                      __FUNCTION__,array_ncsi_if[ncsi_priv->mii_index].control_packet_statistics.error_jabber_packets_received);
                err = 0;
            }
            else if (STATUS_BUSY == err)
            {
                err = (-EBUSY);
            }
            else if (NCSI_RES_CODE_CMD_NORESPONSE == err)
            {
                DRV_URGENT("%s(): No response.\n", __FUNCTION__);
                err = (-ENODATA);
            }
            else
            {
                err = (-EIO);
            }

            break;

        case GET_PASSTHROUGH_STATISTICS_CMD:
            DRV_URGENT("%s(): chnl_id=0x%lx, mii_index=0x%x\n", __FUNCTION__,
                  ncsi_priv->ncsi_chnl_id, ncsi_priv->mii_index);

            /* The private data can't be NULL. It's type is (struct sNCSILIB_PASSTHROUGH_STATISTICS*). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): GET_LINK_STATUS_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }

            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_PASSTHROUGH_STATISTICS)))
            {
                DRV_URGENT("Debugfs: %s() GET_LINK_STATUS_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }
            priv_data = ifr->ifr_ifru.ifru_data;

            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
            err = ncsi_cmd_get_ncsi_passthrough_statistics(&array_ncsi_if[ncsi_priv->mii_index],
                                            ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->retries,
                                            ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->timeout);
            ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.response_code
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.response_code;
            ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.reason_code
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.reason_code;
            if (err == 0)
            {

                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkts_rcved_on_ncsi_rx_interface
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_tx_pkts_rcved_on_ncsi_rx_interface;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkts_dropped
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_tx_pkts_dropped;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkt_chn_state_errors
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_tx_pkt_chn_state_errors;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkt_undersized_errors
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_tx_pkt_undersized_errors;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkt_oversized_errors
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_tx_pkt_oversized_errors;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkts_rcved_on_lan_interface
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_rx_pkts_rcved_on_lan_interface;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.total_pt_rx_pkts_dropped
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.total_pt_rx_pkts_dropped;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkt_chn_state_errors
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_rx_pkt_chn_state_errors;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkt_undersized_errors
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_rx_pkt_undersized_errors;
                ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkt_oversized_errors
                    = array_ncsi_if[ncsi_priv->mii_index].passthrough_statistics.pt_rx_pkt_oversized_errors;

                DRV_URGENT("%s(): pt_tx_pkts_rcved_on_ncsi_rx_interface=0x%llx\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkts_rcved_on_ncsi_rx_interface);
                DRV_URGENT("%s(): pt_tx_pkts_dropped=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkts_dropped);
                DRV_URGENT("%s(): pt_tx_pkt_chn_state_errors=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkt_chn_state_errors);
                DRV_URGENT("%s(): pt_tx_pkt_undersized_errors=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkt_undersized_errors);
                DRV_URGENT("%s(): pt_tx_pkt_oversized_errors=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_tx_pkt_oversized_errors);
                DRV_URGENT("%s(): pt_rx_pkts_rcved_on_lan_interface=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkts_rcved_on_lan_interface);
                DRV_URGENT("%s(): total_pt_rx_pkts_dropped=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.total_pt_rx_pkts_dropped);
                DRV_URGENT("%s(): pt_rx_pkt_chn_state_errors=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkt_chn_state_errors);
                DRV_URGENT("%s(): pt_rx_pkt_undersized_errors=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkt_undersized_errors);
                DRV_URGENT("%s(): pt_rx_pkt_oversized_errors=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_PASSTHROUGH_STATISTICS *)priv_data)->ncsi_passthrough_statistics.pt_rx_pkt_oversized_errors);
            }

            if (STATUS_OK == err)
            {
                err = 0;
            }
            else if (STATUS_BUSY == err)
            {
                err = (-EBUSY);
            }
            else if (NCSI_RES_CODE_CMD_NORESPONSE == err)
            {
                DRV_URGENT("%s(): No response.\n", __FUNCTION__);
                err = (-ENODATA);
            }
            else
            {
                err = (-EIO);
            }
            break;
        case GET_VERSION_ID_CMD:
            DRV_URGENT("%s(): chnl_id=0x%lx, mii_index=0x%x\n", __FUNCTION__,
                  ncsi_priv->ncsi_chnl_id, ncsi_priv->mii_index);

            /* The private data can't be NULL. It's type is (struct sNCSILIB_Link_Status*). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): GET_LINK_STATUS_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }

            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_Link_Status)))
            {
                DRV_URGENT("Debugfs: %s() GET_LINK_STATUS_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }
            priv_data = ifr->ifr_ifru.ifru_data;

            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
            err = ncsi_cmd_get_version_id_with_rt (&array_ncsi_if[ncsi_priv->mii_index],
                                            ((struct sNCSILIB_VERSION*)priv_data)->retries,
                                            ((struct sNCSILIB_VERSION*)priv_data)->timeout , true);
            ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.reason_code
                = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.reason_code;
            ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.response_code
                = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.response_code;
            if (0 == err)
            {
                memcpy(((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.ncsi_version,
                      array_ncsi_if[ncsi_priv->mii_index].ncsi_version.ncsi_version, 4);
                ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.reserved_alpha2
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.reserved_alpha2;
                memcpy((void*)((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.fw_name,
                      (void*)array_ncsi_if[ncsi_priv->mii_index].ncsi_version.fw_name, 12);
                ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.fw_version
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.fw_version;
                ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.pci_did
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.pci_did;
                ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.pci_vid
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.pci_vid;
                ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.pci_ssid
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.pci_ssid;
                ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.pci_svid
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.pci_svid;
                ((struct sNCSILIB_VERSION*)priv_data)->ncsi_version.iana
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_version.iana;
            }

            if (STATUS_OK == err)
            {
                err = 0;
            }
            else if (STATUS_BUSY == err)
            {
                err = (-EBUSY);
            }
            else if (NCSI_RES_CODE_CMD_NORESPONSE == err)
            {
                DRV_URGENT("%s(): No response.\n", __FUNCTION__);
                err = (-ENODATA);
            }
            else
            {
                err = (-EIO);
            }
            break;
        case GET_CAPABILITIES_CMD:
            DRV_URGENT("%s(): chnl_id=0x%lx, mii_index=0x%x\n", __FUNCTION__,
                  ncsi_priv->ncsi_chnl_id, ncsi_priv->mii_index);

            /* The private data can't be NULL. It's type is (struct sNCSILIB_Link_Status*). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): GET_LINK_STATUS_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }

            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, sizeof(struct sNCSILIB_Link_Status)))
            {
                DRV_URGENT("Debugfs: %s() GET_LINK_STATUS_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }
            priv_data = ifr->ifr_ifru.ifru_data;
            array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;

            err = ncsi_cmd_get_capabilities_with_rt (&array_ncsi_if[ncsi_priv->mii_index],
                                            ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->retries,
                                            ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->timeout, 0 );

            ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.reason_code
                = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.reason_code;
            ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.response_code
                = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.response_code;

            if (0 == err)
            {
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.capabilities_flags
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.capabilities_flags;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.bcast_filter_flag
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.bcast_filter_flag;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.mcast_filter_flag
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.mcast_filter_flag;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.buf_cap
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.buf_cap;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.aen_ctrl
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.aen_ctrl;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.vlan_filter_cnt
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.vlan_filter_cnt;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.mixed_filter_cnt
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.mixed_filter_cnt;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.mcast_filter_cnt
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.mcast_filter_cnt;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.ucast_filter_cnt
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.ucast_filter_cnt;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.reserved2
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.reserved2;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.vlan_mode
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.vlan_mode;
                ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.chnl_cnt
                    = array_ncsi_if[ncsi_priv->mii_index].ncsi_chnl_caps.chnl_cnt;

                DRV_URGENT("%s(): capabilities_flags=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.capabilities_flags);
                DRV_URGENT("%s(): bcast_filter_flag=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.bcast_filter_flag);
                DRV_URGENT("%s(): mcast_filter_flag=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.mcast_filter_flag);
                DRV_URGENT("%s(): buf_cap =0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.buf_cap);
                DRV_URGENT("%s(): aen_ctrl=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.aen_ctrl);
                DRV_URGENT("%s(): vlan_filter_cnt=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.vlan_filter_cnt);
                DRV_URGENT("%s(): mixed_filter_cnt=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.mixed_filter_cnt);
                DRV_URGENT("%s(): mcast_filter_cnt=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.mcast_filter_cnt);
                DRV_URGENT("%s(): ucast_filter_cnt=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.ucast_filter_cnt);
                DRV_URGENT("%s(): reserved2=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.reserved2);
                DRV_URGENT("%s(): vlan_mode=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.vlan_mode);
                DRV_URGENT("%s(): chnl_cnt=0x%x\n", __FUNCTION__,
                    ((struct sNCSILIB_CHNL_CAPABILITIES*)priv_data)->ncsi_channel_capabilities.chnl_cnt);
            }

            if (STATUS_OK == err)
            {
                err = 0;
            }
            else if (STATUS_BUSY == err)
            {
                err = (-EBUSY);
            }
            else if (NCSI_RES_CODE_CMD_NORESPONSE == err)
            {
                DRV_URGENT("%s(): No response.\n", __FUNCTION__);
                err = (-ENODATA);
            }
            else
            {
                err = (-EIO);
            }
            break;
        case SEND_COMMAND:
            err = ncsi_ioctl_sendcommand (dev, ifr, cmd);
            break;
        case GET_NCSI_DRIVER_VERSION_CMD:
            /* The private data can't be NULL. It's type is (struct sNCSILIB_Link_Status*). */
            if (NULL == ifr->ifr_ifru.ifru_data)
            {
                DRV_ERROR("%s(): GET_LINK_STATUS_CMD ifr->ifr_ifru.ifru_data is NULL!\n", __FUNCTION__);
                return -EINVAL;
            }

            if (!access_ok(VERIFY_WRITE, (void *) ifr->ifr_ifru.ifru_data, NCSI_MAX_VERSION_SIZE))
            {
                DRV_URGENT("Debugfs: %s() GET_LINK_STATUS_CMD buffer access error Exit!\n", __FUNCTION__);

                /* error occur */
                return (-EFAULT);
            }
            priv_data = ifr->ifr_ifru.ifru_data;
            copy_to_user ((void*)priv_data,
                    (void*)au8NCSIDriverVersion, sizeof (au8NCSIDriverVersion));
            err = 0;
            break;
        default:
            return -EINVAL;
    }

    switch (cmd)
    {
        case SET_VLAN_CONFIG_CMD:
        case SET_VLAN_ENABLED_CMD:
        case SET_VLAN_DISABLED_CMD:
        case SENDOEM50_CMD:
        case GET_STATISTICS_CMD:
        case GET_LINK_STATUS_CMD:
        case GET_CONTROLLER_PACKET_STATISTICS_CMD:
        case GET_PASSTHROUGH_STATISTICS_CMD:
        case GET_VERSION_ID_CMD:
        case GET_CAPABILITIES_CMD:
        case SEND_COMMAND:
            if( NOT_SUPPORT_HW_ARBITRATION_MODE == array_ncsi_if[ncsi_priv->mii_index].arbitration_mode )
            {
                array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id | 0x1f;
                array_ncsi_if[ncsi_priv->mii_index].dev = ncsi_priv->parent_dev;
                err = ncsi_cmd_deselect_package((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index] , 1 );
                if (err == NCSI_RES_CODE_CMD_NORESPONSE)
                {
                    DRV_INFO("ncsi_cmd_deselect_package() Fail!!\n" );
                }
                array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
            }
            break;
    }

    DRV_URGENT("%s(): return code=0x%x\n", __FUNCTION__,err);
    return (err);
}

/******************************************************************************
*   FUNCTION        :   ncsi_set_mac_address
******************************************************************************/
/**
 *  @brief      NC-SI device set mac address function.
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_set_mac_address(
                                   /** net_device */
                                   struct net_device *dev,

                                   /** MAC address */
                                   void *addr
                               )
{
      if (netif_running(dev))
      {
            printk(KERN_INFO "%s(): Net device is busy now. It can't set MAC address.\n", __FUNCTION__);
            return -EBUSY;
      }

    /** - Set MAC address to following devices:
          1. net_device structure of kernel */
    /** - The first two bytes of addr is address family [AF_xxx],
            the third byte to eighth byte MACaddress */
      memcpy(dev->dev_addr,addr+2,ETH_ALEN);

    return 0;
}

/******************************************************************************
*   FUNCTION        :   ncsi_detect_device
******************************************************************************/
/**
 *  @brief      NC-SI device auto detect function.
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_detect_device(
                                  /** net_device */
                                  struct net_device *dev,

                                  /** MII interface index ID */
                                  int mii_index
                              )
{
    int err = 0;
    int index = 0;
    int package_id;
    int internal_chnl_id;
    int dev_count = 0;
    int result = 0;
    //struct vir_dev_priv *ncsi_priv = NULL ;
    int package_flag[CONFIG_NCSI_MAX_PACKAGE_NUMBER]={0};


    array_ncsi_if[mii_index].dev = dev;
    if (!mii_if[mii_index].init_flag)
    {
        mii_if[mii_index].mii_ncsi_dev_start_id = ncsi_dev_count;
    }

    index = mii_if[mii_index].mii_ncsi_dev_start_id;
    DRV_INFO("ncsi_detect_device(%s,%d): Enter!!\n",dev->name,mii_index);

    total_package_no[mii_index] = 0;


   //Because we have no idea if the packages are selected before or not,
   //so deselect all packages at first
    for (package_id = 0 ; package_id < CONFIG_NCSI_MAX_PACKAGE_NUMBER ; package_id++ )
    {
        array_ncsi_if[mii_index].cmd_iid[package_id] = 1;
        array_ncsi_if[mii_index].chnl_id = CHNL_ID(package_id,0x1F);
        ncsi_cmd_deselect_package((sNCSI_IF_Info *) &array_ncsi_if[mii_index] , 1 );
        DRV_INFO("Deselect the package %x at first \n", package_id);
    }

    //Use Select package command to detect the devices
    for (package_id = 0 ; package_id < CONFIG_NCSI_MAX_PACKAGE_NUMBER ; package_id++ )
    {
        array_ncsi_if[mii_index].chnl_id = CHNL_ID(package_id,0x1F);
        array_ncsi_if[mii_index].ncsi_hardware_arb = CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_DISABLE;
        err = ncsi_cmd_select_package((sNCSI_IF_Info *) &array_ncsi_if[mii_index] , 1 );
        DRV_INFO("new ncsi_detect_device():  now scan package_id = %d \n", package_id);

        if (err != NCSI_RES_CODE_CMD_NORESPONSE)
        {
            // find package
            package_flag[package_id] = 1;
            total_package_no[mii_index] ++;
            DRV_INFO("new ncsi_detect_device(): detect package_id = %d, total_package_no[%d] = %d !!\n", package_id, mii_index, total_package_no[mii_index]);
        }
        else
        {
            DRV_INFO("new ncsi_detect_device():ncsi_cmd_select_package(%d) not found!!\n", package_id);
        }

        ncsi_cmd_deselect_package((sNCSI_IF_Info *) &array_ncsi_if[mii_index], 1);

        /* skip the discovery if we have detected 4 packages */
        if (total_package_no[mii_index] >= 4)
        {
            DRV_INFO("ncsi_detect_device():total_package_no[%d]=%d\n", mii_index, total_package_no[mii_index]);
            break;
        }
    }

    for (package_id = 0 ;
         package_id < CONFIG_NCSI_MAX_PACKAGE_NUMBER;
         package_id++ )
    {
        if (package_flag[package_id] != 1)
        {
            DRV_INFO(" Skip package_id %x \n", package_id);
            continue;
        }

        array_ncsi_if[mii_index].chnl_id = CHNL_ID(package_id,0x1F);

        DRV_INFO("ncsi_detect_device(): ncsi_if.chnl_id is 0x%02x!!\n",array_ncsi_if[mii_index].chnl_id);

        array_ncsi_if[mii_index].ncsi_hardware_arb = CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_DISABLE;
        err = ncsi_cmd_select_package((sNCSI_IF_Info *) &array_ncsi_if[mii_index] , 1 );
        if (err == NCSI_RES_CODE_CMD_NORESPONSE)
        {
            DRV_INFO("ncsi_detect_device():ncsi_cmd_select_package(%d) Fail!!\n", package_id);
            continue;
        }
        else
        {
            DRV_INFO("ncsi_detect_device():ncsi_cmd_select_package(%d) Okay!!\n", package_id);
        }

        result = 1 ;
        //MAX 4 channel in one package
        array_ncsi_if[mii_index].arbitration_mode = SUPPORT_HW_ARBITRATION_MODE ;
        for (internal_chnl_id = 0 ; internal_chnl_id < 4 ; internal_chnl_id ++ )
        {
            array_ncsi_if[mii_index].chnl_id = CHNL_ID(package_id,internal_chnl_id);
            err = ncsi_cmd_clear_initial_state((sNCSI_IF_Info *) &array_ncsi_if[mii_index] , 1 );

            if(err == NCSI_RES_CODE_CMD_OK )
            {
                err = ncsi_cmd_get_capabilities((sNCSI_IF_Info *) &array_ncsi_if[mii_index] , 1 );
                if(err == NCSI_RES_CODE_CMD_OK )
                {
                    DRV_INFO("  array_ncsi_if[mii_index].ncsi_chnl_caps.capabilities_flags = %x \n", array_ncsi_if[mii_index].ncsi_chnl_caps.capabilities_flags);

                    if(( NOT_SUPPORT_HW_ARBITRATION_MODE == (array_ncsi_if[mii_index].ncsi_chnl_caps.capabilities_flags & SUPPORT_HW_ARBITRATION_MODE)) && (total_package_no[mii_index] != 1))
                    {
                        DRV_INFO(" Not Support Hardware Arbitration mode\n");
                        array_ncsi_if[mii_index].arbitration_mode = NOT_SUPPORT_HW_ARBITRATION_MODE ;
                    }
                    else
                    {
                        DRV_INFO(" Support Hardware Arbitration mode !!\n");
                    }

                    err = ncsi_cmd_disable_channel_network_tx((sNCSI_IF_Info *) &array_ncsi_if[mii_index], DISABLE_ERROR_RECOVERY );
                    if(err != 0)
                    {
                        DRV_ERROR("ncsi_detect_device(): ncsi%d ncsi_cmd_disable_channel_network_tx() Fail!!\n", ncsi_dev_count);
                    }

                    DRV_INFO(" call ncsi_new_device for package_id = %x, internal_chnl_id = %x \n",package_id,internal_chnl_id);
                    ncsi_new_device(dev,CHNL_ID(package_id,internal_chnl_id),index,mii_index);
                    dev_count++;
                    index++;
                    if (!mii_if[mii_index].init_flag)
                    {
                        ncsi_dev_count++;

                        /* skip the discovery if we have detected 4 channels */
                        if (ncsi_dev_count >= 4)
                            break;
                    }
                }
                else
                {
                    DRV_INFO("ncsi_detect_device():ncsi_cmd_get_capabilities(%d) Fail!!\n", internal_chnl_id);
                }
            }
        }
        array_ncsi_if[mii_index].chnl_id = CHNL_ID(package_id,0x1F);
        err = ncsi_cmd_deselect_package((sNCSI_IF_Info *) &array_ncsi_if[mii_index] , 1 );

        mii_if[mii_index].mii_ncsi_dev_count = dev_count;
        ncsi_package_status[mii_index][package_id] = 1;
        mii_if[mii_index].init_flag = 1;
    }

    if (array_ncsi_if[mii_index].arbitration_mode == SUPPORT_HW_ARBITRATION_MODE)
    {
        /* send select package command to each package to enable hardware arbitration */
        for (package_id = 0; package_id < CONFIG_NCSI_MAX_PACKAGE_NUMBER; package_id++)
        {
            if(package_flag[package_id] != 1)
            {
                DRV_INFO(" Skip package_id %x \n", package_id);
                continue;
            }

            array_ncsi_if[mii_index].chnl_id = CHNL_ID(package_id, 0x1F);
            array_ncsi_if[mii_index].ncsi_hardware_arb = CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_ENABLE;
            err = ncsi_cmd_select_package((sNCSI_IF_Info *) &array_ncsi_if[mii_index], 1);
        }
    }

    return (result) ;
}

/******************************************************************************
*   FUNCTION        :   ncsi_new_device
******************************************************************************/
/**
 *  @brief      NC-SI device create function.
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_new_device(
                              /** net_device. */
                              struct net_device *edev,

                              /** NC-SI device channel ID. */
                              unsigned long ncsi_chnl_id,

                              /** NC-SI device index ID. */
                              int index,

                              /** MII interface index ID. */
                              int mii_index
                          )
{
    int err;
    struct net_device *ndev;
    struct vir_dev_priv *ncsi_priv;
    u8 device_name[IFNAMSIZ];

    sprintf(device_name, "ncsi%d", index);
    ndev = alloc_netdev(sizeof(struct vir_dev_priv), device_name, ncsi_setup);
    if (!ndev)
    {
        DRV_ERROR("%s(): Can not alloc net device!\n", __FUNCTION__);
        return -ENOMEM;
    }

    ncsi_priv = netdev_priv(ndev);
    ncsi_priv->ncsidev = ndev;
    ncsi_priv->parent_dev = edev;
    ncsi_priv->ncsi_chnl_id = ncsi_chnl_id;
    ncsi_priv->active_tx = 0;
    ncsi_priv->dev_index = index;
    ncsi_priv->mii_index = mii_index;
    /** - VLAN configuration data init */
    ncsi_priv->vlan_config.VID = 0;
    ncsi_priv->vlan_config.UserPriority = 0;
    ncsi_priv->vlan_config.CFI = 0;
    ncsi_priv->vlan_config.FilterSelector = 0;
    ncsi_priv->vlan_config.VMode = VLAN_PROMISCUOUS_MODE;
    ncsi_priv->vlan_config.Enabled = 0;
    memset(ndev->dev_addr, 0, ETH_ALEN);

    err = dev_alloc_name(ndev, ndev->name);
    if (err < 0)
    {
        DRV_ERROR("%s(): Can not alloc net device name!\n", __FUNCTION__);
        goto error;
    }

    err = register_netdevice(ndev);
    if (err)
    {
        DRV_ERROR("%s(): Can not register net device!\n", __FUNCTION__);
        goto error;
    }

    lockdep_set_class(&ndev->_xmit_lock, &ncsi_netdev_xmit_lock_key);

    /* List protected by RTNL */
    list_add_tail_rcu(&ncsi_priv->ncsi_list, &ncsi_devices);

    return 0;

 error:
    DRV_ERROR("%s(): Error!!\n", __FUNCTION__);
    free_netdev(ndev);
    return err;
}

/******************************************************************************
*   FUNCTION        :   ncsi_free_device
******************************************************************************/
/**
 *  @brief      NC-SI device delete and free function.
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static void ncsi_free_device(
                                /** net_device */
                                struct net_device *ndev
                            )
{
    struct vir_dev_priv *ncsi_priv = netdev_priv(ndev);

    if (ndev != NULL)
    {
        DRV_INFO("%s(): try to free dev %s!\n", __FUNCTION__, ndev->name);
        netif_carrier_off(ndev);
        unregister_netdevice(ndev);
        list_del_rcu(&ncsi_priv->ncsi_list);
        DRV_INFO("%s(): Done!\n", __FUNCTION__);
    }
    else
    {
        DRV_ERROR("%s(): dev is NULL!\n", __FUNCTION__);
    }
}

/******************************************************************************
*   FUNCTION        :   ncsi_virtual_open
******************************************************************************/
/**
 *  @brief      open function for NC-SI virtual devices.
 *
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_virtual_open(
                                /** pointer to NC-SI interface. */
                                struct net_device *dev
                            )
{
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    int retry_count = 0;

    if (NOT_SUPPORT_HW_ARBITRATION_MODE == array_ncsi_if[ncsi_priv->mii_index].arbitration_mode)
    {
        printk(KERN_EMERG "ncsi_virtual_open() fail--SW ARBITRATION");
        return -EIO;
    }

    ncsi_priv->channel_enabled = 0;

	while (1)
	{
		if (ncsi_control_flag[ncsi_priv->mii_index] != 1)
		{
			/** - We need to init. NC-SI device. */
		    if (0 == ncsi_open(dev))
		    {
		        ncsi_set_recovery_flag(&array_ncsi_if[ncsi_priv->mii_index], 0, __FUNCTION__, __LINE__);
		        netif_wake_queue(dev);
		        DRV_INFO("%s(): NCSI device %s is up!\n", __FUNCTION__, dev->name);
		        break;
		    }
		    else
		    {
		        netif_stop_queue(dev);
		        DRV_ERROR("%s(): NCSI device %s open fail!, retry_count = %d\n", __FUNCTION__, dev->name, retry_count);
		    }
		}
		else
		{
			DRV_ERROR("%s(): NCSI device %s open fail!, retry_count = %d, ncsi_control_flag=%d\n", __FUNCTION__, dev->name, retry_count, ncsi_control_flag[ncsi_priv->mii_index]);
		}

		retry_count++;

        if (retry_count >= NCSI_ERROR_RETRY_COUNT)
        {
            return -EPERM;
        }
        else
		{
			msleep(NCSI_ERROR_RETRY_DELAY * 5);
		}
	}

    ncsi_priv->active = 1;

    return 0;
}

/******************************************************************************
*   FUNCTION        :   ncsi_virtual_close
******************************************************************************/
/**
 *  @brief      close function for NC-SI virtual devices.
 *
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_virtual_close(
                               /** pointer to NC-SI interface. */
                               struct net_device *dev
                             )
{
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    int err = 0;
    int retry_count = 0;

    if (u8TraceTxEnable)
        printk("C-0\n");

    ncsi_priv->sync_tx = 1;

    if (NOT_SUPPORT_HW_ARBITRATION_MODE == array_ncsi_if[ncsi_priv->mii_index].arbitration_mode)
    {
        printk(KERN_EMERG"ncsi_virtual_close() fail--SW ARBITRATION");

        if (u8TraceTxEnable)
            printk("C-1\n");

        ncsi_priv->sync_tx = 0;
        return -EIO;
    }

	while (1)
	{
		if (ncsi_control_flag[ncsi_priv->mii_index] != 1)
		{
		    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
		    err = ncsi_cmd_disable_channel_network_tx((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index], ENABLE_ERROR_RECOVERY);
		    if (err != 0)
		    {
		        DRV_ERROR("ncsi_virtual_close(): %s ncsi_cmd_disable_channel_network_tx() Fail!!\n", dev->name);

		        if (u8TraceTxEnable)
		            printk("C-2\n");
		    }
			else
			{
				ncsi_priv->active_tx = 0;

			    array_ncsi_if[ncsi_priv->mii_index].chnl_id = ncsi_priv->ncsi_chnl_id;
			    err == ncsi_cmd_disable_channel((sNCSI_IF_Info *) &array_ncsi_if[ncsi_priv->mii_index]);
			    if (err != 0)
			    {
			        DRV_ERROR("ncsi_virtual_close(): %s ncsi_cmd_disable_channel_network_tx() Fail!!\n", dev->name);

			        if (u8TraceTxEnable)
			            printk("C-3\n");
			    }
				else
			    {
					ncsi_priv->channel_enabled = 0;

					DRV_INFO("%s(): NCSI device %s is down!\n", __FUNCTION__, dev->name);
					break;
			    }
			}
		}
		else
		{
			DRV_ERROR("%s(): NCSI device %s close fail!, retry_count = %d, ncsi_control_flag=%d\n", __FUNCTION__, dev->name, retry_count, ncsi_control_flag[ncsi_priv->mii_index]);
		}

		retry_count++;

        if (retry_count >= NCSI_ERROR_RETRY_COUNT)
        {
	        if (u8TraceTxEnable)
	            printk("C-4\n");

			ncsi_priv->sync_tx = 0;
            return -EPERM;
        }
        else
        {
			msleep(NCSI_ERROR_RETRY_DELAY * 5);
		}
	}

	if (err != 0)
	{
		if (u8TraceTxEnable)
			printk("C-5\n");

		ncsi_priv->sync_tx = 0;
		return err;
	}
	else
	{
	    netif_stop_queue(dev);
		ncsi_priv->active = 0;

		if (u8TraceTxEnable)
			printk("C-6\n");

		ncsi_priv->sync_tx = 0;
		return 0;
	}
}

/******************************************************************************
*   FUNCTION        :   ncsi_virtual_xmit
******************************************************************************/
/**
 *  @brief      Transmit function for NC-SI virtual devices.
 *
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int ncsi_virtual_xmit(
                              /** pointer to transmit socket buffer. */
                              struct sk_buff *skb,

                              /** pointer to nic device. */
                              struct net_device *dev
                            )
{
    struct vir_dev_priv *ncsi_priv = netdev_priv(dev);
    unsigned int len = skb->len;
    int ret;
    int mii_id = ncsi_get_mii_index_with_name(ncsi_priv->parent_dev->name);

    if( NOT_SUPPORT_HW_ARBITRATION_MODE == array_ncsi_if[ncsi_priv->mii_index].arbitration_mode)
    {
        printk(KERN_EMERG"ncsi_virtual_close() fail--SW ARBITRATION");
        return -EIO ;
    }

	netif_stop_queue(dev);

    /** - Check NC-SI parent device's status. */
    if (!netif_carrier_ok(ncsi_priv->parent_dev) || !netif_running(ncsi_priv->parent_dev))
    {
       ncsi_set_recovery_flag(&array_ncsi_if[mii_id], 1, __FUNCTION__, __LINE__);
       DRV_ERROR("%s(): ncsi_virtual_xmit() error! %s is down!\n", __FUNCTION__, ncsi_priv->parent_dev->name);
       return NETDEV_TX_BUSY;
    }

    if( array_ncsi_if[mii_id].error_recovery_enable_flag == 1 )
    {
        DRV_ERROR("%s(): in error_recovery mode!\n", __FUNCTION__);
        return NETDEV_TX_BUSY;

    }

    #ifdef DEBUG_SHOW_SEND_NCSI_CONTROL_PACKET
    printk( KERN_EMERG"ncsi_virtual_xmit(1) %s  enable=%d, active=%d, active_tx=%d \n" , dev->name,   ncsi_priv->channel_enabled , ncsi_priv->active , ncsi_priv->active_tx );
    #endif

    /** - Check NC-SI device's tx status and switch internel channel. */
    ncsi_tx_handler(dev);

    dev->trans_start = jiffies;
    skb->dev = ncsi_priv->parent_dev;

    /** - Transmit skb packet buff to NCSI parent device. */
    ret = dev_queue_xmit(skb);

    if (likely(ret == NET_XMIT_SUCCESS))
    {
        /** - Transmit ok. */
        dev->stats.tx_packets++;
        dev->stats.tx_bytes += len;
        DRV_INFO("%s(): dev_queue_xmit ok! [%s|%s]\n", __FUNCTION__, dev->name, skb->dev->name);
    }
    else
    {
        /** - Transmit fail. */
        dev->stats.tx_errors++;
        dev->stats.tx_aborted_errors++;
        if(skb)
        {
            kfree_skb(skb);
            skb = NULL;
        }
        DRV_ERROR("%s(): dev_queue_xmit fail!\n", __FUNCTION__);
    }
    netif_wake_queue(dev);
    return NETDEV_TX_OK;
}

/******************************************************************************
*   FUNCTION        :   ncsi_proto_init
******************************************************************************/
/**
 *  @brief      Init function for ncsi protocol.
 *
 *
 *  @return     0: Success -1: Fail.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static int __init ncsi_proto_init(
                                     /** void. */
                                     void
                                 )
{
    int result = 0;
    int mii_index = 0;

    for (mii_index=0; mii_index < NCSI_MAX_MII_NUM ; mii_index++)
    {
        skb_queue_head_init((struct sk_buff_head *)&array_ncsi_if[mii_index].ncsi_queue);
        init_waitqueue_head((wait_queue_head_t *)&array_ncsi_if[mii_index].inq);
        ncsi_init((sNCSI_IF_Info *) &array_ncsi_if[mii_index]);
        array_ncsi_if[mii_index].chnl_id = 0;
        array_ncsi_if[mii_index].active_chnl_id = 0;
        ncsi_set_recovery_flag(&array_ncsi_if[mii_index], 0, __FUNCTION__, __LINE__);
        array_ncsi_if[mii_index].NCSI_transition_flag = 0;
        spin_lock_init(&array_ncsi_if[mii_index].lock);
        sema_init(&ncsi_control_sem[mii_index], 1);
    }

    /** - Register netdevice notifier. */
    result = register_netdevice_notifier(&ncsi_netdev_notifier);
    if (result)
    {
        printk(KERN_ERR "%s(): Fail to register netdevice notifier.\n", __FUNCTION__);
        return result;
    }

    /** - Create debugfs files. */
    aess_debugfs_default_create("ncsi_protocol", NULL, 0, 0);
    aess_debugfs_create_file("ncsi_protocol","u8DumpMsgSwitch",(u8 *)&u8dumpmsgswitch, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","u32RecoveryCountPre",(u32 *)&u32RecoveryCountPre, DBG_TYPE32, 0);
    aess_debugfs_create_file("ncsi_protocol","u32RecoveryCount",(u32 *)&u32RecoveryCount, DBG_TYPE32, 0);
    aess_debugfs_create_file("ncsi_protocol","u8RecoveryEnable0",(u8 *)&array_ncsi_if[0].error_recovery_enable_flag, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","u8RecoveryEnable1",(u8 *)&array_ncsi_if[1].error_recovery_enable_flag, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","u8RecoveryEnable2",(u8 *)&array_ncsi_if[2].error_recovery_enable_flag, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","ScanComplete",(u8 *)&u8ScanComplete, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","TraceRecovery",(u8 *)&u8TraceRecovery, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","TraceTxEnable",(u8 *)&u8TraceTxEnable, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","TraceTxFun",(u8 *)&u8TraceTxFun, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","TraceMsgTimeout",(u8 *)&u8TraceMsgTimeout, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","ForceTXEnable",(u8 *)&u8ForceTXEnable, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","TraceAENEnable",(u8 *)&u8TraceAENEnable, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","DelayMethod",(u8 *)&u8DelayMethod, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","TraceSendMessage",(u8 *)&u8TraceSendMessage, DBG_TYPE8, 0);
    aess_debugfs_create_file("ncsi_protocol","TraceNotifier",(u8 *)&u8TraceNotifier, DBG_TYPE8, 0);

    /** - Register sysfs. */
    //result = driver_register(&ncsi_device_driver);
    result = platform_driver_register(&ncsi_device_driver);
    if (result)
    {
        printk(KERN_ERR "%s(): can't register sysfs.\n", __FUNCTION__);
        return result;
    }

    /** - Create sysfs files. */
    result = driver_create_file(&(ncsi_device_driver.driver), &drv_attr_ncsi_device_info);
    if (result)
    {
        printk(KERN_ERR "%s(): Fail to create sysfs attrb.\n", __FUNCTION__);
        return result;
    }

    /** - Create sysfs files. */
    result = driver_create_file(&(ncsi_device_driver.driver), &drv_attr_ncsi_device_number);
    if (result)
    {
        printk(KERN_ERR "%s(): Fail to create sysfs attrb.\n", __FUNCTION__);
        return result;
    }

      return 0;
}

/******************************************************************************
*   FUNCTION        :   ncsi_proto_exit
******************************************************************************/
/**
 *  @brief      NC-SI protocol exit function.
 *
 *
 *  @return     0: Success.
 *
 *  @dependency None.
 *
 *  @limitation None.
 *
 *  @warning    None.
 *
 *  @note       None.
 *
 *  @internal   Function Type: static function\n
 *
 *****************************************************************************/
static void __exit ncsi_proto_exit(
                                    /** void. */
                                    void
                                  )
{
    int mii_index = 0;

    DRV_INFO("ncsi_proto_exit() called\n");

    /** - Remove sysfs files. */
    driver_remove_file(&(ncsi_device_driver.driver), &drv_attr_ncsi_device_info);
    driver_remove_file(&(ncsi_device_driver.driver), &drv_attr_ncsi_device_number);

    /** - Unregister sysfs. */
    //driver_unregister(&(ncsi_device_driver));
    platform_driver_unregister(&(ncsi_device_driver));

    for (mii_index=0; mii_index < NCSI_MAX_MII_NUM ; mii_index++)
    {

        /** - To stop ncsi_command_handler kernel thread. */
        if(array_ncsi_if[mii_index].cmd_handler_pid != 0)
        {
             array_ncsi_if[mii_index].cmd_exit_flag = 1;
			#if LINUX_VERSION_CODE > KERNEL_VERSION(3, 11, 0)
				kthread_stop(array_ncsi_if[mii_index].thread_task);
			#elif LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 23)
				kill_proc_info( SIGTERM ,(struct siginfo *)1 ,array_ncsi_if[mii_index].cmd_handler_pid );
			#else
				kill_proc(array_ncsi_if[mii_index].cmd_handler_pid, SIGTERM, 1);
			#endif
             wait_for_completion((struct completion *) &array_ncsi_if[mii_index].cmd_exited);
             skb_queue_purge((struct sk_buff_head *) &array_ncsi_if[mii_index].ncsi_res_quene.ncsi_queue);
        }

        array_ncsi_if[mii_index].cmd_handler_pid = 0;
        array_ncsi_if[mii_index].NCSI_transition_flag = 0;

        /** - To empty ncsi command queue list. */
        skb_queue_purge((struct sk_buff_head *) &array_ncsi_if[mii_index].ncsi_queue);
    }

    /** - Unregister netdevice notifier. */
    unregister_netdevice_notifier(&ncsi_netdev_notifier);

    /** - Remove debugfs files. */
    aess_debugfs_remove("ncsi_protocol", NULL, 0, 0);
}

module_init(ncsi_proto_init);
module_exit(ncsi_proto_exit);
MODULE_LICENSE("GPL v2");
MODULE_VERSION(au8NCSIDriverVersion);
MODULE_DESCRIPTION("AESS NC-SI PROTOCOL");

/* End of code */
