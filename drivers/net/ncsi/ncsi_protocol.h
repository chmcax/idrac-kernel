/*
*
* (C) 2009-2015 Avocent Corporation
*
* This file is subject to the terms and conditions of the GNU
* General Public License Version 2. This program is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
* PURPOSE. See the GNU General Public License Version 2 for more details.
*

 *----------------------------------------------------------------------------\n
 *  MODULES     linux driver \n
 *----------------------------------------------------------------------------\n
 *  @file   ncsi_protocol.h
 *  @brief  This is the header file for ncsi protocol
 *
 *  @internal
 *  $RCSfile:  $
 *  $Revision:  $
 *  $Date:  $
 *  $Author:      $
 *  $Source:   $
 *  $Name:  $
 *----------------------------------------------------------------------------*/
/*
 * $RCSfile: ncsi_protocol.h,v $
 * $Revision: 1.3 $
 * $Date: 2008/09/15 09:13:56 $
 * $Author: sonata $
 *
 * NCSI protocol header file.
 *
 * Copyright (C) 2006 Avocent Corp.
 *
 * This file is subject to the terms and conditions of the GNU
 * General Public License. This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 */

#ifndef NCSI_PROTOCOL_H
#define NCSI_PROTOCOL_H
#include <linux/mii.h>

#include <linux/version.h>
#include <linux/semaphore.h>
#include <aess_drivers/aess_ncsi_protocol_common.h>

/* NCSI Hardware Configuration */
#define NCSI_MAX_MII_NUM      3
#define NCSI_MAX_DEVICE_NUM   8
#define BNCSI_LINK_UP         0x01
#define NCSI_ETHER_TYPE       0x88F8
#define NCSI_AEN_PACKET_IID   0
#define NCSI_RES_PACKET_MASK  0x80
#define MAX_CMD_RETRY_COUNT   2
#define MAX_CMD_TIMEOUT       (1000 * HZ / 1000)    /* 1000ms */
#define NCSI_MC_ID            0x0
#define NCSI_HEADER_REVISION  0x1

#define NCSI_ERROR_RETRY_DELAY 10
#define NCSI_ERROR_RETRY_COUNT 5

#define VLAN_EXPLICIT_MODE    0x1
#define VLAN_MIXED_MODE       0x2
#define VLAN_PROMISCUOUS_MODE 0x3
#define NCSI_RES_PACKET_QUEUE_LEN           16
#define NCSI_CHNL_ID_UNAVAILABLE            0xFF
#define NCSI_ENTIRE_PKG_INTERNAL_CHNL_ID    0x1F
#define CHNL_ID(pkgid, internel_chnlid)    \
    (((pkgid & 0x7) << 5) | (internel_chnlid & 0x1F))
#define PKG_ID(chnl_id)             ((chnl_id & 0xE0) >> 5)
#define INTERNAL_CHNL_ID(chnl_id)   (chnl_id & 0x1F)

/* NCSI AEN */
#define AEN_LINK_STATUS_CHANGE  0x0
#define AEN_RECONFIG_REQUIRED   0x1
#define AEN_OS_DRIVER_CHANGE    0x2

/* NCSI command */
#define CMD_CLEAR_INITIAL_STATE                0x00
#define CMD_SELECT_PACKAGE                     0x01
#define CMD_DESELECT_PACKAGE                   0x02
#define CMD_ENABLE_CHANNEL                     0x03
#define CMD_DISABLE_CHANNEL                    0x04
#define CMD_RESET_CHANNEL                      0x05
#define CMD_ENABLE_CHANNEL_NETWORK_TX          0x06
#define CMD_DISABLE_CHANNEL_NETWORK_TX         0x07
#define CMD_AEN_ENABLE                         0x08
#define CMD_SET_LINK                           0x09
#define CMD_GET_LINK_STATUS                    0x0A
#define CMD_SET_VLAN_FILTERS                   0x0B
#define CMD_ENABLE_VLAN                        0x0C
#define CMD_DISABLE_VLAN                       0x0D
#define CMD_SET_MAC_ADDRESS                    0x0E
#define CMD_ENABLE_BROADCAST_FILTERING         0x10
#define CMD_DISABLE_BROADCAST_FILTERING        0x11
#define CMD_ENABLE_GLOBAL_MULTICAST_FILTERING  0x12
#define CMD_DISABLE_GLOBAL_MULTICAST_FILTERING 0x13
#define CMD_SET_NCSI_FLOW_CONTROL              0x14
#define CMD_GET_VERSION_ID                     0x15
#define CMD_GET_CAPABILITIES                   0x16
#define CMD_GET_PARAMETERS                     0x17
#define CMD_GET_CONTROLLER_PACKET_STATISTICS   0x18
#define CMD_GET_NCSI_STATISTICS                0x19
#define CMD_GET_NCSI_PASSTHROUGH_STATISTICS    0x1A
#define CMD_OEM_COMMAND                        0x50

/* NCSI command payload length */
#define CMD_CLEAR_INITIAL_STATE_PAYLOAD_LEN                   0
#define CMD_SELECT_PACKAGE_PAYLOAD_LEN                        4
#define CMD_DESELECT_PACKAGE_PAYLOAD_LEN                      0
#define CMD_ENABLE_CHANNEL_PAYLOAD_LEN                        0
#define CMD_DISABLE_CHANNEL_PAYLOAD_LEN                       4
#define CMD_RESET_CHANNEL_PAYLOAD_LEN                         4
#define CMD_ENABLE_CHANNEL_NETWORK_TX_PAYLOAD_LEN             0
#define CMD_DISABLE_CHANNEL_NETWORK_TX_PAYLOAD_LEN            0
#define CMD_AEN_ENABLE_PAYLOAD_LEN                            8
#define CMD_SET_LINK_PAYLOAD_LEN                              8
#define CMD_GET_LINK_STATUS_PAYLOAD_LEN                       0
#define CMD_SET_VLAN_FILTERS_PAYLOAD_LEN                      8
#define CMD_ENABLE_VLAN_PAYLOAD_LEN                           4
#define CMD_DISABLE_VLAN_PAYLOAD_LEN                          0
#define CMD_SET_MAC_ADDRESS_PAYLOAD_LEN                       8
#define CMD_ENABLE_BROADCAST_FILTERING_PAYLOAD_LEN            4
#define CMD_DISABLE_BROADCAST_FILTERING_PAYLOAD_LEN           0
#define CMD_ENABLE_GLOBAL_MULTICAST_FILTERING_PAYLOAD_LEN     4
#define CMD_DISABLE_GLOBAL_MULTICAST_FILTERING_PAYLOAD_LEN    0
#define CMD_SET_NCSI_FLOW_CONTROL_PAYLOAD_LEN                 4
#define CMD_GET_VERSION_ID_PAYLOAD_LEN                        0
#define CMD_GET_CAPABILITIES_PAYLOAD_LEN                      0
#define CMD_GET_PARAMETERS_PAYLOAD_LEN                        0
#define CMD_GET_CONTROLLER_PACKET_STATISTICS_PAYLOAD_LEN      0
#define CMD_GET_NCSI_STATISTICS_PAYLOAD_LEN                   0
#define CMD_GET_NCSI_PASSTHROUGH_STATISTICS_PAYLOAD_LEN       0

/* NCSI command payload data */
/* SELECT_PACKAGE command payload */
#define CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_ENABLE    0x00
#define CMD_SELECT_PACKAGE_PAYLOAD_HW_ARB_DISABLE   0x01

/* DISABLE_CHANNEL command payload */
#define CMD_DISABLE_CHANNEL_PAYLOAD_KEEP_LINK_UP    0x0
#define CMD_DISABLE_CHANNEL_PAYLOAD_ALLOW_LINK_DOWN 0x1

/* SET_LINK command payload */
#define CMD_SET_LINK_PAYLOAD_AUTO_NEGO_DISABLE              0x0
#define CMD_SET_LINK_PAYLOAD_AUTO_NEGO_ENABLE               0x1
#define CMD_SET_LINK_PAYLOAD_LINK_SPEED_10M                 (0x1<<1)
#define CMD_SET_LINK_PAYLOAD_LINK_SPEED_100M                (0x1<<2)
#define CMD_SET_LINK_PAYLOAD_LINK_SPEED_1G                  (0x1<<3)
#define CMD_SET_LINK_PAYLOAD_LINK_SPEED_10G                 (0x1<<4)
#define CMD_SET_LINK_PAYLOAD_HALF_DUPLEX                    (0x1<<8)
#define CMD_SET_LINK_PAYLOAD_FULL_DUPLEX                    (0x1<<9)
#define CMD_SET_LINK_PAYLOAD_PAUSE_CAPS_DISABLE             0x0
#define CMD_SET_LINK_PAYLOAD_PAUSE_CAPS_ENABLE              0x1
#define CMD_SET_LINK_PAYLOAD_ASYMMETRIC_PAUSE_CAPS_DISABLE  0x0
#define CMD_SET_LINK_PAYLOAD_ASYMMETRIC_PAUSE_CAPS_ENABLE   0x1
#define CMD_SET_LINK_PAYLOAD_OEM_SETTINGS_DISABLE           0x0
#define CMD_SET_LINK_PAYLOAD_OEM_SETTINGS_ENABLE            0x1

/* SET_MAC_ADDRESS command payload */
#define CMD_SET_MAC_ADDRESS_PAYLOAD_ADDR_TYPE_UNICAST       0x00
#define CMD_SET_MAC_ADDRESS_PAYLOAD_ADDR_TYPE_MULTICAST     0x80
#define CMD_SET_MAC_ADDRESS_PAYLOAD_DISABLE_MAC_ADDR_FILTER 0x00
#define CMD_SET_MAC_ADDRESS_PAYLOAD_ENABLE_MAC_ADDR_FILTER  0x01

/* ENABLE_BROADCAST command payload */
#define CMD_ENABLE_BROADCAST_PAYLOAD_ARP_ENABLE         0x1
#define CMD_ENABLE_BROADCAST_PAYLOAD_ARP_DISABLE        0x0
#define CMD_ENABLE_BROADCAST_PAYLOAD_DHCP_ENABLE        0x1
#define CMD_ENABLE_BROADCAST_PAYLOAD_DHCP_DISABLE       0x0
#define CMD_ENABLE_BROADCAST_PAYLOAD_NETBIOS_ENABLE     0x1
#define CMD_ENABLE_BROADCAST_PAYLOAD_NETBIOS_DISABLE    0x0

/* SET_VLAN_FILTERS command payload */
#define CMD_SET_VLAN_FILTERS_PAYLOAD_DISABLE_FILTER     0x0
#define CMD_SET_VLAN_FILTERS_PAYLOAD_ENABLE_FILTER      0x1

/* ENABLE_VLAN command payload */
#define CMD_ENABLE_VLAN_PAYLOAD_VLAN_ONLY               0x1
#define CMD_ENABLE_VLAN_PAYLOAD_VLAN_NONVLAN            0x2
#define CMD_ENABLE_VLAN_PAYLOAD_ANYVLAN_NONVLAN         0x3

/* SET_NCSI_FLOW_CONTROL command payload */
#define CMD_SET_NCSI_FLOW_CONTROL_DISABLE               0x0
#define CMD_SET_NCSI_FLOW_CONTROL_TX_ENABLE             0x1
#define CMD_SET_NCSI_FLOW_CONTROL_RX_ENABLE             0x2
#define CMD_SET_NCSI_FLOW_CONTROL_TX_RX_ENABLE          0x3

/* CMD_GET_VERSION_ID command */
#define FIRMWARE_NAME_STRING_LENGTH   12

/* GET PARAMETERS command payload */
#define CMD_GET_PARAMETERS_BROADCAST_PACKET_FILTER_STATUS           0x01
#define CMD_GET_PARAMETERS_CHANNEL_ENABLED                          0x02
#define CMD_GET_PARAMETERS_CHANNEL_NETWORK_TX_ENABLED               0x04
#define CMD_GET_PARAMETERS_GLOBAL_MULTICAST_PACKET_FILTER_STATUS    0x08

/* AEN_ENABLE command payload */
#define CMD_AEN_ENABLE_PAYLOAD_LINK_STATUS_CHANGE_DISABLE   0x0
#define CMD_AEN_ENABLE_PAYLOAD_LINK_STATUS_CHANGE_ENABLE    0x1
#define CMD_AEN_ENABLE_PAYLOAD_RECONFIG_REQUIRED_DISABLE    0x0
#define CMD_AEN_ENABLE_PAYLOAD_RECONFIG_REQUIRED_ENABLE     0x1
#define CMD_AEN_ENABLE_PAYLOAD_OS_DRIVER_CHANGE_DISABLE     0x0
#define CMD_AEN_ENABLE_PAYLOAD_OS_DRIVER_CHANGE_ENABLE      0x1

/* TBD: GET_LINK_STATUS command payload */



/* NCSI standard response codes */
#define NCSI_RES_CODE_CMD_OK            0x00
#define NCSI_RES_CODE_CMD_FAILED        0x01
#define NCSI_RES_CODE_CMD_INVALID       0x02
#define NCSI_RES_CODE_CMD_UNSUPPORTED   0x03
#define NCSI_RES_CODE_CMD_NORESPONSE    0xFF

/* NCSI standard reason codes */
#define NCSI_REASON_CODE_NO_ERROR                   0x00
#define NCSI_REASON_CODE_INTERFACE_INIT_REQUIRED    0x01
#define NCSI_REASON_CODE_PARAMETER_INVALID          0x02
#define NCSI_REASON_CODE_CHNL_NOT_READY             0x03
#define NCSI_REASON_CODE_PKG_NOT_READY              0x04
#define NCSI_REASON_CODE_INVALID_PAYLOAD_LENGTH     0x05
#define NCSI_REASON_CODE_UNKNOWN_CMD_TYPE           0x7FFF


/* NCSI Specific reason code */
#define NCSI_SET_MAC_ADDRESS_REASON_CODE_MAC_ADDR_IS_ZERO               0x04
#define NCSI_SET_VLAN_FILTERS_REASON_CODE_VLAN_TAG_IS_INVAILD           0x04
#define NCSI_SET_LINK_REASON_CODE_SET_LINK_HOST_OS_DRIVER_CONFLICT      0x05
#define NCSI_SET_LINK_REASON_CODE_SET_LINK_MEDIA_CONFLICT               0x06
#define NCSI_SET_LINK_REASON_CODE_SET_LINK_PARAMETER_CONFLICT           0x07
#define NCSI_SET_LINK_REASON_CODE_SET_LINK_POWER_MODE_CONFLICT          0x08
#define NCSI_SET_LINK_REASON_CODE_SET_LINK_LINK_SPEED_CONFLICT          0x09
#define NCSI_SET_LINK_REASON_CODE_SET_LINK_CMD_FAIL_HW_ACCESS_ERR       0x0A
#define NCSI_GET_LINK_STATUS_REASON_CODE_LINK_CMD_FAIL_HW_ACCESS_ERR    0x0A

#define IF_NONE      0x0
#define IF_RMII_PHY  0x01
#define IF_MII_PHY   0x02
#define IF_GMII_PHY  0x04
#define IF_NCSI      0x10
#define IF_RMII_PHY_NCSI  (IF_RMII_PHY|IF_NCSI)
#define IF_MII_PHY_NCSI   (IF_MII_PHY|IF_NCSI)
#define IF_GMII_PHY_NCSI  (IF_GMII_PHY|IF_NCSI)

#define MAX_NCSI_ERR_RECOVERY_TIMES 10

/* The type of ump packet queue state by UMP send command */
enum ncsi_queue_type {
    NCSI_RES_QUEUE,     /* NCSI response packet queue */
};

/******************************************************************************
*   STRUCT      :   nic_interface_info
******************************************************************************/
/**
 *  @brief   NIC interface mode structure definition
 *
 *****************************************************************************/
struct nic_interface_info {
    /** NIC interface.
        Possible values: NONE(0x0)|RMII_PHY(0x01)|MII_PHY(0x02)|GMII_PHY(0x04)
                         |NCSI(0x10)|NCSI+RMII(0x11)|NCSI+MII(0x12)
                         |NCSI+GMII(0x14)*/
    u8 interface_mode;
    /** Reserved */
    u8 u8Reserved;
    /**Reserved*/
    u16 u16Reserved;
};

/******************************************************************************
*   STRUCT      :   nic_info_private
******************************************************************************/
/**
 *  @brief   NIC device private data structure definition
 *
 *****************************************************************************/
struct nic_info_private {
    struct nic_interface_info interface_info;
};

/******************************************************************************
*   STRUCT      :   ncsi_vlan_configs
******************************************************************************/
/**
 *  @brief   NCSI device VLAN configuration structure definition
 *
 *****************************************************************************/
struct ncsi_vlan_configs {
    int VID;            /** - VLAN ID (12bits, Zeros = no VLAN)  */
    int UserPriority;   /** - User Priority (3bits, typical value = 000b) */
    int CFI;            /** - CFI (1bit, Canonical Format Indicator = 0b) */
    int FilterSelector; /** - Filter Selector */
    int VMode;          /** - VLAN Mode (1byte, VLAN only           = 0x01
                              VLAN + non-VLAN     = 0x02
                              Any VLAN + non-VLAN = 0x03 )*/
    int Enabled;        /** - disabled = 0
                              enabled  = 1 */
};

/******************************************************************************
*   STRUCT      :   sNCSI_HDR
******************************************************************************/
/**
 *  @brief   NCSI packet header definition
 *
 *****************************************************************************/
struct sNCSI_HDR
{
    /** management controller ID */
    u8  mc_id;
    /** Header Revision */
    u8  hdr_rev;
    /** reserved field */
    u8  reserved0;
    /** command instance id */
    u8  cmd_iid;
    /** command type number */
    u8  cmd;
    /** channel id */
    u8  chnl_id;
    /** the lower 4 bits is the high nibble of the payload length */
    u8  payload_len_h_nibble;
    /** payload length */
    u8  payload_len;
    /** reserved */
    u32 reserved2;
    /** reserved */
    u32 reserved3;
};

#define NCSI_HLEN            (sizeof(struct sNCSI_HDR))        /* 16 bytes */
#define NCSI_CHECKSUM_LEN    (4)

/******************************************************************************
*   STRUCT      :   sNCSI_RES_HDR
******************************************************************************/
/**
 *  @brief   NCSI packet response header definition
 *
 *****************************************************************************/
struct sNCSI_RES_HDR
{
    /** ncsi command header */
    struct sNCSI_HDR ncsi_hdr;
    /** reponse code */
    u16 response_code;
    /** reason code */
    u16 reason_code;
    /** Payload data start pointer. Express the checksum if no payload data */
    u8*  payload_data;
};

/******************************************************************************
*   STRUCT      :   sNCSI_AEN_HDR
******************************************************************************/
/**
 *  @brief   NCSI AEN packet header definition
 *
 *****************************************************************************/
struct sNCSI_AEN_HDR
{
    /** ncsi packet header */
    struct sNCSI_HDR ncsi_hdr;
    /** reserved */
    u16 reserved1;
    /** reserved */
    u8 reserved2;
    /** AEN type */
    u8 aen_type;
};

/******************************************************************************
*   STRUCT      :   ncsi_aen_link_hdr
******************************************************************************/
/**
 *  @brief   Link status change AEN
 *
 *****************************************************************************/
struct ncsi_aen_link_hdr
{
    /** AEN header */
    struct sNCSI_AEN_HDR aenh;
    /** link status field */
    u32    link_stat;
    /** oen link status field */
    u32    oem_link_stat;
};

/******************************************************************************
*   STRUCT      :   skb_queue
******************************************************************************/
/**
 *  @brief   skb queue for ncsi response packet
 *
 *****************************************************************************/
struct skb_queue
{
    /** response skb queue */
    struct sk_buff_head ncsi_queue;
    /** semaphore for response queue */
    struct semaphore sem;
    /** wait queue */
    wait_queue_head_t inq;
};


/******************************************************************************
*   STRUCT      :   sNCSI_MII_INFO
******************************************************************************/
/**
 *  @brief   NCSI MII Information
 *
 *****************************************************************************/
typedef struct
{
    /** mii device name */
    u8 name[IFNAMSIZ];
    /** MII interface index */
    u16 mii_interface_index;
    /** To store NC-SI device count on mii interface */
    u16 mii_ncsi_dev_count;
    /** To store NC-SI device start id on mii interface */
    u16 mii_ncsi_dev_start_id;
    /** To store mii interface NC-SI device init flag*/
    u16 init_flag;
} sNCSI_MII_INFO;

/******************************************************************************
*   STRUCT      :   sNCSI_MAC_INFO
******************************************************************************/
/**
 *  @brief   NCSI MAC Information
 *
 *****************************************************************************/
struct sNCSI_MAC_INFO
{
    /** store the MAC address */
    u8 mac_addr[6];
    /** number of the MAC address filters */
    u8 mac_num;
    /** mac address type: 0x0: unicast MAC address, 0x1: multicast MAC address */
    u8 addr_type_enable;
};

/******************************************************************************
*   STRUCT      :   sNCSI_MAC_INFO
******************************************************************************/
/**
 *  @brief   NCSI VLAN Information
 *
 *****************************************************************************/
struct sNCSI_VLAN_INFO
{
    /** reserved */
    u16 reserved1;
    /** VLAN ID is used for VLAN filtering */
    u16 vlan_id;
    /** reserved */
    u16 reserved2;
    /** Select the VLAN filter number */
    u16 selector_enable;
};

/******************************************************************************
*   STRUCT      :   sNCSI_SET_LINK
******************************************************************************/
/**
 *  @brief   NCSI Set Link Information
 *
 *****************************************************************************/
struct sNCSI_SET_LINK
{
    /** link setting */
    u32 link_settings;
    /** oem link setting */
    u32 oem_link_settings;
};

/******************************************************************************
*   STRUCT      :   sNCSI_GET_LINK_STATUS
******************************************************************************/
/**
 *  @brief   NCSI Get Link Status Information
 *
 *****************************************************************************/
struct sNCSI_GET_LINK_STATUS
{
    /* Link Status */
    u32 link_status;
    /* Other Indications */
    u32 other_indications;
    /* OEM Link Status */
    u32 oem_link_status;
};


/******************************************************************************
*   STRUCT      :   sNCSI_CHNL_PARAMETER
******************************************************************************/
/**
 *  @brief   NCSI Channel Parameter
 *
 *****************************************************************************/
struct sNCSI_CHNL_PARAMETER
{
    /** the number of MAC addresses supported by the channel and Mac Address Flags */
    u32 mac_addr_cnt_flag;
    /** VLAN Tag count field and VLAN Tag Flags field */
    u32 vlan_tag_cnt_flag;
    /** link setting value */
    struct sNCSI_SET_LINK link_setting;
    /** broadcast packet filter settings. */
    u32 bcast_filter_flag;
    /** Configuration flags */
    u32 config_flag;
    /** vlan mode */
    u8 vlan_mode;
    /** flow control enable field */
    u8 flow_control_enable;
    /** reserved */
    u16 reserved;
    /** aen control flag */
    u32 aen_ctrl;
};

/******************************************************************************
*   STRUCT      :   sNCSI_IF_Info
******************************************************************************/
/**
 *  @brief   NCSI Interface information
 *
 *****************************************************************************/
typedef struct
{
    /** spinlock for NCSI control packet command */
    spinlock_t  lock;
    /** NCSI response packet queue */
    struct skb_queue    ncsi_res_quene;
    /** The pid of NCSI command handler thread */
    pid_t   cmd_handler_pid;
    /** Create this flag to stop the while loop in command handler thread */
    int     cmd_exit_flag;
	/** thread task */
	struct task_struct *thread_task;
    /** Use for closing the command handler function */
    struct completion   cmd_exited;
    /** The pointer to the NCSI physical device */
    struct net_device   *dev;
    /** version information */
    struct sNCSI_VERSION    ncsi_version;
    /** channel capabilities */
    struct sNCSI_CHNL_CAPABILITIES  ncsi_chnl_caps;
    /** MAC information */
    struct sNCSI_MAC_INFO   ncsi_mac_info;
    /** VLAN Mode setting */
    u32 ncsi_vlan_mode;
    /** Flow Control Mode setting */
    u32 ncsi_flow_ctrl;
    /** hardware arbitration setting */
    u32 ncsi_hardware_arb;
    /** VLAN information */
    struct sNCSI_VLAN_INFO  ncsi_vlan_info;
    /** AEN control information */
    u32 ncsi_aen_ctrl;
    /** Link status */
    struct sNCSI_GET_LINK_STATUS    ncsi_link_status;
    /** Statistics. */
    struct sNCSI_NCSI_STATISTICS    ncsi_statistics;
    /** Control packet statistics. */
    struct sNCSI_CONTROL_PACKET_STATISTICS  control_packet_statistics;
    /** NC-SI Pass-through Statistics. */
    struct sNCSI_PASSTHROUGH_STATISTICS passthrough_statistics;
    /** Channel parameters */
    struct sNCSI_CHNL_PARAMETER ncsi_chnl_parameters;
    /** NC-SI control packet queue */
    struct sk_buff_head ncsi_queue;
    /** NC-SI control packet wait queue */
    wait_queue_head_t   inq;
    /** Receive control and status flasg */
    u32 u32rcsf;
    /** NC-SI MII interface index */
    u16 ncsi_mii_index;
    /** Active channel id in bonding mode, used to failover  */
    u8 active_chnl_id;
    /** New active channel id in bonding mode, used to failover */
    u8 new_chnl_id;
    /** Current channel id */
    u8 chnl_id;
    /** Query channel id */
    u8 query_chnl_id;
    /** Enable when NCSI command is transmit/receive */
    u8 NCSI_transition_flag;
    /** Enable when the channels need error recovery */
    u8 error_recovery_enable_flag;
    /** NCSI command Instance ID (1-255) */
    u8 cmd_iid[8];
    /** Error recovery counter */
    u8 error_recovery_counter;
    /** reserved */
    u8 arbitration_mode;
    /** reserved */
    u16 u16reserved;
} sNCSI_IF_Info;
#define NOT_SUPPORT_HW_ARBITRATION_MODE 0
#define SUPPORT_HW_ARBITRATION_MODE 1
/** Receive control and status flasg */
#define RCSF_PACKET_RECEIVED   (0)
#define RCSF_BUSY_WAIT         (1)


/******************************************************************************
*   STRUCT      :   vir_dev_priv
******************************************************************************/
/**
 *  @brief   private area of the virtual ethernet device
 *
 *****************************************************************************/
struct vir_dev_priv
{
    /** spinlock for NCSI device */
    spinlock_t lock;
    /** list of ncsi devices chain */
    struct list_head ncsi_list;
    /** The pointer to the NCSI device */
    struct net_device *ncsidev;
    /** The pointer to the NCSI parent device */
    struct net_device *parent_dev;
    /** the nic stastics data of virtual device */
    struct net_device_stats stats;
    /** the ncsi channel id */
    unsigned long ncsi_chnl_id;
    /** the ncsi vlan configuration */
    struct ncsi_vlan_configs vlan_config;
    /** the ncis's link status */
    struct sNCSI_Link_Status ncsi_link_status;
    /** Channel parameters */
    struct sNCSI_CHNL_PARAMETER ncsi_chnl_parameters;
    /** MII interface index */
    int mii_index;
    /** the flag for ncsi open status */
    u8 active;
    /** Active TX */
    u8 active_tx;
    /** NCSI device index */
    u8 dev_index;
    /** Indicate that the particular ncsi port is set in enabled or disabled state  */
    u8 channel_enabled;
    /** ncsi version information */
    struct sNCSI_VERSION ncsi_version;
    /** Sync TX enable */
    u8 sync_tx;
};

#define PCI_VID_INTEL       0x8086
#define PCI_VID_BROADCOM    0x14e4

#define IANA_INTEL          0x00000157
#define IANA_BROADCOM       0x0000113d

#define IANA_DEFAULT        IANA_BROADCOM

#endif
 /* End of code */
