/**
 *<center>
 *               Avocent Corporation. Proprietary Information.
 * \n<em>
 *      This software is supplied under the terms of a license agreement or
 *      nondisclosure agreement with Avocent Corporation, or its subsidiary, and
 *      may not be copied, disseminated, or distributed except in accordance
 *      with the terms of that agreement.
 *
 *      2001 Gateway Place, Suite 520W, San Jose, California, 95110 U.S.A.
 *\n
 *                  US phone: 408.436.6333
 *
 *        Copyright &copy; 2008 Avocent Corporation.
 *</em> </center>
 *----------------------------------------------------------------------------\n
 *  MODULES     linux driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_wdtdrv.c
 *  @brief  SH7757B0 watchdog driver.
 *
 *  @internal
 *----------------------------------------------------------------------------*/
/******************************************************************************
* Content
* ----------------
*   sh_wdt_read_cnt()        - Read timer counter.
*   sh_wdt_write_cnt()       - Write data into timer counter.
*   sh_wdt_read_cntew()      - Read early warning timer counter.
*   sh_wdt_write_cntew()     - Write data into early warning timer counter.
*   sh_wdt_read_csr()        - Read data from control/status register.
*   sh_wdt_write_csr()       - Write data into control/status register.
*   sh_wdt_read_wrstcsr()    - Read reset control/status register.
*   sh_wdt_write_wrstcsr()   - Write value into reset control/status register.
*   sh_wdt_clear_wovf()      - Clear watchdog overflow flag.
*   sh_wdt_start()           - Start the watchdog.
*   sh_wdt_stop()            - Stop the Watchdog.
*   aess_wdt_keepalive()     - Keep watchdog live.
*   aess_shwdt_isr()         - Watchdog interrupt service routine.
*   aess_shwdt_open()        - Watchdog device is opened and started.
*   aess_shwdt_close()       - Close the watchdog device.
*   aess_shwdt_write()       - Write to watchdog device.
*   aess_shwdt_mmap()        - Map WDT/CPG registers into userspace.
*   aess_shwdt_ioctl()       - The I/O control function of watchdog driver.
*   aess_shwdt_notify_sys()  - Notifier Handler.
*   aess_shwdt_init()        - Initialize module. 
*   aess_shwdt_exit()        - Deinitialize module. 
******************************************************************************/
/*
 * drivers/watchdog/aess_wdtdrv.c
 *
 * Watchdog driver for integrated watchdog in the SH7757B0 processors.
 *
 * Copyright (C) 2001, 2002, 2003 Paul Mundt <lethal@linux-sh.org>
 * Copyright (C) 2009 Renesas Solutions Corp.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * 14-Dec-2001 Matt Domsch <Matt_Domsch@dell.com>
 *     Added nowayout module option to override CONFIG_WATCHDOG_NOWAYOUT
 *
 * 19-Apr-2002 Rob Radez <rob@osinvestor.com>
 *     Added expect close support, made emulated timeout runtime changeable
 *     general cleanups, add some ioctls
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/reboot.h>
#include <linux/notifier.h>
#include <linux/ioport.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/interrupt.h>
#include <asm/hw_regs.h>
#include "aess_wdtdrv.h"
#include <linux/tracebuf.h>

#define PFX "aess_wdtdrv: "

#ifdef CONFIG_WATCHDOG_NOWAYOUT
#define WATCHDOG_NOWAYOUT	1    /**< Watchdog driver is no way out. */
#else
#define WATCHDOG_NOWAYOUT	0    /**< Watchdog driver can be removed. */
#endif

/*
 * Default clock division ratio is 5.25 msecs. For an additional table of
 * values, consult the asm-sh/watchdog.h. Overload this at module load
 * time.
 *
 * In order for this to work reliably we need to have HZ set to 1000 or
 * something quite higher than 100 (or we need a proper high-res timer
 * implementation that will deal with this properly), otherwise the 10ms
 * resolution of a jiffy is enough to trigger the overflow. For things like
 * the SH-4 and SH-5, this isn't necessarily that big of a problem, though
 * for the SH-2 and SH-3, this isn't recommended unless the WDT is absolutely
 * necssary.
 *
 * As a result of this timing problem, the only modes that are particularly
 * feasible are the 4096 and the 2048 divisors, which yeild 5.25 and 2.62ms
 * overflow periods respectively.
 *
 * Also, since we can't really expect userspace to be responsive enough
 * before the overflow happens, we maintain two separate timers .. One in
 * the kernel for clearing out WOVF every 2ms or so (again, this depends on
 * HZ == 1000), and another for monitoring userspace writes to the WDT device.
 *
 * As such, we currently use a configurable heartbeat interval which defaults
 * to 30s. In this case, the userspace daemon is only responsible for periodic
 * writes to the device before the next heartbeat is scheduled. If the daemon
 * misses its deadline, the kernel timer will allow the WDT to overflow.
 */
#if defined(CONFIG_CPU_SUBTYPE_SH7757)
//static int clock_division_ratio = WTCSR_CKS_8192;   /**12s< Time period that watchdog timer will overflow. */
static int clock_division_ratio = WTCSR_CKS_65536;   /**89s< Time period that watchdog timer will overflow. */
#else
static int clock_division_ratio = WTCSR_CKS_4096;   /**< Time period that watchdog timer will overflow. */
#endif

int aess_wdt_keepalive(struct file *);

static unsigned long shwdt_is_open;                 /**< Flag to indicate watchdog device is opened. */
static const struct watchdog_info aess_shwdt_info;  /**< Watchdog information structure. */
static char shwdt_expect_close;                     /**< Watchdog is closed correctly or not. */
static DEFINE_SPINLOCK(shwdt_lock);                 /**< Spin lock that protect watchdog registers. */

static int nowayout = WATCHDOG_NOWAYOUT;            /**< Indicate no way out status. */

/******************************************************************************
*   FUNCTION        :   sh_wdt_read_cnt
******************************************************************************/
/**
 *  @brief      Read timer counter.
 *
 *  @return     Content of timer counter.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline __u16 sh_wdt_read_cnt(void)
{
    /** @scope */
    /** - Return content of timer counter. */
	return ctrl_inw(WTCNT_R);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_read_cntew
******************************************************************************/
/**
 *  @brief      Read early warning timer counter.
 *
 *  @return     Content of timer counter.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline __u16 sh_wdt_read_cntew(void)
{
    /** @scope */
    /** - Return content of early warning timer counter. */
	return ctrl_inw(WCNTEW);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_write_cnt
******************************************************************************/
/**
 *  @brief      Write data into timer counter.
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline void sh_wdt_write_cnt(
                                   /* Value to be wrote into. */
                                   __u16 val
                                   )
{
    /** @scope */
    /** - Write value into timer counter. */
	ctrl_outw(val, WTCNT);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_write_cntew
******************************************************************************/
/**
 *  @brief      Write data into early warning timer counter.
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline void sh_wdt_write_cntew(
                                   /* Value to be wrote into. */
                                   __u16 val
                                   )
{
    /** @scope */
    /** - Write value into early warning timer counter. */
	ctrl_outw(val, WCNTEW);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_read_csr
******************************************************************************/
/**
 *  @brief      Read data from control/status register.
 *
 *  @return     Content of control/status register.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline __u8 sh_wdt_read_csr(void)
{
    /** @scope */
    /** - Return content of control/status register. */
	return ctrl_inb(WTCSR_R);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_write_csr 
******************************************************************************/
/**
 *  @brief      Write data into control/status register.
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline void sh_wdt_write_csr (
                                    /** Value to be wrote into. */
                                    __u8 val
                                    )
{
    /** @scope */
    /** - Write value into control/status register. */
	ctrl_outw((WTCSR_HIGH << 8) | (__u16)val, WTCSR);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_read_wrstcsr
******************************************************************************/
/**
 *  @brief      Read reset control/status register.
 *
 *  @return     Content of reset control/status register.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline __u8 sh_wdt_read_wrstcsr(void)
{
    /** @scope */
    /** - Return value of reset control/status register. */
	return ctrl_inb(WRSTCSR_R);
}


/******************************************************************************
*   FUNCTION        :   sh_wdt_write_wrstcsr
******************************************************************************/
/**
 *  @brief      Write value into reset control/status register.
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline void sh_wdt_write_wrstcsr (
                                    /** Value to be wrote into. */
                                    __u8 val
                                        )
{
    /** @scope */
    /** - Write value into reset control/status register. */
	ctrl_outw((WRSTCSR_HIGH << 8) | (__u16)val, WRSTCSR);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_clear_wovf
******************************************************************************/
/**
 *  @brief      Clear watchdog overflow flag.
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static inline void sh_wdt_clear_wovf(void)
{
    /** @scope */
    u8 u8wrstcsr;

    /** - Check watchdog overflow flag, if it is true, clear it. */
    u8wrstcsr = ctrl_inb(WRSTCSR_R);
    if (u8wrstcsr | 0x80)
    {
	    ctrl_outw((WOVF_HIGH << 8), WRSTCSR);
    }
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_start
******************************************************************************/
/**
 *  @brief      Start the watchdog.
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static void sh_wdt_start(void)
{
	__u8 csr;
	__u8 wrstcsr;
	unsigned long flags;

    /** - Set watchdog timeout value. */
	spin_lock_irqsave(&shwdt_lock, flags);

    /** - Clear watchdog timer overflow flag. */
    sh_wdt_clear_wovf();

    /** - Set reset control/status register to enable EW interrupt. */
    wrstcsr = WRSTCSR_EWE | WRSTCSR_RSTE;
    sh_wdt_write_wrstcsr(wrstcsr);

    /** - Set timeout value and set watchdog module to watchdog mode. */
    csr = (clock_division_ratio) | WTCSR_WT | WTCSR_TME;
	sh_wdt_write_csr(csr);

    /** - Initial counter and EW counter. Set EW counter to prior timer counter 1. */
	sh_wdt_write_cntew(4392);     //83.5 seconds for early warning interrupt
	sh_wdt_write_cnt(0);          //89.5 seconds for HW-controlled PRESET

	spin_unlock_irqrestore(&shwdt_lock, flags);
}

/******************************************************************************
*   FUNCTION        :   sh_wdt_stop
******************************************************************************/
/**
 *  @brief      Stop the Watchdog.
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static void sh_wdt_stop(void)
{
	__u8 csr;
	unsigned long flags;

    /** @scope */
    /** - Stop the watchdog timer. */
	spin_lock_irqsave(&shwdt_lock, flags);

	csr = sh_wdt_read_csr();
	csr &= ~WTCSR_TME;
	sh_wdt_write_csr(csr);

	spin_unlock_irqrestore(&shwdt_lock, flags);
}


/******************************************************************************
*   FUNCTION        :   aess_wdt_keepalive
******************************************************************************/
/**
 *  @brief      Keep watchdog live.
 *
 *  @return     0: Success.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
int aess_wdt_keepalive(
                    /** Pointer of file structure. */
                    struct file *file
                      )
{
	unsigned long flags;
    __u8 csr;

    /** @scope */
	spin_lock_irqsave(&shwdt_lock, flags);

    /** - Clear internal timer overflow flag. */
    csr = sh_wdt_read_csr();
    csr &= ~WTCSR_IOVF;
    sh_wdt_write_csr(csr);

    /** - Clear internal timer counter. */
    sh_wdt_write_cnt(0);          //89.5 seconds for HW-controlled PRESET
    sh_wdt_write_cntew(4392);     //83.5 seconds for early warning interrupt

	spin_unlock_irqrestore(&shwdt_lock, flags);
    return 0;
}

/******************************************************************************
*   FUNCTION        :   aess_shwdt_isr
******************************************************************************/
/**
 *  @brief      Watchdog interrupt service routine.
 *
 *  @return     IRQ handled status.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private function\n
 *
 *****************************************************************************/
static irqreturn_t aess_shwdt_isr(
        
        /** IRQ number. */
        int irq, 
        
        /**  Point to current dev data structure.  */
        void *dev_id
                                )
{
	volatile __u8 wrstcsr;
    volatile irqreturn_t rval = IRQ_NONE;
	volatile unsigned long flags;

#if 0
    /* Dump ETH registers, because of issues seen in 12G */
    printk(KERN_ERR "%s(): DUMPING ETHERNET registers : \n",__FUNCTION__);
    printk(KERN_ERR " EESR0 = 0x%x\n",ctrl_inl(0xFEF00028));
    printk(KERN_ERR " EESR1 = 0x%x\n",ctrl_inl(0xFEF00828));
    printk(KERN_ERR " EESIPR0 = 0x%x\n",ctrl_inl(0xFEF00030));
    printk(KERN_ERR " EESIPR1 = 0x%x\n",ctrl_inl(0xFEF00830));
    printk(KERN_ERR " ECSR0 = 0x%x\n",ctrl_inl(0xFEF00110));
    printk(KERN_ERR " ECSR1 = 0x%x\n",ctrl_inl(0xFEF00910));
    printk(KERN_ERR " ECSIPR0 = 0x%x\n",ctrl_inl(0xFEF00118));
    printk(KERN_ERR " ECSIPR1 = 0x%x\n",ctrl_inl(0xFEF00918));

    printk(KERN_ERR " EESR0 = 0x%x\n",ctrl_inl(0xFEE00428));
    printk(KERN_ERR " EESR1 = 0x%x\n",ctrl_inl(0xFEE00C28));
    printk(KERN_ERR " EESIPR0 = 0x%x\n",ctrl_inl(0xFEE00430));
    printk(KERN_ERR " EESIPR1 = 0x%x\n",ctrl_inl(0xFEE00C30));
    printk(KERN_ERR " ECSR0 = 0x%x\n",ctrl_inl(0xFEE00510));
    printk(KERN_ERR " ECSR1 = 0x%x\n",ctrl_inl(0xFEE00D10));
    printk(KERN_ERR " ECSIPR0 = 0x%x\n",ctrl_inl(0xFEE00518));
    printk(KERN_ERR " ECSIPR1 = 0x%x\n",ctrl_inl(0xFEE00D18));
#endif

    printk(KERN_ERR "Watchdog ISR is triggered!0x%x\n", sh_wdt_read_cnt());


	spin_lock_irqsave(&shwdt_lock, flags);
    /** @scope */
    /** - No reason to clear watchdog overflow  */
    //sh_wdt_clear_wovf();

    /** - Disable EWE since we already got the interrupt.. */
    wrstcsr = sh_wdt_read_wrstcsr();
	wrstcsr &= ~WRSTCSR_EWE;
    sh_wdt_write_wrstcsr(wrstcsr);

    spin_unlock_irqrestore(&shwdt_lock, flags);

    /* if there's a home set aside in u-boot for it, save tracebuffer */
    printk("%s(): DUMPING TRACEBUFFER for next uboot init...\n",__FUNCTION__);
    memcpy(nmi_buf_start, trabbuf, TRAB_SIZE);

#if 0
    /* Dump ETH registers, because of issues seen in 12G */
    printk("%s(): DUMPING ETHERNET registers : \n",__FUNCTION__);
    printk(" EESR0 = 0x%x\n",ctrl_inl(0xFEF00028));
    printk(" EESR1 = 0x%x\n",ctrl_inl(0xFEF00828));
    printk(" EESIPR0 = 0x%x\n",ctrl_inl(0xFEF00030));
    printk(" EESIPR1 = 0x%x\n",ctrl_inl(0xFEF00830));
    printk(" ECSR0 = 0x%x\n",ctrl_inl(0xFEF00110));
    printk(" ECSR1 = 0x%x\n",ctrl_inl(0xFEF00910));
    printk(" ECSIPR0 = 0x%x\n",ctrl_inl(0xFEF00118));
    printk(" ECSIPR1 = 0x%x\n",ctrl_inl(0xFEF00918));
#endif
    /** Shutdown hardware, but allow PRESET WDT to reset */
    emergency_restart();  

    rval = IRQ_HANDLED;
    return rval;
} 

/******************************************************************************
*   FUNCTION        :   aess_shwdt_open
******************************************************************************/
/**
 *  @brief      Watchdog device is opened and started.
 *
 *  @return     -EBUSY: Watchdog device is opened. \n
 *              0: Success.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static int aess_shwdt_open (
                            /**Inode of device. */
                            struct inode *inode, 
                            /**File handle of device. */
                            struct file *file
                           )
{
	__u8 csr;
	__u8 wrstcsr;
	unsigned long flags;
    int rc = -1;

	/* If u-boot has disabled WDT0 via OSWDOG_debug, then exit*/
	if( (sh_wdt_read_csr() & WTCSR_TME) == 0)
		return(-EPERM);

    /** @scope */
    /** - Set flag to opened. */
	if (test_and_set_bit(0, &shwdt_is_open))
    {
		return -EBUSY;
    }
	if (nowayout)
		__module_get(THIS_MODULE);

    /** - Request an IRQ. */
    rc = request_irq (WDT_INTEW_IRQ, aess_shwdt_isr, IRQF_SHARED, AESS_SHWDTDRV_NAME,(void *)&aess_shwdt_isr);

    if (rc < 0)
    {
        printk("[watchdog] request irq fail.\n");
    }

    sh_wdt_start();

	return nonseekable_open(inode, file);
}

/******************************************************************************
*   FUNCTION        :   aess_shwdt_close
******************************************************************************/
/**
 *  @brief      Close the watchdog device.
 *
 *  @return     0: Success.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static int aess_shwdt_close (
                        /**Inode of device. */
                        struct inode *inode, 
                        /**File handle of device. */
                        struct file *file
                            )
{
    int rc;

	/* 
	*	If u-boot has disabled WDT0 via OSWDOG_debug, then we fail the open.
	*	No one saved the fact that open failed so reboot will call close 
	*	and we will free_irq(), then sometimes get a crash.
	*	Exit if watchdog was never started.
	*/
	if( (sh_wdt_read_csr() & WTCSR_TME) == 0)
		return(-EPERM);


    /** @scope */
    /** - Close the watchdog device. If it is no way out, watchdog will not be stopped. */
	if (shwdt_expect_close == 42)
    {
		sh_wdt_stop();
        free_irq (WDT_INTEW_IRQ, (void*)&aess_shwdt_isr);
    	clear_bit(0, &shwdt_is_open);
	    shwdt_expect_close = 0;
	} 
    else
    {
		printk(KERN_CRIT PFX "Unexpected close, not stopping watchdog!0x%x\n",sh_wdt_read_cnt());
		aess_wdt_keepalive(file);
	}

	return 0;
}

/******************************************************************************
*   FUNCTION        :   aess_shwdt_write
******************************************************************************/
/**
 *  @brief      Write to watchdog device.
 *
 *  @return     Written size.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static ssize_t aess_shwdt_write (
                            /**File handle of device. */
                            struct file *file, 
                            /**Buffer to write. */
                            const char *buf,
                            /**Length of buffer. */
			                size_t count, 
                            /**Offset. */
                            loff_t *ppos
                                )
{
    /** @scope */
    /** - If data is 'V', set watchdog to able to closed normally. */
	if (count) {
		if (!nowayout) {
			size_t i;

			shwdt_expect_close = 0;

			for (i = 0; i != count; i++) {
				char c;
				if (get_user(c, buf + i))
					return -EFAULT;
				if (c == 'V')
					shwdt_expect_close = 42;
			}
		}
        /** - Invoke aess_wdt_keepalive() to keep watchdog live. */
		aess_wdt_keepalive(file);
	}

	return count;
}

/******************************************************************************
*   FUNCTION        :   aess_shwdt_mmap
******************************************************************************/
/**
 *  @brief      Map WDT/CPG registers into userspace. \n
 * 	A simple mmap() implementation for the corner cases where the counter \n
 * 	needs to be mapped in userspace directly. Due to the relatively small \n
 * 	size of the area, neighbouring registers not necessarily tied to the \n
 * 	CPG will also be accessible through the register page, so this remains \n
 * 	configurable for users that really know what they're doing.l \n \n
 *
 *	Additionaly, the register page maps in the CPG register base relative\n
 *	to the nearest page-aligned boundary, which requires that userspace do\n
 *	the appropriate CPU subtype math for calculating the page offset for\n
 *	the counter value.
 *
 *  @return     -EINVAL: No equal to a register page. \n
 *              -EAGAIN: Map fail. \n
 *              0: Success.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static int aess_shwdt_mmap (
                            /** File structure for the device. */
                            struct file *file, 
                            /** VMA to map the registers into. */
                            struct vm_area_struct *vma
                           )
{
	int ret = -ENOSYS;

    /** @scope */
    /** - Map register to virtual memory. */
#ifdef CONFIG_SH_WDT_MMAP
	unsigned long addr;

	/* Only support the simple cases where we map in a register page. */
	if (((vma->vm_end - vma->vm_start) != PAGE_SIZE) || vma->vm_pgoff)
		return -EINVAL;

	/*
	 * Pick WTCNT as the start, it's usually the first register after the
	 * FRQCR, and neither one are generally page-aligned out of the box.
	 */
	addr = WTCNT & ~(PAGE_SIZE - 1);

	vma->vm_flags |= VM_IO;
	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);

	if (io_remap_pfn_range(vma, vma->vm_start, addr >> PAGE_SHIFT,
			       PAGE_SIZE, vma->vm_page_prot)) {
		printk(KERN_ERR PFX "%s: io_remap_pfn_range failed\n",
		       __func__);
		return -EAGAIN;
	}

	ret = 0;
#endif

	return ret;
}

/******************************************************************************
*   FUNCTION        :   aess_shwdt_ioctl
******************************************************************************/
/**
 *  @brief      The I/O control function of watchdog driver.
 *
 *  @return     -EFAULT: 1. Access user space variable fail. \n
 *                       2. Timeout value is out of range. \n
 *              0: Success. \n
 *              -ENOTTY: No support command.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static long aess_shwdt_ioctl (
                            /** File handle of device. */
                            struct file *file, 
                            /** Watchdog command. */
                            unsigned int cmd,
                            /** Argument. */
							unsigned long arg)
{
	int options, retval = -EINVAL;
    u8  u8csr;

    /** @scope */
	switch (cmd)
    {
        /** - If command is WDIOC_GETSUPPORT, copy watchdog information structure to user space. */
    	case WDIOC_GETSUPPORT:
    		return copy_to_user((struct watchdog_info *)arg,
    			  &aess_shwdt_info, sizeof(aess_shwdt_info)) ? -EFAULT : 0;
        /** - If command is WDIOC_GETSTATUS or WDIOC_GETBOOTSTATUS, put 0 to user space. */
    	case WDIOC_GETSTATUS:
    	case WDIOC_GETBOOTSTATUS:
    		return put_user(0, (int *)arg);
        /** - If command is WDIOC_SETOPTIONS, disable or enable watchdog driver. */
    	case WDIOC_SETOPTIONS:
    		if (get_user(options, (int *)arg))
    			return -EFAULT;
    
    		if (options & WDIOS_DISABLECARD) {
    			sh_wdt_stop();
    			retval = 0;
    		}
    
    		if (options & WDIOS_ENABLECARD) {
    			sh_wdt_start();
    			retval = 0;
    		}
    
    		return retval;
        /** - If command is WDIOC_KEEPALIVE, invoke aess_wdt_keepalive(). */
    	case WDIOC_KEEPALIVE:
    		aess_wdt_keepalive(file);
    		return 0;
        /** - If command is WDIOC_SETTIMEOUT, update watchdog timeout value according to user space parameter. */
    	case WDIOC_SETTIMEOUT:
    
    		if (get_user(options, (int *)arg))
            {
    			return -EFAULT;
            }
            if (options < 0 || options > 0xf)
            {
    			return -EFAULT;
            } 
    	    u8csr = sh_wdt_read_csr();
            u8csr &= ~(WTCSR_IOVF | WTCSR_TME);
    	    sh_wdt_write_csr(u8csr);
            u8csr &= (0xf0 | options); 
    	    sh_wdt_write_csr(u8csr);
    
    		aess_wdt_keepalive(file);
    		return 0;
    		/* Fall through. */
        /** - If command is WDIOC_GETTIMEOUT, return current timeout setting. */
    	case WDIOC_GETTIMEOUT:
    		return put_user(sh_wdt_read_csr() & 0x0f, (int *)arg);
    
    	default:
    		return -ENOTTY;
	}
	return 0;
}

/******************************************************************************
*   FUNCTION        :   aess_shwdt_notify_sys
******************************************************************************/
/**
 *  @brief      Notifier Handler. Handles specific events, \n
 *              such as turning off the watchdog during a shutdown event.
 *
 *  @return     NOTIFY_DONE: Success.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static int aess_shwdt_notify_sys (
                    /** Notifier block. */
                    struct notifier_block *this,
                    /** Notifier event. */
			        unsigned long code, 
                    /** Unused. */
                    void *unused
                                 )
{
    /** @scope */
    /** - If system is going to down, stop the watchdog. */
	if (code == SYS_DOWN || code == SYS_HALT)
    {
		sh_wdt_stop();
    }

	return NOTIFY_DONE;
}

static const struct file_operations sh_wdt_fops = {
	.owner		    = THIS_MODULE,
	.llseek		    = no_llseek,
	.write		    = aess_shwdt_write,
	.unlocked_ioctl	= aess_shwdt_ioctl,
	.open		    = aess_shwdt_open,
	.release	    = aess_shwdt_close,
	.mmap		    = aess_shwdt_mmap,
}; /**< Watchdog driver file operations. */

static const struct watchdog_info aess_shwdt_info = {
	.options		  = WDIOF_KEEPALIVEPING | WDIOF_SETTIMEOUT | WDIOF_MAGICCLOSE,
	.firmware_version = 1,
	.identity		  = "AESS WATCHDOG DRIVER",
}; /**< Watchdog driver informations. */

static struct notifier_block aess_shwdt_notifier = {
	.notifier_call		= aess_shwdt_notify_sys,
};

static struct miscdevice sh_wdt_miscdev = {
	.minor		= WATCHDOG_MINOR,
	.name		= AESS_SHWDTDRV_NAME,
	.fops		= &sh_wdt_fops,
}; /**< Watchdog driver device information. */

/******************************************************************************
*   FUNCTION        :   aess_shwdt_init
******************************************************************************/
/**
 *  @brief      Initialize module. \nRegisters the device and notifier handler. Actual device\n
 *              initialization is handled by aess_shwdt_open().
 *
 *  @return     0: Success.
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static int __init aess_shwdt_init(void)
{
	int rc;

    /** @scope */
    /** - Set initial timeout value. */
#if !defined(CONFIG_CPU_SUBTYPE_SH7757)
	if (clock_division_ratio < 0x5 || clock_division_ratio > 0x7) {
		//clock_division_ratio = WTCSR_CKS_8192; //12s
		clock_division_ratio = WTCSR_CKS_65536;  //89s
		printk(KERN_INFO PFX
		  "clock_division_ratio value must be 0x5<=x<=0x7, using %d\n",
				clock_division_ratio);
	}
#endif

    /** - Register reboot notifier. */
	rc = register_reboot_notifier(&aess_shwdt_notifier);
	if (unlikely(rc)) {
		printk(KERN_ERR PFX
			"Can't register reboot notifier (err=%d)\n", rc);
		return rc;
	}

    /** - Register watchdog device. */
	rc = misc_register(&sh_wdt_miscdev);
	if (unlikely(rc)) {
		printk(KERN_ERR PFX
			"Can't register miscdev on minor=%d (err=%d)\n",
						sh_wdt_miscdev.minor, rc);
		unregister_reboot_notifier(&aess_shwdt_notifier);
		return rc;
	}

    /** show this message for wdtdrv driver installation, used by shell */
	printk("mknod /dev/aess_wdtdrv c 10 %d \n", sh_wdt_miscdev.minor);

	printk(KERN_INFO PFX "initialized. (nowayout=%d)\n", nowayout);

	return 0;
}

/******************************************************************************
*   FUNCTION        :   aess_shwdt_exit
******************************************************************************/
/**
 *  @brief      Deinitialize module. \nUnregisters the device and notifier handler. \n
 *              Actual device.deinitialization is handled by aess_shwdt_close().
 *
 *  @return     None
 *
 *  @dependency None
 *
 *  @limitation None
 *
 *  @warning    None
 *
 *  @note       None
 *
 *  @internal   Function Type: Private  function\n
 *
 *****************************************************************************/
static void __exit aess_shwdt_exit(void)
{
    /** @scope */
    /** - Unregister watchdog device. */
	misc_deregister(&sh_wdt_miscdev);
    /** - Unregister reboot notifier. */
	unregister_reboot_notifier(&aess_shwdt_notifier);
}

MODULE_AUTHOR("Arthur Chen <arthur.chen@avocent.com>");
MODULE_DESCRIPTION("SH7757B0 watchdog driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS_MISCDEV(WATCHDOG_MINOR);

module_param(clock_division_ratio, int, 0);
MODULE_PARM_DESC(clock_division_ratio, "Clock division ratio. Valid ranges are from 0x0 (2.7ms) to 0xf (179s). (default=" __MODULE_STRING(clock_division_ratio) ")");

module_param(nowayout, int, 0);
MODULE_PARM_DESC(nowayout, "Watchdog cannot be stopped once started (default=" __MODULE_STRING(WATCHDOG_NOWAYOUT) ")");

module_init(aess_shwdt_init);
module_exit(aess_shwdt_exit);
EXPORT_SYMBOL(aess_wdt_keepalive);


