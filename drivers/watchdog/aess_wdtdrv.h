/**
 *<center>
 *               Avocent Corporation. Proprietary Information.
 * \n<em>
 *      This software is supplied under the terms of a license agreement or
 *      nondisclosure agreement with Avocent Corporation, or its subsidiary, and
 *      may not be copied, disseminated, or distributed except in accordance
 *      with the terms of that agreement.
 *
 *      2001 Gateway Place, Suite 520W, San Jose, California, 95110 U.S.A.
 *\n
 *                  US phone: 408.436.6333
 *
 *        Copyright &copy; 2008 Avocent Corporation.
 *</em> </center>
 *----------------------------------------------------------------------------\n
 *  MODULES     linux driver
 *----------------------------------------------------------------------------\n
 *  @file   aess_wdtdrv.h
 *  @brief  SH7757B0 watchdog timer driver header.
 *
 *  @internal
 *----------------------------------------------------------------------------*/

#ifndef WATCHDOG_H
#define WATCHDOG_H

#define	WATCHDOG_IOCTL_BASE	'W'   /**< Define of I/O control magic number of watchdog driver. */

/********************************************************************************
 *  Enum      :   watchdog_info 
 *******************************************************************************/
/*
 *  @brief   Stores watchdog related information.
 *
 ********************************************************************************/
struct watchdog_info {
    /** Options the card/driver supports */
	__u32 options;

    /** Firmware version of the card */
	__u32 firmware_version;

    /** Identity of the board */
	__u8  identity[32];
};

#define	WDIOC_GETSUPPORT	_IOR(WATCHDOG_IOCTL_BASE, 0, struct watchdog_info)  /**< Define of I/O control command code: GETSUPPORT. */
#define	WDIOC_GETSTATUS		_IOR(WATCHDOG_IOCTL_BASE, 1, int)                   /**< Define of I/O control command code: GETSTATUS. */
#define	WDIOC_GETBOOTSTATUS	_IOR(WATCHDOG_IOCTL_BASE, 2, int)                   /**< Define of I/O control command code: GETBOOTSTATUS. */
#define	WDIOC_GETTEMP		_IOR(WATCHDOG_IOCTL_BASE, 3, int)                   /**< Define of I/O control command code: GETTEMP. */
#define	WDIOC_SETOPTIONS	_IOR(WATCHDOG_IOCTL_BASE, 4, int)                   /**< Define of I/O control command code: SETOPTIONS. */
#define	WDIOC_KEEPALIVE		_IOR(WATCHDOG_IOCTL_BASE, 5, int)                   /**< Define of I/O control command code: KEEPALIVE. */
#define	WDIOC_SETTIMEOUT    _IOWR(WATCHDOG_IOCTL_BASE, 6, int)                  /**< Define of I/O control command code: SETTIMEOUT. */
#define	WDIOC_GETTIMEOUT    _IOR(WATCHDOG_IOCTL_BASE, 7, int)                   /**< Define of I/O control command code: GETTIMEOUT. */
#define	WDIOC_SETPRETIMEOUT	_IOWR(WATCHDOG_IOCTL_BASE, 8, int)                  /**< Define of I/O control command code: SETPRETIMEOUT. */
#define	WDIOC_GETPRETIMEOUT	_IOR(WATCHDOG_IOCTL_BASE, 9, int)                   /**< Define of I/O control command code: GETPRETIMEOUT. */
#define	WDIOC_GETTIMELEFT	_IOR(WATCHDOG_IOCTL_BASE, 10, int)                  /**< Define of I/O control command code: GETTIMELEFT. */

#define	WDIOF_UNKNOWN		-1	    /**< Unknown flag error */
#define	WDIOS_UNKNOWN		-1	    /**< Unknown status error */

#define	WDIOF_OVERHEAT		0x0001	/**< Reset due to CPU overheat */
#define	WDIOF_FANFAULT		0x0002	/**< Fan failed */
#define	WDIOF_EXTERN1		0x0004	/**< External relay 1 */
#define	WDIOF_EXTERN2		0x0008	/**< External relay 2 */
#define	WDIOF_POWERUNDER	0x0010	/**< Power bad/power fault */
#define	WDIOF_CARDRESET		0x0020	/**< Card previously reset the CPU */
#define	WDIOF_POWEROVER		0x0040	/**< Power over voltage */
#define	WDIOF_SETTIMEOUT	0x0080  /**< Set timeout (in seconds) */
#define	WDIOF_MAGICCLOSE	0x0100	/**< Supports magic close char */
#define	WDIOF_PRETIMEOUT	0x0200  /**< Pretimeout (in seconds), get/set */
#define	WDIOF_KEEPALIVEPING	0x8000	/**< Keep alive ping reply */

#define	WDIOS_DISABLECARD	0x0001	/**< Turn off the watchdog timer */
#define	WDIOS_ENABLECARD	0x0002	/**< Turn on the watchdog timer */
#define	WDIOS_TEMPPANIC		0x0004	/**< Kernel panic on temperature trip */

/* Watchdog IRQ number. */
#define WDT_TIMER_OV_IRQ    304     /**< IRQ number of WDT0B (Overflow of watchdog channel 0). */
#define WDT_INTEW_IRQ       305     /**< IRQ number of WDT1B (early warning overflow of watchdog channel 0). */

/* Bit definitions */
#define WTCSR_IOVF	        0x80    /**< Bit definition of watchdog overflow flag. */
#define WTCSR_WT	        0x40    /**< Bit definition of watchdog mode. */
#define WTCSR_TME	        0x20    /**< Bit definition of enable timer. */
#define WRSTCSR_RSTE        0x40    /**< Bit definition of "Reset Enable". */
#define WRSTCSR_EWE         0x08    /**< Bit definition of "Early Warning Interrtup Enable". */

#ifndef WTCNT_R
#  define WTCNT_R	WTCNT           /**< Read address of timer counter.*/
#endif

#ifndef WTCSR_R
#  define WTCSR_R	WTCSR           /**< Read address of timer control/status register. */
#endif


#define WTCNT_HIGH	    0x5a        /**< Write protect byte of timer counter register. */
#define WTCSR_HIGH	    0xa5        /**< Write protect byte of timer control/status. */
#define WRSTCSR_HIGH	0x69        /**< Write protect byte of reset control/status register. */
#define WOVF_HIGH       0x96        /**< Write protect byte of watchdog timer overflow flag. */

#define WTCSR_CKS_2        0x00     /**< Timeout setting of   2.7 ms. */
#define WTCSR_CKS_4        0x01     /**< Timeout setting of   5.5 ms. */
#define WTCSR_CKS_8        0x02     /**< Timeout setting of  10.9 ms. */
#define WTCSR_CKS_16       0x03     /**< Timeout setting of  21.8 ms. */
#define WTCSR_CKS_32       0x04     /**< Timeout setting of  43.7 ms. */
#define WTCSR_CKS_64       0x05     /**< Timeout setting of  87.4 ms. */
#define WTCSR_CKS_128      0x06     /**< Timeout setting of 174.8 ms. */
#define WTCSR_CKS_512      0x07     /**< Timeout setting of 699.04ms. */
#define WTCSR_CKS_1024     0x08     /**< Timeout setting of   1.4s. */
#define WTCSR_CKS_2048     0x09     /**< Timeout setting of   2.8s. */
#define WTCSR_CKS_4096     0x0a     /**< Timeout setting of   5.6s. */
#define WTCSR_CKS_8192     0x0b     /**< Timeout setting of  12.2s. */
#define WTCSR_CKS_16384    0x0c     /**< Timeout setting of  22.4s. */
#define WTCSR_CKS_32768    0x0d     /**< Timeout setting of  44.8s. */
#define WTCSR_CKS_65536    0x0e     /**< Timeout setting of  89.5s. */
#define WTCSR_CKS_131072   0x0f     /**< Timeout setting of 179.0s. */


#define AESS_SHWDTDRV_NAME "aess_wdtdrv"  /**< Driver name of watchdog. */

#endif   /* WATCHDOG_H */

/* End of code */ 
