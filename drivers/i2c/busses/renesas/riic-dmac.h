/*
 * from include/asm-sh/dma.h
 *
 * Copyright (C) 2003, 2004  Paul Mundt
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */

#ifndef __RIIC_DMAC_H
#define __RIIC_DMAC_H

#include <asm-generic/dma.h>

/* #include <cpu/dma.h> */
/*
 * The multiple irq numbers are: DMAC0 to 5, DMAC6 to 7, DMAC8 to 11.
 * We define a value of IRQ number so that it may be directly got from
 * dmte_irq_map[].
 */
#define DMTE0_IRQ	34
#define DMTE4_IRQ	34	/* real irq number: 44 */
#define DMTE6_IRQ	46
#define DMTE8_IRQ	88
#define DMTE9_IRQ	88	/* real irq number: 89 */
#define DMTE10_IRQ	88	/* real irq number: 90 */
#define DMTE11_IRQ	88	/* real irq number: 91 */
#define DMTE12_IRQ	272
#define DMTE13_IRQ	273
#define DMTE14_IRQ	274
#define DMTE15_IRQ	275
#define DMTE16_IRQ	276
#define DMTE17_IRQ	279
#define DMTE18_IRQ	280
#define DMTE19_IRQ	281
#define DMTE20_IRQ	282
#define DMTE21_IRQ	283
#define DMTE22_IRQ	284
#define DMTE23_IRQ	288
#define DMAE0_IRQ	34	/* real irq number: 38, DMA Error IRQ */
#define DMAE2_IRQ	323	/* DMA Error IRQ (channel: 12 to 17) */
#define DMAE3_IRQ	324	/* DMA Error IRQ (channel: 18 to 23) */
#define SH_DMAC_BASE0	0xFF608020
#define SH_DMAC_BASE1	0xFF618020
#define SH_DMAC_BASE2	0xFF708020
#define SH_DMAC_BASE3	0xFF718020
#define SH_DMARS_BASE	0xFF609000
#define REQ_HE		0x000000C0
#define REQ_H		0x00000080
#define REQ_LE		0x00000040
#define TM_BURST	0x00000020

/* DMA register */
#define SAR	0x00
#define DAR	0x04
#define TCR	0x08
#define CHCR	0x0c
#define DMAOR	0x40

/* DMAOR definitions */
#define DMAOR_AE	0x00000004
#define DMAOR_NMIF	0x00000002
#define DMAOR_DME	0x00000001

/* Definitions for the SuperH DMAC */
#define REQ_L	0x00000000
#define REQ_E	0x00080000
#define RACK_H	0x00000000
#define RACK_L	0x00040000
#define ACK_R	0x00000000
#define ACK_W	0x00020000
#define ACK_H	0x00000000
#define ACK_L	0x00010000
#define DM_INC	0x00004000
#define DM_DEC	0x00008000
#define DM_FIX	0x0000c000
#define SM_INC	0x00001000
#define SM_DEC	0x00002000
#define SM_FIX	0x00003000
#define RS_IN	0x00000200
#define RS_OUT	0x00000300
#define TS_BLK	0x00000040
#define TM_BUR	0x00000020
#define CHCR_DE	0x00000001
#define CHCR_TE	0x00000002
#define CHCR_IE	0x00000004

/*
 * Define the default configuration for dual address memory-memory transfer.
 * The 0x400 value represents auto-request, external->external.
 */
#define RS_DUAL	(DM_INC | SM_INC | 0x400)

#define DMAOR_INIT	DMAOR_DME

/* from include/cpu-sh4/cpu/dma-register.h */
#define CHCR_TS_LOW_MASK	0x00000018
#define CHCR_TS_LOW_SHIFT	3
#define CHCR_TS_HIGH_MASK	0x00100000
#define CHCR_TS_HIGH_SHIFT	(20 - 2)	/* 2 bits for shifted low TS */

/* Transmit sizes and respective CHCR register values */
enum {
	XMIT_SZ_8BIT		= 0,
	XMIT_SZ_16BIT		= 1,
	XMIT_SZ_32BIT		= 2,
	XMIT_SZ_64BIT		= 7,
	XMIT_SZ_128BIT		= 3,
	XMIT_SZ_256BIT		= 4,
	XMIT_SZ_128BIT_BLK	= 0xb,
	XMIT_SZ_256BIT_BLK	= 0xc,
};

/* log2(size / 8) - used to calculate number of transfers */
#define TS_SHIFT {			\
	[XMIT_SZ_8BIT]		= 0,	\
	[XMIT_SZ_16BIT]		= 1,	\
	[XMIT_SZ_32BIT]		= 2,	\
	[XMIT_SZ_64BIT]		= 3,	\
	[XMIT_SZ_128BIT]	= 4,	\
	[XMIT_SZ_256BIT]	= 5,	\
	[XMIT_SZ_128BIT_BLK]	= 4,	\
	[XMIT_SZ_256BIT_BLK]	= 5,	\
}

#define MAX_DMA_CHANNELS	24

/*
 * Read and write modes can mean drastically different things depending on the
 * channel configuration. Consult your DMAC documentation and module
 * implementation for further clues.
 */
#define DMA_MODE_READ		0x00
#define DMA_MODE_WRITE		0x01
#define DMA_MODE_MASK		0x01

/*
 * DMAC (dma_info) flags
 */
enum {
	DMAC_CHANNELS_CONFIGURED	= 0x01,
	DMAC_CHANNELS_TEI_CAPABLE	= 0x02,	/* Transfer end interrupt */
};

/*
 * DMA channel capabilities / flags
 */
enum {
	DMA_CONFIGURED			= 0x01,

	/*
	 * Transfer end interrupt, inherited from DMAC.
	 * wait_queue used in dma_wait_for_completion.
	 */
	DMA_TEI_CAPABLE			= 0x02,
};

struct dma_channel;

struct dma_ops {
	int (*request)(struct dma_channel *chan);
	void (*free)(struct dma_channel *chan);

	int (*get_residue)(struct dma_channel *chan);
	int (*xfer)(struct dma_channel *chan);
	int (*configure)(struct dma_channel *chan, unsigned long flags);
	int (*ex_resource)(struct dma_channel *chan, unsigned char flags);
};

struct dma_channel {
	char dev_id[16];		/* unique name per DMAC of channel */

	unsigned int chan;		/* DMAC channel number */
	unsigned int vchan;		/* Virtual channel number */

	unsigned int mode;
	unsigned int count;

	unsigned long sar;
	unsigned long dar;

	const char **caps;

	unsigned long flags;
	atomic_t busy;

	wait_queue_head_t wait_queue;
	void (*callback)(void *);
	void *callback_data;

	void *priv_data;
};

struct dma_info {
	const char *name;
	unsigned int nr_channels;
	unsigned long flags;

	struct dma_ops *ops;
	struct dma_channel *channels;

	struct list_head list;
	int first_channel_nr;
	int first_vchannel_nr;
};

struct dma_chan_caps {
	int ch_num;
	const char **caplist;
};

/* arch/sh/drivers/dma/dma-api.c */
extern int dma_xfer(unsigned int chan, unsigned long from,
		    unsigned long to, size_t size, unsigned int mode);

#define dma_write(chan, from, to, size)	\
	dma_xfer(chan, from, to, size, DMA_MODE_WRITE)
#define dma_write_page(chan, from, to)	\
	dma_write(chan, from, to, PAGE_SIZE)

#define dma_read(chan, from, to, size)	\
	dma_xfer(chan, from, to, size, DMA_MODE_READ)
#define dma_read_page(chan, from, to)	\
	dma_read(chan, from, to, PAGE_SIZE)

extern int request_dma_bycap(const char **dmac, const char **caps,
			     const char *dev_id);
extern int get_dma_residue(unsigned int chan);
extern struct dma_info *get_dma_info(unsigned int chan);
extern struct dma_channel *get_dma_channel(unsigned int chan);
extern void dma_wait_for_completion(unsigned int chan);
extern void dma_completion_callback(unsigned int chan, void (*callback)(void *),
				void *data);
extern void dma_configure_channel(unsigned int chan, unsigned long flags);
extern void dma_ex_resource(unsigned int chan, unsigned char id);
extern int register_dmac(struct dma_info *info);
extern void unregister_dmac(struct dma_info *info);

static inline unsigned long get_dmaor_reg(int n)
{
	unsigned long reg;

	switch (n) {
	case 0:
		reg = SH_DMAC_BASE0;
		break;
	case 1:
		reg = SH_DMAC_BASE1;
		break;
	case 2:
		reg = SH_DMAC_BASE2;
		break;
	case 3:
		reg = SH_DMAC_BASE3;
		break;
	default:
		BUG();
		break;
	}
	return reg + DMAOR;
}

static inline unsigned short dmaor_read_reg(int n)
{
	return __raw_readw(get_dmaor_reg(n));
}

static inline void dmaor_write_reg(int n, unsigned short data)
{
	__raw_writew(data, get_dmaor_reg(n));
}

static int dmte_irq_map[] __maybe_unused = {
    DMTE0_IRQ,
    DMTE0_IRQ,
    DMTE0_IRQ,
    DMTE0_IRQ,
    DMTE4_IRQ,
    DMTE4_IRQ,
    DMTE6_IRQ,
    DMTE6_IRQ,
    DMTE8_IRQ,
    DMTE9_IRQ,
    DMTE10_IRQ,
    DMTE11_IRQ,
    DMTE12_IRQ,
    DMTE13_IRQ,
    DMTE14_IRQ,
    DMTE15_IRQ,
    DMTE16_IRQ,
    DMTE17_IRQ,
    DMTE18_IRQ,
    DMTE19_IRQ,
    DMTE20_IRQ,
    DMTE21_IRQ,
    DMTE22_IRQ,
    DMTE23_IRQ,
};

static u32 dma_base_addr[] __maybe_unused = {
	SH_DMAC_BASE0 + 0x00,	/* channel 0 */
	SH_DMAC_BASE0 + 0x10,
	SH_DMAC_BASE0 + 0x20,
	SH_DMAC_BASE0 + 0x30,
	SH_DMAC_BASE0 + 0x50,
	SH_DMAC_BASE0 + 0x60,
	SH_DMAC_BASE1 + 0x00,
	SH_DMAC_BASE1 + 0x10,
	SH_DMAC_BASE1 + 0x20,
	SH_DMAC_BASE1 + 0x30,
	SH_DMAC_BASE1 + 0x50,
	SH_DMAC_BASE1 + 0x60, /* channel 11 */
	SH_DMAC_BASE2 + 0x00,
	SH_DMAC_BASE2 + 0x10,
	SH_DMAC_BASE2 + 0x20,
	SH_DMAC_BASE2 + 0x30,
	SH_DMAC_BASE2 + 0x50,
	SH_DMAC_BASE2 + 0x60, /* channel 17 */
	SH_DMAC_BASE3 + 0x00,
	SH_DMAC_BASE3 + 0x10,
	SH_DMAC_BASE3 + 0x20,
	SH_DMAC_BASE3 + 0x30,
	SH_DMAC_BASE3 + 0x50,
	SH_DMAC_BASE3 + 0x60, /* channel 23 */
};

#endif /* __RIIC_DMAC_H */
