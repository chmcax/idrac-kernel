/*
 * RIIC bus driver
 *
 * Copyright (C) 2012  Renesas Solutions Corp.
 *
 * Based on i2c-sh_mobile.c
 * Portion Copyright (C) 2008 Magnus Damm
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/io.h>
#include <linux/timer.h>
#include <linux/delay.h>

#include "riic-core.h"

#define I2C_RIIC_VERSION	"2012-10-12"
#define I2C_RIIC_DEFAULT_CLOCK			400
#define I2C_RIIC_DEFAULT_SLAVE_TIMEOUT		100
#define I2C_RIIC_DEFAULT_SLAVE_BUFFER_SIZE	4096
#define I2C_RIIC_DEFAULT_SLAVE_NUM_OF_PACKETS	16

/* If you need more large buffer, you can modify this definition */
#define I2C_RIIC_MAX_SLAVE_BUFFER_SIZE		65536	/* for sysfs */

struct riic_data {
	struct device *dev;
	struct i2c_adapter adap;
	unsigned char slave_address;
	int clock;
	int slave_timeout;
	int slave_buffer_size;
	int slave_number_of_packets;
	unsigned long slave_options;

	/* for slave */
	unsigned char *slv_buf;
	int slv_buf_index;
	int slv_buf_remain;
};

static int riic_xfer(struct i2c_adapter *adapter, struct i2c_msg *msgs,
		     int num)
{
	struct riic_data *pd = i2c_get_adapdata(adapter);
	struct riic_core_packet *pkt;
	int i, ret = 0;

	dev_dbg(pd->dev, "enter %s\n", __func__);

	pkt = kzalloc(sizeof(struct riic_core_packet) * num, GFP_KERNEL);
	if (!pkt)
		return -ENOMEM;

	/* msgs to pkt */
	for (i = 0; i < num; i++) {
		pkt[i].slave_address = msgs[i].addr;
		if (msgs[i].flags & I2C_M_RD)
			pkt[i].rw = RIIC_CORE_RW_MASTER_RECEIVE;
		else
			pkt[i].rw = RIIC_CORE_RW_MASTER_TRANSMIT;
		pkt[i].len = msgs[i].len;
		pkt[i].data = msgs[i].buf;
	}

	/* xfer riic */
	ret = riic_core_master(adapter->nr, pkt, num, 1000, pd->clock);
	switch (ret) {
	case RIIC_CORE_NO_ERROR:
		ret = num;
		break;
	case RIIC_CORE_BUSY:
		ret = -EBUSY;
		break;
	case RIIC_CORE_TIMEOUT:
		ret = -ETIMEDOUT;
		break;
	case RIIC_CORE_AL_ADDR:
		ret = -EFAULT;
		break;
	case RIIC_CORE_AL_DATA:
		ret = -EIO;
		break;
	case RIIC_CORE_NACK_ADDR:
		ret = -ENXIO;
		break;
	case RIIC_CORE_NACK_DATA:
		ret = -EAGAIN;
		break;
	default:
		ret = -EINVAL;
		break;
	}

	kfree(pkt);

	return ret;
}

static u32 riic_func(struct i2c_adapter *adapter)
{
	return I2C_FUNC_I2C | I2C_FUNC_SMBUS_EMUL;
}

static struct i2c_algorithm riic_algorithm = {
	.functionality	= riic_func,
	.master_xfer	= riic_xfer,
};

/* SYSFS */
static ssize_t riic_show_slv_addr(struct device *dev,
				  struct device_attribute *dattr,
				  char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	return sprintf(buf, "0x%x\n", pd->slave_address);
}

static ssize_t riic_set_slv_addr(struct device *dev,
				 struct device_attribute *dattr,
				 const char *buf, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(dev);
	struct attribute *attr = &dattr->attr;
	unsigned int sa;
	int ret;

	sscanf(buf, "%x", &sa);

	if (strcmp(attr->name, "slv0_addr") == 0) {
		ret = riic_core_slave_config(pd->adap.nr, sa, pd->slave_timeout,
					     pd->slave_buffer_size,
					     pd->slave_number_of_packets,
					     pd->slave_options);
		if (ret == RIIC_CORE_NO_ERROR)
			pd->slave_address = sa;
		else
			printk(KERN_ERR "%s: ret = %d\n", __func__, ret);
	}

	return count;
}

static ssize_t riic_show_clock(struct device *dev,
			       struct device_attribute *dattr,
			       char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	return sprintf(buf, "default = %d, dynamic = %d\n",
			I2C_RIIC_DEFAULT_CLOCK, pd->clock);
}

static ssize_t riic_set_clock(struct device *dev,
			      struct device_attribute *dattr,
			      const char *buf, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(dev);
	unsigned int clock;

	sscanf(buf, "%d", &clock);
	pd->clock = clock;

	return count;
}

static ssize_t riic_show_slv_data(struct file *file, struct kobject *kobj,
				  struct bin_attribute *attr,
				  char *buf, loff_t off, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(
				container_of(kobj, struct device, kobj));
	int ret;
	struct riic_core_packet pkt;
	int size = 0;

	/*
	 * - The count of this function will be PAGE_SIZE (4096).
	 * - At least, this function will be called two times.
	 * -- At first time, the off will be 0.
	 * -- At next time, the off will be !0.
	 * -- If the return value is 0, a read operation will be finish.
	 */
	if (!off) {
		pd->slv_buf = kmalloc(pd->slave_buffer_size, GFP_KERNEL);
		if (!pd->slv_buf)
			return 0;

		memset(&pkt, 0, sizeof(pkt));
		pkt.rw = RIIC_CORE_RW_SLAVE_RECEIVE;
		pkt.len = pd->slave_buffer_size;
		pkt.data = pd->slv_buf;
		ret = riic_core_slave(pd->adap.nr, &pkt, 1000);

		if (ret == RIIC_CORE_NO_ERROR) {
			pd->slv_buf_index = 0;
			pd->slv_buf_remain = pkt.actual_len;
		}
	}

	if (pd->slv_buf_remain) {
		size = min_t(int, pd->slv_buf_remain, count);
		memcpy(buf, pd->slv_buf + pd->slv_buf_index, size);
		pd->slv_buf_index += size;
		pd->slv_buf_remain -= size;
	}

	if (!pd->slv_buf_remain) {
		kfree(pd->slv_buf);
		pd->slv_buf = NULL;
	}

	return size;
}

static ssize_t riic_show_debug(struct device *dev,
			       struct device_attribute *dattr,
			       char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);
	struct riic_core_status status;
	int ret;

	ret = riic_core_get_status(pd->adap.nr, &status);
	if (ret != RIIC_CORE_NO_ERROR)
		return 0;

	return sprintf(buf, "%d\n", status.debug);
}

static ssize_t riic_set_debug(struct device *dev,
			      struct device_attribute *dattr,
			      const char *buf, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(dev);
	unsigned int debug;

	sscanf(buf, "%d", &debug);
	riic_core_debug(pd->adap.nr, debug);

	return count;
}

static ssize_t riic_show_slv_timeout(struct device *dev,
				     struct device_attribute *dattr,
				     char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	return sprintf(buf, "%d\n", pd->slave_timeout);
}

static ssize_t riic_set_slv_timeout(struct device *dev,
				    struct device_attribute *dattr,
				    const char *buf, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	sscanf(buf, "%d", &pd->slave_timeout);

	return count;
}

static ssize_t riic_show_slv_bufsize(struct device *dev,
				     struct device_attribute *dattr,
				     char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	return sprintf(buf, "%d\n", pd->slave_buffer_size);
}

static ssize_t riic_set_slv_bufsize(struct device *dev,
				    struct device_attribute *dattr,
				    const char *buf, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	sscanf(buf, "%d", &pd->slave_buffer_size);
	if (pd->slave_buffer_size > I2C_RIIC_MAX_SLAVE_BUFFER_SIZE)
		pd->slave_buffer_size = I2C_RIIC_MAX_SLAVE_BUFFER_SIZE;
	return count;
}

static ssize_t riic_show_slv_num_of_packets(struct device *dev,
				struct device_attribute *dattr, char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	return sprintf(buf, "%d\n", pd->slave_number_of_packets);
}

static ssize_t riic_set_slv_num_of_packets(struct device *dev,
					   struct device_attribute *dattr,
					   const char *buf, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	sscanf(buf, "%d", &pd->slave_number_of_packets);

	return count;
}

static ssize_t riic_show_slv_options(struct device *dev,
				     struct device_attribute *dattr,
				     char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	return sprintf(buf, "%lx\n", pd->slave_options);
}

static ssize_t riic_set_slv_options(struct device *dev,
				    struct device_attribute *dattr,
				    const char *buf, size_t count)
{
	struct riic_data *pd = dev_get_drvdata(dev);

	sscanf(buf, "%lx", &pd->slave_options);

	return count;
}

static ssize_t riic_show_dump(struct device *dev,
			      struct device_attribute *dattr,
			      char *buf)
{
	struct riic_data *pd = dev_get_drvdata(dev);
	struct riic_core_status status;
	int ret, i;

	ret = riic_core_get_status(pd->adap.nr, &status);
	if (ret != RIIC_CORE_NO_ERROR)
		return 0;

	for (i = 0; i < RIIC_CORE_NUM_REGS; i++) {
		if (i && !(i % 8))
			sprintf(buf, "%s   ", buf);
		sprintf(buf, "%s%02x ", buf, status.regs[i]);
	}
	sprintf(buf, "%s\n", buf);
	if (status.slave_enabled)
		sprintf(buf, "%sslave_buffer_size=%d, "
			"slave_number_of_packets=%d\n", buf,
			status.slave_buffer_size,
			status.slave_number_of_packets);
	return sprintf(buf, "%sslave_enabled=%d, debug=%d\n", buf,
		status.slave_enabled, status.debug);
}

static struct device_attribute riic_device_attributes[] = {
	__ATTR(slv0_addr, 0644, riic_show_slv_addr,
				riic_set_slv_addr),
	__ATTR(clock, 0644, riic_show_clock, riic_set_clock),
	__ATTR(debug, 0644, riic_show_debug, riic_set_debug),
	__ATTR(slv_timeout, 0644, riic_show_slv_timeout,
				  riic_set_slv_timeout),
	__ATTR(slv_bufsize, 0644, riic_show_slv_bufsize,
				  riic_set_slv_bufsize),
	__ATTR(slv_num_of_packets, 0644, riic_show_slv_num_of_packets,
				  riic_set_slv_num_of_packets),
	__ATTR(slv_options, 0644, riic_show_slv_options,
				  riic_set_slv_options),
	__ATTR(dump, 0444, riic_show_dump, NULL),
};

static struct bin_attribute dev_bin_attr_slv_data = {
	.attr = {.name = "slv_data", .mode = 0444 },
	.read = riic_show_slv_data,
	.size = I2C_RIIC_MAX_SLAVE_BUFFER_SIZE,
};

/* platform device */
static int  riic_remove(struct platform_device *pdev)
{
	struct riic_data *pd = platform_get_drvdata(pdev);
	int i;

	for (i = 0; i < ARRAY_SIZE(riic_device_attributes); i++)
		device_remove_file(&pdev->dev,
				&riic_device_attributes[i]);
	device_remove_bin_file(&pdev->dev, &dev_bin_attr_slv_data);
	riic_core_close(pd->adap.nr);
	i2c_del_adapter(&pd->adap);
	kfree(pd);

	return 0;
}

static int  riic_probe(struct platform_device *pdev)
{
	struct riic_data *pd = NULL;
	struct i2c_adapter *adap;
	int ret = 0, i;

	pd = kzalloc(sizeof(struct riic_data), GFP_KERNEL);
	if (pd == NULL) {
		ret = -ENOMEM;
		dev_err(&pdev->dev, "kzalloc failed.\n");
		goto clean_up;
	}

	pd->dev = &pdev->dev;
	platform_set_drvdata(pdev, pd);

	adap = &pd->adap;
	i2c_set_adapdata(adap, pd);

	adap->owner = THIS_MODULE;
	adap->algo = &riic_algorithm;
	adap->dev.parent = &pdev->dev;
	adap->retries = 5;
	adap->nr = pdev->id;

	riic_core_open(adap->nr, I2C_RIIC_DEFAULT_CLOCK);
	pd->slave_timeout = I2C_RIIC_DEFAULT_SLAVE_TIMEOUT;
	pd->slave_buffer_size = I2C_RIIC_DEFAULT_SLAVE_BUFFER_SIZE;
	pd->slave_number_of_packets = I2C_RIIC_DEFAULT_SLAVE_NUM_OF_PACKETS;

	strlcpy(adap->name, pdev->name, sizeof(adap->name));

	for (i = 0; i < ARRAY_SIZE(riic_device_attributes); i++) {
		ret = device_create_file(&pdev->dev,
				&riic_device_attributes[i]);
		if (ret < 0) {
			printk(KERN_ERR "device_create_file failed\n");
			goto clean_up2;
		}
	}

	ret = device_create_bin_file(&pdev->dev, &dev_bin_attr_slv_data);
	if (ret < 0) {
		printk(KERN_ERR "device_create_file error\n");
		goto clean_up2;
	}

	ret = i2c_add_numbered_adapter(adap);
	if (ret < 0) {
		dev_err(&pdev->dev, "i2c_add_numbered_adapter failed.\n");
		goto clean_up3;
	}

	dev_info(&pdev->dev, "SAMPLE DRIVER %s\n", I2C_RIIC_VERSION);
	return ret;

clean_up3:
	device_remove_bin_file(&pdev->dev, &dev_bin_attr_slv_data);

clean_up2:
	for (i = 0; i < ARRAY_SIZE(riic_device_attributes); i++)
		device_remove_file(&pdev->dev,
				&riic_device_attributes[i]);
clean_up:
	kfree(pd);

	return ret;
}

static struct platform_driver riic_driver = {
	.probe =	riic_probe,
	.remove =	riic_remove,
	.driver		= {
		.name	= "i2c-riic",
		.owner	= THIS_MODULE,
	},
};
module_platform_driver(riic_driver);

MODULE_DESCRIPTION("RIIC I2C Driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yoshihiro Shimoda");
MODULE_ALIAS("platform:i2c-riic");

