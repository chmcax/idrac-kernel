/*
 * Renesas RIIC core driver
 *
 * Copyright (C) 2012  Renesas Solutions Corp.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/workqueue.h>
#include <linux/sched.h>
#include <linux/dma-mapping.h>
#include <linux/version.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 4, 0)
#include "riic-dmac.h"
#else
#include <asm/dma.h>
#endif

#include "riic-core.h"

#define RIIC_NUM_IRQS	4

#define DMAC_DMAER2		0xfffa0000

#define LOST_IRQ_WORKAROUND	1
#define RIIC_DRIVER_REVISION	"2012-10-12"

static const unsigned long riic_phys_addr[RIIC_NUM_CHANNELS] = {
	0xfe500000, 0xfe510000, 0xfe520000, 0xfe530000, 0xfe540000,
	0xfe550000, 0xfe560000, 0xfe570000, 0xfe580000, 0xfe590000,
};

static const int riic_irqs[RIIC_NUM_CHANNELS][RIIC_NUM_IRQS] = {
	{ 144, 145, 146, 147 },		/* ch0 */
	{ 148, 151, 152, 153 },		/* ch1 */
	{ 154, 155, 156, 160 },		/* ch2 */
	{ 161, 162, 167, 168 },		/* ch3 */
	{ 174, 176, 177, 178 },		/* ch4 */
	{ 179, 180, 181, 182 },		/* ch5 */
	{ 183, 184, 185, 188 },		/* ch6 */
	{ 189, 192, 193, 194 },		/* ch7 */
	{ 195, 196, 197, 202 },		/* ch8 */
	{ 203, 204, 208, 209 },		/* ch9 */
};

enum {
	RIIC_TXI,
	RIIC_TEI,
	RIIC_RXI,
	RIIC_EEI
};

static const char *riic_name[RIIC_NUM_CHANNELS] = {
	"riic.0", "riic.1", "riic.2", "riic.3", "riic.4",
	"riic.5", "riic.6", "riic.7", "riic.8", "riic.9",
};

enum riic_state {
	RIIC_STATE_IDLE,
	RIIC_STATE_MASTER,
	RIIC_STATE_SLAVE,
	RIIC_STATE_BUS_RECOVERY
};

enum riic_master_state {
	RIIC_MASTER_PREPARE,
	RIIC_MASTER_ISSUED_START,
	RIIC_MASTER_SENT_SLAVE_ADDRESS,
	RIIC_MASTER_DMAC_FINISHED,
	RIIC_MASTER_TRANSMIT_LAST,
	RIIC_MASTER_RECEIVE_LAST,
	RIIC_MASTER_ISSUED_STOP,
	RIIC_MASTER_END,
};

enum riic_slave_state {
	RIIC_SLAVE_ISSUED_START,
	RIIC_SLAVE_START_XFER,
	RIIC_SLAVE_DMAC_FINISHED,
	RIIC_SLAVE_ISSUED_STOP,

	RIIC_SLAVE_ISSUED_RESTART,
	RIIC_SLAVE_TIMEOUT,
};

struct riic_data {
	int		channel;
	const char	*name;
	int		irqs[RIIC_NUM_IRQS];
	void __iomem	*reg;
	unsigned long	phys_addr;
	spinlock_t	lock;
	int		clock;
	enum riic_state	state;

	/* for master mode */
	enum riic_master_state	master_state;
	struct riic_core_packet	*pkt;
	int			num_pkt;
	int			pkt_index;
	wait_queue_head_t	wait;
	int			sending_addr;	/* for _AL or _NACK */

	/* for slave mode */
	enum riic_slave_state	slave_state;
	struct riic_core_packet	*slv_rx_pkt;
	int			slv_rx_head;
	int			slv_rx_tail;
	int			slv_timeout;
	int			slv_buffer_size;
	int			slv_num_of_packets;
	wait_queue_head_t	slv_wait;
	struct timer_list	slv_timer;

	/* for local DMA */
	int		dma_ch;
	unsigned char	dma_rx_resource;
	unsigned char	dma_tx_resource;
#ifdef LOST_IRQ_WORKAROUND
	unsigned char	dummy_src[2];
	unsigned char	dummy_dst[2];
	int		dummy_dma;
#endif
	unsigned	dma_callbacked:1;
	unsigned	dma_act:1;

	/* for debug */
	int		debug;
	enum riic_state	old_state;
	unsigned char	old_icsr2;

	/* status */
	unsigned	slave_enabled:1;
	unsigned	combine_slv_rd:1;
	/* status for master mode */
	unsigned	completed:1;
	unsigned	aled:1;
	unsigned	nacked:1;

#if RIIC_CORE_PARAM_LOGGING
	char		log_str[RIIC_CORE_PARAM_LOGGING];
	int		log_index;
#endif
};

static struct riic_data *riic_data[RIIC_NUM_CHANNELS];

static irqreturn_t riic_irq(int irq, void *data);
#if RIIC_CORE_PARAM_LOGGING
static void _riic_dmac_preamble(struct riic_data *rd, int read, int line);
#define riic_dmac_preamble(rd, read)	_riic_dmac_preamble(rd, read, __LINE__);
#else
static void riic_dmac_preamble(struct riic_data *rd, int read);
#endif
static void riic_slave_disable_address(struct riic_data *rd, int reg_index);
static int riic_slave_enable_address(struct riic_data *rd, int reg_index);
static void riic_slave_prepare(struct riic_data *rd);
static void riic_irq_idle_start(struct riic_data *rd, unsigned char icsr2);

#if RIIC_CORE_PARAM_LOGGING
#define riic_core_log_get_buf(rd)	(rd->log_str + rd->log_index)
#define riic_core_log_add(rd, param...)					\
do {									\
	char _log_str[64];						\
	int remain;							\
	unsigned long flags;						\
									\
	if (rd->debug) {						\
		local_irq_save(flags);					\
		remain = RIIC_CORE_PARAM_LOGGING - rd->log_index;	\
		snprintf(_log_str, sizeof(_log_str), param);		\
		if (strlen(_log_str) + 1 < remain)			\
			rd->log_index += snprintf(riic_core_log_get_buf(rd), \
						  remain, _log_str);	\
		else							\
			rd->log_index = RIIC_CORE_PARAM_LOGGING;	\
		local_irq_restore(flags);				\
	}								\
} while (0)

#define riic_core_log_dump(rd)						\
do {									\
	unsigned long flags;						\
									\
	if (rd->debug) {						\
		local_irq_save(flags);					\
		if (rd->log_index)					\
			printk("ch%d: %s\n", rd->channel, rd->log_str);	\
		local_irq_restore(flags);				\
	}								\
} while (0)

#define riic_core_log_clear(rd)						\
do {									\
	unsigned long flags;						\
									\
	if (rd->debug) {						\
		local_irq_save(flags);					\
		rd->log_str[0] = 0x00;					\
		rd->log_index = 0;					\
		local_irq_restore(flags);				\
	}								\
} while (0)
#else
#define riic_core_log_add(rd, param...)		do { } while (0)
#define riic_core_log_dump(rd)			do { } while (0)
#define riic_core_log_clear(rd)			do { } while (0)
#endif

static struct riic_data *riic_get_data(int channel)
{
	return riic_data[channel];
}

static unsigned char riic_read(struct riic_data *rd, unsigned long addr)
{
	return ioread8(rd->reg + addr);
}

static void riic_write(struct riic_data *rd, unsigned char data,
		       unsigned long addr)
{
	iowrite8(data, rd->reg + addr);
}

static void riic_set_bit(struct riic_data *rd, unsigned char val,
			 unsigned long offset)
{
	unsigned char tmp;
	unsigned long flags;

	local_irq_save(flags);

	tmp = riic_read(rd, offset);
	tmp |= val;
	riic_write(rd, tmp, offset);

	local_irq_restore(flags);
}

static void riic_clear_bit(struct riic_data *rd, unsigned char val,
			   unsigned long offset)
{
	unsigned char tmp;
	unsigned long flags;

	local_irq_save(flags);

	tmp = riic_read(rd, offset);
	tmp &= ~val;
	riic_write(rd, tmp, offset);

	local_irq_restore(flags);
}

static void riic_set_transmit_nack(struct riic_data *rd, int nack)
{
	if (nack)
		riic_set_bit(rd, ICMR3_ACKBT, RIIC_ICMR3);
	else
		riic_clear_bit(rd, ICMR3_ACKBT, RIIC_ICMR3);
}

static void riic_dump(struct riic_data *rd, const char *str)
{
	int i;

	if (!rd->debug)
		return;

	/* Driver state */
	printk("from %s: ch%d, %d / %d, %d, %d, %d, %d, %d / %d, %d, %d "
		"/ %d, %02x, %d\n",
		str,
		rd->channel, rd->state,
		rd->master_state, rd->num_pkt, rd->pkt_index,
		rd->completed, rd->nacked, rd->aled,
		rd->slave_state, rd->slv_rx_head, rd->slv_rx_tail,
		rd->dma_callbacked, rd->old_icsr2, rd->old_state);

	/* Registers */
	for (i = 0; i < RIIC_CORE_NUM_REGS; i++) {
		if (i && !(i % 8))
			printk("   ");
		printk("%02x ", riic_read(rd, i));
	}
	printk("\n");
}

/* DMAC */
#if RIIC_CORE_PARAM_LOGGING
#define riic_stop_dma(rd)	_riic_stop_dma(rd, __LINE__)
static inline void _riic_stop_dma(struct riic_data *rd, int line)
#else
static inline void riic_stop_dma(struct riic_data *rd)
#endif
{
	unsigned long flags;

#if RIIC_CORE_PARAM_LOGGING
	riic_core_log_add(rd, "SD%d,", line);
#endif

	local_irq_save(flags);
	dma_configure_channel(rd->dma_ch, 0x0);
	dma_ex_resource(rd->dma_ch, 0);
	rd->dma_act = 0;
	local_irq_restore(flags);
}

static void riic_master_dma_callback(struct riic_data *rd)
{
	struct riic_core_packet *pkt = rd->pkt;

	if (rd->master_state != RIIC_MASTER_SENT_SLAVE_ADDRESS) {
		rd->master_state = RIIC_MASTER_DMAC_FINISHED;
		return;
	}

	if (pkt->rw == RIIC_CORE_RW_MASTER_TRANSMIT) {
		pkt->actual_len = pkt->len - get_dma_residue(rd->dma_ch);
		riic_set_bit(rd, ICIER_TEIE, RIIC_ICIER);
		rd->master_state = RIIC_MASTER_TRANSMIT_LAST;
	} else {
		rd->master_state = RIIC_MASTER_RECEIVE_LAST;
		pkt->actual_len = pkt->len - 2 - get_dma_residue(rd->dma_ch);
		/* Since ICIER_RIE is always set, we can preamble it */
		riic_dmac_preamble(rd, 1);
	}
}

static void riic_slave_dma_callback(struct riic_data *rd)
{
	rd->slave_state = RIIC_SLAVE_DMAC_FINISHED;
}

static void riic_dmac_callback(void *_data)
{
	struct riic_data *rd = _data;

	riic_stop_dma(rd);

	rd->dma_callbacked = 1;
	rd->dma_act = 0;
#ifdef LOST_IRQ_WORKAROUND
	if (rd->dummy_dma) {
		/*
		 * Actual IRQ number does not matter since we only
		 * have one ISR for all IRQ's.
		 */
		rd->dummy_dma = 0;
		riic_irq(rd->irqs[RIIC_RXI], rd);
	} else
#endif
	{
		switch (rd->state) {
		case RIIC_STATE_MASTER:
			riic_master_dma_callback(rd);
			break;
		case RIIC_STATE_SLAVE:
			riic_slave_dma_callback(rd);
			break;
		default:
			break;
		}
	}
}

#ifdef LOST_IRQ_WORKAROUND
#if RIIC_CORE_PARAM_LOGGING
static void _riic_dmac_preamble(struct riic_data *rd, int read, int line)
#else
static void riic_dmac_preamble(struct riic_data *rd, int read)
#endif
{
#if RIIC_CORE_PARAM_LOGGING
	riic_core_log_add(rd, "DP%c%d,", read ? 'r' : 'w', line);
#endif
	rd->dma_callbacked = 0;
	rd->dma_act = 1;
	if (read) {
		riic_clear_bit(rd, ICIER_TIE, RIIC_ICIER);
		dma_configure_channel(rd->dma_ch, 0x40004804);
		dma_ex_resource(rd->dma_ch, rd->dma_rx_resource);
		dma_completion_callback(rd->dma_ch, riic_dmac_callback, rd);
		rd->dummy_dma = 1;
		dma_read(rd->dma_ch, virt_to_phys(&rd->dummy_src[0]),
			 virt_to_phys(&rd->dummy_dst[0]), 1);
	} else {
		riic_set_bit(rd, ICIER_TIE, RIIC_ICIER);
		dma_configure_channel(rd->dma_ch, 0x40001804);
		dma_ex_resource(rd->dma_ch, rd->dma_tx_resource);
		dma_completion_callback(rd->dma_ch, riic_dmac_callback, rd);
		rd->dummy_dma = 2;
		dma_write(rd->dma_ch, virt_to_phys(&rd->dummy_src[0]),
			  virt_to_phys(&rd->dummy_dst[0]), 2);
	}
}
#else
#define riic_dmac_preamble(rd, read) do { } while (0)
#endif

/* Master Mode */
static int riic_master_is_last_packet(struct riic_data *rd)
{
	if (rd->num_pkt - (rd->pkt_index + 1) == 0)
		return 1;
	else
		return 0;
}

static void riic_master_update_packet(struct riic_data *rd)
{
	/* update current pkt */
	rd->pkt->done = 1;

	/* increment pkt pointer if needed */
	if (!riic_master_is_last_packet(rd)) {
		rd->pkt++;
		rd->pkt_index++;
	} else {
		rd->pkt = NULL;
	}
}

static int riic_master_is_sending_address(struct riic_data *rd)
{
	int ret = 0;

	switch (rd->master_state) {
	case RIIC_MASTER_PREPARE:
	case RIIC_MASTER_ISSUED_START:
		ret = 1;
		break;
	default:
		break;
	}

	return ret;
}

static void riic_master_clean_icier(struct riic_data *rd)
{
	/* disable for master mode */
	riic_clear_bit(rd, ICIER_NAKIE | ICIER_ALIE | ICIER_TEIE | ICIER_SPIE,
		       RIIC_ICIER);

	/* enable for slave mode */
	riic_set_bit(rd, ICIER_STIE, RIIC_ICIER);
}

static int riic_master_issue_stop(struct riic_data *rd)
{
	if (riic_read(rd, RIIC_ICCR2) & ICCR2_MST) {
		riic_set_bit(rd, ICIER_SPIE, RIIC_ICIER);
		riic_set_bit(rd, ICCR2_SP, RIIC_ICCR2);
		return 0;
	}

	return -EFAULT;
}

static void riic_master_try_to_free_sda(struct riic_data *rd)
{
	unsigned long flags;
	int i, j;

	if (!(riic_read(rd, RIIC_ICCR2) & ICCR2_MST))
		return;
	/* If SDA is already high, RIIC will not output extra SCL */
	if (riic_read(rd, RIIC_ICCR1) & ICCR1_SDAI)
		return;
	/* If SCL is low held by other device, RIIC cannot output extra SCL */
	if (!(riic_read(rd, RIIC_ICCR1) & ICCR1_SCLI))
		return;

	spin_lock_irqsave(&rd->lock, flags);
	riic_clear_bit(rd, ICFER_MALE, RIIC_ICFER);
	for (i = 0; i < 32; i++) {
		if (riic_read(rd, RIIC_ICCR1) & ICCR1_SDAI)
			break;

		riic_set_bit(rd, ICCR1_CLO, RIIC_ICCR1);
		j = 3000;
		while (riic_read(rd, RIIC_ICCR1) & ICCR1_CLO) {
			udelay(1);
			if (j-- < 0) {
				printk(KERN_ERR "%s: CLO timeout\n", __func__);
				break;
			}
		}
	}
	riic_set_bit(rd, ICFER_MALE, RIIC_ICFER);
	spin_unlock_irqrestore(&rd->lock, flags);
}

static void riic_irq_master_al(struct riic_data *rd, unsigned char icsr2)
{
	rd->state = RIIC_STATE_IDLE;
	rd->sending_addr = riic_master_is_sending_address(rd);
	riic_master_clean_icier(rd);
	riic_slave_prepare(rd);	/* enter RIIC_STATE_SLAVE */
	rd->pkt = NULL;
	rd->aled = 1;
	wake_up_interruptible(&rd->wait);
}

static void riic_send_slave_address(struct riic_data *rd)
{
	unsigned char sa_rw;
	int read = (rd->pkt->rw == RIIC_CORE_RW_MASTER_TRANSMIT) ? 0 : 1;

	sa_rw = (rd->pkt->slave_address << 1) | read;
	riic_dmac_preamble(rd, read);
	if (!read)
		riic_set_bit(rd, ICIER_TEIE, RIIC_ICIER);
	riic_write(rd, sa_rw, RIIC_ICDRT);
}

static void riic_master_kick_transmit(struct riic_data *rd)
{
	unsigned char *buf = (unsigned char *)rd->pkt->data;

	if (rd->pkt->len > 1) {
		riic_stop_dma(rd);
		riic_set_bit(rd, ICIER_TIE, RIIC_ICIER);

		dma_configure_channel(rd->dma_ch, 0x40001804);
		dma_ex_resource(rd->dma_ch, rd->dma_tx_resource);
		dma_completion_callback(rd->dma_ch, riic_dmac_callback, rd);
		dma_cache_sync(NULL, buf, rd->pkt->len, DMA_TO_DEVICE);
		rd->dma_callbacked = 0;
#ifdef LOST_IRQ_WORKAROUND
		rd->dummy_dma = 0;
#endif
		rd->dma_act = 1;
		dma_write(rd->dma_ch, virt_to_phys(&buf[1]),
			  rd->phys_addr + RIIC_ICDRT,
			  rd->pkt->len - 1);
	}

	/* kick off the DMA transfer by writing the first byte */
	riic_write(rd, buf[0], RIIC_ICDRT);
	rd->pkt->actual_len = 1;
	if (rd->pkt->len <= 1) {
		rd->master_state = RIIC_MASTER_TRANSMIT_LAST;
		riic_set_bit(rd, ICIER_TEIE, RIIC_ICIER);
	}
}

static void riic_irq_master_nackf(struct riic_data *rd)
{
	if (rd->master_state != RIIC_MASTER_END) {
		rd->sending_addr = riic_master_is_sending_address(rd);
		rd->master_state = RIIC_MASTER_END;
		riic_clear_bit(rd, ICIER_TEIE, RIIC_ICIER);
		riic_stop_dma(rd);
		riic_read(rd, RIIC_ICDRR);	/* dummy read */
		riic_master_issue_stop(rd);
	}

	rd->nacked = 1;
}

static void riic_irq_master_tend(struct riic_data *rd)
{
	switch (rd->master_state) {
	case RIIC_MASTER_ISSUED_START:
		rd->master_state = RIIC_MASTER_SENT_SLAVE_ADDRESS;
		riic_master_kick_transmit(rd);
		break;
	case RIIC_MASTER_TRANSMIT_LAST:
		/* Last packet? */
		if (riic_master_is_last_packet(rd)) {
			/* issue STOP */
			riic_master_issue_stop(rd);
		} else {
			riic_master_update_packet(rd);
			/* issue RESTART */
			riic_set_bit(rd, ICIER_STIE, RIIC_ICIER);
			riic_set_bit(rd, ICCR2_RS, RIIC_ICCR2);
		}
		break;
	default:
		printk(KERN_ERR "%s: ch%d, unexpect master_state (%d)\n",
			__func__, rd->channel, rd->master_state);
		break;
	}
}

static void riic_master_kick_receive(struct riic_data *rd)
{
	rd->pkt->actual_len = 0;
	if (rd->pkt->len > 2) {
		riic_stop_dma(rd);
		riic_clear_bit(rd, ICIER_TIE, RIIC_ICIER);

		dma_configure_channel(rd->dma_ch, 0x40004804);
		dma_ex_resource(rd->dma_ch, rd->dma_rx_resource);
		dma_completion_callback(rd->dma_ch, riic_dmac_callback, rd);
		dma_cache_sync(NULL, rd->pkt->data, rd->pkt->len,
			       DMA_BIDIRECTIONAL);
		rd->dma_callbacked = 0;
#ifdef LOST_IRQ_WORKAROUND
		rd->dummy_dma = 0;
#endif
		rd->dma_act = 1;
		dma_read(rd->dma_ch, rd->phys_addr + RIIC_ICDRR,
			 virt_to_phys(rd->pkt->data), rd->pkt->len - 2);
		/* Start DMA by issuing a dummy read */
		riic_read(rd, RIIC_ICDRR);
	} else {
		riic_dmac_preamble(rd, 1);
		if (rd->pkt->len == 1)
			riic_set_transmit_nack(rd, 1);

		rd->master_state = RIIC_MASTER_RECEIVE_LAST;
		/* issue a dummy read */
		riic_read(rd, RIIC_ICDRR);
	}
}

static void riic_irq_master_rdrf(struct riic_data *rd)
{
	struct riic_core_packet *pkt = rd->pkt;
	unsigned char *buf = pkt->data;

	switch (rd->master_state) {
	case RIIC_MASTER_ISSUED_START:
		rd->master_state = RIIC_MASTER_SENT_SLAVE_ADDRESS;
		riic_master_kick_receive(rd);
		break;
	case RIIC_MASTER_RECEIVE_LAST:
		if (pkt->len - pkt->actual_len >= 2) {
			riic_set_transmit_nack(rd, 1);
			riic_dmac_preamble(rd, 1);
			buf[pkt->actual_len++] = riic_read(rd, RIIC_ICDRR);
		} else if (pkt->len - pkt->actual_len >= 1) {
			if (riic_master_is_last_packet(rd)) {
				/* issue STOP */
				riic_master_issue_stop(rd);
				riic_read(rd, RIIC_ICCR2); /* dummy read */
			}

			buf[pkt->actual_len++] = riic_read(rd, RIIC_ICDRR);

			if (!riic_master_is_last_packet(rd)) {
				riic_master_update_packet(rd);
				/* issue RESTART */
				riic_set_bit(rd, ICIER_STIE, RIIC_ICIER);
				riic_set_bit(rd, ICCR2_RS, RIIC_ICCR2);
			}
		}
		break;
	default:
		break;
	}

}

static void riic_irq_master_start(struct riic_data *rd, unsigned char icsr2)
{
	if (!(riic_read(rd, RIIC_ICCR2) & ICCR2_MST)) {
		riic_dump(rd, __func__);
		riic_slave_prepare(rd);
		return;
	}

	switch (rd->master_state) {
	case RIIC_MASTER_PREPARE:
	case RIIC_MASTER_TRANSMIT_LAST:
	case RIIC_MASTER_RECEIVE_LAST:
		rd->master_state = RIIC_MASTER_ISSUED_START;
		if (icsr2 & ICSR2_TDRE)
			riic_send_slave_address(rd);
		break;
	default:
		printk(KERN_ERR "%s: ch%d, unexpect master_state (%d)\n",
			__func__, rd->channel, rd->master_state);
		break;
	}
}

static void riic_irq_master_stop(struct riic_data *rd)
{
	switch (rd->master_state) {
	case RIIC_MASTER_TRANSMIT_LAST:
	case RIIC_MASTER_RECEIVE_LAST:
	case RIIC_MASTER_END:
		rd->master_state = RIIC_MASTER_ISSUED_STOP;
		riic_master_clean_icier(rd);

		rd->state = RIIC_STATE_IDLE;
		if (!rd->nacked) {
			riic_master_update_packet(rd);
			rd->completed = 1;
		}
		wake_up_interruptible(&rd->wait);
		break;
	case RIIC_MASTER_ISSUED_START:
		/* avoid to output the following message */
		break;
	default:
		riic_dump(rd, __func__);
		printk(KERN_ERR "%s: ch%d, unexpect master_state (%d)\n",
			__func__, rd->channel, rd->master_state);
		break;
	}
}

static void riic_irq_master(struct riic_data *rd, unsigned char icsr2)
{
	if (icsr2 & ICSR2_AL) {
		riic_clear_bit(rd, ICSR2_AL, RIIC_ICSR2);
		riic_irq_master_al(rd, icsr2);
		return;
	}
	if (icsr2 & ICSR2_NACKF) {
		riic_clear_bit(rd, ICSR2_NACKF, RIIC_ICSR2);
		riic_irq_master_nackf(rd);
		return;
	}

	if (icsr2 & ICSR2_TEND &&
	    riic_read(rd, RIIC_ICIER) & ICIER_TEIE) {
		riic_clear_bit(rd, ICIER_TEIE, RIIC_ICIER);
		riic_irq_master_tend(rd);
	}
	if (icsr2 & ICSR2_RDRF)
		riic_irq_master_rdrf(rd);

	switch ((icsr2 & (ICSR2_START | ICSR2_STOP))) {
	case ICSR2_START:
		riic_clear_bit(rd, ICIER_STIE, RIIC_ICIER);
		riic_clear_bit(rd, ICSR2_START, RIIC_ICSR2);
		riic_irq_master_start(rd, icsr2);
		break;
	case (ICSR2_START | ICSR2_STOP):
	case ICSR2_STOP:
		/* At first, the driver processes STOP of this RIIC master */
		riic_clear_bit(rd, ICIER_SPIE, RIIC_ICIER);
		riic_clear_bit(rd, ICSR2_STOP, RIIC_ICSR2);
		riic_irq_master_stop(rd);

		/*
		 * If RIIC detected START of other master device at this timing,
		 * the driver will call riic_irq_idle_start() directly because
		 * riic_dmac_preamble() should be called quickly.
		 */
		if ((rd->state == RIIC_STATE_IDLE) &&
		    (icsr2 & ICSR2_START)) {
			riic_clear_bit(rd, ICSR2_START, RIIC_ICSR2);
			riic_irq_idle_start(rd, icsr2);
		}
		break;
	default:
		break;
	}
}

static void riic_master_clean_up(struct riic_data *rd, int timeout)
{
	unsigned long flags;
	int ret;

retry:
	spin_lock_irqsave(&rd->lock, flags);
	if (rd->state != RIIC_STATE_MASTER) {
		spin_unlock_irqrestore(&rd->lock, flags);
		return;
	}
	if (!(riic_read(rd, RIIC_ICCR2) & ICCR2_MST)) {
		spin_unlock_irqrestore(&rd->lock, flags);
		goto out;
	}

	rd->master_state = RIIC_MASTER_END;
	riic_stop_dma(rd);
	/* dummy read if needed */
	if (riic_read(rd, RIIC_ICSR2) & ICSR2_RDRF)
		riic_read(rd, RIIC_ICDRR);
	riic_master_try_to_free_sda(rd);
	ret = riic_master_issue_stop(rd);
	rd->completed = 0;
	spin_unlock_irqrestore(&rd->lock, flags);

	if (!ret)
		ret = wait_event_interruptible_timeout(rd->wait, rd->completed,
					msecs_to_jiffies(timeout));

	/* if a signal happens, the driver sleeps a little to wait STOP */
	if (ret == -ERESTARTSYS)
		msleep(10);

	if (riic_read(rd, RIIC_ICCR2) & ICCR2_MST) {
		riic_dump(rd, "re-issue stop");
		goto retry;
	}

out:
	spin_lock_irqsave(&rd->lock, flags);
	rd->state = RIIC_STATE_IDLE;
	riic_master_clean_icier(rd);
	rd->pkt = NULL;
	spin_unlock_irqrestore(&rd->lock, flags);
}

/* Slave Mode */
static struct riic_core_packet *riic_slave_get_pkt_tail(struct riic_data *rd)
{
	if (likely(rd->slv_rx_pkt))
		return &rd->slv_rx_pkt[rd->slv_rx_tail];
	else
		return NULL;
}

static void riic_slave_update_pkt_tail(struct riic_data *rd)
{
	struct riic_core_packet *pkt = riic_slave_get_pkt_tail(rd);
	int next;

	if (unlikely(!pkt))
		return;

	/* update current pkt */
	pkt->done = 1;

	next = (rd->slv_rx_tail + 1) % rd->slv_num_of_packets;
	if (next != rd->slv_rx_head) {
		/* increment slv_rx_tail */
		rd->slv_rx_tail = next;
		pkt = riic_slave_get_pkt_tail(rd);
		if (unlikely(!pkt))
			return;
		pkt->done = 0;
	}
}

static struct riic_core_packet *riic_slave_get_pkt_head(struct riic_data *rd)
{
	if (likely(rd->slv_rx_pkt))
		return &rd->slv_rx_pkt[rd->slv_rx_head];
	else
		return NULL;
}

static void riic_slave_update_pkt_head(struct riic_data *rd)
{
	if (rd->slv_rx_head != rd->slv_rx_tail)
		rd->slv_rx_head = (rd->slv_rx_head + 1) %
						rd->slv_num_of_packets;
}

static int riic_slave_is_empty(struct riic_data *rd)
{
	return rd->slv_rx_head == rd->slv_rx_tail;
}

static void riic_slave_kick_receive(struct riic_data *rd)
{
	struct riic_core_packet *pkt = riic_slave_get_pkt_tail(rd);

	if (unlikely(!pkt))
		return;

	rd->slave_state = RIIC_SLAVE_START_XFER;

	riic_stop_dma(rd);
	riic_clear_bit(rd, ICIER_TIE, RIIC_ICIER);

	dma_configure_channel(rd->dma_ch, 0x40004804);
	dma_ex_resource(rd->dma_ch, rd->dma_rx_resource);
	dma_completion_callback(rd->dma_ch, riic_dmac_callback, rd);
	dma_cache_sync(NULL, pkt->data, pkt->len, DMA_BIDIRECTIONAL);
	rd->dma_callbacked = 0;
#ifdef LOST_IRQ_WORKAROUND
	rd->dummy_dma = 0;
#endif
	rd->dma_act = 1;
	dma_read(rd->dma_ch, rd->phys_addr + RIIC_ICDRR,
		 virt_to_phys(pkt->data), pkt->len);
	pkt->actual_len = 0;
	pkt->slave_address = riic_read(rd, RIIC_ICDRR);
}

static void riic_slave_check_address_detected(struct riic_data *rd)
{
	unsigned char icsr1 = riic_read(rd, RIIC_ICSR1);

	/* Check if the slave address was received */
	if (icsr1 & (ICSR1_AAS0 | ICSR1_AAS1 | ICSR1_AAS2)) {
		if (!rd->combine_slv_rd)
			riic_slave_disable_address(rd, 0);
		riic_slave_kick_receive(rd);
	}
}

static void riic_irq_slave_rdrf(struct riic_data *rd)
{
	switch (rd->slave_state) {
	case RIIC_SLAVE_ISSUED_START:
		riic_slave_check_address_detected(rd);
		break;
	case RIIC_SLAVE_TIMEOUT:
		/* avoid to output the following message */
		break;
	default:
		printk(KERN_ERR "%s: ch%d, unexpect slave_state (%d)\n",
			__func__, rd->channel, rd->slave_state);
		break;
	}
}

static void riic_slave_start_rx_timer(struct riic_data *rd)
{
	mod_timer(&rd->slv_timer, jiffies + msecs_to_jiffies(rd->slv_timeout));
}

static void riic_slave_stop_rx_timer(struct riic_data *rd)
{
	del_timer(&rd->slv_timer);
}

static void riic_slave_prepare(struct riic_data *rd)
{
	riic_set_bit(rd, ICIER_SPIE, RIIC_ICIER);

	if (!rd->slave_enabled)
		return;

	rd->state = RIIC_STATE_SLAVE;
	rd->slave_state = RIIC_SLAVE_ISSUED_START;
	riic_clear_bit(rd, ICIER_STIE, RIIC_ICIER);
	riic_set_transmit_nack(rd, 0);
	riic_slave_start_rx_timer(rd);
	riic_dmac_preamble(rd, 1);
}

static void riic_slave_clean_up(struct riic_data *rd)
{
	unsigned char icsr2_mask = ICSR2_RDRF | ICSR2_START;

	riic_stop_dma(rd);
	if (riic_slave_enable_address(rd, 0) < 0) {
		riic_dump(rd, __func__);
		return;
	}
	if ((riic_read(rd, RIIC_ICSR2) & icsr2_mask) == ICSR2_RDRF) {
		riic_dump(rd, "remain RDRF without START");
		return;
	}

	riic_slave_stop_rx_timer(rd);

	rd->slave_state = RIIC_SLAVE_ISSUED_STOP;
	rd->state = RIIC_STATE_IDLE;
	riic_set_bit(rd, ICIER_STIE, RIIC_ICIER);
}

static void riic_slave_receive_done(struct riic_data *rd)
{
	struct riic_core_packet *pkt = riic_slave_get_pkt_tail(rd);

	if (unlikely(!pkt))
		return;

	pkt->actual_len = pkt->len - get_dma_residue(rd->dma_ch);

	/* update for next transfer */
	riic_slave_update_pkt_tail(rd);
	riic_slave_clean_up(rd);

	wake_up_interruptible(&rd->wait);
}

static void riic_irq_slave_stop(struct riic_data *rd)
{
	switch (rd->slave_state) {
	case RIIC_SLAVE_START_XFER:
	case RIIC_SLAVE_DMAC_FINISHED:
		riic_slave_receive_done(rd);
		break;
	default:
		riic_slave_clean_up(rd);
		break;
	}
}

static void riic_slave_timer(unsigned long data)
{
	struct riic_data *rd = (struct riic_data *)data;
	unsigned long flags;

	local_irq_save(flags);

	if (rd->slave_state != RIIC_SLAVE_TIMEOUT)
		riic_dump(rd, __func__);

	rd->slave_state = RIIC_SLAVE_TIMEOUT;
	riic_stop_dma(rd);
	riic_clear_bit(rd, ICIER_STIE | ICIER_SPIE, RIIC_ICIER);

	if (riic_read(rd, RIIC_ICSR2) & ICSR2_RDRF) {
		/* If we don't call this preamble, we will fail in next xfer */
		riic_dmac_preamble(rd, 1);
		riic_read(rd, RIIC_ICDRR);	/* dummy read */
	}

	/* restart slave timer if the BUS is still busy */
	if (riic_read(rd, RIIC_ICCR2) & ICCR2_BBSY) {
		riic_slave_start_rx_timer(rd);
	} else {
		riic_clear_bit(rd, ICSR2_STOP, RIIC_ICSR2);
		riic_slave_clean_up(rd);
	}

	local_irq_restore(flags);
}

static void riic_irq_slave(struct riic_data *rd, unsigned char icsr2)
{
	if (icsr2 & ICSR2_AL)
		riic_clear_bit(rd, ICSR2_AL, RIIC_ICSR2);
	if (icsr2 & ICSR2_NACKF)
		riic_clear_bit(rd, ICSR2_NACKF, RIIC_ICSR2);
	if (icsr2 & ICSR2_TEND &&
	    riic_read(rd, RIIC_ICIER) & ICIER_TEIE)
		riic_clear_bit(rd, ICIER_TEIE, RIIC_ICIER);

	if (icsr2 & ICSR2_RDRF)
		riic_irq_slave_rdrf(rd);
	switch ((icsr2 & (ICSR2_START | ICSR2_STOP))) {
	case ICSR2_START:
		riic_clear_bit(rd, ICIER_STIE, RIIC_ICIER);
		riic_clear_bit(rd, ICSR2_START, RIIC_ICSR2);
		riic_dump(rd, "irq_slave: start!");
		/* FIXME: maybe restart */
		break;
	case (ICSR2_START | ICSR2_STOP):
		if (rd->combine_slv_rd) {
			/*
			 * Next transaction already started, keep DMAC on
			 * and wait for next STOP
			 */
			riic_clear_bit(rd, ICIER_STIE, RIIC_ICIER);
			riic_clear_bit(rd, ICSR2_START | ICSR2_STOP,
				       RIIC_ICSR2);
			break;
		}
		/* Through */
	case ICSR2_STOP:
		/*
		 * Since the driver will process START interruption next time,
		 * it doesn't clear the START flag.
		 */
		riic_clear_bit(rd, ICSR2_STOP, RIIC_ICSR2);
		riic_clear_bit(rd, ICIER_SPIE, RIIC_ICIER);
		riic_irq_slave_stop(rd);
		break;
	default:
		break;
	}

}

/* Idle */
static void riic_irq_idle_start(struct riic_data *rd, unsigned char icsr2)
{
	unsigned char iccr2 = riic_read(rd, RIIC_ICCR2);

	if (iccr2 & ICCR2_MST) {
		printk(KERN_ERR "%s: No longer in slave mode %d,%02x,%02x\n",
			__func__, rd->channel, icsr2, iccr2);
		return;
	}
	if (!(iccr2 & ICCR2_BBSY)) {
		printk(KERN_ERR "%s: no busy %d, %02x\n", __func__,
			rd->channel, iccr2);
		return;
	}

	riic_slave_prepare(rd);
}

static void riic_irq_idle(struct riic_data *rd, unsigned char icsr2)
{
	if (icsr2 & ICSR2_AL)
		riic_clear_bit(rd, ICSR2_AL, RIIC_ICSR2);
	if (icsr2 & ICSR2_NACKF)
		riic_clear_bit(rd, ICSR2_NACKF, RIIC_ICSR2);
	if (icsr2 & ICSR2_TEND &&
	    riic_read(rd, RIIC_ICIER) & ICIER_TEIE)
		riic_clear_bit(rd, ICIER_TEIE, RIIC_ICIER);

	switch ((icsr2 & (ICSR2_START | ICSR2_STOP))) {
	case (ICSR2_START | ICSR2_STOP):
		/*
		 * If the following sequence, we should clear the STOP flag:
		 *  START -> NACK -> STOP ---> START -> ACK -> (IRQ happens)
		 *  Then, the RIIC's ICSR2 will be set (START | STOP).
		 */
		riic_clear_bit(rd, ICSR2_STOP, RIIC_ICSR2);
		/* through */
	case ICSR2_START:
		riic_clear_bit(rd, ICSR2_START, RIIC_ICSR2);
		riic_irq_idle_start(rd, icsr2);
		break;
	case ICSR2_STOP:
		riic_clear_bit(rd, ICSR2_STOP, RIIC_ICSR2);
		riic_clear_bit(rd, ICIER_SPIE, RIIC_ICIER);
		break;
	default:
		break;
	}
}

static irqreturn_t riic_irq(int irq, void *data)
{
	struct riic_data *rd = data;
	unsigned char icsr2 = riic_read(rd, RIIC_ICSR2);

	rd->old_state = rd->state;
	rd->old_icsr2 = icsr2;

	riic_core_log_add(rd, "RI%02X-%02X%02X", rd->old_icsr2,
			  riic_read(rd, RIIC_ICCR1), riic_read(rd, RIIC_ICCR2));

	switch (rd->state) {
	case RIIC_STATE_MASTER:
		riic_core_log_add(rd, "M%d,", rd->master_state);
		riic_irq_master(rd, icsr2);
		break;
	case RIIC_STATE_SLAVE:
		riic_core_log_add(rd, "S%d,", rd->slave_state);
		riic_irq_slave(rd, icsr2);
		break;
	case RIIC_STATE_IDLE:
		riic_core_log_add(rd, "I,");
		riic_irq_idle(rd, icsr2);
		break;
	default:
		break;
	}

	return IRQ_HANDLED;
}

static int riic_request_irq(struct riic_data *rd)
{
	int i, ret;

	for (i = 0; i < RIIC_NUM_IRQS; i++) {
		ret = request_irq(rd->irqs[i], riic_irq, IRQF_DISABLED,
				  rd->name, rd);
		if (ret < 0) {
			/* call free_irq() for requested irqs */
			for (i = i - 1; i >= 0; i--)
				free_irq(rd->irqs[i], rd);
			return ret;
		}
	}

	return 0;
}

static void riic_free_irq(struct riic_data *rd)
{
	int i;

	for (i = 0; i < RIIC_NUM_IRQS; i++)
		free_irq(rd->irqs[i], rd);
}

static int riic_get_dmac_channel(int channel)
{
	if (channel <= 4)
		return 12 + channel;
	else
		return 18 + channel - 5;
}

static void riic_get_dmac_resource(int channel, unsigned char *tx,
				   unsigned char *rx)
{
	const unsigned char tx_resource[] = {
		0x21, 0x29, 0xa1, 0xab, 0xc5,
		0x21, 0x29, 0x41, 0x45, 0x51
	};
	const unsigned char rx_resource[] = {
		0x22, 0x2a, 0xa2, 0xaf, 0xc6,
		0x22, 0x2a, 0x42, 0x46, 0x52
	};
	if (channel >= RIIC_NUM_CHANNELS)
		return;

	*tx = tx_resource[channel];
	*rx = rx_resource[channel];
}

static int riic_request_dma(struct riic_data *rd)
{
	rd->dma_ch = riic_get_dmac_channel(rd->channel);
	riic_get_dmac_resource(rd->channel, &rd->dma_tx_resource,
			       &rd->dma_rx_resource);
	__raw_writel(__raw_readl(DMAC_DMAER2) | (1 << rd->channel),
		     DMAC_DMAER2);
	return request_dma(rd->dma_ch, rd->name);
}

static void riic_free_dma(struct riic_data *rd)
{
	free_dma(rd->dma_ch);
}

static void riic_reset(struct riic_data *rd, int internal)
{
	if (!internal)
		riic_clear_bit(rd, ICCR1_ICE, RIIC_ICCR1);
	riic_set_bit(rd, ICCR1_IICRST, RIIC_ICCR1);
	riic_clear_bit(rd, ICCR1_IICRST, RIIC_ICCR1);
}

/* slave setting */
static void riic_slave_set_address(struct riic_data *rd, int reg_index,
				   unsigned char slave_address)
{
	const unsigned long sarl[3] = { RIIC_SARL0, RIIC_SARL1, RIIC_SARL2 };
	const unsigned long saru[3] = { RIIC_SARU0, RIIC_SARU1, RIIC_SARU2 };

	if (reg_index >= 3)
		return;

	riic_write(rd, slave_address << 1, sarl[reg_index]);
	riic_write(rd, 0, saru[reg_index]);
}

static unsigned char riic_slave_get_sar_bit(int reg_index)
{
	const unsigned char sar[3] = { ICSER_SAR0E, ICSER_SAR1E, ICSER_SAR2E };

	if (reg_index >= 3)
		return 0;

	return sar[reg_index];
}

static void riic_slave_disable_address(struct riic_data *rd, int reg_index)
{
	unsigned char sar = riic_slave_get_sar_bit(reg_index);

	if (!sar)
		return;
	riic_clear_bit(rd, sar, RIIC_ICSER);
}

static int riic_slave_enable_address(struct riic_data *rd, int reg_index)
{
	unsigned char sar = riic_slave_get_sar_bit(reg_index);
	unsigned char icser;
	int timeout = 1000;

	if (!sar)
		return -EINVAL;

	icser = riic_read(rd, RIIC_ICSER) & sar;
	if (icser)
		return 0;	/* The SARnE is already set */

	while (riic_read(rd, RIIC_ICCR2) & ICCR2_BBSY) {
		udelay(1);
		if (timeout-- < 0) {
			printk(KERN_ERR "%s: ch%d, timeout\n", __func__,
				rd->channel);
			return -EBUSY;
		}
	}
	riic_set_bit(rd, sar, RIIC_ICSER);

	return 0;
}

static void riic_slave_free_buffers(struct riic_data *rd)
{
	int i;

	if (!rd->slv_rx_pkt)
		return;

	for (i = 0; i < rd->slv_num_of_packets; i++)
		kfree(rd->slv_rx_pkt[i].data);

	kfree(rd->slv_rx_pkt);
	rd->slv_rx_pkt = NULL;
}

static int riic_slave_alloc_buffers(struct riic_data *rd)
{
	int i;

	rd->slv_rx_pkt = kzalloc(sizeof(struct riic_core_packet) *
				 rd->slv_num_of_packets, GFP_KERNEL);
	if (!rd->slv_rx_pkt)
		return -ENOMEM;

	for (i = 0; i < rd->slv_num_of_packets; i++)
		rd->slv_rx_pkt[i].data = NULL;

	for (i = 0; i < rd->slv_num_of_packets; i++) {
		rd->slv_rx_pkt[i].data = kzalloc(rd->slv_buffer_size,
						 GFP_KERNEL);
		if (!rd->slv_rx_pkt[i].data) {
			riic_slave_free_buffers(rd);
			return -ENOMEM;
		}

		rd->slv_rx_pkt[i].len = rd->slv_buffer_size;
	}
	rd->slv_rx_head = rd->slv_rx_tail = 0;

	return 0;
}

static int riic_is_valid_clock_rate(int clock_rate)
{
	if (clock_rate == 100 || clock_rate == 400 || clock_rate == 1000)
		return 1;
	else
		return 0;
}

static void riic_set_clock(struct riic_data *rd, int clock)
{
	switch (clock) {
	case 100:
		riic_clear_bit(rd, ICMR1_CKS_MASK, RIIC_ICMR1);
		riic_set_bit(rd, ICMR1_CKS(3), RIIC_ICMR1);
		riic_clear_bit(rd, ICFER_FMPE, RIIC_ICFER);
		riic_write(rd, ICBRH_RESERVED | 23, RIIC_ICBRH);
		riic_write(rd, ICBRL_SDID(3) | ICBRL_RESERVED | 23, RIIC_ICBRL);
		break;
	case 400:
		riic_clear_bit(rd, ICMR1_CKS_MASK, RIIC_ICMR1);
		riic_set_bit(rd, ICMR1_CKS(1), RIIC_ICMR1);
		riic_clear_bit(rd, ICFER_FMPE, RIIC_ICFER);
		riic_write(rd, ICBRH_RESERVED | 20, RIIC_ICBRH);
		riic_write(rd, ICBRL_SDID(3) | ICBRL_RESERVED | 19, RIIC_ICBRL);
		break;
	case 1000:
		riic_clear_bit(rd, ICMR1_CKS_MASK, RIIC_ICMR1);
		riic_set_bit(rd, ICMR1_CKS(0), RIIC_ICMR1);
		riic_set_bit(rd, ICFER_FMPE, RIIC_ICFER);
		riic_write(rd, ICBRH_RESERVED | 14, RIIC_ICBRH);
		riic_write(rd, ICBRL_SDID(3) | ICBRL_RESERVED | 14, RIIC_ICBRL);
		break;
	default:
		break;
	}
}

static void riic_enable_channel(struct riic_data *rd)
{
	riic_reset(rd, 0);

	riic_write(rd, ICMR1_BC(0), RIIC_ICMR1);
	riic_set_clock(rd, rd->clock);

	/* Set single-buffer, Auto-ACK mode */
	riic_write(rd, ICMR3_WAIT | ICMR3_ACKWP, RIIC_ICMR3);

	/* Clear IRQ's and status */
	riic_write(rd, 0, RIIC_ICIER);
	riic_write(rd, 0, RIIC_ICSR2);

	/* Set internal hold time */
	riic_write(rd, 0x26, RIIC_ICMR2);

	riic_write(rd, ICMR3_WAIT | ICMR3_ACKWP, RIIC_ICMR3);

	riic_set_bit(rd, ICFER_MALE, RIIC_ICFER);
	riic_set_bit(rd, ICCR1_ICE, RIIC_ICCR1);

#ifdef LOST_IRQ_WORKAROUND
	disable_irq(rd->irqs[RIIC_TXI]);
	disable_irq(rd->irqs[RIIC_RXI]);
	riic_set_bit(rd, ICIER_TIE | ICIER_RIE | ICIER_STIE, RIIC_ICIER);
#endif
}

static void riic_disable_channel(struct riic_data *rd)
{
	riic_reset(rd, 0);
}

/*
 * RIIC core APIs
 */
int riic_core_open(int channel, int clock_rate)
{
	struct riic_data *rd = riic_get_data(channel);
	int i, ret;

	if (rd)
		return RIIC_CORE_ALREADY_OPEN;

	if (!rd) {
		/* Allocate the riic_data */
		riic_data[channel] = kzalloc(sizeof(struct riic_data),
					     GFP_KERNEL);
		if (!riic_data[channel])
			return RIIC_CORE_INVALID;
		rd = riic_data[channel];
	}

	if (!riic_is_valid_clock_rate(clock_rate))
		goto error_malloced;

	/* initialize riic_data */
	rd->channel = channel;
	rd->clock = clock_rate;
	rd->state = RIIC_STATE_IDLE;
	for (i = 0; i < RIIC_NUM_IRQS; i++)
		rd->irqs[i] = riic_irqs[channel][i];
	rd->name = riic_name[channel];
	rd->phys_addr = riic_phys_addr[channel];
	rd->reg = ioremap(rd->phys_addr, 0x20);
	if (!rd->reg)
		goto error_malloced;
	init_waitqueue_head(&rd->wait);
	init_waitqueue_head(&rd->slv_wait);

	ret = riic_request_irq(rd);
	if (ret < 0)
		goto error1;

	ret = riic_request_dma(rd);
	if (ret < 0)
		goto error2;

	riic_enable_channel(rd);

	rd->debug = RIIC_CORE_PARAM_DEFAULT_DEBUG;

	printk(KERN_INFO "Renesas RIIC core driver rev.%s (channel=%d)\n",
		RIIC_DRIVER_REVISION, rd->channel);

	return RIIC_CORE_NO_ERROR;

error2:
	riic_free_irq(rd);
error1:
	iounmap(rd->reg);
error_malloced:
	kfree(riic_data[channel]);
	riic_data[channel] = NULL;

	return RIIC_CORE_INVALID;
}
EXPORT_SYMBOL(riic_core_open);

int riic_core_close(int channel)
{
	struct riic_data *rd = riic_get_data(channel);
	unsigned long flags;

	if (!rd)
		return RIIC_CORE_INVALID;

	local_irq_save(flags);
	if (rd->slave_enabled) {
		del_timer(&rd->slv_timer);
		riic_slave_disable_address(rd, 0);
		riic_slave_free_buffers(rd);
		rd->slave_enabled = 0;
	}

	riic_disable_channel(rd);
	riic_free_dma(rd);
	riic_free_irq(rd);
	iounmap(rd->reg);
	local_irq_restore(flags);

	rd->slave_enabled = 0;

	kfree(riic_data[channel]);
	riic_data[channel] = NULL;

	return RIIC_CORE_NO_ERROR;
}
EXPORT_SYMBOL(riic_core_close);

int riic_core_master(int channel, struct riic_core_packet *pkt, int num_pkt,
		     int timeout, int clock_rate)
{
	struct riic_data *rd = riic_get_data(channel);
	int ret, ret_master;
	unsigned long flags;

	if (!rd)
		return RIIC_CORE_INVALID;
	if (clock_rate && !riic_is_valid_clock_rate(clock_rate))
		return RIIC_CORE_INVALID;

	spin_lock_irqsave(&rd->lock, flags);
	if (rd->state != RIIC_STATE_IDLE ||
	    riic_read(rd, RIIC_ICCR2) & ICCR2_BBSY) {
		spin_unlock_irqrestore(&rd->lock, flags);
		return RIIC_CORE_BUSY;
	}

	rd->state = RIIC_STATE_MASTER;
	rd->master_state = RIIC_MASTER_PREPARE;
	rd->pkt = pkt;
	rd->num_pkt = num_pkt;
	rd->pkt_index = 0;
	rd->completed = rd->nacked = rd->aled = 0;

	riic_set_bit(rd, ICIER_STIE | ICIER_NAKIE | ICIER_ALIE, RIIC_ICIER);
	riic_clear_bit(rd, ICIER_TEIE, RIIC_ICIER);
	riic_set_transmit_nack(rd, 0);

	/* change the clock_rate if needed */
	if (clock_rate && rd->clock != clock_rate)
		riic_set_clock(rd, clock_rate);

	/* Send START */
	riic_set_bit(rd, ICCR2_ST, RIIC_ICCR2);
	spin_unlock_irqrestore(&rd->lock, flags);

	ret = wait_event_interruptible_timeout(rd->wait, rd->completed ||
			rd->nacked || rd->aled, msecs_to_jiffies(timeout));

	if ((!rd->completed && ret == 0) || ret < 0) {
		riic_master_clean_up(rd, timeout);
		ret_master = RIIC_CORE_TIMEOUT;
	} else if (rd->nacked) {
		/* already issued stop in riic_irq_master_nackf() */
		if (rd->sending_addr)
			ret_master = RIIC_CORE_NACK_ADDR;
		else
			ret_master = RIIC_CORE_NACK_DATA;
	} else if (rd->aled) {
		/* FIXME */
		if (rd->sending_addr)
			ret_master = RIIC_CORE_AL_ADDR;
		else
			ret_master = RIIC_CORE_AL_DATA;
	} else {
		ret_master = RIIC_CORE_NO_ERROR;
	}

	/* get back the clock_rate if needed */
	if (clock_rate && rd->clock != clock_rate)
		riic_set_clock(rd, rd->clock);

	return ret_master;
}
EXPORT_SYMBOL(riic_core_master);

/* FIXME: this function cannot set 2 or more slave addresses */
int riic_core_slave_config(int channel, unsigned short slave_address,
			   int timeout, int buffer_size, int number_of_packets,
			   unsigned long options)
{
	struct riic_data *rd = riic_get_data(channel);
	unsigned long flags;

	if (!rd)
		return RIIC_CORE_INVALID;
	if (slave_address > 0x7f)
		return RIIC_CORE_INVALID;

	if (!rd->slave_enabled && !slave_address)
		return RIIC_CORE_NO_ERROR;

	/* if slave buffer is already allocated, we don't call the alloc */
	if (!rd->slave_enabled) {
		if (buffer_size <= 16 || number_of_packets <= 3)
			return RIIC_CORE_INVALID;
		rd->slv_buffer_size = buffer_size;
		rd->slv_num_of_packets = number_of_packets;
		/* try to allocate slave buffer */
		if (riic_slave_alloc_buffers(rd) < 0)
			return RIIC_CORE_NOMEM;
	} else if (!slave_address) {
		/* disable slave transfer */
		del_timer(&rd->slv_timer);
		spin_lock_irqsave(&rd->lock, flags);
		riic_slave_disable_address(rd, 0);
		riic_slave_free_buffers(rd);
		rd->slave_enabled = 0;
		spin_unlock_irqrestore(&rd->lock, flags);
		return RIIC_CORE_NO_ERROR;
	}

	/* enable slave transfer */
	spin_lock_irqsave(&rd->lock, flags);
	riic_slave_disable_address(rd, 0);
	riic_slave_set_address(rd, 0, slave_address);
	rd->slv_timeout = timeout;
	setup_timer(&rd->slv_timer, riic_slave_timer, (unsigned long)rd);
	riic_slave_enable_address(rd, 0);
	rd->slave_enabled = 1;
	rd->combine_slv_rd = !!(options & RIIC_CORE_COMBINE_SLAVE_READS);
	spin_unlock_irqrestore(&rd->lock, flags);

	return RIIC_CORE_NO_ERROR;
}
EXPORT_SYMBOL(riic_core_slave_config);

int riic_core_slave(int channel, struct riic_core_packet *pkt, int timeout)
{
	struct riic_data *rd = riic_get_data(channel);
	struct riic_core_packet *slv_pkt;
	int ret;

	if (!rd)
		return RIIC_CORE_INVALID;
	if (!rd->slave_enabled)
		return RIIC_CORE_INVALID;

	ret = wait_event_interruptible_timeout(rd->slv_wait,
			!riic_slave_is_empty(rd), msecs_to_jiffies(timeout));

	if (riic_slave_is_empty(rd) && ret == 0)
		return RIIC_CORE_TIMEOUT;

	slv_pkt = riic_slave_get_pkt_head(rd);
	if (unlikely(!slv_pkt))
		return RIIC_CORE_INVALID;

	pkt->done = slv_pkt->done;
	if (pkt->done) {
		pkt->slave_address = slv_pkt->slave_address;
		pkt->actual_len = min(pkt->len, slv_pkt->actual_len);
		memcpy(pkt->data, slv_pkt->data, pkt->actual_len);
	}

	riic_slave_update_pkt_head(rd);

	return RIIC_CORE_NO_ERROR;
}
EXPORT_SYMBOL(riic_core_slave);

int riic_core_get_status(int channel, struct riic_core_status *status)
{
	struct riic_data *rd = riic_get_data(channel);
	int i;

	if (!rd)
		return RIIC_CORE_INVALID;
	if (!status)
		return RIIC_CORE_INVALID;

	for (i = 0; i < RIIC_CORE_NUM_REGS; i++)
		status->regs[i] = riic_read(rd, i);

	if (rd->slave_enabled) {
		status->slave_buffer_size = rd->slv_buffer_size;
		status->slave_number_of_packets = rd->slv_num_of_packets;
	}
	status->slave_enabled = rd->slave_enabled;
	status->debug = rd->debug;

	return RIIC_CORE_NO_ERROR;
}
EXPORT_SYMBOL(riic_core_get_status);

int riic_core_debug(int channel, int debug)
{
	struct riic_data *rd = riic_get_data(channel);

	if (!rd)
		return RIIC_CORE_INVALID;

	rd->debug = !!debug;
	riic_core_log_dump(rd);
	riic_core_log_clear(rd);

	return RIIC_CORE_NO_ERROR;
}
EXPORT_SYMBOL(riic_core_debug);
