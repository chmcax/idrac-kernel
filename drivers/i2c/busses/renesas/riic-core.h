/*
 * Renesas RIIC core driver
 *
 * Copyright (C) 2012  Renesas Solutions Corp.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __RIIC_CORE_H__
#define __RIIC_CORE_H__

/* User definitions */
#define RIIC_CORE_PARAM_DEFAULT_DEBUG		1
#define RIIC_CORE_PARAM_LOGGING			1024	/* if 0, no logging */

#define RIIC_NUM_CHANNELS	10
#define RIIC_NUM_IRQS		4

/* Register definitions */
#define RIIC_ICCR1	0x00
#define RIIC_ICCR2	0x01
#define RIIC_ICMR1	0x02
#define RIIC_ICMR2	0x03
#define RIIC_ICMR3	0x04
#define RIIC_ICFER	0x05
#define RIIC_ICSER	0x06
#define RIIC_ICIER	0x07
#define RIIC_ICSR1	0x08
#define RIIC_ICSR2	0x09
#define RIIC_SARL0	0x0a
#define RIIC_SARU0	0x0b
#define RIIC_SARL1	0x0c
#define RIIC_SARU1	0x0d
#define RIIC_SARL2	0x0e
#define RIIC_SARU2	0x0f
#define RIIC_ICBRL	0x10
#define RIIC_ICBRH	0x11
#define RIIC_ICDRT	0x12
#define RIIC_ICDRR	0x13
#define RIIC_CORE_NUM_REGS	(RIIC_ICBRH + 1)

/* ICCR1 */
#define ICCR1_ICE	0x80
#define ICCR1_IICRST	0x40
#define ICCR1_CLO	0x20
#define ICCR1_SOWP	0x10
#define ICCR1_SCLO	0x08
#define ICCR1_SDAO	0x04
#define ICCR1_SCLI	0x02
#define ICCR1_SDAI	0x01

/* ICCR2 */
#define ICCR2_BBSY	0x80
#define ICCR2_MST	0x40
#define ICCR2_TRS	0x20
#define ICCR2_SP	0x08
#define ICCR2_RS	0x04
#define ICCR2_ST	0x02

/* ICMR1 */
#define ICMR1_MTWP	0x80
#define ICMR1_CKS_MASK	0x70
#define ICMR1_BCWP	0x08
#define ICMR1_BC_MASK	0x07

#define ICMR1_CKS(_x)	((_x << 4) & ICMR1_CKS_MASK)
#define ICMR1_BC(_x)	(_x & ICMR1_BC_MASK)

/* ICMR2 */
#define ICMR2_DLCS	0x80
#define ICMR2_SDDL_MASK	0x70
#define ICMR2_TMOH	0x04
#define ICMR2_TMOL	0x02
#define ICMR2_TMOS	0x01

/* ICMR3 */
#define ICMR3_SMBS	0x80
#define ICMR3_WAIT	0x40
#define ICMR3_RDRFS	0x20
#define ICMR3_ACKWP	0x10
#define ICMR3_ACKBT	0x08
#define ICMR3_ACKBR	0x04
#define ICMR3_NF_MASK	0x03

/* ICFER */
#define ICFER_FMPE	0x80
#define ICFER_SCLE	0x40
#define ICFER_NFE	0x20
#define ICFER_NACKE	0x10
#define ICFER_SALE	0x08
#define ICFER_NALE	0x04
#define ICFER_MALE	0x02
#define ICFER_TMOE	0x01

/* ICSER */
#define ICSER_HOAE	0x80
#define ICSER_DIDE	0x20
#define ICSER_GCAE	0x08
#define ICSER_SAR2E	0x04
#define ICSER_SAR1E	0x02
#define ICSER_SAR0E	0x01

/* ICIER */
#define ICIER_TIE	0x80
#define ICIER_TEIE	0x40
#define ICIER_RIE	0x20
#define ICIER_NAKIE	0x10
#define ICIER_SPIE	0x08
#define ICIER_STIE	0x04
#define ICIER_ALIE	0x02
#define ICIER_TMOIE	0x01

/* ICSR1 */
#define ICSR1_HOA	0x80
#define ICSR1_DID	0x20
#define ICSR1_GCA	0x08
#define ICSR1_AAS2	0x04
#define ICSR1_AAS1	0x02
#define ICSR1_AAS0	0x01

/* ICSR2 */
#define ICSR2_TDRE	0x80
#define ICSR2_TEND	0x40
#define ICSR2_RDRF	0x20
#define ICSR2_NACKF	0x10
#define ICSR2_STOP	0x08
#define ICSR2_START	0x04
#define ICSR2_AL	0x02
#define ICSR2_TMOF	0x01

/* SARLn */
#define SARL_SVA_MASK	0xfe	/* SVA[7:1] */
#define SARL_SVA	0x01

/* SARUn */
#define SARU_SVA_MASK	0x06	/* SVA[9:8] */
#define SARU_FS		0x01

/* ICBRH */
#define ICBRH_RESERVED	0xe0	/* The write value shoud always be 1 */
#define ICBRH_BRH_MASK	0x1f

/* ICBRL */
#define ICBRL_SDID_MASK	0xc0
#define ICBRL_SDID(_x)	((_x << 6) & ICBRL_SDID_MASK)
#define ICBRL_RESERVED	0x20	/* The write value shoud always be 1 */
#define ICBRL_BRL_MASK	0x1f


/* Error codes */
enum {
	RIIC_CORE_NO_ERROR = 0,
	RIIC_CORE_INVALID,
	RIIC_CORE_ALREADY_OPEN,
	RIIC_CORE_NOMEM,
	RIIC_CORE_BUSY,
	RIIC_CORE_TIMEOUT,
	RIIC_CORE_AL_ADDR,
	RIIC_CORE_AL_DATA,
	RIIC_CORE_NACK_ADDR,
	RIIC_CORE_NACK_DATA,
};

struct riic_core_packet {
	unsigned short slave_address;
	void *data;
	int len;
	int actual_len;
	unsigned char rw;
#define RIIC_CORE_RW_MASTER_TRANSMIT	0
#define RIIC_CORE_RW_MASTER_RECEIVE	1
#define RIIC_CORE_RW_SLAVE_TRANSMIT	2
#define RIIC_CORE_RW_SLAVE_RECEIVE	3

	unsigned done:1;
};

struct riic_core_status {
	unsigned char	regs[RIIC_CORE_NUM_REGS];

	/* core driver's status */
	int		slave_buffer_size;
	int		slave_number_of_packets;
	unsigned	slave_enabled:1;
	unsigned	debug:1;
};

/* The option of riic_core_slave_config() definitions */
#define RIIC_CORE_COMBINE_SLAVE_READS	BIT(0)

extern int riic_core_open(int channel, int clock_rate);
extern int riic_core_close(int channel);
extern int riic_core_master(int channel, struct riic_core_packet *pkt,
			    int num_pkt, int timeout, int clock_rate);
extern int riic_core_slave_config(int channel, unsigned short slave_address,
				  int timeout, int buffer_size,
				  int number_of_packets, unsigned long options);
extern int riic_core_slave(int channel, struct riic_core_packet *pkt,
			   int timeout);
extern int riic_core_get_status(int channel, struct riic_core_status *status);
extern int riic_core_debug(int channel, int debug);
#endif
