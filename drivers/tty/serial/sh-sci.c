/*
 * SuperH on-chip serial module support.  (SCI with no FIFO / with FIFO)
 *
 *  Copyright (C) 2002 - 2011  Paul Mundt
 *  Modified to support SH7720 SCIF. Markus Brunner, Mark Jonas (Jul 2007).
 *
 * based off of the old drivers/char/sh-sci.c by:
 *
 *   Copyright (C) 1999, 2000  Niibe Yutaka
 *   Copyright (C) 2000  Sugioka Toshinobu
 *   Modified to support multiple serial ports. Stuart Menefy (May 2000).
 *   Modified to support SecureEdge. David McCullough (2002)
 *   Modified to support SH7300 SCIF. Takashi Kusuda (Jun 2003).
 *   Removed SH7300 support (Jul 2007).
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */
#if defined(CONFIG_SERIAL_SH_SCI_CONSOLE) && defined(CONFIG_MAGIC_SYSRQ)
#define SUPPORT_SYSRQ
#endif

#define DEBUG

#include <linux/clk.h>
#include <linux/console.h>
#include <linux/ctype.h>
#include <linux/cpufreq.h>
#include <linux/delay.h>
#include <linux/dmaengine.h>
#include <linux/dma-mapping.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/major.h>
#include <linux/module.h>
#include <linux/mm.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/pm_runtime.h>
#include <linux/scatterlist.h>
#include <linux/serial.h>
#include <linux/serial_sci.h>
#include <linux/sh_dma.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/sysrq.h>
#include <linux/timer.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/aess_debugfs.h>

#ifdef CONFIG_SUPERH
#include <asm/sh_bios.h>
#endif

#include "sh-sci.h"

/* Offsets into the sci_port->irqs array */
enum {
	SCIx_ERI_IRQ,
	SCIx_RXI_IRQ,
	SCIx_TXI_IRQ,
	SCIx_BRI_IRQ,
	SCIx_NR_IRQS,

	SCIx_MUX_IRQ = SCIx_NR_IRQS,	/* special case */
};

#define SCIx_IRQ_IS_MUXED(port)			\
	((port)->irqs[SCIx_ERI_IRQ] ==	\
	 (port)->irqs[SCIx_RXI_IRQ]) ||	\
	((port)->irqs[SCIx_ERI_IRQ] &&	\
	 ((port)->irqs[SCIx_RXI_IRQ] < 0))

struct sci_port {
	struct uart_port	port;

	/* Platform configuration */
	struct plat_sci_port	*cfg;
	int			overrun_bit;
	unsigned int		error_mask;
	unsigned int		sampling_rate;


	/* Break timer */
	struct timer_list	break_timer;
	int			break_flag;

	/* Interface clock */
	struct clk		*iclk;
	/* Function clock */
	struct clk		*fclk;

	int			irqs[SCIx_NR_IRQS];
	char			*irqstr[SCIx_NR_IRQS];

	struct dma_chan			*chan_tx;
	struct dma_chan			*chan_rx;

#ifdef CONFIG_SERIAL_SH_SCI_DMA
	struct dma_async_tx_descriptor	*desc_tx;
	struct dma_async_tx_descriptor	*desc_rx[2];
	dma_cookie_t			cookie_tx;
	dma_cookie_t			cookie_rx[2];
	dma_cookie_t			active_rx;
	struct scatterlist		sg_tx;
	unsigned int			sg_len_tx;
	struct scatterlist		sg_rx[2];
	size_t				buf_len_rx;
	struct sh_dmae_slave		param_tx;
	struct sh_dmae_slave		param_rx;
	struct work_struct		work_tx;
	struct work_struct		work_rx;
	struct timer_list		rx_timer;
	unsigned int			rx_timeout;
#endif
#ifdef __iDRAC__
  struct file *filp;
#endif
};

#define SCI_NPORTS CONFIG_SERIAL_SH_SCI_NR_UARTS
#ifdef __iDRAC__

static unsigned int S_ttySC346_Baudrate = 0;
static unsigned char S_tchar = 0;
static unsigned int S_OpenPortMask = 0;
static unsigned int S_EnableDebug = 0;
static unsigned int S_OpenDebug = 0;
static unsigned int S_SuccessOpen = 0;
static unsigned int S_CloseDebug = 0;
static unsigned int S_TermiosDebug = 0;
static unsigned int S_IncludeP4Debug = 0;
static unsigned int S_SolPrintEnable = 0;
static unsigned int S_BasicModeDebug = 0;
static unsigned int S_PrintTTY = 0;
static unsigned int S_PrintTTY_ISR = 0;
static unsigned int S_MuxDebug = 0;

static unsigned char G_Txbuffer[4000] = {0};
static unsigned char G_Rxbuffer[4000] = {0};

#define TIOCSERGMUX     0x5495 /* Get iDRAC MUX setting */
#define TIOCSERSMUX     0x5496 /* Set iDRAC MUX */
#define TIOCHISTON      0x5497 /* Read history buffer */
#define TIOCIDRACTTYBAUD   0x5498 /* Set default iDRAC baudrates */
#define TIOCSNOOPON     0x5499 /* Snoop input stream */
#define TIOCSNOOPOFF    0x549a /* Stop snooping input stream */
#define TIOCSDEBUG      0x549b /* Debug switch */
#define TIOCHISTBUFMAX	0x549c /* History buffer max size */
#define RACTTY_PID                  3  // Racadm virtual serial port
#define IPMITTY_PID                 4  // IPMI virtual serial port (Mode2A/Mode2B and maps to SCIF3)
#define IDRACTTY_PID                5 // Serial mux virtual serial port
#define SOLTTY_PID                  6   // SOL virtual serial port (IPMI SOL and console com2)
#define IPMI2TTY_PID               7  // IPMI virtual serial port (Mode3 and maps to SCIF2)
#define SDCTTY_PID                 8   // Serial Data Capture / WCS serial port (Mode3 and maps to SCIF3)

#define START_VIRTUAL_SERIAL_PORT_OFFSET 3
#define RAC_VIRTUAL_SERIAL_OFFSET  (RAC_VIRTUAL_SERIAL_PORT - START_VIRTUAL_SERIAL_PORT_OFFSET)
#define IPMI_VIRTUAL_SERIAL1_OFFSET (IPMI_VIRTUAL_SERIAL_PORT1 - START_VIRTUAL_SERIAL_PORT_OFFSET)
#define IDRACTTY_DAEMON_SERIAL_OFFSET  (IDRACTTY_DAEMON_SERIAL_PORT - START_VIRTUAL_SERIAL_PORT_OFFSET)
#define SOL_VIRTUAL_SERIAL_OFFSET  (SOL_VIRTUAL_SERIAL_PORT - START_VIRTUAL_SERIAL_PORT_OFFSET)
#define IPMI_VIRTUAL_SERIAL2_OFFSET (IPMI_VIRTUAL_SERIAL_PORT2 - START_VIRTUAL_SERIAL_PORT_OFFSET)
#define SERIAL_DATA_CAPTURE_OFFSET (SERIAL_DATA_CAPTURE_PORT - START_VIRTUAL_SERIAL_PORT_OFFSET)

#define IDRACDEVS          6
#define UART_MSR_CTS 0x10
#define SUPPORT_HISTBUF
//#define SUPPORT_DCD
//#define IDRAC_SERIALDRIVER_DEBUG

#define ESC             0x1b
static struct tty_port *idractty[6];  /* ttySC3-8 are idracttys */
static struct tty_port **idracSCIF3ldr=idractty;
static struct tty_port **idracSCIF2ldr = idractty+4;  // Only have P7 that maps to SCIF2
//static struct tty_port **preemptedldr=0;
static unsigned char idraclastch='\0';

static wait_queue_head_t idracttyldr_wait;
#define MODE1   1
#define MODE2A  3
#define MODE2B  4
#define MODE2C  2
#define MODE3   5

static int idracmux=MODE1;
static int idraclastmux=MODE1;
static int idracmuxreq=MODE1;

pid_t idracttypid[SCI_NPORTS];
struct task_struct * as_VspTaskStructs[SCI_NPORTS];

static unsigned char SolRxBuffer[200] = {0};
static int SolRxBufIndex = 0;
static unsigned char SolTxBuffer[200] = {0};
static int SolTxBufIndex = 0;

static int snoopon=true;
static int idracdebug=false;
void do_sci_serial_bh(unsigned long);
DECLARE_TASKLET(bh_tasklet, do_sci_serial_bh,0);
unsigned int idrac_flags = 0;
static DEFINE_SPINLOCK(idrac_serial_spinlock);

#ifdef SUPPORT_HISTBUF
#define HISTBUFMAX  (8*1024)
static int histbufmax=HISTBUFMAX;
static unsigned char histbuf[HISTBUFMAX];
static unsigned char *histinp=histbuf, *histoutp=histbuf, *myoutp;
struct tty_port *flushtty;
struct timer_list flushtimer;
static int flushinghist=false;
static int histon=false;
#endif

static unsigned char irq_flag[MAX_VIRTUAL_SERIAL_PORT];
#endif //__iDRAC__

/* Function prototypes */
static void sci_start_tx(struct uart_port *port);
static void sci_stop_tx(struct uart_port *port);
static void sci_start_rx(struct uart_port *port);

#define SCI_NPORTS CONFIG_SERIAL_SH_SCI_NR_UARTS

static struct sci_port sci_ports[SCI_NPORTS];
static struct uart_driver sci_uart_driver;

static inline struct sci_port *
to_sci_port(struct uart_port *uart)
{
	return container_of(uart, struct sci_port, port);
}

#ifdef __iDRAC__
#ifdef SUPPORT_HISTBUF
/*
 * These histbuf functions should be called with interrupts masked
 */
 
static inline void resethistbuf(void)
{
    histinp = histoutp = histbuf;
    flushinghist = false;
    del_timer(&flushtimer);
}


//increase index of histbuf with 1
static inline void wrapinc(char **p)
{
	if (++(*p) == histbuf+histbufmax)
		*p = histbuf;
}


//put character into histbuf
static inline void stuffhistbuf(unsigned char ch)
{
    *histinp = ch;
    wrapinc(&histinp);
    if (histinp == histoutp) {
        //when hist buffer is full, old data will be skipped.
        //    histbufoutp = histbufinp + 1
        if (flushinghist && myoutp == histoutp)
            wrapinc(&myoutp);
        wrapinc(&histoutp);
    }
}
    

/*
 * WARNING: flushhist() must be called with serial_lock held.
 */
//flush data in histbuf to ttySC6(read direction)
static void flushhist(unsigned long firsttime)
{
    int size;
    unsigned long flags;
    int i=0;

    spin_lock_irqsave(&idrac_serial_spinlock, idrac_flags);

    if (firsttime)
    {
        myoutp = histoutp;
    }
	if (myoutp == histinp)
	{
		flushinghist = false;
	}
	
    for ( ; flushinghist; ) 
    {
        if (myoutp < histinp)
            size = histinp - myoutp;
        else
            size = histbuf + histbufmax - myoutp;
       
         if(NULL!=flushtty->buf.tail)
         {
         	//???????????/

            if (size > (N_TTY_BUF_SIZE - flushtty->buf.tail->used))
               size = N_TTY_BUF_SIZE - flushtty->buf.tail->used;
         }       

        if (size == 0) {
            init_timer(&flushtimer);
	        flushtimer.expires = jiffies + (HZ>>1); 
	        flushtimer.data = (unsigned long)false;
	        flushtimer.function = &flushhist;
	        add_timer(&flushtimer);
            break;
        }

        for(i=0; i<size;i++)
        {
          tty_insert_flip_char(flushtty,myoutp[i],TTY_NORMAL);
        }


        myoutp += size;
        if (myoutp >= histbuf + histbufmax)
            myoutp = histbuf;
        if (myoutp == histinp)
            flushinghist = false;

	    tty_flip_buffer_push(flushtty);
    }
   spin_unlock_irqrestore(&idrac_serial_spinlock, idrac_flags);
}

#endif //end of #ifdef SUPPORT_HISTBUF
char *invalid(int mux)
{
    static char str[30];

    sprintf(str, "invalid (%d)", mux);
    return(str);
}


char *modestr(int mux)
{
    return(mux == MODE1 ? "MODE1" :
        mux == MODE2A ? "MODE2a" :
        mux == MODE2B ? "MODE2b" :
        mux == MODE2C ? "MODE2c" :
        mux == MODE3 ? "MODE3" :        
            invalid(mux));
}


 //It will be called by driver to inform idracttyd to change mux stae.
static void muxchange_sendsignal(pid_t ttyspid, unsigned long index)
{
    struct task_struct *p = as_VspTaskStructs[index];

    if ((ttyspid == 0) || (p == NULL) || (p->pid != ttyspid))
    {
        if (S_MuxDebug) pr_debug("%s(): error.  ttyspid=%d, p = %p, p->pid=%d\n", __FUNCTION__, ttyspid, p, p->pid);
        return;
    }
    read_lock(&tasklist_lock);
	
    /**kernel 2.6.30??????*/
//    p = find_task_by_vpid(ttyspid);
    if (S_MuxDebug)
        pr_debug("mux: [%d %d] -> %d\n", index, ttyspid, p->pid);
    if(p != NULL)
    {
        if (p->pid == ttyspid) 
        {
            if (IDRACTTY_PID==index)
            {
                if ((S_BasicModeDebug == 1) || (S_EnableDebug == 1))	
                    pr_debug("[%lu] : mux: %s->%s\n", jiffies, modestr(idracmux),modestr(idracmuxreq));
                send_sig(SIGUSR2,p,1);
                send_sig(SIGCONT,p,1);
            }
            else if(RACTTY_PID == index || IPMITTY_PID == index || IPMI2TTY_PID == index)
            {
                if(index == RACTTY_PID)
                {
                    if ((S_BasicModeDebug == 1) || (S_EnableDebug == 1))	
                        pr_debug("[%lu] HU RAC : %d.\n", jiffies, p->pid);
                }
                else if(index == IPMITTY_PID)
                {
                    if ((S_BasicModeDebug == 1) || (S_EnableDebug == 1))	
                        pr_debug("[%lu] : HU P4 : %d.\n", jiffies, p->pid);
                }
                else if(index == IPMI2TTY_PID)
                {
                    if ((S_BasicModeDebug == 1) || (S_EnableDebug == 1))	
                        pr_debug("[%lu] : HU P7 : %d.\n", jiffies, p->pid);
                }
                send_sig(SIGUNUSED,p,1);
                send_sig(SIGCONT,p,1);
            }
        }

    }
    read_unlock(&tasklist_lock);
}

void do_sci_serial_bh(unsigned long data)
{
  muxchange_sendsignal(idracttypid[IDRACTTY_PID],IDRACTTY_PID);
}

 //Change ttySC port when mux state is changed or ttySC is opened,.....
 //Question: When the previous leader was preempted, should it be blocked or hang up?
/*
 * This function should always be protected by serial_lock.
 */
static void newttyldr(void)
{
    struct tty_port **SCIF3oldldr, **SCIF2oldldr;
    int i = 0 ;
    pid_t ttypid;
    /*
     * ttySC0 - unused
     * ttySC1 - unused
     * ttySC2 - reserved for debug port
     * ttySC3 - RAC serial console (preempts IPMI serial console)
     * ttySC4 - IPMI serial console (MODE2B)
     * ttySC5 - mux change channel
     * ttySC6 - RAC/IPMI host console redirection (preempts all)
     * ttySC7 - IPMI serial console (MODE3)
     */
    if (S_PrintTTY == 1)
        pr_debug("%s() 1 [%lu] : SCIF2=0x%p, SCIF3=0x%p, P3=0x%p, P4=0x%p, P5=0x%p, P6=0x%x, P7=0x%p, P8=0x%p\n",
                  __FUNCTION__, jiffies, *idracSCIF2ldr, *idracSCIF3ldr, idractty[0], idractty[1], idractty[2], idractty[3], idractty[4], idractty[5]);
     
    SCIF3oldldr = idracSCIF3ldr;
    SCIF2oldldr = idracSCIF2ldr;
//    if (S_EnableDebug == 1)
//        pr_debug(KERN_EMERG"%s(): mux=%s \n", __FUNCTION__, modestr(idracmux));
    
    switch (idracmux) {
    case MODE1:     /* ttySC6 */
        idracSCIF3ldr = idractty+3;
        break;
    case MODE2A:    /* ttySC3 or ttySC4, in that order */
    case MODE2B:
        idracSCIF3ldr = idractty[0] ? idractty : (idractty[1] ? idractty+1 : idractty);
        break;
    case MODE2C:    /* ttySC6 */
        idracSCIF3ldr = idractty+3;
        break;
    case MODE3:    /* ttySC7 and ttySC8*/
        idracSCIF2ldr = idractty+4;  // ttySC7
        idracSCIF3ldr = idractty+5;  // ttySC8
        break;
    }
    if (S_PrintTTY == 1)
        pr_debug("%s() 2 [%lu] : SCIF2=0x%p, SCIF3=0x%p, P3=0x%p, P4=0x%p, P5=0x%p, P6=0x%p, P7=0x%p, P8=0x%p\n",
                 __FUNCTION__, jiffies, *idracSCIF2ldr, *idracSCIF3ldr, idractty[0], idractty[1], idractty[2], idractty[3], idractty[4], idractty[5]);

    /*
     * When leadership changes, hangup the old leader and wakeup the new guy.
     */
#if 1 // Not needed since we can't open a port like this with the new kernel.  All opens use non-blocking option.
    if (SCIF3oldldr != idracSCIF3ldr) {
        if (*SCIF3oldldr) 
        {
//            preemptedldr = SCIF3oldldr;//"preemptedldr == 0" means a ttyS is closed. It will be cleared in idracttyclr.
            i=(SCIF3oldldr-idractty)+START_VIRTUAL_SERIAL_PORT_OFFSET;
            if(i>=START_VIRTUAL_SERIAL_PORT_OFFSET && i<SCI_NPORTS)
            {
                ttypid = idracttypid[i];
                if(sci_ports[i].port.state)
                {
                   sci_ports[i].port.state->port.flags|= ASYNC_NORMAL_ACTIVE; 
                   
                   tty_hangup((*SCIF3oldldr)->tty);
                   muxchange_sendsignal(ttypid, i);
                }

            }      
              
        } else if (*idracSCIF3ldr) {
            wake_up_interruptible_all(&idracttyldr_wait);
        }

    }
#endif    
}

//close a tty.
//set idractty[i] to 0 and 
static void idracttyclr(struct uart_port *port, int switchmux)
{
    struct tty_port *tport = &port->state->port;
    unsigned long flags;
    int i;
//    pr_debug("%s(): P%d\n", __FUNCTION__, port->line);
    for (i = 0; i < IDRACDEVS+1; i++)
    {

        if (idractty[i] == tport)
            break;
    }
    if (i == IDRACDEVS+1)
    {
//        pr_debug("%s(): Error\n", __FUNCTION__);
        return;
    }
//    pr_debug("clear: i=%d, P%d, tty=0x%08x, SCIF2=0x%08x, SCIF3=0x%08x, 0=0x%08x, 1=0x%08x, 2=0x%08x, 3=0x%08x, 4=0x%08x, 5=0x%08x, idracSCIF3ldr= 0x%08x, idractty = 0x%08x\n",
//                    i, port->line, tty, *idracSCIF2ldr, *idracSCIF3ldr, idractty[0], idractty[1], idractty[2], idractty[3], idractty[4], idractty[5], idracSCIF3ldr, idractty);    
    /*
     * When the idracSCIF3ldr quits, may need a mux change or have a new election.
     */
    spin_lock_irqsave(&idrac_serial_spinlock, idrac_flags);
//    pr_debug("%s(): Clear P%d : offset %d\n", __FUNCTION__, port->line, i);
    idractty[i] = 0;

    if (idracSCIF3ldr-idractty == i) {
        if ((idracmux == MODE2C) && (switchmux))
        {
            if (S_MuxDebug)
                pr_debug("[%lu] : cleanup : P%d : %s to %s\n", jiffies, port->line, modestr(idracmux), modestr(idracmuxreq));
            idracmuxreq = idraclastmux;
            muxchange_sendsignal(idracttypid[IDRACTTY_PID],IDRACTTY_PID);
        } else {
            newttyldr();
        }
    }

    spin_unlock_irqrestore(&idrac_serial_spinlock, idrac_flags);
}
//reset variables to initial value and call idracttyclr()
void idracttycleanup(struct uart_port *port, int muxswitch)
{

//    struct tty_port *tport = &port->state->port;

    int offset = port->line - START_VIRTUAL_SERIAL_PORT_OFFSET;
//    if (port->line != 2)
//        pr_debug("%s(): [%lu] : P%d : offset %d\n", __FUNCTION__, jiffies, port->line, offset);
    /*
     * Early in rs_open, the async_struct is not yet linked to the tty.
     */
    if (!(offset >= 0 && offset < sizeof(idractty)/sizeof(struct tty_port *)))
        return;
        
    if (port->line >1 && port->line < SCI_NPORTS)
    {
        idracttypid[port->line] = 0;
        as_VspTaskStructs[port->line] = NULL;
        if (S_MuxDebug)
           pr_debug("%s(): P%d clear ttypid and task struct\n", __FUNCTION__, port->line);
    }

    if (offset == SOL_VIRTUAL_SERIAL_OFFSET)
    {
        #ifdef SUPPORT_HISTBUF
        histon = false;
        #endif
    }
    idracttyclr(port, muxswitch);

}

void sci_setfilp(struct uart_port *port, struct file *filp)
{
    sci_ports[port->line].filp = filp;
}
#endif


struct plat_sci_reg {
	u8 offset, size;
};

/* Helper for invalidating specific entries of an inherited map. */
#define sci_reg_invalid	{ .offset = 0, .size = 0 }

static struct plat_sci_reg sci_regmap[SCIx_NR_REGTYPES][SCIx_NR_REGS] = {
	[SCIx_PROBE_REGTYPE] = {
		[0 ... SCIx_NR_REGS - 1] = sci_reg_invalid,
	},

	/*
	 * Common SCI definitions, dependent on the port's regshift
	 * value.
	 */
	[SCIx_SCI_REGTYPE] = {
		[SCSMR]		= { 0x00,  8 },
		[SCBRR]		= { 0x01,  8 },
		[SCSCR]		= { 0x02,  8 },
		[SCxTDR]	= { 0x03,  8 },
		[SCxSR]		= { 0x04,  8 },
		[SCxRDR]	= { 0x05,  8 },
		[SCFCR]		= sci_reg_invalid,
		[SCFDR]		= sci_reg_invalid,
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= sci_reg_invalid,
		[SCLSR]		= sci_reg_invalid,
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common definitions for legacy IrDA ports, dependent on
	 * regshift value.
	 */
	[SCIx_IRDA_REGTYPE] = {
		[SCSMR]		= { 0x00,  8 },
		[SCBRR]		= { 0x01,  8 },
		[SCSCR]		= { 0x02,  8 },
		[SCxTDR]	= { 0x03,  8 },
		[SCxSR]		= { 0x04,  8 },
		[SCxRDR]	= { 0x05,  8 },
		[SCFCR]		= { 0x06,  8 },
		[SCFDR]		= { 0x07, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= sci_reg_invalid,
		[SCLSR]		= sci_reg_invalid,
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common SCIFA definitions.
	 */
	[SCIx_SCIFA_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x20,  8 },
		[SCxSR]		= { 0x14, 16 },
		[SCxRDR]	= { 0x24,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= { 0x1c, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= sci_reg_invalid,
		[SCLSR]		= sci_reg_invalid,
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common SCIFB definitions.
	 */
	[SCIx_SCIFB_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x40,  8 },
		[SCxSR]		= { 0x14, 16 },
		[SCxRDR]	= { 0x60,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= sci_reg_invalid,
		[SCTFDR]	= { 0x38, 16 },
		[SCRFDR]	= { 0x3c, 16 },
		[SCSPTR]	= sci_reg_invalid,
		[SCLSR]		= sci_reg_invalid,
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common SH-2(A) SCIF definitions for ports with FIFO data
	 * count registers.
	 */
	[SCIx_SH2_SCIF_FIFODATA_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x0c,  8 },
		[SCxSR]		= { 0x10, 16 },
		[SCxRDR]	= { 0x14,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= { 0x1c, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= { 0x20, 16 },
		[SCLSR]		= { 0x24, 16 },
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common SH-3 SCIF definitions.
	 */
	[SCIx_SH3_SCIF_REGTYPE] = {
		[SCSMR]		= { 0x00,  8 },
		[SCBRR]		= { 0x02,  8 },
		[SCSCR]		= { 0x04,  8 },
		[SCxTDR]	= { 0x06,  8 },
		[SCxSR]		= { 0x08, 16 },
		[SCxRDR]	= { 0x0a,  8 },
		[SCFCR]		= { 0x0c,  8 },
		[SCFDR]		= { 0x0e, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= sci_reg_invalid,
		[SCLSR]		= sci_reg_invalid,
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common SH-4(A) SCIF(B) definitions.
	 */
	[SCIx_SH4_SCIF_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x0c,  8 },
		[SCxSR]		= { 0x10, 16 },
		[SCxRDR]	= { 0x14,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= { 0x1c, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= { 0x20, 16 },
		[SCLSR]		= { 0x24, 16 },
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common HSCIF definitions.
	 */
	[SCIx_HSCIF_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x0c,  8 },
		[SCxSR]		= { 0x10, 16 },
		[SCxRDR]	= { 0x14,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= { 0x1c, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= { 0x20, 16 },
		[SCLSR]		= { 0x24, 16 },
		[HSSRR]		= { 0x40, 16 },
	},

	/*
	 * Common SH-4(A) SCIF(B) definitions for ports without an SCSPTR
	 * register.
	 */
	[SCIx_SH4_SCIF_NO_SCSPTR_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x0c,  8 },
		[SCxSR]		= { 0x10, 16 },
		[SCxRDR]	= { 0x14,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= { 0x1c, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= sci_reg_invalid,
		[SCLSR]		= { 0x24, 16 },
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * Common SH-4(A) SCIF(B) definitions for ports with FIFO data
	 * count registers.
	 */
	[SCIx_SH4_SCIF_FIFODATA_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x0c,  8 },
		[SCxSR]		= { 0x10, 16 },
		[SCxRDR]	= { 0x14,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= { 0x1c, 16 },
		[SCTFDR]	= { 0x1c, 16 },	/* aliased to SCFDR */
		[SCRFDR]	= { 0x20, 16 },
		[SCSPTR]	= { 0x24, 16 },
		[SCLSR]		= { 0x28, 16 },
		[HSSRR]		= sci_reg_invalid,
	},

	/*
	 * SH7705-style SCIF(B) ports, lacking both SCSPTR and SCLSR
	 * registers.
	 */
	[SCIx_SH7705_SCIF_REGTYPE] = {
		[SCSMR]		= { 0x00, 16 },
		[SCBRR]		= { 0x04,  8 },
		[SCSCR]		= { 0x08, 16 },
		[SCxTDR]	= { 0x20,  8 },
		[SCxSR]		= { 0x14, 16 },
		[SCxRDR]	= { 0x24,  8 },
		[SCFCR]		= { 0x18, 16 },
		[SCFDR]		= { 0x1c, 16 },
		[SCTFDR]	= sci_reg_invalid,
		[SCRFDR]	= sci_reg_invalid,
		[SCSPTR]	= sci_reg_invalid,
		[SCLSR]		= sci_reg_invalid,
		[HSSRR]		= sci_reg_invalid,
	},
};

#define sci_getreg(up, offset)		(sci_regmap[to_sci_port(up)->cfg->regtype] + offset)

/*
 * The "offset" here is rather misleading, in that it refers to an enum
 * value relative to the port mapping rather than the fixed offset
 * itself, which needs to be manually retrieved from the platform's
 * register map for the given port.
 */
static unsigned int sci_serial_in(struct uart_port *p, int offset)
{
	struct plat_sci_reg *reg = sci_getreg(p, offset);

	if (reg->size == 8)
		return ioread8(p->membase + (reg->offset << p->regshift));
	else if (reg->size == 16)
		return ioread16(p->membase + (reg->offset << p->regshift));
	else
		WARN(1, "Invalid register access\n");

	return 0;
}

static void sci_serial_out(struct uart_port *p, int offset, int value)
{
	struct plat_sci_reg *reg = sci_getreg(p, offset);

	if (reg->size == 8)
		iowrite8(value, p->membase + (reg->offset << p->regshift));
	else if (reg->size == 16)
		iowrite16(value, p->membase + (reg->offset << p->regshift));
	else
		WARN(1, "Invalid register access\n");
}

static int sci_probe_regmap(struct plat_sci_port *cfg)
{
	switch (cfg->type) {
	case PORT_SCI:
		cfg->regtype = SCIx_SCI_REGTYPE;
		break;
	case PORT_IRDA:
		cfg->regtype = SCIx_IRDA_REGTYPE;
		break;
	case PORT_SCIFA:
		cfg->regtype = SCIx_SCIFA_REGTYPE;
		break;
	case PORT_SCIFB:
		cfg->regtype = SCIx_SCIFB_REGTYPE;
		break;
	case PORT_SCIF:
		/*
		 * The SH-4 is a bit of a misnomer here, although that's
		 * where this particular port layout originated. This
		 * configuration (or some slight variation thereof)
		 * remains the dominant model for all SCIFs.
		 */
		cfg->regtype = SCIx_SH4_SCIF_REGTYPE;
		break;
	case PORT_HSCIF:
		cfg->regtype = SCIx_HSCIF_REGTYPE;
		break;
	default:
		pr_err("Can't probe register map for given port\n");
		return -EINVAL;
	}

	return 0;
}

static void sci_port_enable(struct sci_port *sci_port)
{
#ifdef __iDRAC__
	struct sci_port *s;
//	struct clk *iclk;
//	struct clk *fclk;
#endif	
	if (!sci_port->port.dev)
		return;

	pm_runtime_get_sync(sci_port->port.dev);
#if 1 //debug
	if (S_EnableDebug == 1)
	{
//    iclk = sci_port->iclk;
//    fclk = sci_port->fclk;
//    pr_debug("%s(): ThisP%d %s\n", __FUNCTION__, sci_port->port.line, iclk->name);
		pr_debug("%s(): ThisP%d \n", __FUNCTION__, sci_port->port.line);

	// Enable ttySC0 (physical serial port)
		s = &sci_ports[RESERVED1_SERIAL_PORT];
//    iclk = s->iclk;
//    fclk = s->fclk;
//    pr_debug("%s(): SCIF2 P%d %s\n", __FUNCTION__, s->port.line, iclk->name);
		pr_debug("%s(): SCIF2 P%d \n", __FUNCTION__, s->port.line);
	
//	clk_prepare_enable(s->iclk);
//	sci_port->port.uartclk = clk_get_rate(s->iclk);
//	clk_prepare_enable(s->fclk);

	// Enable ttySC1 (physical serial port)
		s = &sci_ports[RESERVED2_SERIAL_PORT];
//    iclk = s->iclk;
//    fclk = s->fclk;
//    pr_debug("%s(): SCIF3 P%d %s\n", __FUNCTION__, s->port.line, &s->iclk->name);
		pr_debug("%s(): SCIF3 P%d \n", __FUNCTION__, s->port.line);

//	clk_prepare_enable(s->iclk);
//	sci_port->port.uartclk = clk_get_rate(s->iclk);
//	clk_prepare_enable(s->fclk);
	}
#endif
#if 0
		
	if (sci_port->port.line == DEBUG_SERIAL_PORT)
	{
		clk_prepare_enable(sci_port->iclk);
		sci_port->port.uartclk = clk_get_rate(sci_port->iclk);
		clk_prepare_enable(sci_port->fclk);
	}
	else
	{

		// Enable ttySC0 and ttySC1 (physical serial ports)
		s = &sci_ports[RESERVED1_SERIAL_PORT];
		clk_prepare_enable(s->iclk);
		sci_port->port.uartclk = clk_get_rate(s->iclk);
		clk_prepare_enable(s->fclk);
		s = &sci_ports[RESERVED2_SERIAL_PORT];
		clk_prepare_enable(s->iclk);
		sci_port->port.uartclk = clk_get_rate(s->iclk);
		clk_prepare_enable(s->fclk);

		
	}
#else       
	clk_prepare_enable(sci_port->iclk);
	sci_port->port.uartclk = clk_get_rate(sci_port->iclk);
	clk_prepare_enable(sci_port->fclk);
#endif
}

static void sci_port_disable(struct sci_port *sci_port)
{
    if (S_CloseDebug == 1) pr_debug("%s(): P%d\n", __FUNCTION__, sci_port->port.line);
	if (!sci_port->port.dev)
		return;

	/* Cancel the break timer to ensure that the timer handler will not try
	 * to access the hardware with clocks and power disabled. Reset the
	 * break flag to make the break debouncing state machine ready for the
	 * next break.
	 */
	del_timer_sync(&sci_port->break_timer);
	sci_port->break_flag = 0;

	clk_disable_unprepare(sci_port->fclk);
	clk_disable_unprepare(sci_port->iclk);

	pm_runtime_put_sync(sci_port->port.dev);
}

#if defined(CONFIG_CONSOLE_POLL) || defined(CONFIG_SERIAL_SH_SCI_CONSOLE)

#ifdef CONFIG_CONSOLE_POLL
static int sci_poll_get_char(struct uart_port *port)
{
	unsigned short status;
	int c;

	do {
		status = serial_port_in(port, SCxSR);
		if (status & SCxSR_ERRORS(port)) {
			serial_port_out(port, SCxSR, SCxSR_ERROR_CLEAR(port));
			continue;
		}
		break;
	} while (1);

	if (!(status & SCxSR_RDxF(port)))
		return NO_POLL_CHAR;

	c = serial_port_in(port, SCxRDR);

	/* Dummy read */
	serial_port_in(port, SCxSR);
	serial_port_out(port, SCxSR, SCxSR_RDxF_CLEAR(port));

	return c;
}
#endif

static void sci_poll_put_char(struct uart_port *port, unsigned char c)
{
	unsigned short status;

	do {
		status = serial_port_in(port, SCxSR);
	} while (!(status & SCxSR_TDxE(port)));

	serial_port_out(port, SCxTDR, c);
	serial_port_out(port, SCxSR, SCxSR_TDxE_CLEAR(port) & ~SCxSR_TEND(port));
}
#endif /* CONFIG_CONSOLE_POLL || CONFIG_SERIAL_SH_SCI_CONSOLE */

static void sci_init_pins(struct uart_port *port, unsigned int cflag)
{
	struct sci_port *s = to_sci_port(port);
	struct plat_sci_reg *reg = sci_regmap[s->cfg->regtype] + SCSPTR;

	/*
	 * Use port-specific handler if provided.
	 */
	if (s->cfg->ops && s->cfg->ops->init_pins) {
		s->cfg->ops->init_pins(port, cflag);
		return;
	}

	/*
	 * For the generic path SCSPTR is necessary. Bail out if that's
	 * unavailable, too.
	 */
	if (!reg->size)
		return;

	if ((s->cfg->capabilities & SCIx_HAVE_RTSCTS) &&
	    ((!(cflag & CRTSCTS)))) {
		unsigned short status;

		status = serial_port_in(port, SCSPTR);
		status &= ~SCSPTR_CTSIO;
		status |= SCSPTR_RTSIO;
		serial_port_out(port, SCSPTR, status); /* Set RTS = 1 */
	}
}

static int sci_txfill(struct uart_port *port)
{
	struct plat_sci_reg *reg;

	reg = sci_getreg(port, SCTFDR);
	if (reg->size)
		return serial_port_in(port, SCTFDR) & ((port->fifosize << 1) - 1);

	reg = sci_getreg(port, SCFDR);
	if (reg->size)
		return serial_port_in(port, SCFDR) >> 8;

	return !(serial_port_in(port, SCxSR) & SCI_TDRE);
}

static int sci_txroom(struct uart_port *port)
{
	return port->fifosize - sci_txfill(port);
}

static int sci_rxfill(struct uart_port *port)
{
	struct plat_sci_reg *reg;

	reg = sci_getreg(port, SCRFDR);
	if (reg->size)
		return serial_port_in(port, SCRFDR) & ((port->fifosize << 1) - 1);

	reg = sci_getreg(port, SCFDR);
	if (reg->size)
		return serial_port_in(port, SCFDR) & ((port->fifosize << 1) - 1);

	return (serial_port_in(port, SCxSR) & SCxSR_RDxF(port)) != 0;
}

/*
 * SCI helper for checking the state of the muxed port/RXD pins.
 */
static inline int sci_rxd_in(struct uart_port *port)
{
	struct sci_port *s = to_sci_port(port);

	if (s->cfg->port_reg <= 0)
		return 1;

	/* Cast for ARM damage */
	return !!__raw_readb((void __iomem *)(uintptr_t)s->cfg->port_reg);
}

/* ********************************************************************** *
 *                   the interrupt related routines                       *
 * ********************************************************************** */

static void sci_transmit_chars(struct uart_port *port)
{
	struct circ_buf *xmit = &port->state->xmit;
	unsigned int stopped = uart_tx_stopped(port);
	unsigned short status;
	unsigned short ctrl;
	int count;
	int j;  

	status = serial_port_in(port, SCxSR);
	if (!(status & SCxSR_TDxE(port))) {
		ctrl = serial_port_in(port, SCSCR);
		if (uart_circ_empty(xmit))
			ctrl &= ~SCSCR_TIE;
		else
			ctrl |= SCSCR_TIE;
		serial_port_out(port, SCSCR, ctrl);
		return;
	}

	count = sci_txroom(port);

	do {
		unsigned char c;

		if (port->x_char) {
			c = port->x_char;
			port->x_char = 0;
		} else if (!uart_circ_empty(xmit) && !stopped) {
			c = xmit->buf[xmit->tail];
			xmit->tail = (xmit->tail + 1) & (UART_XMIT_SIZE - 1);
		} else {
			break;
		}

		serial_port_out(port, SCxTDR, c);

		port->icount.tx++;

		if (((port->line == 6) || (port->line == 8)) && (S_SolPrintEnable == 1))
		{
			S_tchar = c;
			if (SolTxBufIndex < 199)
				SolTxBuffer[SolTxBufIndex++] = c;
		}
	} while (--count > 0);

	serial_port_out(port, SCxSR, SCxSR_TDxE_CLEAR(port));

	if (uart_circ_chars_pending(xmit) < WAKEUP_CHARS)
		uart_write_wakeup(port);
	if (uart_circ_empty(xmit)) {
		sci_stop_tx(port);
	} else {
		ctrl = serial_port_in(port, SCSCR);

		if (port->type != PORT_SCI) {
			serial_port_in(port, SCxSR); /* Dummy read */
			serial_port_out(port, SCxSR, SCxSR_TDxE_CLEAR(port));
		}

		ctrl |= SCSCR_TIE;
		serial_port_out(port, SCSCR, ctrl);
	}
       if (((port->line == 6) || (port->line == 8)) && (S_SolPrintEnable == 1))
       {
	    for(j=0;j<SolTxBufIndex;j++)
	    {
		  sprintf(G_Txbuffer + j*3, "%02x ", SolTxBuffer[j]);
	    }
	    pr_debug("Tx: %s\n", G_Txbuffer);
	    SolTxBufIndex = 0;	
	}    
}

/* On SH3, SCIF may read end-of-break as a space->mark char */
#define STEPFN(c)  ({int __c = (c); (((__c-1)|(__c)) == -1); })

static void sci_receive_chars(struct uart_port *port)
{
	struct sci_port *sci_port = to_sci_port(port);
#ifdef __iDRAC__
	struct tty_port *tport = &port->state->port;
#endif	
	int i, j, count, copied = 0;
	unsigned short status;
	unsigned char flag;

	int bufferindex = 0;
	unsigned char c;

	status = serial_port_in(port, SCxSR);
	if (!(status & SCxSR_RDxF(port)))
		return;

	while (1) {
		/* Don't copy more bytes than there is room for in the buffer */
		count = tty_buffer_request_room(tport, sci_rxfill(port));

		/* If for any reason we can't copy more data, we're done! */
		if (count == 0)
			break;

		if (port->type == PORT_SCI) {
			c = (unsigned char) serial_port_in(port, SCxRDR);
			if (uart_handle_sysrq_char(port, c) ||
			    sci_port->break_flag)
				count = 0;
			else
				tty_insert_flip_char(tport, c, TTY_NORMAL);
		} else {
			for (i = 0; i < count; i++) {
				c = (unsigned char) serial_port_in(port, SCxRDR);

				status = serial_port_in(port, SCxSR);
#ifdef  __iDRAC__
  
        /*
         * When iDRAC snooping, we ignore chars until the
         * "<ESC>(" sequence is received. Then we pass
         * through chars until the "<ESC>Q" sequence is
         * received, returning to the initial state.
         */
                if ((tport == *idracSCIF3ldr) || (tport == *idracSCIF2ldr))
                {
                    switch (idracmux) 
                    {
                        case MODE1:
                            #ifdef SUPPORT_HISTBUF
                            stuffhistbuf(c);
//                            if (HistIndex < 255)
//	                            Hist[HistIndex++] = c;
                            if (flushinghist)
                                goto ignore_char;
                            if ((port->line == 6) || (port->line == 8)) 
                            {
                                if ((S_SolPrintEnable) && (SolRxBufIndex < 199))
                                   SolRxBuffer[SolRxBufIndex++] = c;
                            }       
                                
                            break;
                            #endif
                        case MODE2A:

                            if (idracmuxreq != MODE2A)//It means that the new state is not MODE2A. 
                               goto ignore_char;   //idracmuxreq != idracmux means that set mux ioctl command is not finished.

                       
                            if (idraclastch == ESC && c == '(') 
                            {
                                idraclastch = '\0';
                                idracmuxreq = MODE2B;
                                if (S_MuxDebug) pr_debug("Mode2A to Mode2B\n");
                                tasklet_schedule(&bh_tasklet);
                                goto ignore_char;
                            }
                            idraclastch = c;
                            goto ignore_char;
                            break;
                        case MODE2B:
                            if (idracmuxreq != MODE2B)
                                goto ignore_char;
                            
                            if ((port->line != IPMI_VIRTUAL_SERIAL_PORT1 || snoopon) &&
                                 idraclastch == ESC && c == 'Q') 
                            {
                                idraclastch = '\0';
                                /*
                                 * Echo back on 2B to 2A transition.
                                 */

                                sci_uart_driver.tty_driver->ops->put_char(tport->tty,ESC);
                                sci_uart_driver.tty_driver->ops->put_char(tport->tty,'*');
                                sci_uart_driver.tty_driver->ops->flush_chars(tport->tty);
                                idracmuxreq = MODE2A;
                                if (S_MuxDebug) pr_debug("Mode2B to Mode2A\n");
                                tasklet_schedule(&bh_tasklet);
                                goto ignore_char;
                            }

                            idraclastch = c;
                            break;
                        case MODE2C:
                        if(port->line == 6) 
                        {
                            if ((S_SolPrintEnable) && (SolRxBufIndex < 199))
                                SolRxBuffer[SolRxBufIndex++] = c;
                        }        
                        break;
                        case MODE3:
                            // Serial data capture not implemented in 13G iDRAC
                            break;
                        default:
                            break;
                    }
                }
                else if ((port->line != 2) && (S_PrintTTY_ISR == 1))
                {
                    pr_debug("SciRX: No matching tty : P%d, tport=0x%08p, SCIF2=0x%08p, SCIF3=0x%08p, P3=0x%08p, P4=0x%08p, P5=0x%08p, P6=0x%08p, P7=0x%08p, P8=0x%08p\n",
                                  port->line, tport, *idracSCIF2ldr, *idracSCIF3ldr, idractty[0], idractty[1], idractty[2], idractty[3], idractty[4], idractty[5]);                    
                }    
#endif 

#if defined(CONFIG_CPU_SH3)
				/* Skip "chars" during break */
				if (sci_port->break_flag) {
					if ((c == 0) &&
					    (status & SCxSR_FER(port))) {
						count--; i--;
						continue;
					}

					/* Nonzero => end-of-break */
					dev_dbg(port->dev, "debounce<%02x>\n", c);
					sci_port->break_flag = 0;

					if (STEPFN(c)) {
						count--; i--;
						continue;
					}
				}
#endif /* CONFIG_CPU_SH3 */
				if (uart_handle_sysrq_char(port, c)) {
					count--; i--;
					continue;
				}

				/* Store data and status */
				if (status & SCxSR_FER(port)) {
					flag = TTY_FRAME;
					port->icount.frame++;
//					dev_notice(port->dev, "frame error\n");
				} else if (status & SCxSR_PER(port)) {
					flag = TTY_PARITY;
					port->icount.parity++;
//					dev_notice(port->dev, "parity error\n");
				} else
					flag = TTY_NORMAL;

				tty_insert_flip_char(tport, c, flag);
			}
		}

		copied += count;
		port->icount.rx += count;
#ifdef __iDRAC__
ignore_char:
        serial_port_in(port, SCxSR); /* dummy read */
		serial_port_out(port, SCxSR, SCxSR_RDxF_CLEAR(port));


#endif //__iDRAC__
	}

	if (copied) {
		/* Tell the rest of the system the news. New characters! */
		tty_flip_buffer_push(tport);
#if __iDRAC__
              if (((port->line == 6) || (port->line == 8)) && (S_SolPrintEnable))
              {
                  if (SolRxBufIndex > 90)
                  {
		        for(j=0;j<SolRxBufIndex;j++)
		        {
		             if ((SolRxBuffer[j] >= 0x20) && (SolRxBuffer[j] <= 0x7E))  // printable ASCII???
		             {
                                 G_Rxbuffer[bufferindex++] = SolRxBuffer[j];
                           }
                           else
                           {
                                 bufferindex += sprintf(&G_Rxbuffer[bufferindex], "%02x ", SolRxBuffer[j]);
                            }     
                                 
		        }
		        G_Rxbuffer[bufferindex] = 0;
                      pr_debug("SOL Rx: %s\n", G_Rxbuffer);
                      SolRxBufIndex = 0;	
                  }        
              }    
#endif
	} else {
#if __iDRAC__
		char c;

		count = sci_rxfill(port);
		/*
		 * When the tty_buffer is full, this code will clear all
		 * SCIF's rx fifo, because we should avoid overrun error.
		 */
		for (i = 0; i < count; i++) {
			c = serial_port_in(port, SCxRDR);
			serial_port_in(port, SCxSR); /* dummy read */
			serial_port_out(port, SCxSR, SCxSR_RDxF_CLEAR(port));
		}
		if (count > 0) 
		{
		    tty_insert_flip_char(tport, 0, TTY_OVERRUN);
		}
#else
		serial_port_in(port, SCxSR); /* dummy read */
		serial_port_out(port, SCxSR, SCxSR_RDxF_CLEAR(port));
#endif
	}
}

#define SCI_BREAK_JIFFIES (HZ/20)

/*
 * The sci generates interrupts during the break,
 * 1 per millisecond or so during the break period, for 9600 baud.
 * So dont bother disabling interrupts.
 * But dont want more than 1 break event.
 * Use a kernel timer to periodically poll the rx line until
 * the break is finished.
 */
static inline void sci_schedule_break_timer(struct sci_port *port)
{
	mod_timer(&port->break_timer, jiffies + SCI_BREAK_JIFFIES);
}

/* Ensure that two consecutive samples find the break over. */
static void sci_break_timer(unsigned long data)
{
	struct sci_port *port = (struct sci_port *)data;

	if (sci_rxd_in(&port->port) == 0) {
		port->break_flag = 1;
		sci_schedule_break_timer(port);
	} else if (port->break_flag == 1) {
		/* break is over. */
		port->break_flag = 2;
		sci_schedule_break_timer(port);
	} else
		port->break_flag = 0;
}

static int sci_handle_errors(struct uart_port *port)
{
	int copied = 0;
	unsigned short status = serial_port_in(port, SCxSR);
	struct tty_port *tport = &port->state->port;
	struct sci_port *s = to_sci_port(port);

	/* Handle overruns */
	if (status & (1 << s->overrun_bit)) {
		port->icount.overrun++;

		/* overrun error */
		if (tty_insert_flip_char(tport, 0, TTY_OVERRUN))
			copied++;

//		dev_notice(port->dev, "overrun error");
	}

	if (status & SCxSR_FER(port)) {
		if (sci_rxd_in(port) == 0) {
			/* Notify of BREAK */
			struct sci_port *sci_port = to_sci_port(port);

			if (!sci_port->break_flag) {
				port->icount.brk++;

				sci_port->break_flag = 1;
				sci_schedule_break_timer(sci_port);

				/* Do sysrq handling. */
				if (uart_handle_break(port))
					return 0;

				dev_dbg(port->dev, "BREAK detected\n");

				if (tty_insert_flip_char(tport, 0, TTY_BREAK))
					copied++;
			}

		} else {
			/* frame error */
			port->icount.frame++;

			if (tty_insert_flip_char(tport, 0, TTY_FRAME))
				copied++;

//			dev_notice(port->dev, "frame error\n");
		}
	}

	if (status & SCxSR_PER(port)) {
		/* parity error */
		port->icount.parity++;

		if (tty_insert_flip_char(tport, 0, TTY_PARITY))
			copied++;

//		dev_notice(port->dev, "parity error");
	}

	if (copied)
		tty_flip_buffer_push(tport);

	return copied;
}

static int sci_handle_fifo_overrun(struct uart_port *port)
{
	struct tty_port *tport = &port->state->port;
	struct sci_port *s = to_sci_port(port);
	struct plat_sci_reg *reg;
	int copied = 0;

	reg = sci_getreg(port, SCLSR);
	if (!reg->size)
		return 0;

	if ((serial_port_in(port, SCLSR) & (1 << s->overrun_bit))) {
		serial_port_out(port, SCLSR, 0);

		port->icount.overrun++;

		tty_insert_flip_char(tport, 0, TTY_OVERRUN);
		tty_flip_buffer_push(tport);

//		dev_notice(port->dev, "overrun error\n");
		copied++;
	}

	return copied;
}

static int sci_handle_breaks(struct uart_port *port)
{
	int copied = 0;
	unsigned short status = serial_port_in(port, SCxSR);
	struct tty_port *tport = &port->state->port;
	struct sci_port *s = to_sci_port(port);

	if (uart_handle_break(port))
		return 0;

	if (!s->break_flag && status & SCxSR_BRK(port)) {
#if defined(CONFIG_CPU_SH3)
		/* Debounce break */
		s->break_flag = 1;
#endif

		port->icount.brk++;

		/* Notify of BREAK */
		if (tty_insert_flip_char(tport, 0, TTY_BREAK))
			copied++;

		dev_dbg(port->dev, "BREAK detected\n");
	}

	if (copied)
		tty_flip_buffer_push(tport);

	copied += sci_handle_fifo_overrun(port);

	return copied;
}

static irqreturn_t sci_rx_interrupt(int irq, void *ptr)
{
#ifdef CONFIG_SERIAL_SH_SCI_DMA
	struct uart_port *port = ptr;
	struct sci_port *s = to_sci_port(port);

	if (s->chan_rx) {
		u16 scr = serial_port_in(port, SCSCR);
		u16 ssr = serial_port_in(port, SCxSR);

		/* Disable future Rx interrupts */
		if (port->type == PORT_SCIFA || port->type == PORT_SCIFB) {
			disable_irq_nosync(irq);
			scr |= SCSCR_RDRQE;
		} else {
			scr &= ~SCSCR_RIE;
		}
		serial_port_out(port, SCSCR, scr);
		/* Clear current interrupt */
		serial_port_out(port, SCxSR, ssr & ~(1 | SCxSR_RDxF(port)));
		dev_dbg(port->dev, "Rx IRQ %lu: setup t-out in %u jiffies\n",
			jiffies, s->rx_timeout);
		mod_timer(&s->rx_timer, jiffies + s->rx_timeout);

		return IRQ_HANDLED;
	}
#endif

	/* I think sci_receive_chars has to be called irrespective
	 * of whether the I_IXOFF is set, otherwise, how is the interrupt
	 * to be disabled?
	 */
	sci_receive_chars(ptr);

	return IRQ_HANDLED;
}

static irqreturn_t sci_tx_interrupt(int irq, void *ptr)
{
	struct uart_port *port = ptr;
	unsigned long flags;

	spin_lock_irqsave(&port->lock, flags);
	sci_transmit_chars(port);
	spin_unlock_irqrestore(&port->lock, flags);

	return IRQ_HANDLED;
}

static irqreturn_t sci_er_interrupt(int irq, void *ptr)
{
	struct uart_port *port = ptr;

	/* Handle errors */
	if (port->type == PORT_SCI) {
		if (sci_handle_errors(port)) {
			/* discard character in rx buffer */
			serial_port_in(port, SCxSR);
			serial_port_out(port, SCxSR, SCxSR_RDxF_CLEAR(port));
		}
	} else {
		sci_handle_fifo_overrun(port);
		sci_rx_interrupt(irq, ptr);
	}

	serial_port_out(port, SCxSR, SCxSR_ERROR_CLEAR(port));

	/* Kick the transmission */
	sci_tx_interrupt(irq, ptr);

	return IRQ_HANDLED;
}

static irqreturn_t sci_br_interrupt(int irq, void *ptr)
{
	struct uart_port *port = ptr;

	/* Handle BREAKs */
	sci_handle_breaks(port);
	serial_port_out(port, SCxSR, SCxSR_BREAK_CLEAR(port));

	return IRQ_HANDLED;
}

static inline unsigned long port_rx_irq_mask(struct uart_port *port)
{
	/*
	 * Not all ports (such as SCIFA) will support REIE. Rather than
	 * special-casing the port type, we check the port initialization
	 * IRQ enable mask to see whether the IRQ is desired at all. If
	 * it's unset, it's logically inferred that there's no point in
	 * testing for it.
	 */
	return SCSCR_RIE | (to_sci_port(port)->cfg->scscr & SCSCR_REIE);
}

static irqreturn_t sci_mpxed_interrupt(int irq, void *ptr)
{
	unsigned short ssr_status, scr_status, err_enabled;
	struct uart_port *port = ptr;
	struct sci_port *s = to_sci_port(port);
	irqreturn_t ret = IRQ_NONE;
#if __iDRAC__
	struct plat_sci_reg *reg;	
	unsigned short ctrl;
#endif

	ssr_status = serial_port_in(port, SCxSR);
	scr_status = serial_port_in(port, SCSCR);
	err_enabled = scr_status & port_rx_irq_mask(port);

#if __iDRAC__
	/* Overrun */
	if (serial_port_in(port, SCLSR) & 0x0001) {
		ret = sci_er_interrupt(irq, ptr);
		if (port->type != PORT_SCI) {
			reg = sci_getreg(port, SCFCR);
			if (reg->size) {
				ctrl = serial_port_in(port, SCFCR);
				serial_port_out(port, SCFCR, SCFCR_RFRST | SCFCR_TFRST);
				ctrl &= ~(SCFCR_RFRST | SCFCR_TFRST);
				serial_port_out(port, SCFCR, ctrl);
			}
		}
	}
#endif
	/* Tx Interrupt */
	if ((ssr_status & SCxSR_TDxE(port)) && (scr_status & SCSCR_TIE) &&
	    !s->chan_tx)
	{
		if ((port->line != 2) && S_MuxDebug)
                pr_debug("P%d tx\n", port->line);
		ret = sci_tx_interrupt(irq, ptr);
	}	

	/*
	 * Rx Interrupt: if we're using DMA, the DMA controller clears RDF /
	 * DR flags
	 */
	if (((ssr_status & SCxSR_RDxF(port)) || s->chan_rx) &&
	    (scr_status & SCSCR_RIE))
	{    
		if ((port->line != 2) && S_MuxDebug)
                pr_debug("P%d rx\n", port->line);
		ret = sci_rx_interrupt(irq, ptr);
	}
	/* Error Interrupt */
	if ((ssr_status & SCxSR_ERRORS(port)) && err_enabled)
	{    
		if ((port->line != 2) && S_MuxDebug)
                pr_debug("P%d er\n", port->line);
		ret = sci_er_interrupt(irq, ptr);
	}
	/* Break Interrupt */
	if ((ssr_status & SCxSR_BRK(port)) && err_enabled)
	{    
		if ((port->line != 2) && S_MuxDebug)
                pr_debug("P%d br\n", port->line);
		ret = sci_br_interrupt(irq, ptr);
	}
	return ret;
}

static struct sci_irq_desc {
	const char	*desc;
	irq_handler_t	handler;
} sci_irq_desc[] = {
	/*
	 * Split out handlers, the default case.
	 */
	[SCIx_ERI_IRQ] = {
		.desc = "rx err",
		.handler = sci_er_interrupt,
	},

	[SCIx_RXI_IRQ] = {
		.desc = "rx full",
		.handler = sci_rx_interrupt,
	},

	[SCIx_TXI_IRQ] = {
		.desc = "tx empty",
		.handler = sci_tx_interrupt,
	},

	[SCIx_BRI_IRQ] = {
		.desc = "break",
		.handler = sci_br_interrupt,
	},

	/*
	 * Special muxed handler.
	 */
	[SCIx_MUX_IRQ] = {
		.desc = "mux",
		.handler = sci_mpxed_interrupt,
	},
};

static int sci_request_irq(struct sci_port *port)
{
	struct uart_port *up = &port->port;
	int i, j, ret = 0;

#if __iDRAC__
	if (up->line == IDRACTTY_DAEMON_SERIAL_PORT)
		return 0;
#endif		
	for (i = j = 0; i < SCIx_NR_IRQS; i++, j++) {
		struct sci_irq_desc *desc;
		int irq;

		if (SCIx_IRQ_IS_MUXED(port)) {
		    if (S_MuxDebug)
		        pr_debug("%s(): P%d is muxed. %d %d %d %d\n", __FUNCTION__, up->line, port->irqs[0], port->irqs[1], port->irqs[2], port->irqs[3]);
			i = SCIx_MUX_IRQ;
			irq = up->irq;
		} else {
			irq = port->irqs[i];

			/*
			 * Certain port types won't support all of the
			 * available interrupt sources.
			 */
			if (unlikely(irq < 0))
				continue;
		}

		desc = sci_irq_desc + i;
		port->irqstr[j] = kasprintf(GFP_KERNEL, "%s:%s",
					    dev_name(up->dev), desc->desc);
		if (!port->irqstr[j]) {
			dev_err(up->dev, "Failed to allocate %s IRQ string\n",
				desc->desc);
			goto out_nomem;
		}
        if (S_OpenDebug) pr_debug("%s(): P%d irq%d irqflags%d\n", __FUNCTION__, up->line, irq, up->irqflags);
		ret = request_irq(irq, desc->handler, up->irqflags,
				  port->irqstr[j], port);
		if (unlikely(ret)) {
			dev_err(up->dev, "Can't allocate %s IRQ\n", desc->desc);
			goto out_noirq;
		}
#ifdef __iDRAC__
	    irq_flag[port->port.line] = 1;
#endif
	
	}

	return 0;

out_noirq:
	while (--i >= 0)
		free_irq(port->irqs[i], port);

out_nomem:
	while (--j >= 0)
		kfree(port->irqstr[j]);

	return ret;
}

static void sci_free_irq(struct sci_port *port)
{
	int i;

	/*
	 * Intentionally in reverse order so we iterate over the muxed
	 * IRQ first.
	 */
	for (i = 0; i < SCIx_NR_IRQS; i++) {
		int irq = port->irqs[i];

		/*
		 * Certain port types won't support all of the available
		 * interrupt sources.
		 */
		if (unlikely(irq < 0))
			continue;

		free_irq(port->irqs[i], port);
		kfree(port->irqstr[i]);

		if (SCIx_IRQ_IS_MUXED(port)) {
			/* If there's only one IRQ, we're done. */
			return;
		}
	}
}

static unsigned int sci_tx_empty(struct uart_port *port)
{
	unsigned short status = serial_port_in(port, SCxSR);
	unsigned short in_tx_fifo = sci_txfill(port);

	return (status & SCxSR_TEND(port)) && !in_tx_fifo ? TIOCSER_TEMT : 0;
}

/*
 * Modem control is a bit of a mixed bag for SCI(F) ports. Generally
 * CTS/RTS is supported in hardware by at least one port and controlled
 * via SCSPTR (SCxPCR for SCIFA/B parts), or external pins (presently
 * handled via the ->init_pins() op, which is a bit of a one-way street,
 * lacking any ability to defer pin control -- this will later be
 * converted over to the GPIO framework).
 *
 * Other modes (such as loopback) are supported generically on certain
 * port types, but not others. For these it's sufficient to test for the
 * existence of the support register and simply ignore the port type.
 */
static void sci_set_mctrl(struct uart_port *port, unsigned int mctrl)
{
	if (mctrl & TIOCM_LOOP) {
		struct plat_sci_reg *reg;

		/*
		 * Standard loopback mode for SCFCR ports.
		 */
		reg = sci_getreg(port, SCFCR);
		if (reg->size)
			serial_port_out(port, SCFCR,
					serial_port_in(port, SCFCR) |
					SCFCR_LOOP);
	}
}

static unsigned int sci_get_mctrl(struct uart_port *port)
{
	/*
	 * CTS/RTS is handled in hardware when supported, while nothing
	 * else is wired up. Keep it simple and simply assert DSR/CAR.
	 */
#ifdef __iDRAC__	
   	/**?????*/
   	/**SCIF 2~4 doesn't has DCD pin. But look at Mode0 of serial mux chapter
   	   It seems that it can read some resgiers to get DCD value. 
   	   Ask Renesas how to do that.*/
	#if 0
	  if (status & UART_MSR_DCD)
		  result |= TIOCM_CAR;
	  if (status & UART_MSR_DSR)
		  result |= TIOCM_DSR;
	#endif	
	return TIOCM_DTR | TIOCM_RTS | TIOCM_DSR | TIOCM_CTS;
#else
	return TIOCM_DSR | TIOCM_CAR;
#endif
}

#ifdef CONFIG_SERIAL_SH_SCI_DMA
static void sci_dma_tx_complete(void *arg)
{
	struct sci_port *s = arg;
	struct uart_port *port = &s->port;
	struct circ_buf *xmit = &port->state->xmit;
	unsigned long flags;

	dev_dbg(port->dev, "%s(%d)\n", __func__, port->line);

	spin_lock_irqsave(&port->lock, flags);

	xmit->tail += sg_dma_len(&s->sg_tx);
	xmit->tail &= UART_XMIT_SIZE - 1;

	port->icount.tx += sg_dma_len(&s->sg_tx);

	async_tx_ack(s->desc_tx);
	s->desc_tx = NULL;

	if (uart_circ_chars_pending(xmit) < WAKEUP_CHARS)
		uart_write_wakeup(port);

	if (!uart_circ_empty(xmit)) {
		s->cookie_tx = 0;
		schedule_work(&s->work_tx);
	} else {
		s->cookie_tx = -EINVAL;
		if (port->type == PORT_SCIFA || port->type == PORT_SCIFB) {
			u16 ctrl = serial_port_in(port, SCSCR);
			serial_port_out(port, SCSCR, ctrl & ~SCSCR_TIE);
		}
	}

	spin_unlock_irqrestore(&port->lock, flags);
}

/* Locking: called with port lock held */
static int sci_dma_rx_push(struct sci_port *s, size_t count)
{
	struct uart_port *port = &s->port;
	struct tty_port *tport = &port->state->port;
	int i, active, room;

	room = tty_buffer_request_room(tport, count);

	if (s->active_rx == s->cookie_rx[0]) {
		active = 0;
	} else if (s->active_rx == s->cookie_rx[1]) {
		active = 1;
	} else {
		dev_err(port->dev, "cookie %d not found!\n", s->active_rx);
		return 0;
	}

	if (room < count)
		dev_warn(port->dev, "Rx overrun: dropping %zu bytes\n",
			 count - room);
	if (!room)
		return room;

	for (i = 0; i < room; i++)
		tty_insert_flip_char(tport, ((u8 *)sg_virt(&s->sg_rx[active]))[i],
				     TTY_NORMAL);

	port->icount.rx += room;

	return room;
}

static void sci_dma_rx_complete(void *arg)
{
	struct sci_port *s = arg;
	struct uart_port *port = &s->port;
	unsigned long flags;
	int count;

	dev_dbg(port->dev, "%s(%d) active #%d\n",
		__func__, port->line, s->active_rx);

	spin_lock_irqsave(&port->lock, flags);

	count = sci_dma_rx_push(s, s->buf_len_rx);

	mod_timer(&s->rx_timer, jiffies + s->rx_timeout);

	spin_unlock_irqrestore(&port->lock, flags);

	if (count)
		tty_flip_buffer_push(&port->state->port);

	schedule_work(&s->work_rx);
}

static void sci_rx_dma_release(struct sci_port *s, bool enable_pio)
{
	struct dma_chan *chan = s->chan_rx;
	struct uart_port *port = &s->port;

	s->chan_rx = NULL;
	s->cookie_rx[0] = s->cookie_rx[1] = -EINVAL;
	dma_release_channel(chan);
	if (sg_dma_address(&s->sg_rx[0]))
		dma_free_coherent(port->dev, s->buf_len_rx * 2,
				  sg_virt(&s->sg_rx[0]), sg_dma_address(&s->sg_rx[0]));
	if (enable_pio)
		sci_start_rx(port);
}

static void sci_tx_dma_release(struct sci_port *s, bool enable_pio)
{
	struct dma_chan *chan = s->chan_tx;
	struct uart_port *port = &s->port;

	s->chan_tx = NULL;
	s->cookie_tx = -EINVAL;
	dma_release_channel(chan);
	if (enable_pio)
		sci_start_tx(port);
}

static void sci_submit_rx(struct sci_port *s)
{
	struct dma_chan *chan = s->chan_rx;
	int i;

	for (i = 0; i < 2; i++) {
		struct scatterlist *sg = &s->sg_rx[i];
		struct dma_async_tx_descriptor *desc;

		desc = dmaengine_prep_slave_sg(chan,
			sg, 1, DMA_DEV_TO_MEM, DMA_PREP_INTERRUPT);

		if (desc) {
			s->desc_rx[i] = desc;
			desc->callback = sci_dma_rx_complete;
			desc->callback_param = s;
			s->cookie_rx[i] = desc->tx_submit(desc);
		}

		if (!desc || s->cookie_rx[i] < 0) {
			if (i) {
				async_tx_ack(s->desc_rx[0]);
				s->cookie_rx[0] = -EINVAL;
			}
			if (desc) {
				async_tx_ack(desc);
				s->cookie_rx[i] = -EINVAL;
			}
			dev_warn(s->port.dev,
				 "failed to re-start DMA, using PIO\n");
			sci_rx_dma_release(s, true);
			return;
		}
		dev_dbg(s->port.dev, "%s(): cookie %d to #%d\n",
			__func__, s->cookie_rx[i], i);
	}

	s->active_rx = s->cookie_rx[0];

	dma_async_issue_pending(chan);
}

static void work_fn_rx(struct work_struct *work)
{
	struct sci_port *s = container_of(work, struct sci_port, work_rx);
	struct uart_port *port = &s->port;
	struct dma_async_tx_descriptor *desc;
	int new;

	if (s->active_rx == s->cookie_rx[0]) {
		new = 0;
	} else if (s->active_rx == s->cookie_rx[1]) {
		new = 1;
	} else {
		dev_err(port->dev, "cookie %d not found!\n", s->active_rx);
		return;
	}
	desc = s->desc_rx[new];

	if (dma_async_is_tx_complete(s->chan_rx, s->active_rx, NULL, NULL) !=
	    DMA_COMPLETE) {
		/* Handle incomplete DMA receive */
		struct dma_chan *chan = s->chan_rx;
		struct shdma_desc *sh_desc = container_of(desc,
					struct shdma_desc, async_tx);
		unsigned long flags;
		int count;

		chan->device->device_control(chan, DMA_TERMINATE_ALL, 0);
		dev_dbg(port->dev, "Read %zu bytes with cookie %d\n",
			sh_desc->partial, sh_desc->cookie);

		spin_lock_irqsave(&port->lock, flags);
		count = sci_dma_rx_push(s, sh_desc->partial);
		spin_unlock_irqrestore(&port->lock, flags);

		if (count)
			tty_flip_buffer_push(&port->state->port);

		sci_submit_rx(s);

		return;
	}

	s->cookie_rx[new] = desc->tx_submit(desc);
	if (s->cookie_rx[new] < 0) {
		dev_warn(port->dev, "Failed submitting Rx DMA descriptor\n");
		sci_rx_dma_release(s, true);
		return;
	}

	s->active_rx = s->cookie_rx[!new];

	dev_dbg(port->dev, "%s: cookie %d #%d, new active #%d\n",
		__func__, s->cookie_rx[new], new, s->active_rx);
}

static void work_fn_tx(struct work_struct *work)
{
	struct sci_port *s = container_of(work, struct sci_port, work_tx);
	struct dma_async_tx_descriptor *desc;
	struct dma_chan *chan = s->chan_tx;
	struct uart_port *port = &s->port;
	struct circ_buf *xmit = &port->state->xmit;
	struct scatterlist *sg = &s->sg_tx;

	/*
	 * DMA is idle now.
	 * Port xmit buffer is already mapped, and it is one page... Just adjust
	 * offsets and lengths. Since it is a circular buffer, we have to
	 * transmit till the end, and then the rest. Take the port lock to get a
	 * consistent xmit buffer state.
	 */
	spin_lock_irq(&port->lock);
	sg->offset = xmit->tail & (UART_XMIT_SIZE - 1);
	sg_dma_address(sg) = (sg_dma_address(sg) & ~(UART_XMIT_SIZE - 1)) +
		sg->offset;
	sg_dma_len(sg) = min((int)CIRC_CNT(xmit->head, xmit->tail, UART_XMIT_SIZE),
		CIRC_CNT_TO_END(xmit->head, xmit->tail, UART_XMIT_SIZE));
	spin_unlock_irq(&port->lock);

	BUG_ON(!sg_dma_len(sg));

	desc = dmaengine_prep_slave_sg(chan,
			sg, s->sg_len_tx, DMA_MEM_TO_DEV,
			DMA_PREP_INTERRUPT | DMA_CTRL_ACK);
	if (!desc) {
		/* switch to PIO */
		sci_tx_dma_release(s, true);
		return;
	}

	dma_sync_sg_for_device(port->dev, sg, 1, DMA_TO_DEVICE);

	spin_lock_irq(&port->lock);
	s->desc_tx = desc;
	desc->callback = sci_dma_tx_complete;
	desc->callback_param = s;
	spin_unlock_irq(&port->lock);
	s->cookie_tx = desc->tx_submit(desc);
	if (s->cookie_tx < 0) {
		dev_warn(port->dev, "Failed submitting Tx DMA descriptor\n");
		/* switch to PIO */
		sci_tx_dma_release(s, true);
		return;
	}

	dev_dbg(port->dev, "%s: %p: %d...%d, cookie %d\n",
		__func__, xmit->buf, xmit->tail, xmit->head, s->cookie_tx);

	dma_async_issue_pending(chan);
}
#endif

static void sci_start_tx(struct uart_port *port)
{
	struct sci_port *s = to_sci_port(port);
	unsigned short ctrl;
#ifdef __iDRAC__
	if (port->line == IDRACTTY_DAEMON_SERIAL_PORT)
		return;
	if(idracmux != MODE2A || DEBUG_SERIAL_PORT==port->line)
	{
#endif//__iDRAC__

#ifdef CONFIG_SERIAL_SH_SCI_DMA
	if (port->type == PORT_SCIFA || port->type == PORT_SCIFB) {
		u16 new, scr = serial_port_in(port, SCSCR);
		if (s->chan_tx)
			new = scr | SCSCR_TDRQE;
		else
			new = scr & ~SCSCR_TDRQE;
		if (new != scr)
			serial_port_out(port, SCSCR, new);
	}

	if (s->chan_tx && !uart_circ_empty(&s->port.state->xmit) &&
	    s->cookie_tx < 0) {
		s->cookie_tx = 0;
		schedule_work(&s->work_tx);
	}
#endif

	if (!s->chan_tx || port->type == PORT_SCIFA || port->type == PORT_SCIFB) {
		/* Set TIE (Transmit Interrupt Enable) bit in SCSCR */
		ctrl = serial_port_in(port, SCSCR);
		serial_port_out(port, SCSCR, ctrl | SCSCR_TIE);
	}
#ifdef __iDRAC__
	}
	else
	{
		sci_stop_tx(port);
	}
#endif//__iDRAC__		  
}

static void sci_stop_tx(struct uart_port *port)
{
	unsigned short ctrl;

	/* Clear TIE (Transmit Interrupt Enable) bit in SCSCR */
	ctrl = serial_port_in(port, SCSCR);

	if (port->type == PORT_SCIFA || port->type == PORT_SCIFB)
		ctrl &= ~SCSCR_TDRQE;

	ctrl &= ~SCSCR_TIE;

	serial_port_out(port, SCSCR, ctrl);
}

static void sci_start_rx(struct uart_port *port)
{
	unsigned short ctrl;

#if __iDRAC__
	if (port->line == IDRACTTY_DAEMON_SERIAL_PORT)
		return;
#endif		
	ctrl = serial_port_in(port, SCSCR) | port_rx_irq_mask(port);

	if (port->type == PORT_SCIFA || port->type == PORT_SCIFB)
		ctrl &= ~SCSCR_RDRQE;

	serial_port_out(port, SCSCR, ctrl);
}

static void sci_stop_rx(struct uart_port *port)
{
	unsigned short ctrl;

	ctrl = serial_port_in(port, SCSCR);

	if (port->type == PORT_SCIFA || port->type == PORT_SCIFB)
		ctrl &= ~SCSCR_RDRQE;

	ctrl &= ~port_rx_irq_mask(port);

	serial_port_out(port, SCSCR, ctrl);
}

static void sci_enable_ms(struct uart_port *port)
{
	/*
	 * Not supported by hardware, always a nop.
	 */
}

static void sci_break_ctl(struct uart_port *port, int break_state)
{
	struct sci_port *s = to_sci_port(port);
	struct plat_sci_reg *reg = sci_regmap[s->cfg->regtype] + SCSPTR;
	unsigned short scscr, scsptr;

	/* check wheter the port has SCSPTR */
	if (!reg->size) {
		/*
		 * Not supported by hardware. Most parts couple break and rx
		 * interrupts together, with break detection always enabled.
		 */
		return;
	}

	scsptr = serial_port_in(port, SCSPTR);
	scscr = serial_port_in(port, SCSCR);

	if (break_state == -1) {
		scsptr = (scsptr | SCSPTR_SPB2IO) & ~SCSPTR_SPB2DT;
		scscr &= ~SCSCR_TE;
	} else {
		scsptr = (scsptr | SCSPTR_SPB2DT) & ~SCSPTR_SPB2IO;
		scscr |= SCSCR_TE;
	}

	serial_port_out(port, SCSPTR, scsptr);
	serial_port_out(port, SCSCR, scscr);
}

#ifdef CONFIG_SERIAL_SH_SCI_DMA
static bool filter(struct dma_chan *chan, void *slave)
{
	struct sh_dmae_slave *param = slave;

	dev_dbg(chan->device->dev, "%s: slave ID %d\n",
		__func__, param->shdma_slave.slave_id);

	chan->private = &param->shdma_slave;
	return true;
}

static void rx_timer_fn(unsigned long arg)
{
	struct sci_port *s = (struct sci_port *)arg;
	struct uart_port *port = &s->port;
	u16 scr = serial_port_in(port, SCSCR);

	if (port->type == PORT_SCIFA || port->type == PORT_SCIFB) {
		scr &= ~SCSCR_RDRQE;
		enable_irq(s->irqs[SCIx_RXI_IRQ]);
	}
	serial_port_out(port, SCSCR, scr | SCSCR_RIE);
	dev_dbg(port->dev, "DMA Rx timed out\n");
	schedule_work(&s->work_rx);
}

static void sci_request_dma(struct uart_port *port)
{
	struct sci_port *s = to_sci_port(port);
	struct sh_dmae_slave *param;
	struct dma_chan *chan;
	dma_cap_mask_t mask;
	int nent;

	dev_dbg(port->dev, "%s: port %d\n", __func__, port->line);

	if (s->cfg->dma_slave_tx <= 0 || s->cfg->dma_slave_rx <= 0)
		return;

	dma_cap_zero(mask);
	dma_cap_set(DMA_SLAVE, mask);

	param = &s->param_tx;

	/* Slave ID, e.g., SHDMA_SLAVE_SCIF0_TX */
	param->shdma_slave.slave_id = s->cfg->dma_slave_tx;

	s->cookie_tx = -EINVAL;
	chan = dma_request_channel(mask, filter, param);
	dev_dbg(port->dev, "%s: TX: got channel %p\n", __func__, chan);
	if (chan) {
		s->chan_tx = chan;
		sg_init_table(&s->sg_tx, 1);
		/* UART circular tx buffer is an aligned page. */
		BUG_ON((uintptr_t)port->state->xmit.buf & ~PAGE_MASK);
		sg_set_page(&s->sg_tx, virt_to_page(port->state->xmit.buf),
			    UART_XMIT_SIZE,
			    (uintptr_t)port->state->xmit.buf & ~PAGE_MASK);
		nent = dma_map_sg(port->dev, &s->sg_tx, 1, DMA_TO_DEVICE);
		if (!nent)
			sci_tx_dma_release(s, false);
		else
			dev_dbg(port->dev, "%s: mapped %d@%p to %pad\n",
				__func__,
				sg_dma_len(&s->sg_tx), port->state->xmit.buf,
				&sg_dma_address(&s->sg_tx));

		s->sg_len_tx = nent;

		INIT_WORK(&s->work_tx, work_fn_tx);
	}

	param = &s->param_rx;

	/* Slave ID, e.g., SHDMA_SLAVE_SCIF0_RX */
	param->shdma_slave.slave_id = s->cfg->dma_slave_rx;

	chan = dma_request_channel(mask, filter, param);
	dev_dbg(port->dev, "%s: RX: got channel %p\n", __func__, chan);
	if (chan) {
		dma_addr_t dma[2];
		void *buf[2];
		int i;

		s->chan_rx = chan;

		s->buf_len_rx = 2 * max(16, (int)port->fifosize);
		buf[0] = dma_alloc_coherent(port->dev, s->buf_len_rx * 2,
					    &dma[0], GFP_KERNEL);

		if (!buf[0]) {
			dev_warn(port->dev,
				 "failed to allocate dma buffer, using PIO\n");
			sci_rx_dma_release(s, true);
			return;
		}

		buf[1] = buf[0] + s->buf_len_rx;
		dma[1] = dma[0] + s->buf_len_rx;

		for (i = 0; i < 2; i++) {
			struct scatterlist *sg = &s->sg_rx[i];

			sg_init_table(sg, 1);
			sg_set_page(sg, virt_to_page(buf[i]), s->buf_len_rx,
				    (uintptr_t)buf[i] & ~PAGE_MASK);
			sg_dma_address(sg) = dma[i];
		}

		INIT_WORK(&s->work_rx, work_fn_rx);
		setup_timer(&s->rx_timer, rx_timer_fn, (unsigned long)s);

		sci_submit_rx(s);
	}
}

static void sci_free_dma(struct uart_port *port)
{
	struct sci_port *s = to_sci_port(port);

	if (s->chan_tx)
		sci_tx_dma_release(s, false);
	if (s->chan_rx)
		sci_rx_dma_release(s, false);
}
#else
static inline void sci_request_dma(struct uart_port *port)
{
}

static inline void sci_free_dma(struct uart_port *port)
{
}
#endif

static int sci_startup(struct uart_port *port)
{
	struct sci_port *s = to_sci_port(port);
	unsigned long flags;
	int ret;
#if __iDRAC__
	unsigned int value;
//	unsigned char c, flag;
//	int count;
	static int init=true;
	int /*rc,*/ idracredirect, offset/*, i*/;
    struct tty_port *tport = &port->state->port;
    struct file* filp ;
#endif

	if (S_OpenDebug == 1) dev_dbg(port->dev, "%s(%d)\n", __func__, port->line);

#if __iDRAC__
//    if ((S_OpenDebug == 1) && (port->line != 2))
    if (S_OpenDebug == 1)
    {
        pr_debug("%s() P%d : mux=%s\n", __FUNCTION__, port->line, modestr(idracmux));
        pr_debug("%s() Begin::tport=0x%p, SCIF2=0x%p, SCIF3=0x%p, P3=0x%p, P4=0x%p, P5=0x%p, P6=0x%p, P7=0x%p, P8=0x%p\n",
                                       __FUNCTION__, tport, *idracSCIF2ldr, *idracSCIF3ldr, idractty[0], idractty[1], idractty[2], idractty[3], idractty[4], idractty[5]);

    }

    if ((port->line == 4) && !((idracmux == MODE2A) || (idracmux == MODE2B)))
    {
         // Not allowed to open this port in this mux mode.  P4 maps to SCIF2 which is only connected to DB9 in MODE3 (HW MODE 5)
        if (S_OpenDebug == 1)
            pr_debug("%s(): P%d fail %s\n", __FUNCTION__, port->line, modestr(idracmux));
        return -EAGAIN;
    }
    if(RAC_VIRTUAL_SERIAL_PORT== port->line || IPMI_VIRTUAL_SERIAL_PORT1 == port->line||
       SOL_VIRTUAL_SERIAL_PORT== port->line )
    {
        filp = sci_ports[port->line].filp;
    }
    if (init) {
        init = false;
        init_waitqueue_head(&idracttyldr_wait);
    }

    /*
     * Get the pid of idracttypid[] opening ttyS3, ttyS4, ttyS5,ttyS6.
     */
  
    if(port->line > 1 && port->line < SCI_NPORTS)
    {         
	idracttypid[port->line] = current->pid;
       as_VspTaskStructs[port->line] = find_task_by_vpid(current->pid);
       if (S_MuxDebug)
          pr_debug("%s(): P%d : pid %d : %d\n", __FUNCTION__, port->line, idracttypid[port->line], as_VspTaskStructs[port->line]->pid);
    }

    if (port->line == IDRACTTY_DAEMON_SERIAL_PORT)    
    	return 0;

    /*
     * Perform iDRAC port mapping.
     */

    idracredirect = false;
    offset = port->line - START_VIRTUAL_SERIAL_PORT_OFFSET;
    if (offset >= 0 && offset < sizeof(idractty)/sizeof(struct tty_port *)) 
    {

        if(RAC_VIRTUAL_SERIAL_PORT == port->line || IPMI_VIRTUAL_SERIAL_PORT1 == port->line 
           || SOL_VIRTUAL_SERIAL_PORT == port->line || IPMI_VIRTUAL_SERIAL_PORT2 == port->line || SERIAL_DATA_CAPTURE_PORT == port->line)
        {
           idracredirect = true;
        }

    }

    /*
     * With iDRAC redirection we may need to kick somebody off the port.
     */
    if (idracredirect) 
    {
        #ifdef SUPPORT_DCD
        if(filp)
        {
            if(filp->f_flags & O_NONBLOCK)
            {
               if (TIOCM_CAR != (sci_get_mctrl(port) & TIOCM_CAR))            
               {
                   idracttypid[port->line] = 0;
                   return -EAGAIN;
               }
            }
        }
        #endif
   
          
        spin_lock_irqsave(&idrac_serial_spinlock, idrac_flags);
        if (idractty[offset] == 0) 
        {
            idractty[offset] = tport;
            if (offset == SOL_VIRTUAL_SERIAL_OFFSET && idracmux != MODE1 && idracmux != MODE2C && idracmux != MODE3)  
            {
                idracmuxreq = MODE2C;
                muxchange_sendsignal(idracttypid[IDRACTTY_PID],IDRACTTY_PID);
            } 
            else if (offset == IPMI_VIRTUAL_SERIAL2_OFFSET)  
            {
                idracSCIF2ldr = idractty+4;
            }
            else// if (offset != SERIAL_DATA_CAPTURE_OFFSET) // P7/P8 not considered in leader code (for now). 
            {
                newttyldr();

            }
        }
        else
        {
           if (S_EnableDebug == 1)
               pr_debug("%s(): tty already exists : idractty[%d]=0x%08x\n", __FUNCTION__, offset, idractty[offset]);
           /*idractty[offset] != 0 means previous tty is not cleaned.*/
           spin_unlock_irqrestore(&idrac_serial_spinlock, idrac_flags);
           idracttypid[port->line] = 0;
           if (S_EnableDebug == 1)
               pr_debug("%s() P%d. EGAIN \n", __FUNCTION__, port->line);

           return -EAGAIN;
        }    
        spin_unlock_irqrestore(&idrac_serial_spinlock, idrac_flags);
        /*
         * Only the idracSCIF3ldr gets to set state->info.
         */
        // 1. preemptedldr != 0 means that ttyS of previous leader is not closed completely.
        // 2. tty != *idracSCIF3ldr means that tty(idracSCIF3ldr) of current leader != current opened tty.
        //   =>when idracmuxreq == MODE2C, if idractty daemon does't set mux. Then tty!=*idracSCIF3ldr
//        if (S_EnableDebug == 1)
//            pr_debug(KERN_EMERG"%s() P%d. check tty\n", __FUNCTION__, port->line);
//        if (port->line != 8)
//        {
//        if (!(tty == *idracSCIF3ldr && preemptedldr == 0)) 
        if ((tport != *idracSCIF3ldr) && (tport != *idracSCIF2ldr))  // if no leader of SCIF2 or SCIF3, return failure.  
        {
            if(filp)
            {
                if (filp->f_flags & O_NONBLOCK) 
                {
                    idracttycleanup(port, false);
                    if(tport != *idracSCIF3ldr)
                    {
                        DRV_INFO("sci_startup(): tty != *idracSCIF3ldr\n");
                    }
                    if (tport == *idracSCIF3ldr)
                    {
                        if ((S_EnableDebug == 1) && ((S_IncludeP4Debug == 1)  || ((S_IncludeP4Debug == 0) && (port->line != 4))))
                            pr_debug("sci_startup P%d return -EAGAIN\n", port->line);
                    }
                    else
                    {
                        if ((S_EnableDebug == 1) && ((S_IncludeP4Debug == 1)  || ((S_IncludeP4Debug == 0) && (port->line != 4))))
                             pr_debug("sci_startup P%d return -EBUSY. mux=%s\n", port->line, modestr(idracmux));
                    }
	                return (tport == *idracSCIF3ldr) ? -EAGAIN : -EBUSY;
	            }
	            else
	               return -EINVAL;  // Must be non-blocking with this kernel. 
	        }

        }

        /*
         * ttyS3, ttyS4, and ttyS6 special case CRTSCTS and CLOCAL.
         */
        spin_lock_irqsave(&port->lock, flags);
        
        if (offset == RAC_VIRTUAL_SERIAL_OFFSET || offset == IPMI_VIRTUAL_SERIAL1_OFFSET || offset == IPMI_VIRTUAL_SERIAL2_OFFSET) 
        {
                //CTSRTS/DCD
                //enable modem status but H/W flow control is set by AP not always on

                /*Disable Modem Status*/
                tport->tty->termios.c_cflag |= CLOCAL;
                tport->tty->termios.c_cflag &= (~HUPCL);
                port->state->port.flags &= (~ASYNC_NORMAL_ACTIVE);                
               #ifdef SUPPORT_DCD
               if (idracmux != MODE2A) 
               {
                 /*Enable Modem Status*/
                 tport->tty->termios.c_cflag &= ~CLOCAL;
                 port->info->flags |= ASYNC_NORMAL_ACTIVE;
                 tport->tty->termios.c_cflag |= (HUPCL);
               }
               #endif
               //tty->termios->c_cflag |= (CRTSCTS);
               tport->tty->termios.c_cc[VMIN] =  1;
               tport->tty->termios.c_cc[VTIME] = 0;
                

        }
        spin_unlock_irqrestore(&port->lock, flags);
    }

  
      
    /*
     * ttySC6 optionally flushes the history in MODE1
     */
    #ifdef SUPPORT_HISTBUF
    DRV_URGENT("sci_startup(): idracmux is %d, offset is %d, histon is %d\n",idracmux,offset,histon);
    if (idracmux == MODE1 && offset == SOL_VIRTUAL_SERIAL_OFFSET && histon) {
        DRV_URGENT("sci_startup(): flush history\n");
        histon = false;
        flushtty = tport;
        flushinghist = true;
        flushhist(true);
    }
    #endif




    if (((port->line == SERIAL_DATA_CAPTURE_PORT) && ((S_OpenPortMask & (1 << SOL_VIRTUAL_SERIAL_PORT)) == SOL_VIRTUAL_SERIAL_PORT)) ||
       ((port->line == SOL_VIRTUAL_SERIAL_PORT) && ((S_OpenPortMask & (1 << SERIAL_DATA_CAPTURE_PORT)) == SERIAL_DATA_CAPTURE_PORT)))
    {
        // SOL and SDC use the same SCIF3.  If one open, return ok to the request to open the 2nd.  Driver will take care of writing to both tty devices
       S_OpenPortMask |= (1 << port->line);
        return 0;
    }

    
    if((DEBUG_SERIAL_PORT==port->line) || (RAC_VIRTUAL_SERIAL_PORT == port->line) ||  (SERIAL_DATA_CAPTURE_PORT == port->line) ||
       (IPMI_VIRTUAL_SERIAL_PORT1==port->line) || (SOL_VIRTUAL_SERIAL_PORT==port->line) || (IPMI_VIRTUAL_SERIAL_PORT2==port->line))
	{

		sci_port_enable(s);

	}

	if (port->line != IDRACTTY_DAEMON_SERIAL_PORT)
	{
	    if (S_EnableDebug == 1)
	        pr_debug("%s():irq_flag %d %d %d %d %d %d %d %d %d\n", __FUNCTION__, irq_flag[0], irq_flag[1], irq_flag[2], irq_flag[3], irq_flag[4], irq_flag[5], irq_flag[6], irq_flag[7], irq_flag[8]);
		if (0==irq_flag[port->line])
		{
       		ret = sci_request_irq(s);
       		if (unlikely(ret < 0))
       		{
				if (S_EnableDebug) pr_debug("P%d irq request failed : %s : ret=%d\n", port->line, modestr(idracmux), ret);
       			return ret;
       		}	
                     if (S_OpenDebug == 1)
                         pr_debug("P%d Open: P4=%d P7=%d P8=%d : %s : %x\n",  port->line, irq_flag[4], irq_flag[7], irq_flag[8], modestr(idracmux), S_OpenPortMask);
       
       		sci_request_dma(port);
       
       	}
       	else
       	{
                     if (S_OpenDebug == 1)
                         pr_debug("P%d Open IRQ FAIL : P4=%d P7=%d P8=%d : %s : %s\n", port->line, irq_flag[4], irq_flag[7], irq_flag[8], modestr(idracmux), S_OpenPortMask);
              }           
	}
	spin_lock_irqsave(&port->lock, flags);

	/* disable all interrupts */
	sci_stop_tx(port);
	sci_stop_rx(port);

	/* clear status register */
	serial_port_out(port, SCxSR, SCxSR_RDxF_CLEAR(port));
	serial_port_out(port, SCxSR, SCxSR_ERROR_CLEAR(port));
	serial_port_out(port, SCxSR, SCxSR_TDxE_CLEAR(port));
	serial_port_out(port, SCxSR, SCxSR_BREAK_CLEAR(port));

    value = serial_port_in(port, SCSCR);
    value |= SCSCR_TE;
    value |= SCSCR_RE;       
    serial_port_out(port, SCSCR, value);  // enable transmit/receive       

	sci_start_rx(port);
	spin_unlock_irqrestore(&port->lock, flags);


    if(DEBUG_SERIAL_PORT == port->line|| SOL_VIRTUAL_SERIAL_PORT ==port->line)
    {
    	tport->tty->termios.c_cflag &= ~(HUPCL | CMSPAR);
    	tport->tty->termios.c_cflag |= CLOCAL;
    }

    S_OpenPortMask |= (1 << port->line);
    if (S_SuccessOpen)
        pr_debug("[%lu] : P%d open: mux %s : last %s : mask %x\n", jiffies, port->line, modestr(idracmux), modestr(idraclastmux), S_OpenPortMask);

//       if ((S_OpenDebug == 1) || (S_EnableDebug == 1))
//       pr_debug("%s() End: tty=0x%08x, SCIF2=0x%08x, SCIF3=0x%08x, P3=0x%08x, P4=0x%08x, P5=0x%08x, P6=0x%08x, P7=0x%08x, P8=0x%08x,\n",
//                __FUNCTION__, tty, *idracSCIF2ldr, *idracSCIF3ldr, idractty[0], idractty[1], idractty[2], idractty[3], idractty[4], idractty[5]);
#else
	sci_request_dma(port);

	ret = sci_request_irq(s);
	if (unlikely(ret < 0)) {
		sci_free_dma(port);
		return ret;
	}

	spin_lock_irqsave(&port->lock, flags);
	sci_start_tx(port);
	sci_start_rx(port);
	spin_unlock_irqrestore(&port->lock, flags);
#endif
	return 0;
}

static void sci_shutdown(struct uart_port *port)
{
	struct sci_port *s = to_sci_port(port);
	unsigned long flags;
  unsigned int i, value;
  struct uart_port *new_port;

	if (S_CloseDebug == 1)	dev_dbg(port->dev, "%s(%d)\n", __func__, port->line);

#ifdef __iDRAC__
    spin_lock_irqsave(&port->lock, flags);
    if (S_CloseDebug == 1)
        pr_debug("close P%d : %s : %02x\n", port->line, modestr(idracmux), S_OpenPortMask);
    if (((port->line == SERIAL_DATA_CAPTURE_PORT) && (S_OpenPortMask & (1 << SOL_VIRTUAL_SERIAL_PORT))) ||
        ((port->line == SOL_VIRTUAL_SERIAL_PORT) && (S_OpenPortMask & (1 << SERIAL_DATA_CAPTURE_PORT))))
    {
        // Don't disable port if closing SOL and SDC port still open.  Reverse is also true. 
        if (S_CloseDebug == 1)	pr_debug("%s(): P%d : Before S_OpenPortMask %x\n", __FUNCTION__, port->line, S_OpenPortMask);
        S_OpenPortMask &= ~(1 << port->line);
        if (S_CloseDebug == 1)	pr_debug("%s(): P%d : After S_OpenPortMask %x\n", __FUNCTION__, port->line, S_OpenPortMask);

        spin_unlock_irqrestore(&port->lock, flags);
        return;
    }

   if(RAC_VIRTUAL_SERIAL_PORT==port->line || IPMI_VIRTUAL_SERIAL_PORT1==port->line ||
        SOL_VIRTUAL_SERIAL_PORT==port->line||DEBUG_SERIAL_PORT==port->line)
    {
       
       if(1==irq_flag[port->line])
       {
          sci_stop_rx(port);
          sci_stop_tx(port);
          sci_free_dma(port);
          sci_free_irq(s);
          irq_flag[port->line] =0; 

          if (S_CloseDebug == 1)
              pr_debug("P%d: Close: P4=%d P7=%d P8=%d : %s : %x\n",  port->line, irq_flag[4], irq_flag[7], irq_flag[8], modestr(idracmux), S_OpenPortMask);
          // Now check to see if we have another port that has registered for the shared interrupt.  Need to find a better way to do this.  
          for (i=RAC_VIRTUAL_SERIAL_PORT;i<=SERIAL_DATA_CAPTURE_PORT;i++)
          {
              if (i==IDRACTTY_DAEMON_SERIAL_PORT)
                  continue;
              if (irq_flag[i] == 1)
              {
                  //if any of the other ports have already registered for the IRQ, then re-enable rx interrupts.
                  /* clear status register */
                  new_port = &sci_ports[i].port;
                  serial_port_out(new_port, SCxSR, SCxSR_RDxF_CLEAR(new_port));
                  serial_port_out(new_port, SCxSR, SCxSR_ERROR_CLEAR(new_port));
                  serial_port_out(new_port, SCxSR, SCxSR_TDxE_CLEAR(new_port));
                  serial_port_out(new_port, SCxSR, SCxSR_BREAK_CLEAR(new_port));
                  value = serial_port_in(new_port, SCSCR);
                  value |= SCSCR_TE;
                  value |= SCSCR_RE;       
                  serial_port_out(new_port, SCSCR, value);  // enable transmit/receive       
                  sci_start_rx(new_port);
                  break;

                 
              }
          }

       }
       else
        {
            if (S_CloseDebug == 1)
                pr_debug("P%d: Close IRQ FAIL : P4=%d P7=%d P8=%d : %s : %x\n",  port->line, irq_flag[4], irq_flag[7], irq_flag[8], modestr(idracmux), S_OpenPortMask);
        }

//       serial_port_out(port, SCSCR, serial_port_in(port, SCSCR) & (~SCSCR_TE));  // disable transmit
//       serial_port_out(port, SCSCR, serial_port_in(port, SCSCR) & (~SCSCR_RE));  // disalbe receive        

       pm_runtime_get_noresume(port->dev);
      
#if 0		       
#ifdef CONFIG_HAVE_CLK
       clk_put(s->clk);
       s->clk = NULL;
#endif  
#endif
    }
     spin_unlock_irqrestore(&port->lock, flags);    
        #ifdef SUPPORT_HISTBUF 
        /*
         * idracttyd always has a socket open to ensure that the history buffer
         * is maintained in MODE1.
         */
        if (idracmux == MODE1 &&  SOL_VIRTUAL_SERIAL_PORT == port->line) 
        {
            if (flushinghist) {
                flushinghist = false;
                del_timer(&flushtimer);
            }
        }

        #endif
    
    idracttycleanup(port, true);
	sci_port_disable(s);
	S_OpenPortMask &= ~(1 << port->line);
	
#else
	spin_lock_irqsave(&port->lock, flags);
	sci_stop_rx(port);
	sci_stop_tx(port);
	spin_unlock_irqrestore(&port->lock, flags);

	sci_free_irq(s);
	sci_free_dma(port);
#endif//__iDRAC__
}

static unsigned int sci_scbrr_calc(struct sci_port *s, unsigned int bps,
				   unsigned long freq)
{
	if (s->sampling_rate)
		return DIV_ROUND_CLOSEST(freq, s->sampling_rate * bps) - 1;

	/* Warn, but use a safe default */
	WARN_ON(1);

	return ((freq + 16 * bps) / (32 * bps) - 1);
}

/* calculate sample rate, BRR, and clock select for HSCIF */
static void sci_baud_calc_hscif(unsigned int bps, unsigned long freq,
				int *brr, unsigned int *srr,
				unsigned int *cks)
{
	int sr, c, br, err;
	int min_err = 1000; /* 100% */

	/* Find the combination of sample rate and clock select with the
	   smallest deviation from the desired baud rate. */
	for (sr = 8; sr <= 32; sr++) {
		for (c = 0; c <= 3; c++) {
			/* integerized formulas from HSCIF documentation */
			br = freq / (sr * (1 << (2 * c + 1)) * bps) - 1;
			if (br < 0 || br > 255)
				continue;
			err = freq / ((br + 1) * bps * sr *
			      (1 << (2 * c + 1)) / 1000) - 1000;
			if (min_err > err) {
				min_err = err;
				*brr = br;
				*srr = sr - 1;
				*cks = c;
			}
		}
	}

	if (min_err == 1000) {
		WARN_ON(1);
		/* use defaults */
		*brr = 255;
		*srr = 15;
		*cks = 0;
	}
}

static void sci_reset(struct uart_port *port)
{
	struct plat_sci_reg *reg;
	unsigned int status;

	do {
		status = serial_port_in(port, SCxSR);
	} while (!(status & SCxSR_TEND(port)));

	serial_port_out(port, SCSCR, 0x00);	/* TE=0, RE=0, CKE1=0 */

	reg = sci_getreg(port, SCFCR);
	if (reg->size)
		serial_port_out(port, SCFCR, SCFCR_RFRST | SCFCR_TFRST);
}

static void sci_set_termios(struct uart_port *port, struct ktermios *termios,
			    struct ktermios *old)
{
	struct sci_port *s = to_sci_port(port);
	struct plat_sci_reg *reg;
	unsigned int baud, smr_val, max_baud, cks = 0;
	int t = -1;
	unsigned int srr = 15;

    #ifdef __iDRAC__
    //Don't allow IDRACTTY_DAEMON_SERIAL_PORT, RESERVED1_SERIAL_PORT, RESERVED2_SERIAL_PORT 
    //to set termios. Otherwise termios may not correct, like baudrate.
    if(IDRACTTY_DAEMON_SERIAL_PORT == port->line || RESERVED1_SERIAL_PORT == port->line || RESERVED2_SERIAL_PORT == port->line) // || DEBUG_SERIAL_PORT == port->line)
    {
       return;
    }   
//    if (S_EnableDebug)
//        pr_debug(KERN_EMERG"%s() for P%d : pid=%d : S_OpenPortMask=0x%x\n", __FUNCTION__, port->line, current->pid, S_OpenPortMask);
    if(RAC_VIRTUAL_SERIAL_PORT == port->line || IPMI_VIRTUAL_SERIAL_PORT1 == port->line) 
    {
       /*Disable Modem Status*/
       termios->c_cflag |= CLOCAL;
       termios->c_cflag &= (~HUPCL);
       port->state->port.flags &= (~ASYNC_CHECK_CD);                

       #ifdef SUPPORT_DCD
       if (idracmux != MODE2A) 
       {
          /*Enable Modem Status*/
          termios->c_cflag &= ~CLOCAL;
          port->state->port.flags |= ASYNC_CHECK_CD;
          termios->c_cflag |= (HUPCL);
       }
       #endif
    }
  #endif//__iDRAC__
	/*
	 * earlyprintk comes here early on with port->uartclk set to zero.
	 * the clock framework is not up and running at this point so here
	 * we assume that 115200 is the maximum baud rate. please note that
	 * the baud rate is not programmed during earlyprintk - it is assumed
	 * that the previous boot loader has enabled required clocks and
	 * setup the baud rate generator hardware for us already.
	 */
	max_baud = port->uartclk ? port->uartclk / 16 : 115200;

	baud = uart_get_baud_rate(port, termios, old, 0, max_baud);
  #ifdef __iDRAC__
  if(RAC_VIRTUAL_SERIAL_PORT == port->line || IPMI_VIRTUAL_SERIAL_PORT1 == port->line || \
     SOL_VIRTUAL_SERIAL_PORT == port->line) 
    S_ttySC346_Baudrate= baud;
  #endif
	if (likely(baud && port->uartclk)) {
		if (s->cfg->type == PORT_HSCIF) {
			sci_baud_calc_hscif(baud, port->uartclk, &t, &srr,
					    &cks);
		} else {
			t = sci_scbrr_calc(s, baud, port->uartclk);
			for (cks = 0; t >= 256 && cks <= 3; cks++)
				t >>= 2;
		}
	}
	if (S_TermiosDebug)
		pr_debug("%s() P%d : baud=%d : maxbaud=%d : t=%d : 0x%x : if=%x, of=%x, cf=%x, lf=%x\n",
		                                    __FUNCTION__, port->line, baud, max_baud, t, S_OpenPortMask, termios->c_iflag, termios->c_oflag, termios->c_cflag, termios->c_lflag);

	sci_port_enable(s);

	sci_reset(port);

	smr_val = serial_port_in(port, SCSMR) & 3;

	if ((termios->c_cflag & CSIZE) == CS7)
		smr_val |= SCSMR_CHR;
	if (termios->c_cflag & PARENB)
		smr_val |= SCSMR_PE;
	if (termios->c_cflag & PARODD)
		smr_val |= SCSMR_PE | SCSMR_ODD;
	if (termios->c_cflag & CSTOPB)
		smr_val |= SCSMR_STOP;

	uart_update_timeout(port, termios->c_cflag, baud);

	if (S_TermiosDebug)
		dev_dbg(port->dev, "%s: SMR %x, cks %x, t %x, SCSCR %x\n",
			__func__, smr_val, cks, t, s->cfg->scscr);

	if (t >= 0) {
		serial_port_out(port, SCSMR, (smr_val & ~SCSMR_CKS) | cks);
		serial_port_out(port, SCBRR, t);
		reg = sci_getreg(port, HSSRR);
		if (reg->size)
			serial_port_out(port, HSSRR, srr | HSCIF_SRE);
		udelay((1000000+(baud-1)) / baud); /* Wait one bit interval */
	} else
		serial_port_out(port, SCSMR, smr_val);

	sci_init_pins(port, termios->c_cflag);

	reg = sci_getreg(port, SCFCR);
	if (reg->size) {
		unsigned short ctrl = serial_port_in(port, SCFCR);

		if (s->cfg->capabilities & SCIx_HAVE_RTSCTS) {
			if ((termios->c_cflag & CRTSCTS) || (SOL_VIRTUAL_SERIAL_PORT == port->line))
				ctrl |= SCFCR_MCE;
			else
				ctrl &= ~SCFCR_MCE;
		}

		/*
		 * As we've done a sci_reset() above, ensure we don't
		 * interfere with the FIFOs while toggling MCE. As the
		 * reset values could still be set, simply mask them out.
		 */
		ctrl &= ~(SCFCR_RFRST | SCFCR_TFRST);

		serial_port_out(port, SCFCR, ctrl);
	}

	serial_port_out(port, SCSCR, s->cfg->scscr);

#ifdef CONFIG_SERIAL_SH_SCI_DMA
	/*
	 * Calculate delay for 1.5 DMA buffers: see
	 * drivers/serial/serial_core.c::uart_update_timeout(). With 10 bits
	 * (CS8), 250Hz, 115200 baud and 64 bytes FIFO, the above function
	 * calculates 1 jiffie for the data plus 5 jiffies for the "slop(e)."
	 * Then below we calculate 3 jiffies (12ms) for 1.5 DMA buffers (3 FIFO
	 * sizes), but it has been found out experimentally, that this is not
	 * enough: the driver too often needlessly runs on a DMA timeout. 20ms
	 * as a minimum seem to work perfectly.
	 */
	if (s->chan_rx) {
		s->rx_timeout = (port->timeout - HZ / 50) * s->buf_len_rx * 3 /
			port->fifosize / 2;
		dev_dbg(port->dev, "DMA Rx t-out %ums, tty t-out %u jiffies\n",
			s->rx_timeout * 1000 / HZ, port->timeout);
		if (s->rx_timeout < msecs_to_jiffies(20))
			s->rx_timeout = msecs_to_jiffies(20);
	}
#endif

	if ((termios->c_cflag & CREAD) != 0)
		sci_start_rx(port);

	sci_port_disable(s);
}

static void sci_pm(struct uart_port *port, unsigned int state,
		   unsigned int oldstate)
{
	struct sci_port *sci_port = to_sci_port(port);

	switch (state) {
	case UART_PM_STATE_OFF:
		sci_port_disable(sci_port);
		break;
	default:
		sci_port_enable(sci_port);
		break;
	}
}

static const char *sci_type(struct uart_port *port)
{
	switch (port->type) {
	case PORT_IRDA:
		return "irda";
	case PORT_SCI:
		return "sci";
	case PORT_SCIF:
		return "scif";
	case PORT_SCIFA:
		return "scifa";
	case PORT_SCIFB:
		return "scifb";
	case PORT_HSCIF:
		return "hscif";
	}

	return NULL;
}

static inline unsigned long sci_port_size(struct uart_port *port)
{
	/*
	 * Pick an arbitrary size that encapsulates all of the base
	 * registers by default. This can be optimized later, or derived
	 * from platform resource data at such a time that ports begin to
	 * behave more erratically.
	 */
	if (port->type == PORT_HSCIF)
		return 96;
	else
		return 64;
}

static int sci_remap_port(struct uart_port *port)
{
	unsigned long size = sci_port_size(port);

	/*
	 * Nothing to do if there's already an established membase.
	 */
	if (port->membase)
		return 0;

	if (port->flags & UPF_IOREMAP) {
		port->membase = ioremap_nocache(port->mapbase, size);
		if (unlikely(!port->membase)) {
			dev_err(port->dev, "can't remap port#%d\n", port->line);
			return -ENXIO;
		}
	} else {
		/*
		 * For the simple (and majority of) cases where we don't
		 * need to do any remapping, just cast the cookie
		 * directly.
		 */
		port->membase = (void __iomem *)(uintptr_t)port->mapbase;
	}

	return 0;
}

static void sci_release_port(struct uart_port *port)
{
	if (port->flags & UPF_IOREMAP) {
		iounmap(port->membase);
		port->membase = NULL;
	}

	release_mem_region(port->mapbase, sci_port_size(port));
}

static int sci_request_port(struct uart_port *port)
{
	unsigned long size = sci_port_size(port);
	struct resource *res;
	int ret;
#ifdef __iDRAC__
// Virtual serial ports will share the same resources.  Need to bypass this for those ports.
       if (port->line < RAC_VIRTUAL_SERIAL_PORT)
       {
	res = request_mem_region(port->mapbase, size, dev_name(port->dev));
	if (unlikely(res == NULL))
		return -EBUSY;

	}
#endif 
	ret = sci_remap_port(port);
	if (unlikely(ret != 0)) {
		release_resource(res);
		return ret;
	}

	return 0;
}

static void sci_config_port(struct uart_port *port, int flags)
{
	if (flags & UART_CONFIG_TYPE) {
		struct sci_port *sport = to_sci_port(port);

		port->type = sport->cfg->type;
		sci_request_port(port);
	}
}

static int sci_verify_port(struct uart_port *port, struct serial_struct *ser)
{
	if (ser->baud_base < 2400)
		/* No paper tape reader for Mitch.. */
		return -EINVAL;

	return 0;
}

static int sci_serial_ioctl(struct uart_port *port, unsigned int cmd , unsigned long arg)
{
//   unsigned long flags = 0;
   unsigned char i = 0;
//   unsigned char j = 0;
   unsigned long histbuflen = 0;
   pid_t ttypid;
  
    switch (cmd) 
    {
#ifdef  __iDRAC__
            
            
        case TIOCSERGMUX:
            /*
             * Supply the idracmuxreq value, so that a daemon can switch the mux.
             */
            if (S_EnableDebug == 1)
                pr_debug("[%lu] : ioctl Get mux from pid=%d.  %s\n", jiffies, current->pid, modestr(idracmuxreq));
            if (copy_to_user((void *)arg, (void *)&idracmuxreq, sizeof(idracmuxreq)))
            {
				        return -EFAULT;
				    }
            break;
        case TIOCSERSMUX:
        {

            static int initialized=true;
            int muxnew;
            //Driver will initialize mux state as Mode2b
            /*
             * The mux daemon is notifying here.
             */
            if (copy_from_user((void *)&muxnew, (void *)arg, sizeof(idracmux)))
		        return -EFAULT;
            if (S_EnableDebug == 1)
                pr_debug("[%lu] : ioctl Set mux from pid=%d.  %s\n", jiffies, current->pid, modestr(muxnew));
               /**For 12G, switch seril mux is done in driver*/
                switch (muxnew) 
                {
                   case MODE1:     /* ttySC6 */
                        //writeb(0x14, 0xfe470000);//mode4
                        //writeb(0x10, 0xfe470000);//temp solution because host side is not ready
                        break;
                   case MODE2A:    /* ttySC3 or ttySC4, in that order */
                        //writeb(0x12, 0xfe470000);//mode2
                       // writeb(0x10, 0xfe470000);//temp solution because host side is not ready
                        break;                   
                   case MODE2B:
                        //writeb(0x10, 0xfe470000);//mode0
                        //writeb(0x10, 0xfe470000);//temp solution because host side is not ready
                         break;
                   case MODE2C:    /* ttySC6 */
                        //writeb(0x14, 0xfe470000);//mode4
                         //writeb(0x10, 0xfe470000);//temp solution because host side is not ready
                    break;
                   case MODE3: 
//                    ConsoleToChsMgrWake = 1;
     	             /** - Change the wait queue flag to wake up the read data process */
//                    wake_up_interruptible(&Console_To_ChsMgr_wq);
//                      pr_debug("%d total bytes in fifo\n", kfifo_len(&ConsoleToChsMgr_kfifo));
                    break;

                   default:
                    break; 
                }
                /**--------------------------------------------*/


  
            spin_lock_irqsave(&idrac_serial_spinlock, idrac_flags);
            if (!initialized) {
                initialized = true;
                idracmux = idraclastmux = idracmuxreq = muxnew;
            } else if (muxnew != idracmux) 
            {

                #ifdef SUPPORT_HISTBUF
                resethistbuf();
                #endif
                if (muxnew == MODE1) {
                    idracmux = idraclastmux = idracmuxreq = MODE1;
                } else if (idraclastmux == MODE1) {
                    idraclastmux = MODE2B;
                    idracmux = idracmuxreq = muxnew;
                } else {
                    /*
                     * The reason idracmuxreq is set here is that the mux may have been changed
                     * by an external source rather than this driver. We react to such
                     * external mux changes as if they were made by this driver.
                     */
                    idraclastmux = idracmux;
                    idracmux = idracmuxreq = muxnew;
                }
  
                
                newttyldr();
                /*
                 * For security reasons, we require a re-open on transitions between
                 * MODE2A and MODE2B. Additionally, MODE2A snooping cannot use CRTSCTS,
                 * but it is required in MODE2B.
                 */
                if (S_EnableDebug)
                    pr_debug("[%lu] : last: %s, req: %s, new: %s, idracmux: %s\n", jiffies, modestr(idraclastmux), modestr(idracmuxreq), modestr(muxnew),modestr(idracmux));
                
                if (*idracSCIF3ldr &&
                        ((idracmux == MODE2A && idraclastmux == MODE2B) ||
                        (idracmux == MODE2B && idraclastmux == MODE2A)))
                 {       

                     i=(idracSCIF3ldr-idractty)+START_VIRTUAL_SERIAL_PORT_OFFSET;
                     if(i>=START_VIRTUAL_SERIAL_PORT_OFFSET && i<SCI_NPORTS)
                     {
                        ttypid = idracttypid[i];
                        
                        /*if AP is slept when opening ttySC, UIF_NORMAL_ACTIVE won't be set in uart_open()*/
                        /*When this ttySC is hung up, the shutdown() won't be called. *
                         *This is because of UIF_NORMAL_ACTIVE won't be set.
                         *If shutdown() is not called, the idracttycleanup() is also not called.*
                         *It will cause unpredictable situation*
                         *So set UIF_NORMAL_ACTIVE in info->flags then shutdown() will be called.*/

                        if(sci_ports[i].port.state)
                        {
                          sci_ports[i].port.state->port.flags|= ASYNC_NORMAL_ACTIVE; 
                          tty_hangup((*idracSCIF3ldr)->tty);
                          muxchange_sendsignal(ttypid,i);
                        }

                    }
                 }

            }

//            if ((S_EnableDebug == 1) || (S_BasicModeDebug == 1))
//                pr_debug("last: %s, req: %s, new: %s, idracmux: %s\n", modestr(idraclastmux), modestr(idracmuxreq), modestr(muxnew),modestr(idracmux));
            spin_unlock_irqrestore(&idrac_serial_spinlock, idrac_flags);
            break;
        }
        case TIOCHISTON:
            #ifdef SUPPORT_HISTBUF
            histon = true;
            #endif
            break;
        case TIOCIDRACTTYBAUD:
        #if 0
        {
#define BAUDSTR(b)  (b == B9600 ? "9600" : b == B19200 ? "19200" : b == B38400 ? "38400" : \
                        b == B57600 ? "57600" : b == B115200 ? "115200" : "invalid")
#define RACSERIAL   0
#define IPMISERIAL  1
#define SOL         2
#define INVALID_NO  0xFF
            int i, idracbauds[3];
            int index = 0;
            pid_t ttypid;

            if (copy_from_user((void *)idracbauds, (void *)arg, sizeof(idracbauds)))
				return -EFAULT;
            pr_debug("idracioctl: bauds ttyS3=%s ttyS4=%s ttyS6=%s\n",
                BAUDSTR(idracbauds[RACSERIAL]), BAUDSTR(idracbauds[IPMISERIAL]), BAUDSTR(idracbauds[SOL]));
            for (i = 0; i < sizeof(idracbauds)/sizeof(int); i++) {
                switch (idracbauds[i]) {
                case B9600:
                case B19200:
                case B38400:
                case B57600:
                case B115200:
                    break;
                default:
                    idracbauds[i] = B115200;
                }
            }
            /*
             * When changing the baud rate, we hangup the port, forcing everyone
             * to reopen at the new baud rate.
             */
            spin_lock_irqsave(&idrac_serial_spinlock, idrac_flags);
            if(*idracSCIF3ldr)
            {   
                i = idracSCIF3ldr-idractty;   
                switch (i)
                {
                    case RAC_VIRTUAL_SERIAL_OFFSET:
                        index = RACSERIAL;
                        break;
                    case IPMI_VIRTUAL_SERIAL_OFFSET:
                        index = IPMISERIAL;
                        break;
                    case SOL_VIRTUAL_SERIAL_OFFSET:
                       index = SOL;
                        break;  
                    default:    
                        index = INVALID_NO;
                        break;
                } 
                if(INVALID_NO != index)
                {
                  if (((*idracSCIF3ldr)->termios->c_cflag&CBAUD) != idracbauds[index] && idracbauds[index]) 
                  {
                     (*idracSCIF3ldr)->termios->c_cflag &= ~CBAUD;
                     (*idracSCIF3ldr)->termios->c_cflag |= idracbauds[index];

                     i += START_VIRTUAL_SERIAL_PORT_OFFSET; 
                     ttypid = idracttypid[i];
                    
                     if(sci_ports[i].port.info)
                     {
                         sci_ports[i].port.info->flags |= UIF_NORMAL_ACTIVE; 
                         tty_hangup(*idracSCIF3ldr);
                         muxchange_sendsignal(ttypid,i);
                     }

                  }
                }
            }
            spin_unlock_irqrestore(&idrac_serial_spinlock, idrac_flags);
            break;
        }
        #else
            return -ENOIOCTLCMD;
        #endif
        case TIOCSNOOPON:
            snoopon = true;
            break;
        case TIOCSNOOPOFF:
            /*snooping always on*/
            //snoopon = FALSE;
            break;
        case TIOCSDEBUG:
            idracdebug = !idracdebug;
            break;
        case TIOCHISTBUFMAX:
            #ifdef SUPPORT_HISTBUF
            if (copy_from_user((void *)&histbuflen, (void *)arg, sizeof(unsigned long)))
		        return -EFAULT;
		        
			if (histbuflen > HISTBUFMAX)
				histbuflen = HISTBUFMAX;
			if (histbuflen == 0)
				return -EINVAL;
			spin_lock_irqsave(&idrac_serial_spinlock, idrac_flags);
			histbufmax = histbuflen;
            resethistbuf();
			spin_unlock_irqrestore(&idrac_serial_spinlock, idrac_flags);
			#endif
            break;
#endif//__iDRAC__          
        default:
          return -ENOIOCTLCMD;
          break;
    }
    


	return 0;
}


static struct uart_ops sci_uart_ops = {
	.tx_empty	= sci_tx_empty,
	.set_mctrl	= sci_set_mctrl,
	.get_mctrl	= sci_get_mctrl,
	.start_tx	= sci_start_tx,
	.stop_tx	= sci_stop_tx,
	.stop_rx	= sci_stop_rx,
	.enable_ms	= sci_enable_ms,
	.break_ctl	= sci_break_ctl,
	.startup	= sci_startup,
	.shutdown	= sci_shutdown,
	.set_termios	= sci_set_termios,
	.pm		= sci_pm,
	.type		= sci_type,
	.release_port	= sci_release_port,
	.request_port	= sci_request_port,
	.config_port	= sci_config_port,
	.verify_port	= sci_verify_port,
#ifdef CONFIG_CONSOLE_POLL
	.poll_get_char	= sci_poll_get_char,
	.poll_put_char	= sci_poll_put_char,
#endif
#ifdef __iDRAC__	
	.ioctl      	= sci_serial_ioctl,
#endif
};

static int sci_init_single(struct platform_device *dev,
			   struct sci_port *sci_port, unsigned int index,
			   struct plat_sci_port *p, bool early)
{
	struct uart_port *port = &sci_port->port;
	const struct resource *res;
	unsigned int sampling_rate;
	unsigned int i;
	int ret;

	sci_port->cfg	= p;
#ifdef __iDRAC__
	sci_port->filp = NULL;
#endif

	port->ops	= &sci_uart_ops;
	port->iotype	= UPIO_MEM;
	port->line	= index;

	res = platform_get_resource(dev, IORESOURCE_MEM, 0);
	if (res == NULL)
		return -ENOMEM;

	port->mapbase = res->start;

	for (i = 0; i < ARRAY_SIZE(sci_port->irqs); ++i)
		sci_port->irqs[i] = platform_get_irq(dev, i);

	/* The SCI generates several interrupts. They can be muxed together or
	 * connected to different interrupt lines. In the muxed case only one
	 * interrupt resource is specified. In the non-muxed case three or four
	 * interrupt resources are specified, as the BRI interrupt is optional.
	 */
	if (sci_port->irqs[0] < 0)
		return -ENXIO;

	if (sci_port->irqs[1] < 0) {
		sci_port->irqs[1] = sci_port->irqs[0];
		sci_port->irqs[2] = sci_port->irqs[0];
		sci_port->irqs[3] = sci_port->irqs[0];
	}

	if (p->regtype == SCIx_PROBE_REGTYPE) {
		ret = sci_probe_regmap(p);
		if (unlikely(ret))
			return ret;
	}

	switch (p->type) {
	case PORT_SCIFB:
		port->fifosize = 256;
		sci_port->overrun_bit = 9;
		sampling_rate = 16;
		break;
	case PORT_HSCIF:
		port->fifosize = 128;
		sampling_rate = 0;
		sci_port->overrun_bit = 0;
		break;
	case PORT_SCIFA:
		port->fifosize = 64;
		sci_port->overrun_bit = 9;
		sampling_rate = 16;
		break;
	case PORT_SCIF:
		port->fifosize = 16;
		if (p->regtype == SCIx_SH7705_SCIF_REGTYPE) {
			sci_port->overrun_bit = 9;
			sampling_rate = 16;
		} else {
			sci_port->overrun_bit = 0;
			sampling_rate = 32;
		}
		break;
	default:
		port->fifosize = 1;
		sci_port->overrun_bit = 5;
		sampling_rate = 32;
		break;
	}

	/* SCIFA on sh7723 and sh7724 need a custom sampling rate that doesn't
	 * match the SoC datasheet, this should be investigated. Let platform
	 * data override the sampling rate for now.
	 */
	sci_port->sampling_rate = p->sampling_rate ? p->sampling_rate
				: sampling_rate;

	if (!early) {
		sci_port->iclk = clk_get(&dev->dev, "sci_ick");
		if (IS_ERR(sci_port->iclk)) {
			sci_port->iclk = clk_get(&dev->dev, "peripheral_clk");
			if (IS_ERR(sci_port->iclk)) {
				dev_err(&dev->dev, "can't get iclk\n");
				return PTR_ERR(sci_port->iclk);
			}
		}

		/*
		 * The function clock is optional, ignore it if we can't
		 * find it.
		 */
		sci_port->fclk = clk_get(&dev->dev, "sci_fck");
		if (IS_ERR(sci_port->fclk))
			sci_port->fclk = NULL;

		port->dev = &dev->dev;

		pm_runtime_enable(&dev->dev);
	}

	sci_port->break_timer.data = (unsigned long)sci_port;
	sci_port->break_timer.function = sci_break_timer;
	init_timer(&sci_port->break_timer);

	/*
	 * Establish some sensible defaults for the error detection.
	 */
	sci_port->error_mask = (p->type == PORT_SCI) ?
			SCI_DEFAULT_ERROR_MASK : SCIF_DEFAULT_ERROR_MASK;

	/*
	 * Establish sensible defaults for the overrun detection, unless
	 * the part has explicitly disabled support for it.
	 */

	/*
	 * Make the error mask inclusive of overrun detection, if
	 * supported.
	 */
	sci_port->error_mask |= 1 << sci_port->overrun_bit;

	port->type		= p->type;
	port->flags		= UPF_FIXED_PORT | p->flags;
	port->regshift		= p->regshift;

	/*
	 * The UART port needs an IRQ value, so we peg this to the RX IRQ
	 * for the multi-IRQ ports, which is where we are primarily
	 * concerned with the shutdown path synchronization.
	 *
	 * For the muxed case there's nothing more to do.
	 */
	port->irq		= sci_port->irqs[SCIx_RXI_IRQ];
#ifdef __iDRAC__
	if (port->line == DEBUG_SERIAL_PORT)
		port->irqflags		= IRQF_DISABLED;
	else
		port->irqflags		= IRQF_SHARED;
#else
	port->irqflags		= 0;
#endif
	port->serial_in		= sci_serial_in;
	port->serial_out	= sci_serial_out;

	if (p->dma_slave_tx > 0 && p->dma_slave_rx > 0)
		dev_dbg(port->dev, "DMA tx %d, rx %d\n",
			p->dma_slave_tx, p->dma_slave_rx);

	return 0;
}

static void sci_cleanup_single(struct sci_port *port)
{
	clk_put(port->iclk);
	clk_put(port->fclk);

	pm_runtime_disable(port->port.dev);
}

#ifdef CONFIG_SERIAL_SH_SCI_CONSOLE
static void serial_console_putchar(struct uart_port *port, int ch)
{
	sci_poll_put_char(port, ch);
}

/*
 *	Print a string to the serial port trying not to disturb
 *	any possible real use of the port...
 */
static void serial_console_write(struct console *co, const char *s,
				 unsigned count)
{
	struct sci_port *sci_port = &sci_ports[co->index];
	struct uart_port *port = &sci_port->port;
	unsigned short bits, ctrl;
	unsigned long flags;
	int locked = 1;

	local_irq_save(flags);
	if (port->sysrq)
		locked = 0;
	else if (oops_in_progress)
		locked = spin_trylock(&port->lock);
	else
		spin_lock(&port->lock);

	/* first save the SCSCR then disable the interrupts */
	ctrl = serial_port_in(port, SCSCR);
	serial_port_out(port, SCSCR, sci_port->cfg->scscr);

	uart_console_write(port, s, count, serial_console_putchar);

	/* wait until fifo is empty and last bit has been transmitted */
	bits = SCxSR_TDxE(port) | SCxSR_TEND(port);
	while ((serial_port_in(port, SCxSR) & bits) != bits)
		cpu_relax();

	/* restore the SCSCR */
	serial_port_out(port, SCSCR, ctrl);

	if (locked)
		spin_unlock(&port->lock);
	local_irq_restore(flags);
}

static int serial_console_setup(struct console *co, char *options)
{
	struct sci_port *sci_port;
	struct uart_port *port;
	int baud = 115200;
	int bits = 8;
	int parity = 'n';
	int flow = 'n';
	int ret;

	/*
	 * Refuse to handle any bogus ports.
	 */
	if (co->index < 0 || co->index >= SCI_NPORTS)
		return -ENODEV;

	sci_port = &sci_ports[co->index];
	port = &sci_port->port;

	/*
	 * Refuse to handle uninitialized ports.
	 */
	if (!port->ops)
		return -ENODEV;

	ret = sci_remap_port(port);
	if (unlikely(ret != 0))
		return ret;

	if (options)
		uart_parse_options(options, &baud, &parity, &bits, &flow);

	return uart_set_options(port, co, baud, parity, bits, flow);
}

static struct console serial_console = {
	.name		= "ttySC",
	.device		= uart_console_device,
	.write		= serial_console_write,
	.setup		= serial_console_setup,
	.flags		= CON_PRINTBUFFER,
	.index		= -1,
	.data		= &sci_uart_driver,
};

static struct console early_serial_console = {
	.name           = "early_ttySC",
	.write          = serial_console_write,
	.flags          = CON_PRINTBUFFER,
	.index		= -1,
};

static char early_serial_buf[32];

static int sci_probe_earlyprintk(struct platform_device *pdev)
{
	struct plat_sci_port *cfg = dev_get_platdata(&pdev->dev);

	if (early_serial_console.data)
		return -EEXIST;

	early_serial_console.index = pdev->id;

	sci_init_single(pdev, &sci_ports[pdev->id], pdev->id, cfg, true);

	serial_console_setup(&early_serial_console, early_serial_buf);

	if (!strstr(early_serial_buf, "keep"))
		early_serial_console.flags |= CON_BOOT;

	register_console(&early_serial_console);
	return 0;
}

#define SCI_CONSOLE	(&serial_console)

#else
static inline int sci_probe_earlyprintk(struct platform_device *pdev)
{
	return -EINVAL;
}

#define SCI_CONSOLE	NULL

#endif /* CONFIG_SERIAL_SH_SCI_CONSOLE */

static const char banner[] __initconst = "SuperH (H)SCI(F) driver initialized";

static struct uart_driver sci_uart_driver = {
	.owner		= THIS_MODULE,
	.driver_name	= "sci",
	.dev_name	= "ttySC",
	.major		= SCI_MAJOR,
	.minor		= SCI_MINOR_START,
	.nr		= SCI_NPORTS,
	.cons		= SCI_CONSOLE,
};

static int sci_remove(struct platform_device *dev)
{
	struct sci_port *port = platform_get_drvdata(dev);

	uart_remove_one_port(&sci_uart_driver, &port->port);

	sci_cleanup_single(port);

	return 0;
}

struct sci_port_info {
	unsigned int type;
	unsigned int regtype;
};

static const struct of_device_id of_sci_match[] = {
	{
		.compatible = "renesas,scif",
		.data = &(const struct sci_port_info) {
			.type = PORT_SCIF,
			.regtype = SCIx_SH4_SCIF_REGTYPE,
		},
	}, {
		.compatible = "renesas,scifa",
		.data = &(const struct sci_port_info) {
			.type = PORT_SCIFA,
			.regtype = SCIx_SCIFA_REGTYPE,
		},
	}, {
		.compatible = "renesas,scifb",
		.data = &(const struct sci_port_info) {
			.type = PORT_SCIFB,
			.regtype = SCIx_SCIFB_REGTYPE,
		},
	}, {
		.compatible = "renesas,hscif",
		.data = &(const struct sci_port_info) {
			.type = PORT_HSCIF,
			.regtype = SCIx_HSCIF_REGTYPE,
		},
	}, {
		/* Terminator */
	},
};
MODULE_DEVICE_TABLE(of, of_sci_match);

static struct plat_sci_port *
sci_parse_dt(struct platform_device *pdev, unsigned int *dev_id)
{
	struct device_node *np = pdev->dev.of_node;
	const struct of_device_id *match;
	const struct sci_port_info *info;
	struct plat_sci_port *p;
	int id;

	if (!IS_ENABLED(CONFIG_OF) || !np)
		return NULL;

	match = of_match_node(of_sci_match, pdev->dev.of_node);
	if (!match)
		return NULL;

	info = match->data;

	p = devm_kzalloc(&pdev->dev, sizeof(struct plat_sci_port), GFP_KERNEL);
	if (!p) {
		dev_err(&pdev->dev, "failed to allocate DT config data\n");
		return NULL;
	}

	/* Get the line number for the aliases node. */
	id = of_alias_get_id(np, "serial");
	if (id < 0) {
		dev_err(&pdev->dev, "failed to get alias id (%d)\n", id);
		return NULL;
	}

	*dev_id = id;

	p->flags = UPF_IOREMAP | UPF_BOOT_AUTOCONF;
	p->type = info->type;
	p->regtype = info->regtype;
	p->scscr = SCSCR_RE | SCSCR_TE;

	return p;
}

static int sci_probe_single(struct platform_device *dev,
				      unsigned int index,
				      struct plat_sci_port *p,
				      struct sci_port *sciport)
{
	int ret;

	/* Sanity check */
	if (unlikely(index >= SCI_NPORTS)) {
		dev_notice(&dev->dev, "Attempting to register port %d when only %d are available\n",
			   index+1, SCI_NPORTS);
		dev_notice(&dev->dev, "Consider bumping CONFIG_SERIAL_SH_SCI_NR_UARTS!\n");
		return -EINVAL;
	}

	ret = sci_init_single(dev, sciport, index, p, false);
	if (ret)
		return ret;

	ret = uart_add_one_port(&sci_uart_driver, &sciport->port);
	if (ret) {
		sci_cleanup_single(sciport);
		return ret;
	}

	return 0;
}

static int sci_probe(struct platform_device *dev)
{
	struct plat_sci_port *p;
	struct sci_port *sp;
	unsigned int dev_id;
	int ret;

	/*
	 * If we've come here via earlyprintk initialization, head off to
	 * the special early probe. We don't have sufficient device state
	 * to make it beyond this yet.
	 */
	if (is_early_platform_device(dev))
		return sci_probe_earlyprintk(dev);

	if (dev->dev.of_node) {
		p = sci_parse_dt(dev, &dev_id);
		if (p == NULL)
			return -EINVAL;
	} else {
		p = dev->dev.platform_data;
		if (p == NULL) {
			dev_err(&dev->dev, "no platform data supplied\n");
			return -EINVAL;
		}

		dev_id = dev->id;
	}

	sp = &sci_ports[dev_id];
	platform_set_drvdata(dev, sp);

	ret = sci_probe_single(dev, dev_id, p, sp);
	if (ret)
		return ret;

#ifdef CONFIG_SH_STANDARD_BIOS
	sh_bios_gdb_detach();
#endif

	return 0;
}

static int sci_suspend(struct device *dev)
{
	struct sci_port *sport = dev_get_drvdata(dev);

	if (sport)
		uart_suspend_port(&sci_uart_driver, &sport->port);

	return 0;
}

static int sci_resume(struct device *dev)
{
	struct sci_port *sport = dev_get_drvdata(dev);

	if (sport)
		uart_resume_port(&sci_uart_driver, &sport->port);

	return 0;
}

static const struct dev_pm_ops sci_dev_pm_ops = {
	.suspend	= sci_suspend,
	.resume		= sci_resume,
};

static struct platform_driver sci_driver = {
	.probe		= sci_probe,
	.remove		= sci_remove,
	.driver		= {
		.name	= "sh-sci",
		.owner	= THIS_MODULE,
		.pm	= &sci_dev_pm_ops,
		.of_match_table = of_match_ptr(of_sci_match),
	},
};

static int __init sci_init(void)
{
	int ret;
	pr_info("%s\n", banner);
	ret = uart_register_driver(&sci_uart_driver);
	if (likely(ret == 0)) {
		ret = platform_driver_register(&sci_driver);
		if (unlikely(ret))
			uart_unregister_driver(&sci_uart_driver);
	}

	aess_debugfs_default_create("aess_serial",NULL,0,0);
	aess_debugfs_create_file("aess_serial","MuxCurrent",(u32 *)&idracmux,DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","MuxLast",(u32 *)&idraclastmux,DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","MuxRequest",(u32 *)&idracmuxreq,DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC0_pid",(u32 *)&idracttypid[0],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC1_pid",(u32 *)&idracttypid[1],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC2_pid",(u32 *)&idracttypid[2],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC3_pid",(u32 *)&idracttypid[3],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC4_pid",(u32 *)&idracttypid[4],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC5_pid",(u32 *)&idracttypid[5],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC6_pid",(u32 *)&idracttypid[6],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC7_pid",(u32 *)&idracttypid[7],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","ttySC8_pid",(u32 *)&idracttypid[8],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","idracttyP3",(u32 *)&idractty[0],DBG_TYPE32,0);	
	aess_debugfs_create_file("aess_serial","idracttyP4",(u32 *)&idractty[1],DBG_TYPE32,0);	
	aess_debugfs_create_file("aess_serial","idracttyP6",(u32 *)&idractty[3],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","idracttyP7",(u32 *)&idractty[4],DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","idracttyP8",(u32 *)&idractty[5],DBG_TYPE32,0);	
	aess_debugfs_create_file("aess_serial","S_OpenPortMask",(u32 *)&S_OpenPortMask,DBG_TYPE32,0);	
	aess_debugfs_create_file("aess_serial","TxChar",(u8 *)&S_tchar,DBG_TYPE8,0);
	aess_debugfs_create_file("aess_serial","ttySC346_SCIF3_Baudrate",(u32 *)&S_ttySC346_Baudrate,DBG_TYPE32,0);	
	aess_debugfs_create_file("aess_serial","histon",(u32 *)&histon,DBG_TYPE32,0);	
	aess_debugfs_create_file("aess_serial","EnableDebug",(u32 *)&S_EnableDebug,DBG_TYPE32,0);		
	aess_debugfs_create_file("aess_serial","OpenDebug",(u32 *)&S_OpenDebug,DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","SuccessOpen",(u32 *)&S_SuccessOpen,DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","TermiosDebug",(u32 *)&S_TermiosDebug,DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","IncludeP4Debug",(u32 *)&S_IncludeP4Debug,DBG_TYPE32,0);		
	aess_debugfs_create_file("aess_serial","SOL",(u32 *)&S_SolPrintEnable,DBG_TYPE32,0);		
	aess_debugfs_create_file("aess_serial","BasicModeDebug",(u32 *)&S_BasicModeDebug,DBG_TYPE32,0);		
	aess_debugfs_create_file("aess_serial","PrintTTY",(u32 *)&S_PrintTTY,DBG_TYPE32,0);	
	aess_debugfs_create_file("aess_serial","PrintTTY_ISR",(u32 *)&S_PrintTTY_ISR,DBG_TYPE32,0);
	aess_debugfs_create_file("aess_serial","CloseDebug",(u32 *)&S_CloseDebug,DBG_TYPE32,0);		
	aess_debugfs_create_file("aess_serial","MuxDebug",(u32 *)&S_MuxDebug,DBG_TYPE32,0);
       memset(irq_flag,0,MAX_VIRTUAL_SERIAL_PORT);
	return ret;
}

static void __exit sci_exit(void)
{
	platform_driver_unregister(&sci_driver);
	uart_unregister_driver(&sci_uart_driver);
}

#ifdef CONFIG_SERIAL_SH_SCI_CONSOLE
early_platform_init_buffer("earlyprintk", &sci_driver,
			   early_serial_buf, ARRAY_SIZE(early_serial_buf));
#endif
module_init(sci_init);
module_exit(sci_exit);

MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:sh-sci");
MODULE_AUTHOR("Paul Mundt");
MODULE_DESCRIPTION("SuperH (H)SCI(F) serial driver");
